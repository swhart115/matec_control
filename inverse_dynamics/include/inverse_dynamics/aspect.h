#ifndef ASPECT_H
#define ASPECT_H

#include <ros/ros.h>
#include <boost/thread.hpp>
#include <time.h>
#include <ctime>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>

#include <std_srvs/Empty.h>

#include <urdf/model.h>
#include <urdf_parser/urdf_parser.h>

#include <rbdl/Model.h>
#include <rbdl/Dynamics.h>
#include <rbdl/rbdl_utils.h>
#include <matec_utils/rbdl_urdfreader.h>
#include <matec_utils/model_helper.h>

#include <sensor_msgs/JointState.h>

#include <tf/tf.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_utils/kdl_urdf_reader.h"

#include "matec_msgs/RobotInertiaProperties.h"

#include <matec_utils/biquad.h>

#include <inverse_dynamics/linear_algebra_math.h>
#include <matec_utils/integrators.h>

#include "matec_utils/urdf_manipulations.h"
#include "matec_msgs/DataVector.h"

namespace inverse_dynamics
{
  struct ASPECTLinkParameters
  {
    double prediction_lpf;
    double update_lpf;
    double sliding_surface_angle;
    double feedback_gain;
    double parameter_update_gain;
    double parameter_update_gain_prediction;
    double epsilon_inertia;
    double omega_inertia;
    double epsilon_mass;
    double omega_mass;
  };

  class AdaptiveDynamicsTreeNode
  {
  public:
    enum JointType
    {
      REVOLUTE,
      FLOATING,
      PRISMATIC,
      FIXED
    };

    AdaptiveDynamicsTreeNode()
    {
      parameter_mutex = new boost::mutex();
    }

    std::string joint_name;
    std::string link_name;
    int joint_idx;

    JointType type;
    KDL::Segment segment;

    Matrix6 urdf_inertia;
    Matrix6 estimated_inertia;
    Matrix6 estimated_composite_inertia;

    Vector6 S; //axis
    Matrix6 iXip;

    Vector6 v;
    Vector6 w;
    Vector6 u;
    Vector6 a;

    Matrix610 f;
    Matrix610 filtered_f;
    Vector6 F;
    Matrix610 beta;
    Vector6 B;

    Vector6 e;

    boost::mutex* parameter_mutex;

    double lambda; //sliding surface angle
    double K; //feedback gain
    double gamma; //parameter update gain
    double nu; //parameter update gain for prediction terms
    double lambda_p; //low pass filter frequency
    double lambda_u; //low pass filter frequency

    //inertia / mass properties
    Vector10 theta;
    Vector10 phi;
    Vector10 filtered_prediction_error;
    Vector10 phi_dot;
    Vector10 old_phi_dot;
    Vector10 theta_min;
    Vector10 theta_max;

    double viscous_friction_theta;
    double viscous_friction_phi;
    double filtered_friction_prediction_error;
    double viscous_friction_phi_dot;
    double old_viscous_friction_phi_dot;
    double viscous_friction_theta_min;
    double viscous_friction_theta_max;

    double epsilon_inertia;
    double omega_inertia;
    double epsilon_mass;
    double omega_mass;

    //volume bounds for the center of mass
    double cx_min;
    double cx_max;
    double cy_min;
    double cy_max;
    double cz_min;
    double cz_max;

    double q;
    double q_dot;
    double q_dot_dot;
    double qd;
    double qd_dot;
    double qd_dot_dot;
    double qr_dot;
    double qr_dot_dot;

    double torque;
    double measured_torque;
    double torque_error;

    long parent_index;
    std::vector<long> child_indices;
    std::vector<long> supported_indices;
  };

  class ASPECT
  {
  public:
    ASPECT();
    void load(urdf::Model model, std::vector<std::string> joint_names, bool randomize_parameters=false);
    void setAdaptiveParameters(std::string joint_name, ASPECTLinkParameters params);

    void computeTorques(matec_msgs::FullJointStates& current, matec_msgs::FullJointStates& desired);
    matec_msgs::RobotInertiaProperties getInertias();
    std::vector<double> getTorques();

    Matrix6 root_transform;

  private:
    urdf::Model m_urdf_model;
    KDL::Tree m_kdl_tree;

    double gravity_constant;
    double randomize_initial_parameters;

    double sample_time;
    double dt;

    std::vector<std::string> link_names;
    std::vector<AdaptiveDynamicsTreeNode> nodes;

    matec_msgs::FullJointStates* m_current;
    matec_msgs::FullJointStates* m_desired;
    Vector6 base_position;
    Vector6 base_velocity;
    Vector6 base_gravity_acceleration;


    boost::array<Matrix6, 10> R;

    void reevaluateParameterBounds(AdaptiveDynamicsTreeNode* self);
    void forwardPass(long i, AdaptiveDynamicsTreeNode* self, AdaptiveDynamicsTreeNode* parent);
    void backwardPass(long i, AdaptiveDynamicsTreeNode* self, AdaptiveDynamicsTreeNode* parent);
    void computeTorquesRecursive(long i);
    void updateRecursive(long i);

    void setCoMBounds(AdaptiveDynamicsTreeNode* node);
    void initializeParametersFromInertia(AdaptiveDynamicsTreeNode* node, Matrix6& inertia);
    void loadSegmentIndex(KDL::Segment segment, int idx);
    void loadRecursive(int idx, const KDL::SegmentMap::const_iterator segment);
  };
}
#endif //ASPECT_H
