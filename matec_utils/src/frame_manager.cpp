#include "matec_utils/frame_manager.h"

namespace matec_utils
{
  FrameManager::FrameManager(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 50.0);
    matec_utils::blockOnSMJointNames(m_joint_names);
    initModel();

    m_joint_states_sub.subscribe("/joint_states", boost::bind(&FrameManager::sensorCallback, this, _1));
    m_root_link_odom_sub.subscribe("/root_link_odom");
  }

  FrameManager::~FrameManager()
  {
  }

  void FrameManager::initModel()
  {
    urdf::Model urdf_model;
    urdf_model.initParam("/dynamics/robot_description");
    m_model = new ModelHelper(urdf_model, m_joint_names);
  }

  void FrameManager::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    nav_msgs::Odometry root_link_odom;
    if(!m_root_link_odom_sub.getCurrentMessage(root_link_odom))
    {
      ROS_WARN_THROTTLE(0.5, "FM: No root link odometry received!");
      return;
    }

    double total_mass;
    m_model->updateModel(msg.position, root_link_odom.pose.pose);
    m_model->findCOM(m_com, total_mass);
  }

  void FrameManager::updateFrames()
  {
    tf::StampedTransform root_link_transform, com_transform;
    if(m_model->getRootLinkTransform(root_link_transform))
    {
      m_tf_broadcaster.sendTransform(root_link_transform);
    }
    if(m_model->getCoMTransform(com_transform))
    {
      m_tf_broadcaster.sendTransform(com_transform);
    }
  }

  void FrameManager::spin()
  {
    ROS_INFO("FrameManager started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      updateFrames();

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "frame_manager");
  ros::NodeHandle nh("~");

  matec_utils::FrameManager node(nh);
  node.spin();

  return 0;
}
