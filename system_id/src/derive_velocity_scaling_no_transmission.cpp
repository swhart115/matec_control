#include "system_id/coefficient_optimizer.h"
#include <cmath>
#include <set>
#include "system_id/common_math.h"

#include "system_id/training_data.h"
#include "system_id/expression_genome.h"

using namespace system_id;

int main(int argc, char* argv[])
{
  if(argc != 2)
  {
    std::cerr << "Args: filename" << std::endl;
    return 0;
  }

  std::string filename(argv[1]);
  system_id::TrainingData data(filename, 9);

  std::vector<std::string> input_symbols = data.getInputLabels();
  EGParams eg_params(input_symbols, 0, 500);

  std::string expression_string = "if(v>0) {C0*(log(C1*(v+C2)) - log(C1*C2))} else {C3*(log(C4*(v+C5)) - log(C4*C5))}";

  system_id::CoefficientOptimizer opt(&data);
  std::vector<std::string> coefficient_symbols;
  coefficient_symbols.push_back("C0");
  coefficient_symbols.push_back("C1");
  coefficient_symbols.push_back("C2");
  coefficient_symbols.push_back("C3");
  coefficient_symbols.push_back("C4");
  coefficient_symbols.push_back("C5");

  ArrayGenome::GeneType min, max, initial_guess;
  for(unsigned int i = 0; i < input_symbols.size(); i++)
  {
    min.first.push_back(0);
    max.first.push_back(0);
  }

  min.second.push_back(-1); //C0
  max.second.push_back(1); //C0

  min.second.push_back(-1); //C1
  max.second.push_back(1); //C1

  min.second.push_back(0); //C2
  max.second.push_back(1); //C2

  min.second.push_back(-1); //C3
  max.second.push_back(1); //C3

  min.second.push_back(-1); //C4
  max.second.push_back(1); //C4

  min.second.push_back(-1); //C5
  max.second.push_back(0); //C5

  GAParams params(7, 3, 0.001, 100000, 200, 10 * data.size());
  system_id::ArrayGenome best = opt.optimize(expression_string, coefficient_symbols, input_symbols, params, &min, &max);
  std::cerr << best << std::endl;

  std::cerr << "\n\n Final equation: " << expression_string << ", " << best << std::endl;

  while(1)
  {

  }
}
