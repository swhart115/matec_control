#ifndef cartesian_pose_control_H
#define cartesian_pose_control_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <geometry_msgs/Point.h>
#include <boost/thread.hpp>
#include <geometry_msgs/WrenchStamped.h>
#include <numeric>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include <math.h>
#include <urdf/model.h>
#include <tf/tf.h>
#include <geometric_shapes/bodies.h>
#include <geometric_shapes/body_operations.h>
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/shape_operations.h>
#include <kdl/tree.hpp>
#include <kdl/frames.hpp>
#include <kdl/segment.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <tf_conversions/tf_kdl.h>
#include <tf/transform_datatypes.h>
#include <tf2/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/WrenchStamped.h>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <sensor_msgs/JointState.h>
#include <cmath>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <matec_msgs/FullJointStates.h>

#include <matec_utils/common_initialization_components.h>
#include <matec_utils/model_helper.h>

namespace cartesian_pose_control
{
  class CartesianPoseControl
  {
  public:
    CartesianPoseControl(const ros::NodeHandle& nh);
    ~CartesianPoseControl();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_controlled_frame;
    std::vector<std::string> m_controlled_joints;
    std::vector<double> m_controlled_joint_weights;
    std::vector<double> m_cartesian_weights;
    std::vector<unsigned char> m_joint_map;

    sensor_msgs::JointState m_js;
    std::vector<std::string> m_joint_names;
    unsigned int m_num_joints;

    std::vector<std::string> m_field_names;
    std::vector<double> m_kps;
    std::vector<double> m_kvs;
    std::vector<double> m_kis;
    std::vector<double> m_i_clamps;
    std::vector<double> m_i_livebands;
    std::vector<double> m_i_accumulators;
    double m_center_spring;

    ros::Publisher m_error_pub;
    ros::Publisher m_current_pub;
    ros::Publisher m_cmd_pub;
    ros::Publisher m_cmd_vis_pub;
    ros::Subscriber m_js_sub;
    ros::Subscriber m_twist_sub;

    boost::mutex m_mutex;

    shared_memory_interface::Publisher<matec_msgs::TaskCommand> m_command_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<geometry_msgs::PoseStamped> m_pose_carrot_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;

    urdf::Model m_urdf_model;
    matec_utils::ModelHelper* m_model;
    KDL::Chain m_chain;
    KDL::ChainIkSolverVel_wdls* m_inv_solver;
    KDL::ChainFkSolverVel_recursive* m_fwd_solver;

    void getGains();
    void initModel();
    void findLongestControlChain();

    void jsCallback(const sensor_msgs::JointStateConstPtr& msg);
    void twistCallback(const geometry_msgs::TwistStampedConstPtr& msg);
    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //cartesian_pose_control_H
