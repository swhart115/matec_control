#include <cmath>
#include <set>

#include "system_id/training_data.h"

int main(int argc, char* argv[])
{
  if(argc != 3)
  {
    std::cerr << "Args: [filename in], [filename out]" << std::endl;
    return 0;
  }

  std::string filename_in(argv[1]);
  std::string filename_out(argv[2]);
  system_id::TrainingData data(filename_in);

  std::vector<std::string> input_labels = data.getInputLabels();
  int cmd_idx = std::find(input_labels.begin(), input_labels.end(), "cmd") - input_labels.begin();

  if(cmd_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'p'!";
    return 0;
  }

  std::vector<std::vector<long double> > input_sets, output_sets;
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    std::vector<long double> output_set = data.getOutputSet(i);

    long double cmd = input_set[cmd_idx];
    if(!((fabs(cmd) > 0.99 && fabs(cmd) < 1.01) || (fabs(cmd) < 0.01)))
    {
      input_sets.push_back(input_set);
      output_sets.push_back(output_set);
    }
  }

  system_id::TrainingData::writeDataFile(filename_out, input_sets, data.getInputLabels(), output_sets, data.getOutputLabels());
}
