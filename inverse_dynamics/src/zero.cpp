#include "ros/ros.h"
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "matec_msgs/FullJointStates.h"
#include "matec_msgs/JointNames.h"
#include "matec_msgs/TaskCommand.h"

#include <boost/thread/thread_time.hpp>

#include "matec_utils/common_initialization_components.h"

using namespace std;
using namespace shared_memory_interface;
int main(int argc, char **argv)
{
  ros::init(argc, argv, "pe_signal_generator");
  ros::NodeHandle n;

  std::vector<std::string> joint_names;
  matec_utils::blockOnSMJointNames(joint_names);
  shared_memory_interface::Publisher<matec_msgs::FullJointStates> carrot_pub;
  carrot_pub.advertise("/pe_carrot");

  matec_msgs::FullJointStates full_cmd;
  full_cmd.position.resize(joint_names.size(), 0.0);
  full_cmd.velocity.resize(joint_names.size(), 0.0);
  full_cmd.acceleration.resize(joint_names.size(), 0.0);
  full_cmd.torque.resize(joint_names.size(), 0.0);

//  full_cmd.position[0] = 1.57; //TODO: remove bad hardcoding!

  carrot_pub.publish(full_cmd);

  return 0;
}
