#ifndef QUATERNION_INTERPOLATOR_H
#define QUATERNION_INTERPOLATOR_H

#include "minimum_jerk_interpolator.h"

namespace interpolators
{
  class QuaternionViaPoint
  {
  public:
    ros::Time finish_time;
    geometry_msgs::Quaternion target_quat;

    QuaternionViaPoint()
    {
    }

    QuaternionViaPoint(ros::Time finish_time_, geometry_msgs::Quaternion quat)
    {
      finish_time = finish_time_;
      target_quat = quat;
    }
  };
  typedef std::deque<QuaternionViaPoint> QuaternionTrajectory;

  class QuaternionInterpolator
  {
  public:
    QuaternionInterpolator()
    {
      m_have_valid_trajectory = false;
    }

    void loadTrajectory(QuaternionTrajectory quat_traj)
    {
      m_quat_traj.clear();
      m_quat_traj.push_back(quat_traj[0]);
      m_quat_traj.push_back(quat_traj[quat_traj.size() - 1]);

      if(quat_traj.size() != 2) //TODO: allow more than two
      {
        ROS_WARN("IGNORING INTERMEDIATE VIAPOINTS AND SKIPPING TO THE END!");
      }

      Trajectory traj;
      traj.push_back(ViaPoint(ros::Time(0), 0, 0));
      traj.push_back(ViaPoint(ros::Time(1), 1, 0));
      m_interpolator.loadTrajectory(traj);

      m_have_valid_trajectory = true;
      m_last_slice_index = 1; //m_traj[0] is the start state
    }

    bool getInterpolatedState(ros::Time time, geometry_msgs::Quaternion& quat)
    {
      if(!m_have_valid_trajectory)
      {
        return false;
      }

      //get current trajectory slice (assumes that finish_times are ordered)
      QuaternionViaPoint from_via;
      QuaternionViaPoint to_via;
      for(unsigned int i = m_last_slice_index; i < m_quat_traj.size(); i++)
      {
        if(time < m_quat_traj[i].finish_time) //found the slice!
        {
          from_via = m_quat_traj[i - 1];
          to_via = m_quat_traj[i];
        }
        else if(i == (m_quat_traj.size() - 1)) //ran out of slices!
        {
          ROS_INFO("Trajectory complete!");
          m_have_valid_trajectory = false;
          quat = m_quat_traj[m_quat_traj.size() - 1].target_quat;
          return false;
        }
      }

      //figure out where we should be
      double time_fraction = (time - from_via.finish_time).toSec() / (to_via.finish_time - from_via.finish_time).toSec();
      assert(!isnan(time_fraction));
      double slerp_point = 1.0;
      State state;
      if(!m_interpolator.getInterpolatedState(ros::Time(time_fraction), state))
      {
        ROS_WARN("Ran out of internal slerp slices without catching it earlier somehow!");
//        return false;
      }
      else
      {
        slerp_point = state.position;
      }

      tf::Quaternion q_from, q_to, q_interpolated;
      tf::quaternionMsgToTF(from_via.target_quat, q_from);
      tf::quaternionMsgToTF(to_via.target_quat, q_to);

      tf::quaternionTFToMsg(tf::slerp(q_from, q_to, slerp_point), quat);

      return true;
    }

    void cancelTrajectory()
    {
      m_quat_traj.clear();
      m_have_valid_trajectory = false;
      m_interpolator.cancelTrajectory();
    }

  private:
    QuaternionTrajectory m_quat_traj;
    MinimumJerkInterpolator m_interpolator;

    unsigned long m_last_slice_index;
    bool m_have_valid_trajectory;
  };
}
#endif //QUATERNION_INTERPOLATOR_H
