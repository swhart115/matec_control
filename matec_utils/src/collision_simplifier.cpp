#include "ros/ros.h"
#include "ros/package.h"
#include "matec_utils/urdf_manipulations.h"
#include "matec_utils/string_parameter_parsers.h"
#include "tinyxml.h"
#include "tf/tf.h"

using namespace matec_utils;

void recurseURDFTree(TiXmlElement* e, bool guess_inertias)
{
  if(!e->FirstChild())
  {
    return;
  }
  TiXmlElement* child = e->FirstChild()->ToElement();
  for(; child; child = child->NextSiblingElement())
  {
    std::string child_type = child->Value();
    if(child_type == "link")
    {
      std::string link_name = child->FirstAttribute()->Value();

      if(child->FirstChild("collision") && child->FirstChild("collision")->FirstChild("geometry") && child->FirstChild("collision")->FirstChild("geometry")->FirstChild("mesh"))
      {
        TiXmlElement* mesh = child->FirstChild("collision")->FirstChild("geometry")->FirstChild("mesh")->ToElement();
        std::string mesh_filename = mesh->FirstAttribute()->Value();
        bodies::BoundingCylinder cyl;
        getBoundingCylinderOfMesh(mesh_filename, cyl);
        Eigen::Vector3d v = cyl.pose.rotation().eulerAngles(0, 1, 2);

        std::string radius_string, length_string, pos_string, rot_string;
        std::stringstream ss;

        ss << cyl.radius;
        radius_string = ss.str();
        ss.str("");

        ss << cyl.length;
        length_string = ss.str();
        ss.str("");

        ss << cyl.pose.translation()(0) << " " << cyl.pose.translation()(1) << " " << cyl.pose.translation()(2);
        pos_string = ss.str();
        ss.str("");

        ss << v(0) << " " << v(1) << " " << v(2);
        rot_string = ss.str();
        ss.str("");

        std::cerr << "link " << link_name << " uses mesh file " << mesh_filename << std::endl;
        std::cerr << "Mesh fits in cylinder r: [" << radius_string << "] l: [" << length_string << "] pos: [" << pos_string << "] rot: [" << rot_string << "]" << std::endl << std::endl;

        TiXmlElement cylinder("cylinder");
        cylinder.SetAttribute("radius", radius_string);
        cylinder.SetAttribute("length", length_string);

        TiXmlElement geometry("geometry");
        geometry.InsertEndChild(cylinder);

        TiXmlElement origin("origin");
        origin.SetAttribute("xyz", pos_string);
        origin.SetAttribute("rpy", rot_string);

        child->FirstChild("collision")->Clear();
        child->FirstChild("collision")->InsertEndChild(origin);
        child->FirstChild("collision")->InsertEndChild(geometry);
      }

      if(guess_inertias)
      {
        if(child->FirstChild("collision") && child->FirstChild("collision")->FirstChild("geometry") && (child->FirstChild("collision")->FirstChild("geometry")->FirstChild("box") || child->FirstChild("collision")->FirstChild("geometry")->FirstChild("cylinder")))
        {
          TiXmlElement* geometry = child->FirstChild("collision")->FirstChild("geometry")->ToElement();
          TiXmlElement* inertial = child->FirstChild("inertial")->ToElement();
          TiXmlElement mass = *(inertial->FirstChild("mass")->ToElement());
          TiXmlElement origin = *(child->FirstChild("collision")->FirstChild("origin")->ToElement());

          Eigen::Matrix3d inertia_rotation;

          double mass_value = atof(mass.Attribute("value"));

          double ixx, iyy, izz;
          if(geometry->FirstChild("box"))
          {
            TiXmlElement* box = geometry->FirstChild("box")->ToElement();
            std::string box_size_string = box->Attribute("size");
            std::vector<double> box_sizes = parameterStringToFPVector(box_size_string);
            assert(box_sizes.size() == 3);
            boxInertia(mass_value, box_sizes[0], box_sizes[1], box_sizes[2], ixx, iyy, izz);
          }
          else if(geometry->FirstChild("cylinder"))
          {
            TiXmlElement* cylinder = geometry->FirstChild("cylinder")->ToElement();
            double radius = atof(cylinder->Attribute("radius"));
            double length = atof(cylinder->Attribute("length"));
            cylinderInertia(mass_value, radius, length, ixx, iyy, izz);
          }
          else
          {
            std::cerr << "Can currently only guess inertias for box / cylinder primitives!" << std::endl;
            assert(0);
          }

          //rotate calculated inertia if necessary
          std::vector<double> collision_rotation = parameterStringToFPVector(origin.Attribute("rpy"));
          tf::Quaternion quat = tf::createQuaternionFromRPY(collision_rotation[0], collision_rotation[1], collision_rotation[2]);
          Eigen::Matrix3d rotation = Eigen::Quaterniond(quat.w(), quat.x(), quat.y(), quat.z()).toRotationMatrix();
          Eigen::Matrix3d inertia_matrix;
          inertia_matrix << ixx, 0, 0, //
          0, iyy, 0, //
          0, 0, izz;
          Eigen::Matrix3d rotated_inertia = rotation.transpose() * inertia_matrix * rotation;
          origin.SetAttribute("rpy", "0.0 0.0 0.0");

          std::string ixx_string, iyy_string, izz_string, ixy_string, ixz_string, iyz_string;
          std::stringstream ss;

          ss << rotated_inertia(0, 0);
          ixx_string = ss.str();
          ss.str("");

          ss << rotated_inertia(1, 1);
          iyy_string = ss.str();
          ss.str("");

          ss << rotated_inertia(2, 2);
          izz_string = ss.str();
          ss.str("");

          ss << rotated_inertia(0, 1);
          ixy_string = ss.str();
          ss.str("");

          ss << rotated_inertia(0, 2);
          ixz_string = ss.str();
          ss.str("");

          ss << rotated_inertia(1, 2);
          iyz_string = ss.str();
          ss.str("");

          TiXmlElement inertia("inertia");
          inertia.SetAttribute("ixx", ixx_string);
          inertia.SetAttribute("iyy", iyy_string);
          inertia.SetAttribute("izz", izz_string);
          inertia.SetAttribute("ixy", ixy_string);
          inertia.SetAttribute("ixz", ixz_string);
          inertia.SetAttribute("iyz", iyz_string);

          inertial->Clear();
          inertial->InsertEndChild(mass);
          inertial->InsertEndChild(origin);
          inertial->InsertEndChild(inertia);
        }
      }
    }
    recurseURDFTree(child, guess_inertias);
  }
}

int main(int argc, char **argv)
{
  std::string filename = ros::package::getPath("atlas_sim") + "/models/atlas_full_pinned/urdf/atlas_full_pinned";
  std::cerr << "Reading from urdf file " << filename << std::endl;

  bool guess_inertias = true; //TODO: param this and file

  TiXmlDocument doc(filename + ".urdf");
  doc.LoadFile();

  for(TiXmlNode* child = doc.FirstChild(); child != 0; child = child->NextSibling())
  {
    recurseURDFTree(child->ToElement(), guess_inertias);
  }

  if(!doc.SaveFile(filename + "_primitive.urdf"))
  {
    std::cerr << "Save failed!!" << std::endl;
  }

  return 0;
}
