#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "reach_prep");
  ros::NodeHandle nh;

  actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> client(nh, "/follow_joint_trajectory", true);

  while(ros::Time::now().toSec() == 0)
  {
  }

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");

  control_msgs::FollowJointTrajectoryGoal goal;
  goal.trajectory.header.stamp = ros::Time::now();
  goal.trajectory.points.resize(1);

  //TODO: generalize
  goal.trajectory.points[0].time_from_start = ros::Duration(3.0);

  goal.trajectory.joint_names.push_back("l_arm_shy");
  goal.trajectory.points[0].positions.push_back(1.0);
  goal.trajectory.joint_names.push_back("l_arm_shx");
  goal.trajectory.points[0].positions.push_back(-0.75);
  goal.trajectory.joint_names.push_back("l_arm_ely");
  goal.trajectory.points[0].positions.push_back(2.15);
  goal.trajectory.joint_names.push_back("l_arm_elx");
  goal.trajectory.points[0].positions.push_back(2.15);
  goal.trajectory.joint_names.push_back("l_arm_wry");
  goal.trajectory.points[0].positions.push_back(0.0);
  goal.trajectory.joint_names.push_back("l_arm_wrx");
  goal.trajectory.points[0].positions.push_back(0.3);

  goal.trajectory.joint_names.push_back("r_arm_shy");
  goal.trajectory.points[0].positions.push_back(1.0);
  goal.trajectory.joint_names.push_back("r_arm_shx");
  goal.trajectory.points[0].positions.push_back(0.75);
  goal.trajectory.joint_names.push_back("r_arm_ely");
  goal.trajectory.points[0].positions.push_back(2.15);
  goal.trajectory.joint_names.push_back("r_arm_elx");
  goal.trajectory.points[0].positions.push_back(-2.15);
  goal.trajectory.joint_names.push_back("r_arm_wry");
  goal.trajectory.points[0].positions.push_back(0.0);
  goal.trajectory.joint_names.push_back("r_arm_wrx");
  goal.trajectory.points[0].positions.push_back(-0.3);

  // Start the trajectory
  client.sendGoal(goal);
  client.waitForResult(ros::Duration(3.5));

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

