#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <tf/tf.h>
#include <matec_msgs/UnPin.h>

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "drop");
  ros::NodeHandle nh;

  while(ros::Time::now().toSec() == 0)
  {
  }

  // wait for action client to come up
  while(!ros::service::exists("/unpin", false))
  {
    ROS_INFO("Waiting for the unpin service to become available");
  }

  //lower
  ROS_INFO("Lowering");
  double low_enough = 0.6475;
  int num_steps = 200;
  for(unsigned int i = 1; i <= num_steps; i++)
  {
    double lowering_distance = (i * low_enough) / ((double) num_steps);
    matec_msgs::UnPin unpin;
    unpin.request.lowering_distance = lowering_distance;
    ros::service::call("/unpin", unpin);
    ros::Duration(0.01).sleep();
  }

  //wait for it to settle
  ROS_INFO("Settling");
  ros::Duration(1.0).sleep();

  //drop
 ROS_INFO("Dropping");
 matec_msgs::UnPin unpin;
 unpin.request.lowering_distance = 0;
 ros::service::call("/unpin", unpin);

  return 0;
}

