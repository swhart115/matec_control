#include "estimators/full_state_estimator.h"

namespace estimators
{
  FullStateEstimator::FullStateEstimator(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    matec_utils::blockOnSMJointNames (m_joint_names);
    matec_utils::blockOnTaskRegistration(m_nh.getNamespace());

    m_nh.param("loop_rate", m_loop_rate, 50.0);

    m_full_states_pub.advertise("/full_joint_states");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&FullStateEstimator::sensorCallback, this, _1));
  }

  FullStateEstimator::~FullStateEstimator()
  {

  }

  void FullStateEstimator::sensorCallback(matec_msgs::FullJointStates& msg)
  {

  }

  void FullStateEstimator::spin()
  {
    ROS_INFO("FullStateEstimator started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "full_state_estimator");
  ros::NodeHandle nh("~");

  estimators::FullStateEstimator node(nh);
  node.spin();

  return 0;
}
