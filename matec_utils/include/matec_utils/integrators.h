#ifndef INTEGRATORS_H
#define INTEGRATORS_H

#include <vector>
#include <cassert>

namespace matec_utils
{
  template<typename T>
  class EulerIntegrator
  {
  public:
    EulerIntegrator()
    {
      m_initialized = false;
      m_last_time = std::numeric_limits<double>::quiet_NaN();
    }

    void initialize(T initial_value)
    {
      m_value = initial_value;
      m_initialized = true;
    }

    T process(T derivative_value, double time)
    {
      if(!m_initialized)
      {
        std::cerr << "Integrator's process function called before it was initialized!";
        assert(0);
      }
      if(isnan(m_last_time)) //first time, just initialize things
      {
        m_last_time = time;
      }
      else
      {
        double dt = time - m_last_time;
        m_value += dt * derivative_value;
        m_last_time = time;
      }
      return m_value;
    }

    T getValue()
    {
      if(!m_initialized)
      {
        std::cerr << "Integrator's getValue function called before it was initialized!";
        assert(0);
      }
      return m_value;
    }

    void setValue(T value)
    {
      m_value = value;
    }

    bool initialized()
    {
      return m_initialized;
    }

  private:
    bool m_initialized;
    T m_value;
    double m_last_time;
  };

  template<typename T>
  class TrapezoidalIntegrator
  {
  public:
    TrapezoidalIntegrator()
    {
      m_initialized = false;
      m_last_time = std::numeric_limits<double>::quiet_NaN();
    }

    void initialize(T initial_value)
    {
      m_value = initial_value;
      m_initialized = true;
    }

    T process(T derivative_value, double time)
    {
      if(!m_initialized)
      {
        std::cerr << "Integrator's process function called before it was initialized!";
        assert(0);
      }
      if(isnan(m_last_time)) //first time, just initialize things
      {
        m_last_time = time;
        m_last_derivative = derivative_value;
      }
      else
      {
        double dt = time - m_last_time;
        m_value += 0.5 * dt * (m_last_derivative + derivative_value);
        m_last_time = time;
        m_last_derivative = derivative_value;
      }
      return m_value;
    }

    T getValue()
    {
      if(!m_initialized)
      {
        std::cerr << "Integrator's getValue function called before it was initialized!";
        assert(0);
      }
      return m_value;
    }

    void setValue(T value)
    {
      m_value = value;
    }

    bool initialized()
    {
      return m_initialized;
    }

  private:
    bool m_initialized;
    T m_value;
    T m_last_derivative;
    double m_last_time;
  };
}

#endif //INTEGRATORS_H
