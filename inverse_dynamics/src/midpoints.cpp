#include "ros/ros.h"
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "matec_msgs/FullJointStates.h"
#include "matec_msgs/JointNames.h"
#include "matec_msgs/TaskCommand.h"

#include <urdf/model.h>
#include <urdf_parser/urdf_parser.h>
#include <boost/thread/thread_time.hpp>

#include "matec_utils/common_initialization_components.h"

using namespace std;
using namespace shared_memory_interface;
int main(int argc, char **argv)
{
  ros::init(argc, argv, "pe_signal_generator");
  ros::NodeHandle n;

  std::vector<std::string> joint_names;
  matec_utils::blockOnSMJointNames(joint_names);
  shared_memory_interface::Publisher<matec_msgs::FullJointStates> carrot_pub;
  carrot_pub.advertise("/pe_carrot");

  while(!n.hasParam("/dynamics/robot_description") && ros::ok())
  {
    ROS_WARN_THROTTLE(0.5, "RecursiveCompositeAdaptiveInverseDynamics waiting for /dynamics/robot_description parameter!");
  }
  urdf::Model model;
  model.initParam("/dynamics/robot_description");

  matec_msgs::FullJointStates full_cmd;
  full_cmd.position.resize(joint_names.size(), 0.0);
  full_cmd.velocity.resize(joint_names.size(), 0.0);
  full_cmd.acceleration.resize(joint_names.size(), 0.0);
  full_cmd.torque.resize(joint_names.size(), 0.0);
  for(unsigned int i = 0; i < joint_names.size(); i++)
  {
    std::string joint_name = joint_names[i];
    boost::shared_ptr<const urdf::Joint> joint = model.getJoint(joint_name);
    double midpoint = (joint->limits->upper + joint->limits->lower) / 2.0;

    ROS_INFO("Setting joint %s (%d) to %g (between %g and %g)", joint_name.c_str(), i, midpoint, joint->limits->upper, joint->limits->lower);

    full_cmd.position[i] = midpoint;
  }

  carrot_pub.publish(full_cmd);

  return 0;
}
