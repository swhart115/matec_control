#ifndef LINEAR_INTERPOLATOR_H
#define LINEAR_INTERPOLATOR_H

#include "interpolator.h"

namespace interpolators
{
  class LinearInterpolator: public Interpolator
  {
  public:
    LinearInterpolator() :
        Interpolator()
    {

    }

    void loadTrajectory(Trajectory traj, bool populate_non_position_fields = true)
    {
      if(!populate_non_position_fields)
      {
        m_traj = traj;
        return;
      }
      else //assume only positions and times have been set in the trajectory
      {
        m_traj.push_back(traj[0]);
        assert(!std::isnan(traj[0].target_state.position) && !std::isinf(traj[0].target_state.position));
        assert(!std::isnan(traj[0].target_state.velocity) && !std::isinf(traj[0].target_state.velocity));
        for(unsigned int i = 1; i < traj.size(); i++)
        {
          double slice_time = (traj[i].finish_time - m_traj[i - 1].finish_time).toSec();
          assert(slice_time >= 0.0);

          if(slice_time <= 0.000001)
          {
            m_traj.push_back(m_traj[i - 1]);
            ROS_WARN("Very small duration detected! Skipping waypoint!");
            continue;
          }

          ViaPoint point;
          point.finish_time = traj[i].finish_time;
          point.target_state.position = traj[i].target_state.position;
          point.target_state.velocity = (traj[i].target_state.position - m_traj[i - 1].target_state.position) / slice_time;
          point.target_state.acceleration = 0;

          assert(!std::isnan(traj[i].target_state.position) && !std::isinf(traj[i].target_state.position));
          assert(!std::isnan(traj[i].target_state.velocity) && !std::isinf(traj[i].target_state.velocity));

          m_traj.push_back(point);
        }
      }

      m_have_valid_trajectory = true;
      m_last_slice_index = 1; //m_traj[0] is the start state
    }

    bool getInterpolatedState(ros::Time time, State& state)
    {
      if(!m_have_valid_trajectory)
      {
        return false;
      }

      //get current trajectory slice (assumes that finish_times are ordered)
      ViaPoint from_via;
      ViaPoint to_via;
      for(unsigned int i = m_last_slice_index; i < m_traj.size(); i++)
      {
        if(time < m_traj[i].finish_time) //found the slice!
        {
          from_via = m_traj[i - 1];
          to_via = m_traj[i];
        }
        else if(i == (m_traj.size() - 1)) //ran out of slices!
        {
          ROS_INFO("Trajectory complete!");
          m_have_valid_trajectory = false;
          return false;
        }
      }

      //figure out where we should be
      double dt = (time - from_via.finish_time).toSec();
      double slice_time = (to_via.finish_time - from_via.finish_time).toSec();
      double time_fraction = dt / slice_time;
      state.position = (1 - time_fraction) * from_via.target_state.position + (time_fraction) * to_via.target_state.position;
      state.velocity = from_via.target_state.velocity;
      state.acceleration = (to_via.target_state.velocity - from_via.target_state.velocity) / slice_time;

//      ROS_INFO("dt:%g, st:%g, tf:%g, p:%g, v:%g, a:%g", dt, slice_time, time_fraction, state.position, state.velocity, state.acceleration);

      return true;
    }
  };
}

#endif //LINEAR_INTERPOLATOR_H
