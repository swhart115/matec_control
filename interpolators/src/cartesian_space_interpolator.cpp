#include "interpolators/cartesian_space_interpolator.h"

namespace interpolators
{
  CartesianSpaceInterpolator::CartesianSpaceInterpolator(const ros::NodeHandle& nh) :
      m_nh(nh), m_server(m_nh, "follow_pose_trajectory", boost::bind(&CartesianSpaceInterpolator::goalCallback, this, _1), boost::bind(&CartesianSpaceInterpolator::cancelCallback, this, _1), false)
  {
    m_nh.param("publish_rate", m_publish_rate, 250.0);
    m_nh.param("always_tare", m_always_tare, true);
    m_nh.param("controlled_frame", m_controlled_frame, std::string(""));
    m_nh.param("output_frame", m_output_frame, std::string(""));

    if(m_controlled_frame.length() == 0)
    {
      ROS_FATAL("CSI: controlled_frame must be specified for the CartesianSpaceInterpolator to work!");
      ros::shutdown();
      return;
    }

    if(m_output_frame.length() == 0)
    {
      ROS_FATAL("CSI: output_frame must be specified for the CartesianSpaceInterpolator to work!");
      ros::shutdown();
      return;
    }

    m_pose_carrot_pub.advertise("carrot");

    tare();

    m_tare_service_server = m_nh.advertiseService("tare", &CartesianSpaceInterpolator::tareCallback, this);

    m_server.start();
  }

  CartesianSpaceInterpolator::~CartesianSpaceInterpolator()
  {

  }

  bool CartesianSpaceInterpolator::tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res)
  {
    tare();
    return true;
  }

  void CartesianSpaceInterpolator::tare()
  {
    boost::mutex::scoped_lock lock(m_mutex);

    ROS_INFO("Taring pose!");

    while(!m_tf_listener.canTransform(m_output_frame, m_controlled_frame, ros::Time(0)))
    {
      ROS_WARN_THROTTLE(0.5, "CSI: can't find %s->%s!", m_output_frame.c_str(), m_controlled_frame.c_str());
    }

    geometry_msgs::PoseStamped zero_pose;
    zero_pose.header.stamp = ros::Time(0);
    zero_pose.pose.position.x = 0;
    zero_pose.pose.position.y = 0;
    zero_pose.pose.position.z = 0;
    zero_pose.pose.orientation = tf::createQuaternionMsgFromYaw(0);
    zero_pose.header.frame_id = m_controlled_frame;

    m_tf_listener.transformPose(m_output_frame, zero_pose, m_last_pose);
  }

  void CartesianSpaceInterpolator::goalCallback(Server::GoalHandle goal_handle)
  {
    if(true)
    {
      ROS_INFO("CSI: Starting Trajectory!");
      cancelCurrentGoal();
      m_goal_handle = goal_handle;
      m_goal_handle.setAccepted();

      matec_actions::FollowPoseTrajectoryFeedback feedback;
      //feedback.progress = 0.0;
      m_goal_handle.publishFeedback(feedback);

      processGoal();
    }
    else
    {
      //TODO: add explanation for rejection
      goal_handle.setRejected();
    }
  }

  void CartesianSpaceInterpolator::cancelCallback(Server::GoalHandle goal_handle)
  {
    cancelCurrentGoal();
    shutdown();
  }

  void CartesianSpaceInterpolator::cancelCurrentGoal()
  {
    if(m_goal_handle.getGoal().get()) //goal doesn't actually exist, so no need to cancel
    {
      actionlib_msgs::GoalStatus status = m_goal_handle.getGoalStatus(); //cancel if current goal is pending, recalling, active, or preempting
      if(status.status == actionlib_msgs::GoalStatus::PENDING || status.status == actionlib_msgs::GoalStatus::RECALLING || status.status == actionlib_msgs::GoalStatus::ACTIVE || status.status == actionlib_msgs::GoalStatus::PREEMPTING)
      {
        m_goal_handle.setCanceled();
        m_goal_handle = Server::GoalHandle();
      }

      m_x_interpolator.cancelTrajectory();
      m_y_interpolator.cancelTrajectory();
      m_z_interpolator.cancelTrajectory();
      m_quaternion_interpolator.cancelTrajectory();
    }
  }

  void CartesianSpaceInterpolator::shutdown()
  {
    //stop everything that we were doing
  }

  bool CartesianSpaceInterpolator::goalActive()
  {
    return m_goal_handle.getGoal();
  }

  void CartesianSpaceInterpolator::processGoal()
  {
    //TODO: transform goals into output_frame first
    m_x_interpolator.cancelTrajectory();
    m_y_interpolator.cancelTrajectory();
    m_z_interpolator.cancelTrajectory();
    m_quaternion_interpolator.cancelTrajectory();

    ros::Time goal_time = m_goal_handle.getGoalID().stamp;

    if(m_always_tare)
    {
      tare();
    }

    Trajectory x_traj;
    Trajectory y_traj;
    Trajectory z_traj;
    QuaternionTrajectory quat_traj;

    x_traj.push_back(ViaPoint(goal_time, m_last_pose.pose.position.x, 0));
    y_traj.push_back(ViaPoint(goal_time, m_last_pose.pose.position.y, 0));
    z_traj.push_back(ViaPoint(goal_time, m_last_pose.pose.position.z, 0));
    quat_traj.push_back(QuaternionViaPoint(goal_time, m_last_pose.pose.orientation));

    for(unsigned int waypoint_idx = 0; waypoint_idx < m_goal_handle.getGoal()->poses.size(); waypoint_idx++)
    {
      ros::Time via_time = goal_time + m_goal_handle.getGoal()->times_from_start[waypoint_idx];
      x_traj.push_back(ViaPoint(via_time, m_goal_handle.getGoal()->poses[waypoint_idx].position.x, 0));
      y_traj.push_back(ViaPoint(via_time, m_goal_handle.getGoal()->poses[waypoint_idx].position.y, 0));
      z_traj.push_back(ViaPoint(via_time, m_goal_handle.getGoal()->poses[waypoint_idx].position.z, 0));
      quat_traj.push_back(QuaternionViaPoint(via_time, m_goal_handle.getGoal()->poses[waypoint_idx].orientation));
    }

    m_x_interpolator.loadTrajectory(x_traj);
    m_y_interpolator.loadTrajectory(y_traj);
    m_z_interpolator.loadTrajectory(z_traj);
    m_quaternion_interpolator.loadTrajectory(quat_traj);
  }

  void CartesianSpaceInterpolator::finishGoal(bool success)
  {
    if(success)
    {
      ROS_INFO("CSI: Finished trajectory!");
      matec_actions::FollowPoseTrajectoryFeedback feedback;
      //feedback.progress = 1.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setSucceeded();
      m_goal_handle = Server::GoalHandle();
    }
    else
    {
      ROS_ERROR("CSI: Trajectory following failed!");
      matec_actions::FollowPoseTrajectoryFeedback feedback;
      //feedback.progress = 0.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setAborted();
      m_goal_handle = Server::GoalHandle();
    }
  }

  void CartesianSpaceInterpolator::calculateNextCommand(ros::Time time, geometry_msgs::PoseStamped& pose_command)
  {
    boost::mutex::scoped_lock lock(m_mutex);

    bool done = false;

    State x_state, y_state, z_state;
    if(!m_x_interpolator.getInterpolatedState(time, x_state))
    {
      done = true;
    }
    else
    {
      pose_command.pose.position.x = x_state.position;
    }

    if(!m_y_interpolator.getInterpolatedState(time, y_state))
    {
      assert(done);
      //everything should finish at the same time
    }
    else
    {
      pose_command.pose.position.y = y_state.position;
    }

    if(!m_z_interpolator.getInterpolatedState(time, z_state))
    {
      assert(done);
      //everything should finish at the same time
    }
    else
    {
      pose_command.pose.position.z = z_state.position;
    }

    geometry_msgs::Quaternion quat;
    if(!m_quaternion_interpolator.getInterpolatedState(time, quat))
    {
      assert(done);
      //everything should finish at the same time
    }
    else
    {
      pose_command.pose.orientation = quat;
    }
  }

  void CartesianSpaceInterpolator::spin()
  {
    ROS_INFO("CartesianSpaceInterpolator started.");
    ros::Rate loop_rate(m_publish_rate);
    while(ros::ok())
    {
      calculateNextCommand(ros::Time::now(), m_last_pose);
      m_last_pose.header.stamp = ros::Time::now();
      m_pose_carrot_pub.publish(m_last_pose);

      loop_rate.sleep();
      ros::spinOnce();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "cartesian_space_interpolator");
  ros::NodeHandle nh("~");

  interpolators::CartesianSpaceInterpolator node(nh);
  node.spin();

  return 0;
}
