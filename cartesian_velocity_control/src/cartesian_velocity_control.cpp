#include "cartesian_velocity_control/cartesian_velocity_control.h"

namespace cartesian_velocity_control
{
  CartesianVelocityControl::CartesianVelocityControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 50.0);

    initModel();
    getGains();

    m_cmd_pub = m_nh.advertise<sensor_msgs::JointState>("/cartesian_velocity_command", 1, true);
    m_cmd_vis_pub = m_nh.advertise<sensor_msgs::JointState>("/cartesian_velocity_command_visualization", 1, true);
    m_js_sub = m_nh.subscribe<sensor_msgs::JointState>("/atlas/joint_states", 1, boost::bind(&CartesianVelocityControl::jsCallback, this, _1));
    m_twist_sub = m_nh.subscribe<geometry_msgs::TwistStamped>("/target_twist", 1, boost::bind(&CartesianVelocityControl::twistCallback, this, _1));
  }

  CartesianVelocityControl::~CartesianVelocityControl()
  {

  }

  void CartesianVelocityControl::jsCallback(const sensor_msgs::JointStateConstPtr& msg)
  {
    m_js = *msg;
  }

  void CartesianVelocityControl::twistCallback(const geometry_msgs::TwistStampedConstPtr& msg)
  {
    KDL::Chain chain;
    m_tree.getChain("utorso", msg->header.frame_id, chain); //NOTE: frame_id must be either l_hand or r_hand right now!

    KDL::Twist twist(KDL::Vector(msg->twist.linear.x, msg->twist.linear.y, msg->twist.linear.z), KDL::Vector(msg->twist.angular.x, msg->twist.angular.y, msg->twist.angular.z));

    KDL::ChainIkSolverVel_wdls solver(chain);
    Eigen::MatrixXd Mx = Eigen::MatrixXd::Identity(6, 6);
    Eigen::MatrixXd Mq = Eigen::MatrixXd::Identity(6, 6);
    double values[6] = {1, 1, 0.5, 1, 0.5, 1}; //non-negative, 0 => don't move
    for(unsigned int i = 0; i < 6; i++)
    {
      Mq(i, i) = values[i];
    }
    solver.setWeightJS(Mq);
    solver.setWeightTS(Mx);

    KDL::JntArray q(chain.getNrOfJoints()), qd(chain.getNrOfJoints());

    assert(m_js.position.size() > 22);
    for(unsigned int i = 0; i < 6; i++)
    {
      if(msg->header.frame_id == "l_hand")
      {
        q(0 + i) = m_js.position[16 + i];
      }
      else if(msg->header.frame_id == "r_hand")
      {
        q(0 + i) = m_js.position[22 + i];
      }
    }

    solver.CartToJnt(q, twist, qd);

    double max_output_vel = -std::numeric_limits<double>::max();
    for(unsigned int i = 0; i < 6; i++)
    {
      if(qd(0 + i) > max_output_vel)
      {
        max_output_vel = qd(0 + i);
      }
    }

    double max_allowable_vel = 0.5;
    twist.vel.x(twist.vel.x() * std::min(1.0, max_allowable_vel / max_output_vel));
    twist.vel.y(twist.vel.y() * std::min(1.0, max_allowable_vel / max_output_vel));
    twist.vel.z(twist.vel.z() * std::min(1.0, max_allowable_vel / max_output_vel));
    twist.rot.x(twist.rot.x() * std::min(1.0, max_allowable_vel / max_output_vel));
    twist.rot.y(twist.rot.y() * std::min(1.0, max_allowable_vel / max_output_vel));
    twist.rot.z(twist.rot.z() * std::min(1.0, max_allowable_vel / max_output_vel));

    solver.CartToJnt(q, twist, qd);

    sensor_msgs::JointState vel_cmd;
    vel_cmd.name = m_js.name;
//    vel_cmd.position = m_js.position;
    vel_cmd.velocity.resize(m_js.velocity.size(), 0.0);
//    vel_cmd.effort.resize(m_js.effort.size(), 0.0);
    for(unsigned int i = 0; i < 6; i++)
    {
      if(msg->header.frame_id == "l_hand")
      {
        vel_cmd.velocity[16 + i] = qd(0 + i);
      }
      else if(msg->header.frame_id == "r_hand")
      {
        vel_cmd.velocity[22 + i] = qd(0 + i);
      }
    }

    m_cmd_pub.publish(vel_cmd);

    sensor_msgs::JointState cmd_vis;
    cmd_vis.name = vel_cmd.name;
    cmd_vis.position.resize(vel_cmd.name.size(), 0.0);
    cmd_vis.velocity.resize(vel_cmd.name.size(), 0.0);
    cmd_vis.effort = vel_cmd.velocity; //NOTE: this allows visualization in rviz
    m_cmd_vis_pub.publish(cmd_vis);
  }

  void CartesianVelocityControl::initModel()
  {
    urdf::Model urdf_model;
    urdf_model.initParam("/robot_description");
    if(!kdl_parser::treeFromUrdfModel((const urdf::Model) urdf_model, m_tree))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }
  }

  void CartesianVelocityControl::getGains()
  {
//    boost::mutex::scoped_lock lock(m_mutex);
//    m_nh.getParamCached("gains/com_x/p", m_com_x_kp);
//    m_nh.getParamCached("gains/com_x/v", m_com_x_kv);
//
//    m_nh.getParamCached("gains/com_y/p", m_com_y_kp);
//    m_nh.getParamCached("gains/com_y/v", m_com_y_kv);
//
//    m_nh.getParamCached("gains/com_z/p", m_com_z_kp);
//    m_nh.getParamCached("gains/com_z/v", m_com_z_kv);
  }

  void CartesianVelocityControl::spin()
  {
    ROS_INFO("CartesianVelocityControl started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "cartesian_velocity_control");
  ros::NodeHandle nh("~");

  cartesian_velocity_control::CartesianVelocityControl node(nh);
  node.spin();

  return 0;
}
