#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <matec_actions/FollowPoseTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "move_frame");
  ros::NodeHandle nh;

  std::string interpolator_name;
  double x, y, z, roll, pitch, yaw;
  geometry_msgs::Pose pose;
  if(argc == 8)
  {
    interpolator_name = argv[1];
    x = atof(argv[2]);
    y = atof(argv[3]);
    z = atof(argv[4]);
    roll = atof(argv[5]);
    pitch = atof(argv[6]);
    yaw = atof(argv[7]);
    pose.position.x = x;
    pose.position.y = y;
    pose.position.z = z;
    pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);
  }
  else
  {
    cout << "args: [interpolator name], x, y, z, roll, pitch, yaw \n";
    return 0;
  }

  actionlib::SimpleActionClient<matec_actions::FollowPoseTrajectoryAction> client(nh, "/" + interpolator_name + "/follow_pose_trajectory", true);

  while(ros::Time::now().toSec() == 0)
  {

  }

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");

  matec_actions::FollowPoseTrajectoryGoal goal;
  goal.header.stamp = ros::Time::now();
  goal.poses.push_back(pose);
  goal.times_from_start.push_back(ros::Duration(3.0));

  // Start the trajectory
  client.sendGoal(goal);

  client.waitForResult(ros::Duration(4.0));

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

