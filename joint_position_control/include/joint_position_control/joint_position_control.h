#ifndef JOINT_POSITION_CONTROL_H
#define JOINT_POSITION_CONTROL_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <boost/thread.hpp>

#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/RegisterTask.h"

namespace joint_position_control
{
  class JointPositionControl
  {
  public:
    JointPositionControl(const ros::NodeHandle& nh);
    ~JointPositionControl();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_interpolator;

    ros::Publisher m_error_pub;
    ros::Publisher m_total_error_pub;
    ros::Publisher m_max_error_pub;
    ros::Publisher m_worst_joint_pub;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;

    std::vector<double> m_kps;
    std::vector<double> m_kvs;
    std::vector<double> m_kis;
    std::vector<double> m_i_clamps;
    std::vector<double> m_i_livebands;
    std::vector<double> m_i_accumulators;

    boost::mutex m_mutex;

    shared_memory_interface::Publisher<matec_msgs::TaskCommand> m_task_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_carrot_sub;

    void getGains();

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //JOINT_POSITION_CONTROL_H
