#include "matec_utils/kinematic_workspace_visualizer.h"

namespace matec_utils
{
  KinematicWorkspaceVisualizer::KinematicWorkspaceVisualizer(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 50.0);
    m_nh.param("steps_per_joint", m_steps_per_joint, 20);
    m_nh.param("root_frame", m_root_frame, std::string(""));
    m_nh.param("end_frame", m_end_frame, std::string(""));
    m_nh.param("model_path", m_model_path, std::string(""));

    if(m_root_frame.length() == 0)
    {
      ROS_ERROR("root_frame must be specified");
      ros::shutdown();
      return;
    }
    if(m_end_frame.length() == 0)
    {
      ROS_ERROR("end_frame must be specified");
      ros::shutdown();
      return;
    }

    initModel();
    calculateCloud();

    m_cloud_pub = m_nh.advertise<sensor_msgs::PointCloud2>("/reachable_cloud", 1, true);
  }

  KinematicWorkspaceVisualizer::~KinematicWorkspaceVisualizer()
  {

  }

  void KinematicWorkspaceVisualizer::initModel()
  {
    if(m_model_path.length() != 0)
    {
      ROS_INFO("Using user-specified model path %s", m_model_path.c_str());
      assert(m_urdf_model.initFile(m_model_path));
    }
    else
    {
      ROS_INFO("Using model stored in /robot_description");
      assert(m_urdf_model.initParam("/robot_description"));
    }

    if(!kdl_parser::treeFromUrdfModel((const urdf::Model) m_urdf_model, m_tree))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }
  }

  double fixAngle(double angle)
  {
    while(angle >= (2.0 * M_PI))
    {
      angle -= 2.0 * M_PI;
    }

    while(angle < 0)
    {
      angle += 2.0 * M_PI;
    }

    return angle;
  }

  void KinematicWorkspaceVisualizer::calculateKinematicsRecursive(int my_idx, KDL::JntArray& j)
  {
    for(int i = 0; i < m_steps_per_joint && ros::ok(); i++)
    {
      j(my_idx) = m_min_bounds[my_idx] + (m_max_bounds[my_idx] - m_min_bounds[my_idx]) * ((double) i / ((double) m_steps_per_joint - 1.0));
      if(my_idx == (int) m_chain.getNrOfJoints() - 1) //we're at the lowest level
      {
        KDL::Frame end_effector_pose;
        KDL::ChainFkSolverPos_recursive solver(m_chain);
        solver.JntToCart(j, end_effector_pose);

        double roll, pitch, yaw;
        end_effector_pose.M.GetRPY(roll, pitch, yaw);

        pcl::PointXYZRGB point;
        point.x = end_effector_pose.p.x();
        point.y = end_effector_pose.p.y();
        point.z = end_effector_pose.p.z();
        point.r = 255 * fixAngle(roll) / (2.0 * M_PI);
        point.g = 255 * fixAngle(pitch) / (2.0 * M_PI);
        point.b = 255 * fixAngle(yaw) / (2.0 * M_PI);
        m_pcl_cloud.points.push_back(point);

        m_num_points_tested++;

        ros::Duration diff = ros::Time::now() - m_start_time;
        float elapsed_time = diff.toSec();
        float completion = ((float) m_num_points_tested) / ((float) m_total_points_to_test);
        float rate = completion / elapsed_time;
        float remaining_time = (1 - completion) / rate;
        ROS_INFO_THROTTLE(1.0, "Elaspsed time is %gs. Testing is %g%% complete. ~%gs remaining.", elapsed_time, completion*100, remaining_time);
      }
      else
      {
        calculateKinematicsRecursive(my_idx + 1, j);
      }
    }
  }

  void KinematicWorkspaceVisualizer::calculateCloud()
  {
    //set up recursion
    m_tree.getChain(m_root_frame, m_end_frame, m_chain);
    m_min_bounds.resize(m_chain.getNrOfJoints());
    m_max_bounds.resize(m_chain.getNrOfJoints());
    for(unsigned int i = 0; i < m_chain.getNrOfJoints(); i++)
    {
      std::string joint_name = m_chain.getSegment(i).getJoint().getName();
      m_min_bounds[i] = m_urdf_model.getJoint(joint_name)->limits->lower;
      m_max_bounds[i] = m_urdf_model.getJoint(joint_name)->limits->upper;
      if(fabs(m_max_bounds[i] - m_min_bounds[i]) >= (2.0 * M_PI)) //don't bother checking more than a full circle
      {
        m_min_bounds[i] = 0;
        m_max_bounds[i] = 2 * M_PI;
      }
    }
    m_start_time = ros::Time::now();
    m_num_points_tested = 0;
    m_total_points_to_test = std::pow(m_steps_per_joint, m_chain.getNrOfJoints());

    //recurse
    KDL::JntArray j(m_chain.getNrOfJoints());
    calculateKinematicsRecursive(0, j);
    m_pcl_cloud.width = m_pcl_cloud.points.size();
    m_pcl_cloud.height = 1;
    m_pcl_cloud.header.frame_id = m_root_frame;
    pcl::toROSMsg(m_pcl_cloud, m_cloud);
  }

  void KinematicWorkspaceVisualizer::spin()
  {
    ROS_INFO("KinematicWorkspaceVisualizer started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      m_cloud.header.stamp = ros::Time::now();
      m_cloud_pub.publish(m_cloud);

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "kinematic_workspace_visualizer");
  ros::NodeHandle nh("~");

  matec_utils::KinematicWorkspaceVisualizer node(nh);
  node.spin();

  return 0;
}
