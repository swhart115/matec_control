#ifndef JOINT_SPACE_INTERPOLATOR_H
#define JOINT_SPACE_INTERPOLATOR_H

#include <ros/ros.h>
#include <actionlib/server/action_server.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <boost/thread/mutex.hpp>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>

#include "interpolators/minimum_jerk_interpolator.h"

#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/Tare.h"

namespace interpolators
{
  typedef actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction> Server;

  class JointSpaceInterpolator
  {
  public:
    JointSpaceInterpolator(const ros::NodeHandle& nh);
    ~JointSpaceInterpolator();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_publish_rate;
    bool m_always_tare;

    Server m_server;
    Server::GoalHandle m_goal_handle;

    ros::ServiceServer m_tare_service_server;

    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_carrot_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;

    std::vector<MinimumJerkInterpolator> m_interpolators;
    matec_msgs::FullJointStates m_last_command;

    boost::mutex m_mutex;

    void goalCallback(Server::GoalHandle goal_handle);
    void cancelCallback(Server::GoalHandle goal_handle);
    void cancelCurrentGoal();
    void setupGoal();
    void shutdown();
    bool goalActive();

    bool checkGoalJoints(std::vector<std::string> goal_joints);

    void finishGoal(bool success);
    void calculateNextCommand(ros::Time time, matec_msgs::FullJointStates& command);
    void interpolate();
    void tare(std::vector<int> subset = std::vector<int>());

    bool tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res);
  };
}
#endif //JOINT_SPACE_INTERPOLATOR_H
