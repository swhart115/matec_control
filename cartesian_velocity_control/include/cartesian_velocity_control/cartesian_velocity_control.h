#ifndef CARTESIAN_VELOCITY_CONTROL_H
#define CARTESIAN_VELOCITY_CONTROL_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <geometry_msgs/Point.h>
#include <boost/thread.hpp>
#include <geometry_msgs/WrenchStamped.h>
#include <numeric>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include <math.h>
#include <urdf/model.h>
#include <tf/tf.h>
#include <geometric_shapes/bodies.h>
#include <geometric_shapes/body_operations.h>
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/shape_operations.h>
#include <kdl/tree.hpp>
#include <kdl/frames.hpp>
#include <kdl/segment.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <tf_conversions/tf_kdl.h>
#include <tf/transform_datatypes.h>
#include <tf2/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/WrenchStamped.h>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <sensor_msgs/JointState.h>
#include <cmath>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>


namespace cartesian_velocity_control
{
  class CartesianVelocityControl
  {
  public:
    CartesianVelocityControl(const ros::NodeHandle& nh);
    ~CartesianVelocityControl();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    sensor_msgs::JointState m_js;

    ros::Publisher m_cmd_pub;
    ros::Publisher m_cmd_vis_pub;
    ros::Subscriber m_js_sub;
    ros::Subscriber m_twist_sub;

    KDL::Tree m_tree;

    void getGains();
    void initModel();

    void jsCallback(const sensor_msgs::JointStateConstPtr& msg);
    void twistCallback(const geometry_msgs::TwistStampedConstPtr& msg);
  };
}
#endif //CARTESIAN_VELOCITY_CONTROL_H
