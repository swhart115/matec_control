#ifndef DEBUG_LEVELS_H
#define DEBUG_LEVELS_H

#include <string>
#include <cstdio>

#define SHOW_NONE   0
#define SHOW_ERROR  1
#define SHOW_WARN   2
#define SHOW_INFO   3
#define SHOW_DEBUG  4
#define DEBUG_LEVEL SHOW_INFO //set to one of the above

#define WHITE_TEXT  "\033[0m"
#define RED_TEXT "\033[31m"
#define YELLOW_TEXT  "\033[33m"
#define GREEN_TEXT  "\033[92m"

#define PRINT_ERROR(...) if(DEBUG_LEVEL >= SHOW_ERROR){printf(RED_TEXT __VA_ARGS__); printf(WHITE_TEXT);}
#define PRINT_WARN(...) if(DEBUG_LEVEL >= SHOW_WARN){printf(YELLOW_TEXT __VA_ARGS__); printf(WHITE_TEXT);}
#define PRINT_INFO(...) if(DEBUG_LEVEL >= SHOW_INFO){printf(WHITE_TEXT __VA_ARGS__); printf(WHITE_TEXT);}
#define PRINT_DEBUG(...) if(DEBUG_LEVEL >= SHOW_DEBUG){printf(GREEN_TEXT __VA_ARGS__); printf(WHITE_TEXT);}

#define PRINTLN_ERROR(...) if(DEBUG_LEVEL >= SHOW_ERROR){printf(RED_TEXT __VA_ARGS__); printf(WHITE_TEXT "\n");}
#define PRINTLN_WARN(...) if(DEBUG_LEVEL >= SHOW_WARN){printf(YELLOW_TEXT __VA_ARGS__); printf(WHITE_TEXT "\n");}
#define PRINTLN_INFO(...) if(DEBUG_LEVEL >= SHOW_INFO){printf(WHITE_TEXT __VA_ARGS__); printf(WHITE_TEXT "\n");}
#define PRINTLN_DEBUG(...) if(DEBUG_LEVEL >= SHOW_DEBUG){printf(GREEN_TEXT __VA_ARGS__); printf(WHITE_TEXT "\n");}

#define PRINT_ERROR_STREAM(...) if(DEBUG_LEVEL >= SHOW_ERROR){std::cerr << RED_TEXT << __VA_ARGS__ << WHITE_TEXT;}
#define PRINT_WARN_STREAM(...) if(DEBUG_LEVEL >= SHOW_WARN){std::cerr << YELLOW_TEXT << __VA_ARGS__ << WHITE_TEXT;}
#define PRINT_INFO_STREAM(...) if(DEBUG_LEVEL >= SHOW_INFO){std::cerr << WHITE_TEXT << __VA_ARGS__ << WHITE_TEXT;}
#define PRINT_DEBUG_STREAM(...) if(DEBUG_LEVEL >= SHOW_DEBUG){std::cerr << GREEN_TEXT << __VA_ARGS__ << WHITE_TEXT;}

#define PRINTLN_ERROR_STREAM(...) if(DEBUG_LEVEL >= SHOW_ERROR){std::cerr << RED_TEXT << __VA_ARGS__ << WHITE_TEXT << std::endl;}
#define PRINTLN_WARN_STREAM(...) if(DEBUG_LEVEL >= SHOW_WARN){std::cerr << YELLOW_TEXT << __VA_ARGS__ << WHITE_TEXT << std::endl;}
#define PRINTLN_INFO_STREAM(...) if(DEBUG_LEVEL >= SHOW_INFO){std::cerr << WHITE_TEXT << __VA_ARGS__ << WHITE_TEXT << std::endl;}
#define PRINTLN_DEBUG_STREAM(...) if(DEBUG_LEVEL >= SHOW_DEBUG){std::cerr << GREEN_TEXT << __VA_ARGS__ << WHITE_TEXT << std::endl;}


#include <boost/thread/thread_time.hpp>
#define PRINTLN_ERROR_THROTTLE(rate, ...) if(DEBUG_LEVEL >= SHOW_ERROR){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_ERROR(__VA_ARGS__); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }
#define PRINTLN_WARN_THROTTLE(rate, ...) if(DEBUG_LEVEL >= SHOW_WARN){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_WARN(__VA_ARGS__); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }
#define PRINTLN_INFO_THROTTLE(rate, ...) if(DEBUG_LEVEL >= SHOW_INFO){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_INFO(__VA_ARGS__); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }
#define PRINTLN_DEBUG_THROTTLE(rate, ...) if(DEBUG_LEVEL >= SHOW_DEBUG){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_DEBUG(__VA_ARGS__); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }

#define PRINTLN_ERROR_STREAM_THROTTLE(rate, args) if(DEBUG_LEVEL >= SHOW_ERROR){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_ERROR_STREAM(args); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }
#define PRINTLN_WARN_STREAM_THROTTLE(rate, args) if(DEBUG_LEVEL >= SHOW_WARN){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_WARN_STREAM(args); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }
#define PRINTLN_INFO_STREAM_THROTTLE(rate, args) if(DEBUG_LEVEL >= SHOW_INFO){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_INFO_STREAM(args); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }
#define PRINTLN_DEBUG_STREAM_THROTTLE(rate, args) if(DEBUG_LEVEL >= SHOW_DEBUG){  \
                                                        static boost::posix_time::ptime next_write_time = boost::get_system_time(); \
                                                        if(next_write_time <= boost::get_system_time()) \
                                                        { \
                                                          PRINTLN_DEBUG_STREAM(args); \
                                                          next_write_time = boost::get_system_time() + boost::posix_time::milliseconds(1000.0/rate); \
                                                        } \
                                                    }

#endif //DEBUG_LEVELS_H
