#ifndef COM_CONTROL_H
#define COM_CONTROL_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/ContactSpecifications.h>
#include <geometry_msgs/Point.h>
#include <matec_msgs/TaskCommand.h>
#include <boost/thread.hpp>
#include <geometry_msgs/WrenchStamped.h>
#include <numeric>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include <cmath>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

#include "matec_utils/model_helper.h"
#include "contact.h"
#include "matec_utils/common_initialization_components.h"

namespace com_control
{
  class COMControl
  {
  public:
    COMControl(const ros::NodeHandle& nh);
    ~COMControl();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    ros::Publisher m_error_pub;

    std::map<std::string, ros::Publisher> m_wrench_pubs;
    ros::Publisher m_desired_com_wrench_pub;
    ros::Publisher m_calculated_com_wrench_pub;

    std::vector<geometry_msgs::WrenchStamped> m_wrench_msgs;
    geometry_msgs::WrenchStamped m_target_com_wrench;

    matec_utils::ModelHelper* m_model;
    std::vector<Contact> m_contacts;

    tf::TransformBroadcaster m_tf_broadcaster;

    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;

    geometry_msgs::Point m_com;

    double m_com_x_kp;
    double m_com_x_kv;
    double m_com_y_kp;
    double m_com_y_kv;
    double m_com_z_kp;
    double m_com_z_kv;

    boost::mutex m_mutex;

    shared_memory_interface::Publisher<matec_msgs::TaskCommand> m_command_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;
    shared_memory_interface::Subscriber<geometry_msgs::Pose> m_com_pose_command_sub;
    shared_memory_interface::Subscriber<matec_msgs::ContactSpecifications> m_contact_sub;

    void getGains();

    void initModel();

    void generateWrenchMsgs(std::vector<matec_utils::KDLWrenchStamped> wrenches);

    matec_utils::KDLWrenchStamped reactionWrenchesToCOMWrench(std::vector<matec_utils::KDLWrenchStamped> local_reaction_wrenches);
    bool COMWrenchToSingleReactionWrench(matec_utils::KDLWrenchStamped desired_com_wrench, Contact contact, matec_utils::KDLWrenchStamped& reaction_wrench, SlipState& slip_state);
    bool COMWrenchToReactionWrenches(matec_utils::KDLWrenchStamped desired_com_wrench, std::vector<matec_utils::KDLWrenchStamped>& reaction_wrenches);

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //COM_CONTROL_H
