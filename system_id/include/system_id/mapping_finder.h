#ifndef MAPPING_FINDER_H
#define MAPPING_FINDER_H

#include "system_id/debug_levels.h"
#include "system_id/training_data.h"
#include "system_id/expression_ga.h"
#include "system_id/coefficient_optimizer.h"

namespace system_id
{
  class MappingFinder
  {
  public:
    system_id::ExpressionGenome optimize(system_id::TrainingData* data, system_id::EGParams eg_params, system_id::EGAParams ega_params)
    {
      m_best = NULL;
      m_training_data = data;

      m_best_fitness = std::numeric_limits<long double>::max();

      system_id::ExpressionGeneticAlgorithm ga(boost::bind(&MappingFinder::fitness_function, this, _1), eg_params);
      ga.optimize(ega_params);
      return *m_best;
    }

    void fitness_function(system_id::ExpressionGenome& genome)
    {
      std::string expression_string = genome.gene.first.getExpressionString();
      std::vector<std::string> input_symbols = genome.input_symbols;
      std::vector<std::string> coefficient_symbols = genome.coefficient_symbols;

      std::cerr << "Testing " << expression_string << " with " << input_symbols.size() << " inputs, and " << coefficient_symbols.size() << " coefficients!" << std::endl;

      GAParams params(coefficient_symbols.size(), 3, 0.001, 100, 10);
      params.print = true;

      CoefficientOptimizer opt;
      system_id::ArrayGenome best_coefficients = opt.optimize(expression_string, coefficient_symbols, input_symbols, m_training_data, params, &genome.gene.second.gene);
      genome.gene.second = best_coefficients;
      genome.fitness = best_coefficients.fitness;

      if(best_coefficients.fitness < m_best_fitness)
      {
        if(!m_best)
        {
          m_best = new ExpressionGenome(genome);
        }
        else
        {
          *m_best = genome;
        }
        std::cerr << "Best full equation is now: " << *m_best << std::endl;
        std::cerr << "fitness: " << m_best->fitness << "," << best_coefficients.fitness<< std::endl;
      }
    }

  private:
    system_id::TrainingData* m_training_data;
    system_id::ExpressionGenome* m_best;
    double m_best_fitness;

    long double m_output_min;
    long double m_output_max;
  };
}

#endif //MAPPING_FINDER_H
