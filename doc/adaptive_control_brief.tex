\documentclass{article}
\usepackage{amsmath}
\usepackage{fullpage}
\usepackage{setspace}
\usepackage{multicol}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\usepackage[section]{placeins}
%\usepackage[margin=0.5in]{geometry}
\usepackage{floatrow}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%
\begin{document}
\title{Adaptive Control Brief}
\author{Joshua James}
\date{}
\maketitle
\onehalfspacing
\tableofcontents
\pagebreak
\section{Adaptive algorithms}
\subsection{Hanlei Wang's work}
Hanlei Wang has recently done a bunch of work on recursive adaptive manipulator control. One of his controllers can supposedly control and learn the parameters of a serial chain manipulator with a floating base, which is almost the case with Atlas (would just need to modify it to handle branching trees). He also had a similar but slightly simpler version for fixed base manipulators. I tried to use the fixed base version on a simple fixed-base 3R model first to see if the adaptation algorithm itself was good, but unfortunately was not able to get it to work at all.

\subsection{Niemeyer, Gunter, and Slotine's direct recursive adaptive controller}
Slotine and Li published a recursive formulation for one of their direct adaptive manipulator control schemes. From what I can tell, pretty much all the recursive adaptive control papers following it (including Wang's) were loosely based on it. Wang's work, however, modified a critical component of it (I verified that the outputs of his calculations are not equivalent to the originals, though I don't know whose calculations were more correct), which may have been why his didn't work. 
\\[4 px]
I tested this controller on 3R, Sim-Atlas, and Real-Atlas (left arm only). It worked decently for converging steady state tracking error (typically less than 1 degree of error), but it was really bad at converging parameter error, and often didn't do as well during motion (likely because it was compensating for incorrect inertias). Only asymptotic convergence of tracking error is guaranteed though, so I guess it probably was working as intended. It also still typically had more error than our current PID, though the commands oscillated a bit less than when using PID as well. This algorithm also has viscous / coulomb feed-forward friction compensation terms, which seemed to help overcome Real-Atlas' friction problems.

\subsection{Slotine and Li's composite non-recursive adaptive controller}
Slotine and Li also later developed a composite adaptive controller that uses both tracking error and predicted torque error to guarantee asymptotic convergence of both tracking and parameter error in steady state (so in theory you should be able to just let it sit still for a very long time and get mass / inertia properties, though I'm not sure how that makes sense), and exponential convergence of both given certain kinds of desired trajectories. However, they did not develop a recursive formulation for this one. Wang did provide a version of the controller that is partially recursive, but the algorithm is significantly harder to implement than the direct adaptive control algorithm.
\\[4 px]
The torque prediction part of composite adaptive control requires the computation of a filtered regressor matrix that is always vaguely described as ``extremely hard to compute.'' I still have not found a paper describing how to actually go about computing it, other than Wang's. I implemented Wang's method, but was unable to get it to match the torque measured by gazebo (filtered with the same filter) even given a correct inertia matrix (so the code thought there was prediction error even when there shouldn't have been any). I then reverse engineered the recursive algorithm to compute the non-filtered version based on Wang's work, and it matched with the gazebo torque very well, so it may be that I was doing the filtering part wrong in some way. However, the non-filtered version requires joint acceleration measurements (only need position / velocity in the filtered version), which are typically considered to not be available. 
I got it to work reasonably well in the 3R case using the non-filtered version of the regressor to determine torque prediction error by cranking up the simulation quality to reduce noise and using accelerations derived from velocity. This seemed to work well in simulation, though it would probably not have worked as well in real life due to sensor noise.
\\[4 px]
As previously mentioned, Wang's algorithm is not fully recursive. It requires some large full-system matrix multiplications in the adaptation part (10*n by 10*n). It is most likely possible to convert them to a recursive form to speed things up, but I didn't get around to doing so.
\\[4 px]
The algorithm also uses a time-varying adaptation gain that requires a potentially unsafe inversion of a very large matrix, which slows things down and occasionally generates nans/infs. Wang's adaptation method is also slightly different from Slotine's, though I couldn't get either method to work particularly well. 
\\[4 px]
I have had some success in getting this method to work for tracking error / parameter convergence on the 3R model, but not Sim-Atlas or Real-Atlas. At its best, the algorithm was consistently able to stably follow a multi-frequency sin wave while converging the parameters to within 1-5\% of their true values within around 30 seconds, even when the initial guess was up to an order of magnitude off (e.g. if mass was 1kg, the initial guess would be randomly chosen between 0.1 and 10kg, with each parameter of each link chosen independently). However, if I changed the model slightly (by adding a mass to the end, for instance), neither the tracking error nor the parameters would converge. It would still try to track if I turned up the gains, but the parameters would often move in the wrong direction, making the tracking worse. It also didn't work well for Sim-Atlas, though this may have been because the large matrix multiplications were slowing it down. Code that ran at 1000hz on the 3R model would typically only run at 50-100hz for Sim-Atlas. Using the unfiltered regressor with a simpler adaptation law, I was able to implement a fully recursive version capable of running at 1000hz on Sim-Atlas. I believe the simplified adaptation should still have caused the parameter error to converge if the dynamics were sufficiently persistently exciting. It never did though, so it may be that the math is still wrong, or the commands I sent were just not sufficiently PE. I never ended up running the code on Real-Atlas, since I figured if it didn't even work in sim, it probably wouldn't work in real life.
\\[4 px]

\subsection{Inertial primitives and other modifications}
I tried using ``inertial primitives'' to speed up the parameter convergence. The idea was that Atlas' links are reasonably cylindrical, so just fitting the inertia matrix of a solid cylinder with approximately the the same dimensions as the mesh to the data seemed like it might work. The only free parameter for each joint in that case would be mass, so convergence would ideally be about 10 times faster. Unfortunately, I was not even able to get it to work with the 3R case, in which the model should have matched the inertial primitive exactly. For the most part, the mass estimates did not even move in the right direction using the composite adaptive laws. I didn't try with the direct adaptive laws, so it's possible that they would have worked better. 
\\[4 px]
I also tried various discretization schemes (rounding to the nearest X decimal places and / or always moving a fixed distance in the direction of the estimate) to try to improve the parameter updates and get rid of some numerical issues. It did solve some numerical issues, but often made parameter convergence worse. 

\section{Projection schemes}
All of the above techniques make a bunch of assumptions about the numerical properties of various matrices including:
\begin{itemize}
  \item Mass has to be positive
  \item The inertia matrix has to be positive definite
  \item $(\dot{H}-2C)$ must be skew symmetric
\end{itemize}
These are automatically satisfied for the parameters of any real-world system, but the running estimate must also be forced to conform to those assumptions. In adaptive control you typically say that the parameter estimate must be ``projected'' on to an appropriate parameter space.
All of the papers comment that you need parameter projection for the control / update laws to even vaguely hold, but never provide any algorithm to implement the projection.
\subsection{Projection types}
I learned two projection methods in my adaptive control class: (non-smooth) ``sigma'' projection and smooth projection. Both use two sets of parameters: one that is modified by the update law ($\bar{\theta}$, ``bar'') and one that is used as the actual estimate during control and is a function of the bar-parameters ($\hat{\theta}$, ``hat''). The math says they should both work with the composite algorithm with a constant learning rate at least (so you only really get asymptotic convergence while PE), though I wasn't able to prove their convergence under the time-varying learning rate used in the best form of the algorithm. This may have been why some of my earlier attempts to use the time-varying learning rate failed.
\\[4 px]
Non-smooth sigma projection basically just sets the hat-parameters to the bar-parameters, then hard-limits the values to be between two boundary values. It also adds a term to the update law that nudges the bar-parameters back toward the boundary if they are outside the valid region. The amount it pushes back towards the boundary depends on the sigma parameter. Also note that the derivative of the parameter estimate is discontinuous when you hit a boundary, which can sometimes cause control issues. 
\\[4 px]
Smooth projection maps bar to hat through a tanh function whose asymptotes are at the desired boundary values and makes no modification to the update law if the boundaries are constant. If the bounds are time varying (as in this case), the update law must be modified accordingly.  
\subsection{Projection space}
 According to several papers, keeping the mass positive and the inertia tensor (in the CoM frame) strictly positive definite for every link is sufficient to satisfy all the assumptions. To ensure that the tensor is PD, all of its principal minors must be positive (sufficient because the matrix is symmetric). That is:
\begin{enumerate}
  \item $I_{xx}>0$
  \item $I_{xx}I_{yy}-(I_{xy})^2>0$
  \item $I_{xx}(I_{yy}I_{zz}-(I_{yz})^2)-I_{xy}(I_{xy}I_{zz}-I_{xz}I_{yz})+I_{xz}(I_{xy}I_{yz}-I_{yy}I_{xy})>0$
\end{enumerate}
One way those constraints can be satisfied by keeping the diagonal terms of the tensor ($I_{xx}$,$I_{yy}$,$I_{zz}$) greater than some $\epsilon$ and bounding the other terms in the following manner:
\begin{enumerate}
  \item $\abs{I_{xy}} < \sqrt{I_{xx}I_{yy}-\epsilon^2}$
  \item $\abs{I_{xz}} < \sqrt{I_{xx}I_{zz}-\epsilon^2}$
  \item $\abs{I_{yz}} < \sqrt{I_{yy}I_{zz}-\epsilon^2}$
\end{enumerate}
The parameterization used in the algorithms ($\hat{\theta}=\{J_{xx},J_{yy},J_{zz},J_{xy},J_{xz},J_{yz}, mC_x,mC_y,mC_z,m\}$), however, uses elements of the inertia tensor in the frame of the joint (the J terms). This makes the equations defining the bounds of the space much more complicated. The full list of bounds I ended up using follows:
\begin{center}
    \begin{tabular}{| l | l | l |}
    \hline
    Parameter & Min bound & Max bound\\ \hline
    $\hat{\theta}_0\;(J_{xx})$ & $\epsilon_I+\hat{m}\hat{C}_y^2+\hat{m}\hat{C}_z^2$ & $\Omega_I$ \\ \hline
    $\hat{\theta}_1\;(J_{yy})$ & $\epsilon_I+\hat{m}\hat{C}_x^2+\hat{m}\hat{C}_z^2$ & $\Omega_I$ \\ \hline
    $\hat{\theta}_2\;(J_{zz})$ & $\epsilon_I+\hat{m}\hat{C}_x^2+\hat{m}\hat{C}_y^2$ & $\Omega_I$ \\ \hline
    $\hat{\theta}_3\;(J_{xy})$ & $-\sqrt{\hat{I}_{xx}\hat{I}_{yy}-\epsilon^2} - \hat{m}\hat{C}_x\hat{C}_y$ & $\sqrt{\hat{I}_{xx}\hat{I}_{yy}-\epsilon^2} - \hat{m}\hat{C}_x\hat{C}_y$ \\ \hline
    $\hat{\theta}_4\;(J_{xz})$ & $-\sqrt{\hat{I}_{xx}\hat{I}_{zz}-\epsilon^2} - \hat{m}\hat{C}_x\hat{C}_z$ & $\sqrt{\hat{I}_{xx}\hat{I}_{zz}-\epsilon^2} - \hat{m}\hat{C}_x\hat{C}_z$  \\ \hline
    $\hat{\theta}_5\;(J_{yz})$ & $-\sqrt{\hat{I}_{yy}\hat{I}_{zz}-\epsilon^2} - \hat{m}\hat{C}_y\hat{C}_z$ & $\sqrt{\hat{I}_{yy}\hat{I}_{zz}-\epsilon^2} - \hat{m}\hat{C}_y\hat{C}_z$  \\ \hline
    $\hat{\theta}_6\;(mC_x)$ & $\hat{m}C_{x,min}$ & $\hat{m}C_{x,max}$ \\ \hline
    $\hat{\theta}_7\;(mC_y)$ & $\hat{m}C_{y,min}$ & $\hat{m}C_{y,max}$ \\ \hline
    $\hat{\theta}_8\;(mC_z)$ & $\hat{m}C_{z,min}$ & $\hat{m}C_{z,max}$ \\ \hline
    $\hat{\theta}_9\;(m)$ & $\epsilon_m$ & $\Omega_m$ \\ \hline
    \end{tabular}\\[4 px]
    $\hat{I}_{xx}={\hat{J}_{xx}-\hat{m}\hat{C}_y^2-\hat{m}\hat{C}_z^2}$\\
    $\hat{I}_{yy}={\hat{J}_{yy}-\hat{m}\hat{C}_x^2-\hat{m}\hat{C}_z^2}$\\
    $\hat{I}_{zz}={\hat{J}_{zz}-\hat{m}\hat{C}_x^2-\hat{m}\hat{C}_y^2}$\\
    $\epsilon_*$ are arbitrary small positive values, while $\Omega_*$ are arbitrary large positive values\\
    $C_{*,min}$ and $C_{*,max}$ define a rectangular prism in the joint frame that encompasses all the mass of the link
\end{center}

\subsection{Parameter update transformation}
The optimal boundary of most of the parameters (using the coupled parameterization of the update law) is a nonlinear function of the others, so they vary weirdly over time. The parameter update can be converted to modify a decoupled parameterization (e.g. just $C_x$ instead of $mC_x$) that is easier to reason about. However, even if the projection is done in that space, the boundaries of the off diagonal terms of the inertia matrix still have to be time varying to keep the matrix PD (unless you just set them to zero all the time), so there's not all that much benefit. The equations to convert the parameter update follow:
\\[4 px]
\begin{math}
\hat{\theta}=\{J_{xx},J_{yy},J_{zz},J_{xy},J_{xz},J_{yz}, mC_x,mC_y,mC_z,m\}\\
\dot{\hat{\psi}}_9=\dot{\hat{m}}=\dot{\hat{\theta}}_9 \\
\dot{\hat{\psi}}_8=\dot{\hat{C}}_z=\frac{1}{\hat{\theta}_9^2}(\dot{\hat{\theta}}_8\hat{\theta}_9-\hat{\theta}_8\dot{\hat{\theta}}_9) \\
\dot{\hat{\psi}}_7=\dot{\hat{C}}_y=\frac{1}{\hat{\theta}_9^2}(\dot{\hat{\theta}}_7\hat{\theta}_9-\hat{\theta}_7\dot{\hat{\theta}}_9) \\
\dot{\hat{\psi}}_6=\dot{\hat{C}}_x=\frac{1}{\hat{\theta}_9^2}(\dot{\hat{\theta}}_6\hat{\theta}_9-\hat{\theta}_6\dot{\hat{\theta}}_9) \\
\dot{\hat{\psi}}_5=\dot{\hat{I}}_{yz}=\frac{1}{\hat{\theta}_9^2}(\hat{\theta}_7\hat{\theta}_9\dot{\hat{\theta}}_8-\hat{\theta}_7\hat{\theta}_8\dot{\hat{\theta}}_9+\hat{\theta}_8\hat{\theta}_9\dot{\hat{\theta}}_7+\hat{\theta}_9^2\dot{\hat{\theta}}_5) \\
\dot{\hat{\psi}}_4=\dot{\hat{I}}_{xz}=\frac{1}{\hat{\theta}_9^2}(\hat{\theta}_6\hat{\theta}_9\dot{\hat{\theta}}_8-\hat{\theta}_6\hat{\theta}_8\dot{\hat{\theta}}_9+\hat{\theta}_8\hat{\theta}_9\dot{\hat{\theta}}_6+\hat{\theta}_9^2\dot{\hat{\theta}}_4) \\
\dot{\hat{\psi}}_3=\dot{\hat{I}}_{xy}=\frac{1}{\hat{\theta}_9^2}(\hat{\theta}_6\hat{\theta}_9\dot{\hat{\theta}}_7-\hat{\theta}_6\hat{\theta}_7\dot{\hat{\theta}}_9+\hat{\theta}_7\hat{\theta}_9\dot{\hat{\theta}}_6+\hat{\theta}_9^2\dot{\hat{\theta}}_3) \\
\dot{\hat{\psi}}_2=\dot{\hat{I}}_{zz}=\frac{1}{\hat{\theta}_9^2}(\hat{\theta}_6^2\dot{\hat{\theta}}_9-2\hat{\theta}_6\hat{\theta}_9\dot{\hat{\theta}}_6+\hat{\theta}_7^2\dot{\hat{\theta}}_9-2\hat{\theta}_7\hat{\theta}_9\dot{\hat{\theta}}_7+\hat{\theta}_9^2\dot{\hat{\theta}}_2) \\
\dot{\hat{\psi}}_1=\dot{\hat{I}}_{yy}=\frac{1}{\hat{\theta}_9^2}(\hat{\theta}_6^2\dot{\hat{\theta}}_9-2\hat{\theta}_6\hat{\theta}_9\dot{\hat{\theta}}_6+\hat{\theta}_8^2\dot{\hat{\theta}}_9-2\hat{\theta}_8\hat{\theta}_9\dot{\hat{\theta}}_8+\hat{\theta}_9^2\dot{\hat{\theta}}_1) \\
\dot{\hat{\psi}}_0=\dot{\hat{I}}_{xx}=\frac{1}{\hat{\theta}_9^2}(\hat{\theta}_7^2\dot{\hat{\theta}}_9-2\hat{\theta}_7\hat{\theta}_9\dot{\hat{\theta}}_7+\hat{\theta}_8^2\dot{\hat{\theta}}_9-2\hat{\theta}_8\hat{\theta}_9\dot{\hat{\theta}}_8+\hat{\theta}_9^2\dot{\hat{\theta}}_0) \\
\end{math}

\subsection{Parameter update redirection}
Using either projection scheme, information from updates that push against the parameter boundaries is essentially lost. Since the parameters are coupled, the ``energy'' that would be wasted on one parameter can be redirected to another. For example, say the update law dictates an increase in $mC_x$ and no change in $m$ parameter normally just increases $C_x$, since mass is its own parameter. If $C_x$ is already at its limit and the combined parameter still wants to increase, the only way to make it bigger is to increase the mass instead. In the case of smoothly bounded projections, some reasonable effective maximum can be chosen instead (e.g. the tanh function is essentially flat at $\pm$5). The equations to do this, however, have the possibility of divisions by zero and / or excessively large values in common situations, so I was unable to effectively use this either.\\[4 px]
\begin{math}
\dot{\hat{\psi}}^9_8=\frac{\hat{\theta}_9}{\hat{\theta}_8}(\dot{\hat{\theta}}_8-\dot{\hat{\psi}}^O_8{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_7=\frac{\hat{\theta}_9}{\hat{\theta}_7}(\dot{\hat{\theta}}_7-\dot{\hat{\psi}}^O_7{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_6=\frac{\hat{\theta}_9}{\hat{\theta}_6}(\dot{\hat{\theta}}_6-\dot{\hat{\psi}}^O_6{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_5=\frac{\hat{\theta}_9}{\hat{\theta}_7\hat{\theta}_8}(\hat{\theta}_7\dot{\hat{\theta}}_8+\hat{\theta}_8\dot{\hat{\theta}}_7+\hat{\theta}_9\dot{\hat{\theta}}_5-\dot{\hat{\psi}}^O_5{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_4=\frac{\hat{\theta}_9}{\hat{\theta}_6\hat{\theta}_8}(\hat{\theta}_6\dot{\hat{\theta}}_8+\hat{\theta}_8\dot{\hat{\theta}}_6+\hat{\theta}_9\dot{\hat{\theta}}_4-\dot{\hat{\psi}}^O_4{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_3=\frac{\hat{\theta}_9}{\hat{\theta}_6\hat{\theta}_7}(\hat{\theta}_6\dot{\hat{\theta}}_7+\hat{\theta}_7\dot{\hat{\theta}}_6+\hat{\theta}_9\dot{\hat{\theta}}_3-\dot{\hat{\psi}}^O_3{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_2=\frac{\hat{\theta}_9}{\hat{\theta}_6+\hat{\theta}_7}(2\hat{\theta}_6\dot{\hat{\theta}}_6+2\hat{\theta}_7\dot{\hat{\theta}}_7-\hat{\theta}_9\dot{\hat{\theta}}_2+\dot{\hat{\psi}}^O_2{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_1=\frac{\hat{\theta}_9}{\hat{\theta}_6+\hat{\theta}_8}(2\hat{\theta}_6\dot{\hat{\theta}}_6+2\hat{\theta}_8\dot{\hat{\theta}}_8-\hat{\theta}_9\dot{\hat{\theta}}_1+\dot{\hat{\psi}}^O_1{\hat{\theta}}_9) \\
\dot{\hat{\psi}}^9_0=\frac{\hat{\theta}_9}{\hat{\theta}_7+\hat{\theta}_8}(2\hat{\theta}_7\dot{\hat{\theta}}_7+2\hat{\theta}_8\dot{\hat{\theta}}_8-\hat{\theta}_9\dot{\hat{\theta}}_0+\dot{\hat{\psi}}^O_0{\hat{\theta}}_9) \\
\end{math}
Where $\dot{\hat{\psi}}^9_*$ is the contribution to the mass from the overflow of the *'th decoupled parameter's update ($\dot{\hat{\psi}}^O_*$).


\section{General conclusions}
Debugging complex matrix / pure math programs is significantly more difficult than I thought it would be. Even for the 3R case with the simplest adaptive controller, there are 33 output signals (3*10 parameters and 3 torques) and hundreds of internal variables. If any of the internal variables are computed incorrectly, the output signals won’t converge as they are supposed to. This is why I kept blaming my implementation rather than the algorithm for my lack of success. I’m honestly still not sure it’s 100\% correct. 
\\[4 px]
In my experience (though it may be because of implementation errors), adaptive control behaves extremely well in some situations, and extremely poorly in others. I suspect that many of these papers found initial conditions that make the controller behave nicely and only showed those.
\\[4 px]
If a paper bothers to prove their work mathematically, they generally prove it with the closed-form dynamics, and assume that the recursive formulation is the same. I’m not sure there is a way to prove the adaptive control laws directly for recursive algorithms. A lot of papers also ``prove'' their controller's validity by just showing pretty matlab plots. 
\\[4 px]
Most people who write papers don’t bother to document all the variables used in their algorithms. The variables can sometimes be found if you traverse the paper graph back in time far enough, but it makes it extremely difficult to understand anything unless (I suspect) you have been reading every paper published since the 70s. Moreover, no two authors use the same notation (and some are even inconsistent with themselves), further decreasing their usefulness. 
\\[4 px]
Basically no one does recursive adaptive manipulator control. The typical adaptive control paper solves it for the closed-form dynamics, generates some matlab plots, and calls it quits (i.e. totally useless in the real world). You sometimes get some sort of two-link-robot real world test, but I don’t think anyone has actually applied adaptive control to a complex robot like Atlas. 
\pagebreak
\section{Notation}
\subsection{Robots}
3R: Triple revolute robot (simulated). Consists of a heavy base link followed by three powered revolute joints in the same plane.\\
Sim-Atlas: Atlas simulated in gazebo\\
Real-Atlas: Atlas in real life\\
\subsection{Matrix formats for a serial manipulator with n links}
\begin{math}
Links\;are\;numbered\;from\;zero\;at\;the\;base\;and\;n\;at\;the\;tip.\\
{^iX_{i-1}} \in \mathbb{R}^{6x6}\;is\;a\;spatial\;transformation\;matrix\;relating\;links\;i\;and\;i-1\\
d_i \in \mathbb{R}^{6}\;represents\;the\;joint\;axis\\
v_i,w_i \in \mathbb{R}^{6}\;represent\;spatial\;velocities\;in\;the\;link\;frame\\
a_i,u_i,\dot{w}_i \in \mathbb{R}^{6}\;represent\;spatial\;accelerations\;in\;the\;link\;frame\\
\theta_i \in \{J_{xx},J_{yy},J_{zz},J_{xy},J_{xz},J_{yz},mC_x,mC_y,mC_z,m\}\\
\theta \in \mathbb{R}^{10n}\\
R^l \in \mathbb{R}^{6x6},\;maps\;a_i^l\;to\;the\;spatial\;inertia\;matrix\\
Y \in \mathbb{R}^{nx10n}=
\begin{bmatrix}
Y_{11}&Y_{12}&\cdots&Y_{1n}\\
 0&Y_{22}&\cdots&Y_{2n}\\
 \vdots&\vdots&\ddots&\vdots\\
 0&0&\cdots&Y_{nn}
\end{bmatrix}\\
Y_{ij} \in \mathbb{R}^{1x10}\\
Y^T \in \mathbb{R}^{nx10n}=
\begin{bmatrix}
Y^T_{11}&0&\cdots&0\\
 Y^T_{12}&Y^T_{22}&\cdots&0\\
 \vdots&\vdots&\ddots&\vdots\\
 Y^T_{1n}&Y^T_{2n}&\cdots&Y^T_{nn}
\end{bmatrix}\\
Y_f\;is\;a\;low\;pass\;filtered\;Y\\
\end{math}
\subsection{Notation changes for a branching manipulator with n links}
\begin{math}
\nu(i)\;is\;the\;set\;of\;all\;links\;supported\;by\;link\;i\\
The\;Y\;and\;Y_f\;matrices\;are\;the\;same\;size\;as\;before,\;but\;with\;Y_{ij}=0\;\forall{j}\not\in\{\nu(i), i\}\;(no\;longer\;triangular)\\
{^iX_{ip}} \in \mathbb{R}^{6x6}\;is\;a\;spatial\;transformation\;matrix\;relating\;links\;i\;and\;the\;parent\;of\;i\;(ip)\\
{^{ic}X_i} \in \mathbb{R}^{6x6}\;is\;a\;spatial\;transformation\;matrix\;relating\;a\;child\;link\;of\;i\;(ic)\;and\;link\;i\\
\sum\limits_{ic}{}\;is\;the\;sum\;over\;all\;children\;of\;link\;i\\
\end{math}
\section{Direct adaptive control}
\subsection{Closed-form algorithm}
\begin{math}
H(q)\ddot{q}+C(q,\dot{q})\dot{q}+G(q)=\tau \\
\dot{q_r} = \dot{q_d} - \Lambda\tilde{q} \\
s = \dot{q}-\dot{q_r} = \dot{\tilde{q}}+\Lambda\tilde{q}\\
Y(q,\dot{q},\dot{q_r},\ddot{q_r})\hat{\theta}=\hat{H}(q)\ddot{q_r}+\hat{C}(q,\dot{q})\dot{q_r}+\hat{G}(q)\\
\tau=Y(q,\dot{q},\dot{q_r},\ddot{q_r})\hat{\theta}-{K}s \\
\dot{\hat{\theta}}=-PY(q,\dot{q},\dot{q_r},\ddot{q_r})^Ts\\
\end{math}
\subsection{Convergence proof}
\begin{math}
V=\frac{1}{2}[s^THs+\tilde{\theta}^TP^{-1}\tilde{\theta}] \ge 0\\
\dot{V}=s^TH\dot{s}+\frac{1}{2}s^T\dot{H}s+\tilde{\theta}^TP^{-1}\dot{\hat{\theta}}\;\;\;(\dot{\tilde{\theta}}=\dot{\hat{\theta}}\;if\;a\;is\;constant)\\
\dot{V}=s^T[\tau-C\dot{q}-G-H\ddot{q}_r]+\frac{1}{2}s^T(\dot{H}-2C)s + s^TCs + \tilde{\theta}^TP^{-1}\dot{\hat{\theta}}\\
\dot{V}=s^T[\tilde{H}\ddot{q}_r+\tilde{C}\dot{q}_r+\tilde{G}-Ks] +\tilde{\theta}^TP^{-1}\dot{\hat{\theta}}\;\;\;((\dot{H}-2C)\;is\;skew\;symmetric)\\
Y\tilde{\theta}=\tilde{H}\ddot{q}_r+\tilde{C}\dot{q}_r+\tilde{G}\\
\dot{V}=-s^TKs + s^TY\tilde{\theta} +\tilde{\theta}^TP^{-1}\dot{\hat{\theta}}\\
\dot{V}=-s^TKs + \tilde{\theta}^T[P^{-1}\dot{\hat{\theta}}+Y^Ts]\\
\dot{V}=-s^TKs \le 0\\
\end{math}
$V\ge0,\dot{V}\le0\implies V,s,\tilde{\theta}\in{\mathcal{L}_\infty},s\in{\mathcal{L}_2}$. $s \in {\mathcal{L}_2}{\cap}{\mathcal{L}_\infty}$ and $\dot{s} \in {\mathcal{L}_\infty}$, so $\lim\limits_{t \to \infty} {s}=0$ by the corollary to Barbalat's lemma.
\subsection{Recursive formulation for serial manipulators}
\begin{math}
forward:\\
\dot{w}_0=-g_0 \\
\dot{q}_{ri} = \dot{q}_{di} - \Lambda\tilde{q}_i \\
\ddot{q}_{ri} = \ddot{q}_{di} - \Lambda\dot{\tilde{q}}_i \\
v_i={^iX_{i-1}}v_{i-1}+d_i\dot{q}_i\\
w_i={^iX_{i-1}}w_{i-1}+d_i\dot{q}_{ri}\\
\dot{w}_i={^iX_{i-1}}\dot{w}_{i-1}+d_i\ddot{q}_{ri} + v_{i-1}\times{d_i}\dot{q}_{ri}\\
e_i=v_i-w_i \\\\
backward:\\
f_i^l=\frac{1}{2}({v_i}{\times}{R^l}{w_i} + {w_i}{\times}{R^l}{v_i} + {R^l}{\times}{w_i}{v_i}) + R^l\dot{w}_i\\
F_i={^{i+1}X_i}^TF_{i+1}+\sum\limits_{l=1}^{10}{f_i^l}{\hat{\theta}_i^l}\\
\tau_i={d_i^T}F_i-K_{i}s\\
\dot{\hat{\theta}}_i^l=-{P_i}{e_i^T}{f_i^l}\\
\end{math}
\subsection{Comparison between closed form and recursive formulations}
In the closed-form version, the compensation torque is calculated as $\tau=Y\hat{\theta}$. By examining the matrix formats above, it can be seen that this can also be written as $\tau_i=\sum\limits_{j=i}^{n}Y_{ij}\hat{\theta}_j$ for a serial manipulator. Note that the torque for joint i contains terms from all of the links that it supports. This agrees with the recursive formulation, assuming that $Y_{ij}={d_i^T}{^jX_i^T}[f_j^1,f_j^2,...,f_j^{10}]$.
\\\\
The closed-form update law is $\dot{\hat{\theta}}=-PY^Ts$. Writing it again as a summation with P as a diagonal gain matrix, $\dot{\hat{a_i}}=-P_i\sum\limits_{j=1}^{i}Y^T_{ji}s_j$. Note that the update for link i contains terms from all of the links that support it. In the recursive formulation, $\dot{\hat{\theta}}_i^l=-{P_i}{e_i^T}{f_i^l}$, which superficially appears to be different from the closed form version. A worked out three-link example follows to show that it does actually match ($l\in[1,10]$):\\
\begin{math}
\\Closed-form:\\
\dot{\hat{\theta}}_1^l=-P_1Y^T_{11}s_1 = -P_1({d_1^T}f_1^l)^Ts_1\\
\dot{\hat{\theta}}_2^l=-P_2[Y^T_{12}s_1 + Y^T_{22}s_2]=-P_2[({d_1^T}{^2X_1^T}f_2^l)^Ts_1+({d_2^T}f_2^l)^Ts_2]\\
\dot{\hat{\theta}}_3^l=-P_3[Y^T_{13}s_1 + Y^T_{23}s_2 + Y^T_{33}s_3]=-P_3[({d_1^T}{^3X_1^T}f_3^l)^Ts_1+({d_2^T}{^3X_2^T}f_3^l)^Ts_2+({d_3^T}f_3^l)^Ts_3]\\
\\Recursive:\\
\dot{\hat{\theta}}_1^l=-P_1e_1^Tf_1^l = -P_1(d_1s_1)^Tf_1^l\\
\dot{\hat{\theta}}_2^l=-P_2e_2^Tf_2^l = -P_2(d_2s_2 + {^2X_1}d_1s_1)^Tf_2^l\\
\dot{\hat{\theta}}_3^l=-P_3e_3^Tf_3^l = -P_3(d_3s_3 + {^3X_2}d_2s_2 + {^3X_1}d_1s_1)^Tf_3^l\\
\end{math}
\\Since the two approaches produce the same torque commands and parameter updates, the closed-form convergence proof applies to the recursive formulation as well. 
\section{Composite adaptive control}
\subsection{Closed-form algorithm}
\begin{math}
H(q)\ddot{q}+C(q,\dot{q})\dot{q}+G(q)=\tau \\
\dot{q_r} = \dot{q_d} - \Lambda\tilde{q} \\
s = \dot{q}-\dot{q_r} = \dot{\tilde{q}}+\Lambda\tilde{q}\\
Y(q,\dot{q},\dot{q_r},\ddot{q_r})\hat{\theta}=\hat{H}(q)\ddot{q_r}+\hat{C}(q,\dot{q})\dot{q_r}+\hat{G}(q)\\
\tau=Y\hat{\theta}-{K}s \\
\dot{\hat{\theta}}=-P[Y^Ts+\gamma{Y_f^T}\tilde{\tau}_f]\\
\dot{P}^{-1}=2(\gamma-\beta){Y_f^T}{Y_f} - 2\lambda({P^{-1}} - {K_0^{-1}})
\end{math}
\subsection{Convergence proof}
\begin{math}
V=\frac{1}{2}[s^THs+\tilde{\theta}^TP^{-1}\tilde{\theta}] \ge 0\\
\dot{V}=s^TH\dot{s}+\frac{1}{2}s^T\dot{H}s+\tilde{\theta}^TP^{-1}\dot{\hat{\theta}}+\frac{1}{2}\tilde{\theta}\dot{P}^{-1}\tilde{\theta}\;\;\;(\dot{\tilde{\theta}}=\dot{\hat{\theta}}\;if\;a\;is\;constant)\\
\dot{V}=s^T[\tau-C\dot{q}-G-H\ddot{q}_r]+\frac{1}{2}s^T(\dot{H}-2C)s + s^TCs + \tilde{\theta}^TP^{-1}\dot{\hat{\theta}}+\frac{1}{2}\tilde{\theta}\dot{P}^{-1}\tilde{\theta}\\
\dot{V}=s^T[\tilde{H}\ddot{q}_r+\tilde{C}\dot{q}_r+\tilde{G}-Ks] +\tilde{\theta}^TP^{-1}\dot{\hat{\theta}}+\frac{1}{2}\tilde{\theta}\dot{P}^{-1}\tilde{\theta}\;\;\;((\dot{H}-2C)\;is\;skew\;symmetric)\\
Y\tilde{\theta}=\tilde{H}\ddot{q}_r+\tilde{C}\dot{q}_r+\tilde{G}\\
\dot{V}=-s^TKs + s^TY\tilde{\theta} +\tilde{\theta}^TP^{-1}\dot{\hat{\theta}}+\frac{1}{2}\tilde{\theta}\dot{P}^{-1}\tilde{\theta}\\
\dot{V}=-s^TKs + \tilde{\theta}^T[P^{-1}\dot{\hat{\theta}}+Y^Ts]\\
\dot{V}=-s^TKs + \tilde{\theta}^T[-Y^Ts-\gamma{Y_f^T}\tilde{\tau}_f+Y^Ts + \gamma{Y_f^T}{Y_f}\tilde{\theta} - \beta{Y_f^T}{Y_f}\tilde{\theta} - \lambda({P^{-1}} - {K_0^{-1}})\tilde{\theta}]\\
\dot{V}=-s^TKs -\tilde{\theta}^T\lambda({P^{-1}} - {K_0^{-1}})\tilde{\theta} -\tilde{\theta}^T\beta{Y_f^T}{Y_f}\tilde{\theta}\le 0 \;when\;({P^{-1}} - {K_0^{-1}}) \ge 0,\;\lambda,\beta \ge 0 \\
\end{math}
$V\ge0,\dot{V}\le0\implies V,s,\tilde{\theta}\in{\mathcal{L}_\infty},s,\tilde{\theta}\in{\mathcal{L}_2}$. $s \in {\mathcal{L}_2}{\cap}{\mathcal{L}_\infty}$ and $\dot{s} \in {\mathcal{L}_\infty}$, so $\lim\limits_{t \to \infty} {s}=0$ by the corollary to Barbalat's lemma. Similarly $\lim\limits_{t \to \infty} {\tilde{\theta}}=0$.
\subsection{Convergence proof with non-smooth projection (with constant learning rate)}
There are three cases in non-smooth projection:
\begin{enumerate}
  \item $\hat{\theta}=\bar{\theta}\;\;\;\;a_{min}\le\bar{\theta}\le a_{max}$ This case is the same as if there were no projection, so the previous proof holds.
  \item $\hat{\theta}=a_{min}\;\;\bar{\theta}\le a_{min}$
  \item $\hat{\theta}=a_{max}\;\;\bar{\theta}\ge a_{max}$
\end{enumerate}
\begin{math}
V=\frac{1}{2}[s^THs+(\bar{\theta}-\theta)^TP^{-1}(\bar{\theta}-\theta)-(\bar{\theta}-\hat{\theta})^TP^{-1}(\bar{\theta}-\hat{\theta})] \ge 0\\
\dot{V}=s^TH\dot{s}+\frac{1}{2}s^T\dot{H}s+(\bar{\theta}-\theta)^TP^{-1}\dot{\bar{\theta}}-(\bar{\theta}-\hat{\theta})^TP^{-1}(\dot{\bar{\theta}}-\dot{\hat{\theta}})]\\
\dot{V}=-s^TKs + s^TY\tilde{\theta} + \tilde{\theta}^TP^{-1}\dot{\bar{\theta}}\;\;\;(\dot{\hat{\theta}}=0\;in\;cases\;2\;and\;3)\\
\dot{V}=-s^TKs + \tilde{\theta}^T[P^{-1}\dot{\hat{\theta}}+Y^Ts]\\
\dot{\hat{\theta}}=-P(Y^Ts+\gamma{Y_f^T}\tilde{\tau}_f-\sigma(\bar{\theta}-\hat{\theta}))\\
\dot{V}=-s^TKs -\tilde{\theta}^TW_f^TW_f\tilde{\theta} - (\hat{\theta}-\theta)(\bar{\theta}-\hat{\theta})\le 0 \;when\;(\hat{\theta}-\theta)(\bar{\theta}-\hat{\theta})\ge0\;and\;W_f\;is\;P.E.\\
\end{math}
In both the second and third cases, the sign of $(\hat{\theta}-\theta)(\bar{\theta}-\hat{\theta})$ is always positive, so the final term of $\dot{V}$ only makes it more negative. 
$V\ge0,\dot{V}\le0\implies V,s,\tilde{\theta}\in{\mathcal{L}_\infty},s,\tilde{\theta}\in{\mathcal{L}_2}$. $s \in {\mathcal{L}_2}{\cap}{\mathcal{L}_\infty}$ and $\dot{s} \in {\mathcal{L}_\infty}$, so $\lim\limits_{t \to \infty} {s}=0$ by the corollary to Barbalat's lemma. Similarly $\lim\limits_{t \to \infty} {\tilde{\theta}}=0$.
\subsection{Convergence proof with smooth projection (with constant learning rate)}
\begin{math}
V=\frac{1}{2}s^THs+\frac{1}{2}\sum\limits_{l=1}^{10n}{\frac{1}{\gamma_l}[ln(cosh(\hat{\phi}_l))-\hat{\phi}_{l}tanh(\phi_l)-ln(cosh(\phi_l))+\phi_{l}tanh(\phi_l)]}\\
\dot{V}=-s^TKs + s^TY\tilde{\theta}+\frac{1}{2}\sum\limits_{l=1}^{10n}{\frac{1}{\gamma_l}[tanh(\hat{\phi}_l)-tanh(\phi_l)]\dot{\hat{\phi}}_{l}}\\
\tilde{\theta}_l=\frac{1}{2}(\theta_{max}-\theta_{min})(tanh(\phi_l)-tanh(\hat{\phi}_l))=\frac{1}{2}\Delta_l(tanh(\phi_l)-tanh(\hat{\phi}_l))\\
\tilde{\theta} = [\tilde{\theta}_1, \tilde{\theta}_2,...,\tilde{\theta}_n]^T\\
\dot{V}=-s^TKs + s^TY\tilde{\theta}-\sum\limits_{l=1}^{10n}{\frac{1}{\gamma_l\Delta_l}\dot{\hat{\phi}}_{l}\tilde{\theta}_l}\\
\dot{V}=-s^TKs + s^TY\tilde{\theta}-\tilde{\theta}^T\Gamma_\Delta^{-1}\dot{\hat{\phi}},\;\;\;\Gamma_\Delta\in\mathcal{R}^{10n\;x\;10n},\;diagonal\;\Gamma_{\Delta{ll}}=\gamma_l\Delta_l\\
\dot{V}=-s^TKs + \tilde{\theta}^T[Y^Ts-\Gamma_\Delta^{-1}\dot{\hat{\phi}}] \\
\dot{\hat{\phi}} = \Gamma_\Delta[Y^Ts+Y^T_f\tilde{\tau}_f]\\
\dot{V}=-s^TKs - \tilde{\theta}^TW_f^TW_f\tilde{\theta}\le 0 \;when\;W_f\;is\;P.E.\\
\end{math}
(similar signal chasing as before)
\subsection{Recursive formulation (constant learning rate, unfiltered regressor)}
\begin{math}
forward\;pass\;1:\\
v_0=v_{base},\;
w_0=v_{base},\;
a_0=a_{base},\;
u_0=a_{base} \\
e_0=0 \\
\dot{q}_{ri} = \dot{q}_{di} - \Lambda\tilde{q}_i \\
\ddot{q}_{ri} = \ddot{q}_{di} - \Lambda\dot{\tilde{q}}_i \\
v_i={^iX_{ip}}v_{ip}+d_i\dot{q}_i\\
w_i={^iX_{ip}}w_{ip}+d_i\dot{q}_{ri}\\
a_i={^iX_{ip}}a_{ip}+d_i\ddot{q}_{i} + v_{i-1}\times{d_i}\dot{q}_{i}\\
u_i={^iX_{ip}}u_{ip}+d_i\ddot{q}_{ri} + v_{i-1}\times{d_i}\dot{q}_{ri}\\[4 px]
(process\;all\;children\;forward)\\[8 px]
backward\;pass\;1:\\
f_i^l=R^la_i-v\times^TR^lv_i\\
F_i=\sum\limits_{ic}{}{^{ic}X_i}^TF_{ic}+\sum\limits_{l=1}^{10}{f_i^l}{\hat{\theta}_i^l}\\
\beta_i^l=\frac{1}{2}({v_i}{\times}{R^l}{w_i} + {w_i}{\times}{R^l}{v_i} + {R^l}{\times}{w_i}{v_i}) + R^l\dot{w}_i\\
B_i=\sum\limits_{ic}{}{^{ic}X_i}^TB_{ic}+\sum\limits_{l=1}^{10}{\beta_i^l}{\hat{\theta}_i^l}\\
\hat{I}^{comp}_i=\hat{I}+\sum\limits_{ic}{}{^{ic}X_i}^T(\hat{I}^{comp}_{ic}){^{ic}X_i}\\
\tau_i={d_i^T}B_i-K_{i}({d_i^T}{\hat{I}^{comp}_i}{d_i})(\dot{q}_{i}-\dot{q}_{ri})\\
\tilde{\tau}_{fi}=\alpha({d_i^T}F_i-\tau_{mi})+(1-\alpha)\tilde{\tau}_{fi}\\
\dot{\hat{\phi}}_i^l=\gamma_l(\theta^l_{i,max}-\theta^l_{i,min}){(v_i-w_i)^T}{\beta_i^l}\\[4 px]
(fully\;complete\;pass\;1)\\[8 px]
forward\;pass\;2\;(starting\;again\;from\;the\;root):\\
e_i={^iX_{ip}}e_{ip}+d_i\tau_{fi}\\
\dot{\hat{\phi}}_i^l = \dot{\hat{\phi}}_i^l + \gamma_l(\theta^l_{i,max}-\theta^l_{i,min})e_i^Tf_i\\
\hat{\theta}_i^l=\frac{1}{2}(\theta^l_{i,max}-\theta^l_{i,min})(1-tanh(\hat{\phi}_i^l))+\theta^l_{i,min}\\[4px]
(re-evaluate\;inertia\;parameter\;bounds\;and\;update\;\phi's\;accordingly)
\end{math}
\pagebreak
\section{Reference material}
\begin{enumerate}
\item Niemeyer, Gunter and Slotine, Jean-Jacques E. ``Performance in Adaptive Manipulator Control.''
\item Slotine, Jean-Jacques E and Li, Weiping ``Composite adaptive control of robot manipulators''
\item Li, W and Slotine, J.-J.E ``Indirect adaptive robot control''
\item Middleton, R. H. and G. C. Goodwin ``Adaptive computed torque control for rigid link manipulator''
\item Akella, Maruthi R and Subbarao, Kamesh ``A novel parameter projection mechanism for smooth and stable adaptive control''
\item Wang, Hanlei ``On the recursive implementation of adaptive control for robot manipulators''
\item Wang, Hanlei ``Recursive composite adaptation for robot manipulators''
\item Wang, Hanlei and Xie, Yongchun ``On the Recursive Adaptive Control for Free-floating Space Manipulators''
\end{enumerate}
\end{document}