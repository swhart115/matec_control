#include <matec_utils/com_visualizer.h>

namespace matec_utils
{
  COMFinder::COMFinder(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 100.0);

//    m_com_pub = m_nh.advertise<geometry_msgs::PointStamped>("/center_of_mass", 1, true);
//    m_acom_pub = m_nh.advertise<sensor_msgs::PointCloud>("/all_centers_of_mass", 1, true);
//    m_acov_pub = m_nh.advertise<sensor_msgs::PointCloud>("/all_centers_of_volume", 1, true);
    m_acom_marker_pub = m_nh.advertise<visualization_msgs::MarkerArray>("/com_arrows", 1, true);
    m_acov_marker_pub = m_nh.advertise<visualization_msgs::MarkerArray>("/cov_arrows", 1, true);

    //get robot model
    while(!m_nh.hasParam("/robot_description") && ros::ok())
    {
      ROS_WARN_THROTTLE(2.0, "com_visualizer is waiting for robot description.");
    }
    m_model.initParam("/robot_description");
//    m_root_frame = m_model.getRoot()->name;
    configureMarkers();
  }

  COMFinder::~COMFinder()
  {
  }

  void COMFinder::configureMarkers()
  {
    std::vector<boost::shared_ptr<urdf::Link> > links;
    m_model.getLinks(links);
    for(unsigned int i = 0; i < links.size(); i++)
    {
      visualization_msgs::Marker marker;
      marker.header.stamp = ros::Time(0);
      marker.header.frame_id = links[i]->name;
      marker.id = i;
      marker.frame_locked = true;
      marker.type = visualization_msgs::Marker::ARROW;
      marker.color.a = 1.0;
      marker.points.resize(2);
      marker.scale.x = 0.01;
      marker.scale.y = 0.015;

      geometry_msgs::Point point;
      point.x = links[i]->inertial->origin.position.x;
      point.y = links[i]->inertial->origin.position.y;
      point.z = links[i]->inertial->origin.position.z;
      marker.ns = "com";
      marker.color.r = 0.0;
      marker.color.b = 0.5;
      marker.color.g = 0.0;
      marker.points[1] = point;
      m_com_array.markers.push_back(marker);

      //cov
      if(getCOV(links[i], point))
      {
        marker.ns = "cov";
        marker.color.r = 0.0;
        marker.color.b = 0.0;
        marker.color.g = 0.5;

        marker.points[1] = point;
        m_cov_array.markers.push_back(marker);
      }
    }
  }

//  void COMFinder::updateCOM()
//  {
//    sensor_msgs::PointCloud acom, acov;
//    std::vector<boost::shared_ptr<urdf::Link> > links;
//    m_model.getLinks(links);
//
//    for(unsigned int i = 0; i < links.size(); i++)
//    {
//      //com
//      geometry_msgs::PointStamped link_frame_point, root_frame_point;
//      link_frame_point.point.x = links[i]->inertial->origin.position.x;
//      link_frame_point.point.y = links[i]->inertial->origin.position.y;
//      link_frame_point.point.z = links[i]->inertial->origin.position.z;
//      link_frame_point.header.frame_id = links[i]->name;
//      link_frame_point.header.stamp = ros::Time(0);
//      m_tf_listener.transformPoint(m_root_frame, link_frame_point, root_frame_point);
//
//      geometry_msgs::Point32 point;
//      point.x = root_frame_point.point.x;
//      point.y = root_frame_point.point.y;
//      point.z = root_frame_point.point.z;
//      acom.points.push_back(point);
//
//      //cov
//      if(m_link_frame_covs.find(links[i]->name) != m_link_frame_covs.end())
//      {
//        link_frame_point = m_link_frame_covs[links[i]->name];
//        m_tf_listener.transformPoint(m_root_frame, link_frame_point, root_frame_point);
//
//        geometry_msgs::Point32 point;
//        point.x = root_frame_point.point.x;
//        point.y = root_frame_point.point.y;
//        point.z = root_frame_point.point.z;
//        acov.points.push_back(point);
//      }
//    }
//
//    acom.header.stamp = ros::Time::now();
//    acom.header.frame_id = m_root_frame;
//    m_acom_pub.publish(acom);
//
//    acov.header.stamp = ros::Time::now();
//    acov.header.frame_id = m_root_frame;
//    m_acov_pub.publish(acov);
//  }

  void COMFinder::spin()
  {
    ros::Rate loop_rate(m_loop_rate);

    while(ros::ok())
    {
//      updateCOM();
      //stupid
      for(unsigned int i = 0;i < m_com_array.markers.size(); i++)
      {
        m_com_array.markers[i].header.stamp = ros::Time::now();
      }
      for(unsigned int i = 0;i < m_cov_array.markers.size(); i++)
      {
        m_cov_array.markers[i].header.stamp = ros::Time::now();
      }

      m_acom_marker_pub.publish(m_com_array);
      m_acov_marker_pub.publish(m_cov_array);
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "com_visualizer");
  ros::NodeHandle nh("~");

  matec_utils::COMFinder node(nh);
  node.spin();

  return 0;
}
