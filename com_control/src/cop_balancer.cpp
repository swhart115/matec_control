#include "com_control/cop_balancer.h"

namespace com_control
{
  using namespace matec_utils;
  CoPBalancer::CoPBalancer(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 50.0);
    m_nh.param("stance_width", m_stance_width, 0.2);
    m_nh.param("stance_length", m_stance_length, 0.3);
    m_nh.param("stance_height", m_stance_height, 0.92);
    m_nh.param("foot_center_offset", m_foot_center_offset, 0.06);
    m_nh.param("turn_out", m_turn_out, 0.0);

    m_nh.param("one_foot", m_one_foot, false);

    m_have_poses = false;

    matec_utils::blockOnSMJointNames(m_joint_names);

    initModel();

    m_left_carrot_pub.advertise("/left_pose_carrot");
    m_right_carrot_pub.advertise("/right_pose_carrot");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&CoPBalancer::sensorCallback, this, _1));
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_wrench_array_sub.subscribe("/ankle_wrenches");
  }

  CoPBalancer::~CoPBalancer()
  {

  }

  void CoPBalancer::initModel()
  {
    urdf::Model urdf_model;
    urdf_model.initParam("/dynamics/robot_description");
    m_model = new ModelHelper(urdf_model, m_joint_names);
  }

  bool CoPBalancer::calculateCoP(matec_msgs::WrenchArray wrenches, geometry_msgs::Point cop)
  {
    cop.x = 0;
    cop.y = 0;
    cop.z = 0;

    assert(wrenches.wrenches.size() == 2);
    //TODO: assuming left, right for now
    int cop_count = 0;
    for(unsigned int i = 0; i < wrenches.wrenches.size(); i++)
    {
      KDLWrenchStamped wrench(wrenches.wrenches[i]);
      KDLWrenchStamped transformed_wrench;
      if(i == 0)
      {
        m_model->transformWrench("l_foot_contact", wrench, transformed_wrench);
      }
      if(i == 1)
      {
        m_model->transformWrench("r_foot_contact", wrench, transformed_wrench);
      }

      geometry_msgs::PointStamped point;
      if(!matec_utils::ModelHelper::calculateWrenchCoP(transformed_wrench, point))
      {
        continue;
      }

      geometry_msgs::PoseStamped pose;
      pose.pose.position = point.point;
      pose.pose.orientation.w = 1;
      pose.header = point.header;
      pose = m_model->transformPose(pose, "global");

      cop.x += pose.pose.position.x;
      cop.y += pose.pose.position.y;
      cop.z += pose.pose.position.z;

      cop_count++;
    }

    cop.x /= cop_count;
    cop.y /= cop_count;
    cop.z /= cop_count;

    return cop_count > 0;
  }

  void CoPBalancer::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    nav_msgs::Odometry root_link_odom;
    if(!m_root_link_odom_sub.getCurrentMessage(root_link_odom))
    {
      ROS_WARN_THROTTLE(0.5, "CoPBalancer: No root link odometry received!");
      return;
    }
    m_model->updateModel(msg.position, root_link_odom.pose.pose);
    geometry_msgs::PoseStamped pelvis_pose = m_model->getFramePose("pelvis", "global");
    geometry_msgs::PoseStamped ub_pose = m_model->getFramePose("upper_body", "global");
    geometry_msgs::PoseStamped left_pose = m_model->getFramePose("l_foot", "global");
    geometry_msgs::PoseStamped right_pose = m_model->getFramePose("r_foot", "global");

    matec_msgs::WrenchArray wrenches;
    if(!m_wrench_array_sub.getCurrentMessage(wrenches))
    {
      ROS_WARN_THROTTLE(0.5, "CoPBalancer: No ankle wrenches received!");
      return;
    }

    geometry_msgs::Point cop;
    if(!calculateCoP(wrenches, cop))
    {
      ROS_ERROR_THROTTLE(1.0, "No ground contact!");
      return;
    }


    bool feet_spread_forward = fabs(left_pose.pose.position.x - right_pose.pose.position.x) > 0.05;
    bool left_foot_leading = left_pose.pose.position.x > right_pose.pose.position.x;

    if(m_one_foot)
    {
      tf::Quaternion ub_quat, zero_quat, slerped_quat;
      tf::quaternionMsgToTF(ub_pose.pose.orientation, ub_quat);
      zero_quat = tf::createQuaternionFromYaw(tf::getYaw(ub_quat));
      slerped_quat = tf::slerp(zero_quat, ub_quat, 0.02);

      double roll, pitch, yaw;
      tf::Matrix3x3(slerped_quat).getRPY(roll, pitch, yaw);

      m_left_target_pose.pose.position = m_com;
      m_left_target_pose.pose.position.x += -m_foot_center_offset + m_stance_length;
      m_left_target_pose.pose.position.y += m_stance_width;
      m_left_target_pose.pose.position.z -= m_stance_height - 0.1;
      m_left_target_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);

      double max_x_off = 0.005;
      double max_y_off = 0.005;

      m_right_target_pose.pose.position = m_com;
      m_right_target_pose.pose.position.x += -m_foot_center_offset + max_x_off * pitch / (2.0 * M_PI);
      m_right_target_pose.pose.position.y -= max_y_off * roll / (2.0 * M_PI);
      m_right_target_pose.pose.position.z -= m_stance_height;
      m_right_target_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);
    }
    else
    {
      tf::Quaternion ub_quat, zero_quat, slerped_quat;
      tf::quaternionMsgToTF(ub_pose.pose.orientation, ub_quat);
      zero_quat = tf::createQuaternionFromYaw(tf::getYaw(ub_quat));
      slerped_quat = tf::slerp(zero_quat, ub_quat, 0.2);

      double roll, pitch, yaw;
      tf::Matrix3x3(slerped_quat).getRPY(roll, pitch, yaw);

//    double max_tilt = std::numeric_limits<double>::max();//0.005;
//    if(roll > max_tilt)
//    {
//      roll = max_tilt;
//    }
//    if(roll < -max_tilt)
//    {
//      roll = -max_tilt;
//    }
//    if(pitch > max_tilt)
//    {
//      pitch = max_tilt;
//    }
//    if(pitch < -max_tilt)
//    {
//      pitch = -max_tilt;
//    }

      bool leading_foot_gets_pitch = pitch > 0;

      double max_y_off = 0.01;
      double max_z_off = 0.01;

      double left_roll = (roll < 0)? roll : 0;
      double left_pitch = feet_spread_forward? (((left_foot_leading && leading_foot_gets_pitch) || (!left_foot_leading && !leading_foot_gets_pitch))? pitch : 0) : pitch;
      double left_yaw = yaw + m_turn_out;
      double left_y_offset = (roll < 0)? max_y_off * roll / (2.0 * M_PI) : 0;
      double left_z_offset = (roll < 0)? max_z_off * roll / (2.0 * M_PI) : 0;

      double right_roll = (roll > 0)? roll : 0;
      double right_pitch = feet_spread_forward? (((!left_foot_leading && leading_foot_gets_pitch) || (left_foot_leading && !leading_foot_gets_pitch))? pitch : 0) : pitch;
      double right_yaw = yaw - m_turn_out;
      double right_y_offset = (roll > 0)? max_y_off * roll / (2.0 * M_PI) : 0;
      double right_z_offset = (roll > 0)? max_z_off * roll / (2.0 * M_PI) : 0;

      m_left_target_pose.pose.position = m_com;
      m_left_target_pose.pose.position.x += -m_foot_center_offset + m_stance_length / 2.0;
      m_left_target_pose.pose.position.y += m_stance_width / 2.0 + left_y_offset;
      m_left_target_pose.pose.position.z -= m_stance_height + left_z_offset;
      m_left_target_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(left_roll, left_pitch, left_yaw);
//    tf::quaternionTFToMsg(slerped_quat, m_left_target_pose.pose.orientation);

      m_right_target_pose.pose.position = m_com;
      m_right_target_pose.pose.position.x += -m_foot_center_offset - m_stance_length / 2.0;
      m_right_target_pose.pose.position.y -= m_stance_width / 2.0 + right_y_offset;
      m_right_target_pose.pose.position.z -= m_stance_height + right_z_offset;
      m_right_target_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(right_roll, right_pitch, right_yaw);
//    tf::quaternionTFToMsg(slerped_quat, m_right_target_pose.pose.orientation);

    }

    m_have_poses = true;
  }

  void CoPBalancer::spin()
  {
    ROS_INFO("CoPBalancer started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      if(m_have_poses)
      {
        m_left_target_pose.header.frame_id = "/global";
        m_left_target_pose.header.stamp = ros::Time::now();
        m_right_target_pose.header = m_left_target_pose.header;
        m_left_carrot_pub.publish(m_left_target_pose);
        m_right_carrot_pub.publish(m_right_target_pose);
      }

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "simple_balancer");
  ros::NodeHandle nh("~");

  com_control::CoPBalancer node(nh);
  node.spin();

  return 0;
}
