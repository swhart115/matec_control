#include "system_id/coefficient_optimizer.h"
#include <cmath>
#include <set>
#include "system_id/common_math.h"

#include "system_id/training_data.h"
#include "system_id/expression_genome.h"

using namespace system_id;

int main(int argc, char* argv[])
{
  if(argc != 2)
  {
    std::cerr << "Args: filename" << std::endl;
    return 0;
  }

  std::string filename(argv[1]);
  system_id::TrainingData data(filename, 0, 1100);

  std::vector<std::string> input_symbols = data.getInputLabels();
  EGParams eg_params(input_symbols, 0, 500);

  std::string expression_string = "if(vx>0 and vy>0) {C0*vx + C1*vy} else if(vx>0 and vy<0) {C0*vx + C3*vy} else if(vx<0 and vy>0) {C2*vx + C1*vy} else {C2*vx + C3*vy}";

  system_id::CoefficientOptimizer opt(&data);
  std::vector<std::string> coefficient_symbols;
  coefficient_symbols.push_back("C0");
  coefficient_symbols.push_back("C1");
  coefficient_symbols.push_back("C2");
  coefficient_symbols.push_back("C3");

  ArrayGenome::GeneType min, max, initial_guess;
  for(unsigned int i = 0; i < input_symbols.size(); i++)
  {
    min.first.push_back(0);
    max.first.push_back(0);
  }
//Final equation: if(vx>0 and vy>0) {C0*tr_x*vx + C1*tr_y*vy} else if(vx>0 and vy<0) {C2*tr_x*vx + C3*tr_y*vy} else if(vx<0 and vy>0) {C4*tr_x*vx + C5*tr_y*vy} else {C6*tr_x*vx + C7*tr_y*vy},
  //(0, 0, 0, 0, 0, 0 | -23.585, 39.58, -19.4169, 31.7469, -25.217, 39.0416, -25.734, 34.1216) => 0.0009087
  min.second.push_back(-2); //C0
  max.second.push_back(4);
  min.second.push_back(-2); //C1
  max.second.push_back(4);
  min.second.push_back(-2); //C2
  max.second.push_back(4);
  min.second.push_back(-2); //C3
  max.second.push_back(4);

  GAParams params(7, 3, 0.00001, 5000, 10, 10 * data.size());
  system_id::ArrayGenome best = opt.optimize(expression_string, coefficient_symbols, input_symbols, params, &min, &max);
  std::cerr << best << std::endl;

  std::cerr << "\n\n Final equation: " << expression_string << ", " << best << std::endl;

  while(1)
  {

  }
}
