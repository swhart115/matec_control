#ifndef RNEA_H
#define RNEA_H

#include <ros/ros.h>
#include <boost/thread.hpp>
#include <time.h>
#include <ctime>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>

#include <std_srvs/Empty.h>

#include <urdf/model.h>
#include <urdf_parser/urdf_parser.h>

#include <rbdl/Model.h>
#include <rbdl/Dynamics.h>
#include <rbdl/rbdl_utils.h>
#include <matec_utils/rbdl_urdfreader.h>
#include <matec_utils/model_helper.h>

#include <sensor_msgs/JointState.h>

#include <tf/tf.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_utils/kdl_urdf_reader.h"

#include "matec_msgs/RobotInertiaProperties.h"

#include <matec_utils/biquad.h>

#include <inverse_dynamics/linear_algebra_math.h>
#include <matec_utils/integrators.h>

#include "matec_utils/urdf_manipulations.h"
#include "matec_msgs/DataVector.h"
#include "matec_msgs/WrenchArray.h"

namespace inverse_dynamics
{
  class DynamicsTreeNode
  {
  public:
    enum JointType
    {
      REVOLUTE,
      FLOATING,
      PRISMATIC,
      FIXED
    };

    std::string joint_name;
    std::string link_name;
    int joint_idx;

    JointType type;
    KDL::Segment segment;

    Matrix6 inertia;
    Matrix6 composite_inertia;

    Vector6 S; //axis
    Matrix6 iXip;

    Vector6 v;
    Vector6 a;
    Vector6 f;

    double q;
    double q_dot;
    double qd;
    double qd_dot;
    double qd_dot_dot;
    Vector6 f_ext;

    double torque;

    long parent_index;
    std::vector<long> child_indices;
  };

  class RNEA
  {
  public:
    RNEA();
    void load(urdf::Model model, std::vector<std::string> joint_names);

    void computeTorques(matec_msgs::FullJointStates& current, matec_msgs::FullJointStates& desired, matec_msgs::WrenchArray& external_wrenches);
    std::vector<double> getTorques();

    double gravity_constant;
    double sample_time;
    double last_sample_time;
    Matrix6 root_transform;

  private:
    urdf::Model m_urdf_model;
    KDL::Tree m_kdl_tree;

    std::vector<std::string> link_names;
    std::vector<DynamicsTreeNode> nodes;

    matec_msgs::FullJointStates* m_current;
    matec_msgs::FullJointStates* m_desired;
    matec_msgs::WrenchArray* m_external_wrenches;
    Vector6 base_position;
    Vector6 base_velocity;
    Vector6 base_gravity_acceleration;

    void computeTorquesRecursive(long i);

    void loadSegmentIndex(KDL::Segment segment, int idx);
    void loadRecursive(int idx, const KDL::SegmentMap::const_iterator segment);
  };
}
#endif //RNEA_H
