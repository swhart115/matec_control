#include "system_id/coefficient_optimizer.h"
#include <cmath>
#include "system_id/common_math.h"

#include "system_id/training_data.h"
#include "system_id/mapping_finder.h"

using namespace system_id;

int main(int argc, char* argv[])
{
  if(argc != 4)
  { //TODO: change data file format to include input/output/names
    std::cerr << "[filename] [num_inputs] [num_outputs]" << std::endl;
    return 0;
  }

  std::string filename(argv[1]);
  unsigned long num_inputs = atoi(argv[2]);
  unsigned long num_outputs = atoi(argv[3]);
  system_id::TrainingData data(filename, num_inputs, num_outputs);

  long double output_min, output_max;
  data.getOutputBounds(output_min, output_max);
  long double max_coeff = 2.0 * std::max(fabs(output_min), fabs(output_max));
  long max_shift = data.size() * 0.05;

  system_id::MappingFinder mf;

  std::vector<std::string> input_symbols;
  input_symbols.push_back("x");

  EGParams eg_params(input_symbols, max_shift, max_coeff);

  EGAParams params(5, 1, 0.001, 20, 5);

  mf.optimize(&data, eg_params, params);
}
