#ifndef MINIMUM_JERK_INTERPOLATOR_H
#define MINIMUM_JERK_INTERPOLATOR_H

#include "interpolator.h"

namespace interpolators
{
  class MinimumJerkInterpolator: public Interpolator
  {
  public:
    MinimumJerkInterpolator() :
        Interpolator()
    {

    }

    //TODO: add function to calculate T based on max vel, accel, and/or jerk

    void loadTrajectory(Trajectory traj, bool populate_non_position_fields = true)
    {
      m_traj.clear();
      m_traj.push_back(traj[0]);
      m_traj.push_back(traj[traj.size() - 1]);

      if(traj.size() != 2)
      {
        ROS_WARN("IGNORING INTERMEDIATE VIAPOINTS AND SKIPPING TO THE END!");
      }

      m_have_valid_trajectory = true;
      m_last_slice_index = 1; //m_traj[0] is the start state
    }

    bool getInterpolatedState(ros::Time time, State& state)
    {
      if(!m_have_valid_trajectory)
      {
        return false;
      }

      //get current trajectory slice (assumes that finish_times are ordered)
      ViaPoint from_via;
      ViaPoint to_via;
      for(unsigned int i = m_last_slice_index; i < m_traj.size(); i++)
      {
        if(time < m_traj[i].finish_time) //found the slice!
        {
          from_via = m_traj[i - 1];
          to_via = m_traj[i];
        }
        else if(i == (m_traj.size() - 1)) //ran out of slices!
        {
          ROS_INFO("Trajectory complete!");
          m_have_valid_trajectory = false;
          state = m_traj[m_traj.size() - 1].target_state;
          return false;
        }
      }

      //figure out where we should be
      double xi = from_via.target_state.position;
      double xf = to_via.target_state.position;

      double t = (time - from_via.finish_time).toSec();
      double t2 = t * t;
      double t3 = t2 * t;
      double t4 = t3 * t;
      double t5 = t4 * t;
      double T = (to_via.finish_time - from_via.finish_time).toSec();
      double T3 = T * T * T;
      double T4 = T3 * T;
      double T5 = T4 * T;
//      double jerk = (xf - xi) * (360.0 / T5 * t2 - 360 / T4 * t + 60 / T3);
      state.acceleration = (xf - xi) * (120 * t3 / T5 - 180 * t2 / T4 + 60 * t / T3);
      state.velocity = (xf - xi) * 60 * (t4 / 2.0 / T5 - t3 / T4 + t2 / 2.0 / T3);
      state.position = (xf - xi) * (6 * t5 / T5 - 15 * t4 / T4 + 10 * t3 / T3) + xi;

      return true;
    }
  };
}
#endif //MINIMUM_JERK_INTERPOLATOR_H
