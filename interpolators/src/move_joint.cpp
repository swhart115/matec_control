#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>

#include "matec_utils/common_initialization_components.h"

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "move_joint");
  ros::NodeHandle nh;

  std::vector<std::string> joint_names;
  matec_utils::blockOnSMJointNames(joint_names);

  unsigned int joint_idx = -1;
  double target_position = 0;
  double duration = 5.0;
  if(argc == 1)
  {
    for(unsigned int i = 0; i < joint_names.size(); i++)
    {
      cout << i << ": " << joint_names[i] << endl;
    }
    cout << "Enter joint index: ";
    cin >> joint_idx; //TODO: add checks
    cout << "Enter the desired position: ";
    cin >> target_position;
    cout << "Enter the desired duration: ";
    cin >> duration;
  }
  else if(argc == 4)
  {
    joint_idx = atoi(argv[1]);
    target_position = atof(argv[2]);
    duration = atof(argv[3]);
  }
  else
  {
    cout << "Invalid parameters. This program must either be run with no parameters or two parameters: joint index and target position\n";
    return 0;
  }

  if(joint_idx >= joint_names.size())
  {
    cout << "Invalid joint index!";
    return 0;
  }

  actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> client(nh, "/follow_joint_trajectory", true);

  while(ros::Time::now().toSec() == 0)
  {

  }

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");

  control_msgs::FollowJointTrajectoryGoal goal;
  goal.trajectory.header.stamp = ros::Time::now();
  goal.trajectory.joint_names.push_back(joint_names[joint_idx]);

  trajectory_msgs::JointTrajectoryPoint point;
  point.positions.push_back(target_position);
  point.time_from_start = ros::Duration(duration);
  goal.trajectory.points.push_back(point);

  // Start the trajectory
  client.sendGoal(goal);

  client.waitForResult(ros::Duration(1.0));

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

