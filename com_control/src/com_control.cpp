#include "com_control/com_control.h"

namespace com_control
{
  using namespace matec_utils;
  COMControl::COMControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 50.0);

    matec_utils::blockOnSMJointNames(m_joint_names);

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints))
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("JointPositionControl couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }

    initModel();
    getGains();

    m_error_pub = m_nh.advertise<geometry_msgs::Point>("/com_position_error", 1, true);
    m_desired_com_wrench_pub = m_nh.advertise<geometry_msgs::WrenchStamped>("/desired_com_wrench", 1, true);
    m_calculated_com_wrench_pub = m_nh.advertise<geometry_msgs::WrenchStamped>("/calculated_com_wrench", 1, true);

    m_command_pub.advertise("task_command");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&COMControl::sensorCallback, this, _1));
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_com_pose_command_sub.subscribe("/com_pose_command");
    m_contact_sub.subscribe("/contacts");

    m_com_x_kp = 100;
    m_com_y_kp = 100;
    m_com_z_kp = 100;
  }

  COMControl::~COMControl()
  {

  }

  void COMControl::initModel()
  {
    urdf::Model urdf_model;
    urdf_model.initParam("/dynamics/robot_description");
    m_model = new ModelHelper(urdf_model, m_joint_names);
  }

  void COMControl::getGains()
  {
    boost::mutex::scoped_lock lock(m_mutex);
    m_nh.getParamCached("gains/com_x/p", m_com_x_kp);
    m_nh.getParamCached("gains/com_x/v", m_com_x_kv);

    m_nh.getParamCached("gains/com_y/p", m_com_y_kp);
    m_nh.getParamCached("gains/com_y/v", m_com_y_kv);

    m_nh.getParamCached("gains/com_z/p", m_com_z_kp);
    m_nh.getParamCached("gains/com_z/v", m_com_z_kv);
  }

  KDLWrenchStamped COMControl::reactionWrenchesToCOMWrench(std::vector<KDLWrenchStamped> local_reaction_wrenches)
  {
    KDLWrenchStamped com_wrench;
    com_wrench.frame_id = "com";
    for(unsigned int i = 0; i < local_reaction_wrenches.size(); i++)
    {
      KDLWrenchStamped wrench_in_com_frame;
      m_model->transformWrench("com", local_reaction_wrenches[i], wrench_in_com_frame);
      com_wrench.wrench += wrench_in_com_frame.wrench;
    }
    return com_wrench;
  }

  bool COMControl::COMWrenchToSingleReactionWrench(KDLWrenchStamped desired_com_wrench, Contact contact, KDLWrenchStamped& reaction_wrench, SlipState& slip_state)
  {
    //if only one contact, there is a closed-form solution
    KDLWrenchStamped wrench_at_contact;
    m_model->transformWrench(contact.contact_frame_name, desired_com_wrench, wrench_at_contact);

    m_model->transformWrench(contact.frame_name, desired_com_wrench, reaction_wrench);
    return !contact.willSlip(wrench_at_contact, slip_state);
  }

  bool COMControl::COMWrenchToReactionWrenches(KDLWrenchStamped desired_com_wrench, std::vector<KDLWrenchStamped>& reaction_wrenches)
  {
    reaction_wrenches.resize(m_contacts.size());
    if(m_contacts.size() == 1) //skip the search since we know what we want
    {
      SlipState slip_state;
      return COMWrenchToSingleReactionWrench(desired_com_wrench, m_contacts[0], reaction_wrenches[0], slip_state);
    }

    //calculate static scaling (favor the foot that most of our weight is over)
    std::vector<double> alphas, betas, gammas, deltas, x_dirs, y_dirs, z_dirs; //TODO: add scalings for the rest of the wrenches, proportional to the surface area of the contact along that axis
    for(unsigned int i = 0; i < m_contacts.size(); i++)
    {
      geometry_msgs::Point global_contact_point = m_model->getGlobalFramePosition(m_contacts[i].contact_frame_name);

      double x_dist = m_com.x - global_contact_point.x;
      double y_dist = m_com.y - global_contact_point.y;
      double z_dist = m_com.z - global_contact_point.z;
      alphas.push_back(fabs(x_dist));
      betas.push_back(fabs(y_dist));
      gammas.push_back(fabs(z_dist));
      deltas.push_back(sqrt(x_dist * x_dist + y_dist * y_dist + z_dist * z_dist));
      x_dirs.push_back(x_dist < 0? -1 : 1);
      y_dirs.push_back(y_dist < 0? -1 : 1);
      z_dirs.push_back(z_dist < 0? -1 : 1);
    }

    //normalize
    double alpha_total = std::accumulate(alphas.begin(), alphas.end(), 0.0);
    double beta_total = std::accumulate(betas.begin(), betas.end(), 0.0);
    double gamma_total = std::accumulate(gammas.begin(), gammas.end(), 0.0);
    double deltas_total = std::accumulate(deltas.begin(), deltas.end(), 0.0);
    for(unsigned int i = 0; i < reaction_wrenches.size(); i++)
    {
      alphas[i] /= alpha_total;
      betas[i] /= beta_total;
      gammas[i] /= gamma_total;
      deltas[i] /= deltas_total;
    }

    std::vector<KDLWrenchStamped> static_reaction_wrenches(reaction_wrenches.size());
    for(unsigned int i = 0; i < static_reaction_wrenches.size(); i++)
    {
      SlipState slip_state;
      COMWrenchToSingleReactionWrench(desired_com_wrench, m_contacts[i], static_reaction_wrenches[i], slip_state);

      static_reaction_wrenches[i].wrench.force.x(0);
      static_reaction_wrenches[i].wrench.force.y(0);
//      static_reaction_wrenches[i].wrench.force.z(desired_com_wrench.wrench.force.z() * deltas[i]); //TODO: make z always point up
      static_reaction_wrenches[i].wrench.force.z(static_reaction_wrenches[i].wrench.force.z() * deltas[i]);
      static_reaction_wrenches[i].wrench.torque.x(0);
      static_reaction_wrenches[i].wrench.torque.y(0);
      static_reaction_wrenches[i].wrench.torque.z(0);
      static_reaction_wrenches[i].frame_id = m_contacts[i].frame_name;
    }

    KDLWrenchStamped remaining_com_wrench = reactionWrenchesToCOMWrench(static_reaction_wrenches);
    remaining_com_wrench.wrench = desired_com_wrench.wrench - remaining_com_wrench.wrench;

    //calculate dynamic scaling (favor the foot that is opposite the direction we want to push)
    //TODO: assign weights based on contacting surface area
    std::vector<KDLWrenchStamped> dynamic_reaction_wrenches(reaction_wrenches.size());
    for(unsigned int i = 0; i < dynamic_reaction_wrenches.size(); i++)
    {
      KDLWrenchStamped dynamic_com_wrench_subset;
      dynamic_com_wrench_subset.wrench.force.x(remaining_com_wrench.wrench.force.x() * alphas[i]);
      dynamic_com_wrench_subset.wrench.force.y(remaining_com_wrench.wrench.force.y() * betas[i]);
      dynamic_com_wrench_subset.wrench.force.z(remaining_com_wrench.wrench.force.z() * gammas[i]);
      dynamic_com_wrench_subset.wrench.torque.x(remaining_com_wrench.wrench.torque.x() * deltas[i]);
      dynamic_com_wrench_subset.wrench.torque.y(remaining_com_wrench.wrench.torque.y() * deltas[i]);
      dynamic_com_wrench_subset.wrench.torque.z(remaining_com_wrench.wrench.torque.z() * deltas[i]);
      dynamic_com_wrench_subset.frame_id = desired_com_wrench.frame_id;

      SlipState slip_state;
      COMWrenchToSingleReactionWrench(dynamic_com_wrench_subset, m_contacts[i], dynamic_reaction_wrenches[i], slip_state);
    }

    bool success = true;
    for(unsigned int i = 0; i < reaction_wrenches.size(); i++)
    {
      reaction_wrenches[i] = static_reaction_wrenches[i];
      reaction_wrenches[i].wrench += dynamic_reaction_wrenches[i].wrench;

      SlipState slip_state;
      if(!m_contacts[i].willSlip(reaction_wrenches[i], slip_state))
      {
        success = false; //TODO: iterate until we have success
      }
    }

    return success;
  }

  void COMControl::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    nav_msgs::Odometry root_link_odom;
    if(!m_root_link_odom_sub.getCurrentMessage(root_link_odom))
    {
      ROS_WARN_THROTTLE(0.5, "CC: No root link odometry received!");
      return;
    }

    matec_msgs::ContactSpecifications contact_specifications;
    if(!m_contact_sub.getCurrentMessage(contact_specifications))
    {
      ROS_WARN_THROTTLE(0.5, "CC: No contact specifications received!");
      return;
    }
    if(contact_specifications.contacts.size() == 0)
    {
      ROS_WARN_THROTTLE(0.5, "CC: No contact specifications received!");
      return;
    }
    m_contacts.resize(contact_specifications.contacts.size());
    for(unsigned int i = 0; i < contact_specifications.contacts.size(); i++)
    {
      m_contacts[i] = Contact(contact_specifications.contacts[i]);
      m_model->addContactFrame(contact_specifications.contacts[i].link_name, contact_specifications.contacts[i].contact_point);
    }

    //update the model
    m_model->updateModel(msg.position, root_link_odom.pose.pose);

    double total_mass;
    m_model->findCOM(m_com, total_mass);

    //figure out how we want to wrench the CoM
    geometry_msgs::Pose com_setpoint; //TODO: change to using COMCommand type to facilitate COM torques
    if(!m_com_pose_command_sub.getCurrentMessage(com_setpoint))
    {
      ROS_WARN_THROTTLE(0.5, "CC: No setpoint received!");
      return;
    }

    KDLWrenchStamped desired_com_wrench;
    desired_com_wrench.frame_id = "com";
    {
      double com_err_x = com_setpoint.position.x - m_com.x;
      double com_err_y = com_setpoint.position.y - m_com.y;
      double com_err_z = com_setpoint.position.z - m_com.z;

      boost::mutex::scoped_lock lock(m_mutex);
      desired_com_wrench.wrench.force.x(com_err_x * m_com_x_kp); // - com_velocity(0) * m_com_x_kv);
      desired_com_wrench.wrench.force.y(com_err_y * m_com_y_kp); // - com_velocity(1) * m_com_y_kv);
      desired_com_wrench.wrench.force.z(9.81 * total_mass + com_err_z * m_com_z_kp); // - com_velocity(2) * m_com_z_kv);
      desired_com_wrench.wrench.torque = KDL::Vector(0, 0, 0); //TODO: apply wrenches based on the COMCommand
    }
    m_desired_com_wrench_pub.publish(desired_com_wrench.toMsg());

    //figure out what reaction forces we need to do that
    std::vector<KDLWrenchStamped> wrenches;
    if(COMWrenchToReactionWrenches(desired_com_wrench, wrenches))
    {
      //publish debug wrenches and build the command
      matec_msgs::TaskCommand id_cmd;
      for(unsigned int i = 0; i < wrenches.size(); i++)
      {
        assert(wrenches[i].frame_id.length() > 0);
        //if we don't already have a publisher, make one
        if(m_wrench_pubs.find(wrenches[i].frame_id) == m_wrench_pubs.end())
        {
          m_wrench_pubs[wrenches[i].frame_id] = m_nh.advertise<geometry_msgs::WrenchStamped>("/" + m_contacts[i].frame_name + "/reaction_wrench", 1, true);
        }

        //publish it
        geometry_msgs::WrenchStamped wrench = wrenches[i].toMsg();
        id_cmd.reaction_forces.push_back(wrench);
        m_wrench_pubs[wrenches[i].frame_id].publish(wrench);
      }

      KDLWrenchStamped calculated_com_wrench = reactionWrenchesToCOMWrench(wrenches);
      m_calculated_com_wrench_pub.publish(calculated_com_wrench.toMsg());

      //publish dynamics command
//    m_smi.publishSerializedROS<matec_msgs::TaskCommand>("task_command", id_cmd);
    }
    else
    {
      ROS_WARN("Couldn't find any valid set of reaction wrenches! Disaster may be imminent!");
      return;
    }
  }

  void COMControl::spin()
  {
    ROS_INFO("COMControl started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();

//      //publish transforms
//      tf::StampedTransform root_link_transform, com_transform;
//      if(m_model->getRootLinkTransform(root_link_transform))
//      {
//        m_tf_broadcaster.sendTransform(root_link_transform);
//      }
//      if(m_model->getCoMTransform(com_transform))
//      {
//        m_tf_broadcaster.sendTransform(com_transform);
//      }

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "com_control");
  ros::NodeHandle nh("~");

  com_control::COMControl node(nh);
  node.spin();

  return 0;
}
