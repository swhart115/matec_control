#ifndef GA_H
#define GA_H

#include <stdio.h>
#include "system_id/genomes.h"
#include <deque>
#include <map>
#include <boost/bind.hpp>
#include <vector>

#include "system_id/debug_levels.h"
#include "system_id/common_math.h"

namespace system_id
{
  class GAParams
  {
  public:
    GAParams(unsigned long regional_population_size_ = 5, unsigned long num_regions_ = 1, long double fit_enough_ = 0.0, unsigned long max_iterations_ = 100, unsigned long stagnation_threshold_ = 10, long double give_up_fitness_ = std::numeric_limits<long double>::max(), long give_up_stagnation_ = std::numeric_limits<
        long>::max(), bool print_ = true)
    {
      regional_population_size = regional_population_size_;
      num_regions = num_regions_;
      fit_enough = fit_enough_;
      max_iterations = max_iterations_;
      stagnation_threshold = stagnation_threshold_;
      print = print_;
      give_up_fitness = give_up_fitness_;
      give_up_stagnation = give_up_stagnation_;
    }

    unsigned long regional_population_size;
    unsigned long num_regions;
    long double fit_enough;
    unsigned long max_iterations;
    unsigned long stagnation_threshold;
    bool print;
    long double give_up_fitness;
    long give_up_stagnation;
  };

  //T must be a subclass of Genome
  class GeneticAlgorithmNumeric
  {
    typedef boost::function<void(ArrayGenome&)> FitnessFunction;

  public:
    GeneticAlgorithmNumeric(FitnessFunction fitness, ArrayGenome::GeneType min, ArrayGenome::GeneType max, bool seed_random = true) :
        m_min(min), m_max(max), m_best(m_min, m_max), m_last_best(m_min, m_max)
    {
      if(seed_random)
      {
        srand(time(NULL));
      }
      m_fitness = fitness;
    }

    ArrayGenome optimize(GAParams params, ArrayGenome::GeneType* initial_guess = NULL)
    {
      if(initial_guess)
      {
        m_best = ArrayGenome(m_min, m_max, initial_guess);
        m_fitness(m_best);

        if(params.print)
        {
          PRINTLN_INFO_STREAM("Using initial guess " << m_best);
        }
      }
      m_evolution_force.first.resize(m_best.gene.first.size(), 0.0);
      m_evolution_force.second.resize(m_best.gene.second.size(), 0.0);

      populate(params.num_regions, params.regional_population_size);

      evaluate(); //make sure we're not done already

      unsigned long iterations = 0;
      unsigned long iterations_since_last_improvement = 0;
      unsigned long last_improvement_iteration = 0;
      m_last_best.fitness = std::numeric_limits<long double>::max();
      while(iterations < params.max_iterations && m_best.fitness > params.fit_enough)
      {
        procreate();
        mutate(false, ((double) iterations) / ((double) params.max_iterations));
        forceEvolution();
        evaluate();

        if(m_best.fitness > params.give_up_fitness)
        {
          PRINTLN_WARN("Fitness %g was so terrible that I gave up!", (double) m_best.fitness);
          return m_best;
        }

        if((iterations - last_improvement_iteration) > params.give_up_stagnation)
        {
          PRINTLN_WARN("Stagnated for more than %d iterations, so I gave up!", (int) params.give_up_stagnation);
          return m_best;
        }

        if(m_best.fitness < m_last_best.fitness)
        {
          //calculate evolution force
          for(unsigned int i = 0; i < m_best.gene.first.size(); i++)
          {
            m_evolution_force.first[i] = m_best.gene.first[i] - m_last_best.gene.first[i];
          }
          for(unsigned int i = 0; i < m_best.gene.second.size(); i++)
          {
            m_evolution_force.second[i] = m_best.gene.second[i] - m_last_best.gene.second[i];
          }

          m_last_best = m_best;
          iterations_since_last_improvement = 0;
          last_improvement_iteration = iterations;
          if(params.print)
          {
            PRINTLN_INFO_STREAM("Performed iteration " << iterations << ". Found new best genome: " << m_best);
          }
        }
        else
        {
          if(params.print)
          {
            PRINTLN_INFO_STREAM_THROTTLE(1.0, "Performed iteration " << iterations << ". Current best genome: " << m_best);
          }
          iterations_since_last_improvement++;
          if(iterations_since_last_improvement > params.stagnation_threshold)
          {
            mutate(true, ((double) iterations) / ((double) params.max_iterations));
            evaluate();
            iterations_since_last_improvement = 0;
          }
        }

        iterations++;
      }

      if(params.print)
      {
        PRINTLN_INFO_STREAM("Finished with " << iterations << ". The final genome was:" << m_best);
      }

      return m_best;
    }

  private:
    void populate(int num_regions, int regional_population_size)
    {
      m_parent_population.clear();
      for(int i = 0; i < num_regions; i++)
      {
        std::deque<ArrayGenome> region;
        for(int j = 0; j < regional_population_size; j++)
        {
          region.push_back(ArrayGenome(m_min, m_max));
        }
        m_parent_population.push_back(region);
      }
    }

    void evaluate()
    {
      for(unsigned int i = 0; i < m_parent_population.size(); i++)
      {
        for(unsigned int j = 0; j < m_parent_population[i].size(); j++)
        {
          m_fitness(m_parent_population[i][j]);

          if(m_parent_population[i][j] < m_best)
          {
            m_best = m_parent_population[i][j];
          }
        }
        std::sort(m_parent_population[i].begin(), m_parent_population[i].end());
      }
    }

    void forceEvolution()
    {
      double force_evolution_probability = 0.1;
      double elite_replacement_probability = 0.05;
      for(unsigned int i = 0; i < m_parent_population.size(); i++)
      {
        long double region_scale = pow(2.0, -1.0 * (double) i); //higher regions mutate slower than lower regions
        for(unsigned int j = 0; j < m_parent_population[i].size(); j++)
        {
          if(system_id::uniformRandom() < elite_replacement_probability)
          {
            m_parent_population[i][j] = m_best;
            m_parent_population[i][j].forceEvolution(m_evolution_force, region_scale);
          }
          else if(system_id::uniformRandom() < force_evolution_probability)
          {
            m_parent_population[i][j].forceEvolution(m_evolution_force, region_scale);
          }
        }
      }
    }

    void mutate(bool hypermutate, long double iterations_completed)
    {
      long double magnitude_base = 1.0 - iterations_completed;
//      std::cerr << magnitude_base * (m_max.second[0] - m_min.second[0]) << std::endl;
      for(unsigned int i = 0; i < m_parent_population.size(); i++)
      {
        long double mutation_magnitude = magnitude_base * pow(2.0, -1 * (double) i); //higher regions mutate slower than lower regions
        for(unsigned int j = 0; j < m_parent_population[i].size(); j++)
        {
          if(hypermutate)
          {
            m_parent_population[i][j].hypermutate();
          }
          else
          {
            m_parent_population[i][j].mutate(mutation_magnitude, m_min.first.size(), m_min.second.size());
          }
        }
      }
    }

    int randomBucket(std::vector<long double> thresholds)
    {
      long double roll = system_id::uniformRandom();
      for(unsigned int i = 0; i < thresholds.size(); i++)
      {
        if(roll < thresholds[i])
        {
          return i;
        }
      }
      assert(0);
      return 0; //THIS SHOULD NEVER HAPPEN!
    }

    std::vector<long double> makeRankThresholds(int num_ranks)
    {
      std::vector<long double> rank_thresholds;

      int normalization = num_ranks * (num_ranks + 1) / 2;

      for(int i = 0; i < num_ranks; i++)
      {
        rank_thresholds.push_back(((long double) (num_ranks - i)) / ((long double) normalization));
        if(i > 0)
        {
          rank_thresholds[i] += rank_thresholds[i - 1];
        }
      }
      //todo: check that last rank threshold == 1

      return rank_thresholds;
    }

    void procreate()
    {
      unsigned int population_size = m_parent_population[0].size();
      std::vector<long double> thresholds = makeRankThresholds(population_size);

      std::deque<std::deque<ArrayGenome> > child_population; //[region][individual]
      for(unsigned int region = 0; region < m_parent_population.size(); region++)
      {
        std::deque<ArrayGenome> child_region;
        if(m_parent_population[region][0].fitness > m_best.fitness) //keep the best individual in every breeding session
        {
          m_parent_population[region].push_front(m_best);
          m_parent_population[region].pop_back();
        }
        for(unsigned int j = 0; j < population_size; j++)
        {
          int parent_idx_1 = randomBucket(thresholds);
          int parent_idx_2 = randomBucket(thresholds);
          if(m_parent_population[region][parent_idx_1] < m_parent_population[region][parent_idx_2])
          {
            child_region.push_back(m_parent_population[region][parent_idx_1].cross(m_parent_population[region][parent_idx_2]));
          }
          else
          {
            child_region.push_back(m_parent_population[region][parent_idx_2].cross(m_parent_population[region][parent_idx_1]));
          }
        }
        child_population.push_back(child_region);
      }

      assert(child_population.size() == m_parent_population.size());
      assert(child_population.size() > 0);
      assert(child_population[0].size() == m_parent_population[0].size());
      m_parent_population = child_population;
    }

    void migrate()
    {
      return;
      //MIGRATION IS CURRENTLY BROKEN!
//
//      if(m_parent_population.size() == 1)
//      {
//        return; //only one region, nothing to do
//      }
//
//      std::deque<ArrayGenome>  regional_best;
//      for(unsigned int i = 0; i < m_parent_population.size(); i++)
//      {
//        regional_best.push_back(m_parent_population[i][0]); //assume the population is already sorted by fitness
//      }
//
//      std::sort(regional_best.begin(), regional_best.end());
//
//      for(unsigned int i = 0; i < m_parent_population.size(); i++)
//      {
//        if(m_parent_population[i].size() <= 2)
//        {
//          return;
//        }
//        m_parent_population[i].pop_back(); //get rid of the worst two
//        m_parent_population[i].pop_back(); //get rid of the worst two
//        m_parent_population[i].push_back(regional_best[0]); //clone the overall champion to everyone
//        int second_idx = 1 + (system_id::uniformRandom() * (regional_best.size() - 2)); //[1...(regional_best.size()-1)]
//        m_parent_population[i].push_back(regional_best[second_idx]); //clone a different regional champion too
//
//        std::sort(m_parent_population[i].begin(), m_parent_population[i].end()); //re-sort with the new individuals
//      }
    }

  private:
    std::deque<std::deque<ArrayGenome> > m_parent_population; //[region][individual]
    FitnessFunction m_fitness;
    ArrayGenome::GeneType m_min;
    ArrayGenome::GeneType m_max;
    ArrayGenome::GeneType m_evolution_force;
    ArrayGenome m_best;
    ArrayGenome m_last_best;
  };
}

#endif //GA_H
