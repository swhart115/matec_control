#include "inverse_dynamics/inverse_dynamics.h"

namespace inverse_dynamics
{
  InverseDynamics::InverseDynamics(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 1.0);
    m_nh.param("adapt", m_adapt, false);

    double gravity;
    m_nh.param("gravity_constant", gravity, 9.81);
    if(m_adapt)
    {
      m_inertias_pub = m_nh.advertise<matec_msgs::RobotInertiaProperties>("/estimated_inertias", 1, true);
    }
    else
    {
      m_rnea.gravity_constant = gravity;
    }

    matec_utils::blockOnSMJointNames(m_joint_names);
    assert(m_joint_names.size() > 0);

    initModel();
    
    m_joint_command_pub.advertise("/joint_commands");
    m_joint_states_sub.subscribe("/joint_states_raw", boost::bind(&InverseDynamics::sensorCallback, this, _1));
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_prioritized_command_sub.subscribe("/prioritized_command");
    m_measured_external_forces_sub.subscribe("/measured_external_wrenches");
  }

  InverseDynamics::~InverseDynamics()
  {
  }

  void InverseDynamics::initModel()
  {
    while(!m_nh.hasParam("/dynamics/robot_description") && ros::ok())
    {
      ROS_WARN_THROTTLE(0.5, "InverseDynamics waiting for /dynamics/robot_description parameter!");
    }

    m_urdf_model.initParam("/dynamics/robot_description");

    if(m_adapt)
    {
      ROS_INFO("ID: USING ASPECT CONTROL");
      bool randomize_parameters;
      m_nh.param("/rcaid/randomize_parameters", randomize_parameters, false);
      m_aspect.load(m_urdf_model, m_joint_names, randomize_parameters);
      pollParameterServerAdaptive();
    }
    else
    {
      ROS_INFO("ID: USING RNEA CONTROL");
      m_rnea.load(m_urdf_model, m_joint_names);
    }
  }

  bool InverseDynamics::parameterChanged(std::string joint_name, std::string param_name, double& value)
  {
    std::string full_param_path = "/rcaid/" + joint_name + "/" + param_name;

    double old_value = value;
    m_nh.param(full_param_path, value, value);
    if(value != old_value)
    {
      ROS_INFO("Setting %s to %g!", full_param_path.c_str(), value);
      return true;
    }
    return false;
  }

  void InverseDynamics::pollParameterServerAdaptive()
  {
    if(aspect_params.size() == 0)
    {
      ASPECTLinkParameters defaults;
      defaults.prediction_lpf = 10.0;
      defaults.update_lpf = 1.0;
      defaults.sliding_surface_angle = 1.0;
      defaults.feedback_gain = 3.0;
      defaults.parameter_update_gain = 1.0;
      defaults.parameter_update_gain_prediction = 10.0;
      defaults.epsilon_inertia = 1.0e-6;
      defaults.omega_inertia = 10.0;
      defaults.epsilon_mass = 1.0e-3;
      defaults.omega_mass = 1.0e3;
      aspect_params.resize(m_joint_names.size(), defaults);

      //make sure everyone is initialized
      for(unsigned int i = 0; i < m_joint_names.size(); i++)
      {
        m_aspect.setAdaptiveParameters(m_joint_names[i], aspect_params[i]);
      }
    }

    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      bool update_required = false;
      update_required = parameterChanged(m_joint_names[i], "prediction_lpf", aspect_params[i].prediction_lpf) || update_required;
      update_required = parameterChanged(m_joint_names[i], "update_lpf", aspect_params[i].update_lpf) || update_required;
      update_required = parameterChanged(m_joint_names[i], "lambda", aspect_params[i].sliding_surface_angle) || update_required;
      update_required = parameterChanged(m_joint_names[i], "K", aspect_params[i].feedback_gain) || update_required;
      update_required = parameterChanged(m_joint_names[i], "gamma", aspect_params[i].parameter_update_gain) || update_required;
      update_required = parameterChanged(m_joint_names[i], "nu", aspect_params[i].parameter_update_gain_prediction) || update_required;
      update_required = parameterChanged(m_joint_names[i], "epsilon_inertia", aspect_params[i].epsilon_inertia) || update_required;
      update_required = parameterChanged(m_joint_names[i], "omega_inertia", aspect_params[i].omega_inertia) || update_required;
      update_required = parameterChanged(m_joint_names[i], "epsilon_mass", aspect_params[i].epsilon_mass) || update_required;
      update_required = parameterChanged(m_joint_names[i], "omega_mass", aspect_params[i].omega_mass) || update_required;
      if(update_required)
      {
        m_aspect.setAdaptiveParameters(m_joint_names[i], aspect_params[i]);
      }
    }
  }

  void augmentJointStates(nav_msgs::Odometry odom, matec_msgs::FullJointStates current_states, matec_msgs::FullJointStates joint_command, matec_msgs::FullJointStates& augmented_current_states, matec_msgs::FullJointStates& augmented_joint_commands)
  {
    double roll, pitch, yaw;
    tf::Quaternion quat;
    tf::quaternionMsgToTF(odom.pose.pose.orientation, quat);
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

    augmented_current_states.position.resize(6, 0.0);
    augmented_current_states.velocity.resize(6, 0.0);
    augmented_current_states.acceleration.resize(6, 0.0);
    augmented_current_states.torque.resize(6, 0.0);
    augmented_joint_commands.position.resize(6, 0.0);
    augmented_joint_commands.velocity.resize(6, 0.0);
    augmented_joint_commands.acceleration.resize(6, 0.0);
    augmented_joint_commands.torque.resize(6, 0.0);

    augmented_current_states.position[0] = odom.pose.pose.position.x;
    augmented_current_states.position[1] = odom.pose.pose.position.y;
    augmented_current_states.position[2] = odom.pose.pose.position.z;
    augmented_current_states.position[3] = roll;
    augmented_current_states.position[4] = pitch;
    augmented_current_states.position[5] = yaw;
    augmented_current_states.velocity[0] = odom.twist.twist.linear.x;
    augmented_current_states.velocity[1] = odom.twist.twist.linear.y;
    augmented_current_states.velocity[2] = odom.twist.twist.linear.z;
    augmented_current_states.velocity[3] = odom.twist.twist.angular.x;
    augmented_current_states.velocity[4] = odom.twist.twist.angular.y;
    augmented_current_states.velocity[5] = odom.twist.twist.angular.z;

    augmented_joint_commands.position.insert(augmented_joint_commands.position.end(), joint_command.position.begin(), joint_command.position.end());
    augmented_joint_commands.velocity.insert(augmented_joint_commands.velocity.end(), joint_command.velocity.begin(), joint_command.velocity.end());
    augmented_joint_commands.acceleration.insert(augmented_joint_commands.acceleration.end(), joint_command.acceleration.begin(), joint_command.acceleration.end());
    augmented_joint_commands.torque.insert(augmented_joint_commands.torque.end(), joint_command.torque.begin(), joint_command.torque.end());
    augmented_current_states.position.insert(augmented_current_states.position.end(), current_states.position.begin(), current_states.position.end());
    augmented_current_states.velocity.insert(augmented_current_states.velocity.end(), current_states.velocity.begin(), current_states.velocity.end());
    augmented_current_states.acceleration.insert(augmented_current_states.acceleration.end(), current_states.acceleration.begin(), current_states.acceleration.end());
    augmented_current_states.torque.insert(augmented_current_states.torque.end(), current_states.torque.begin(), current_states.torque.end());

    augmented_current_states.timestamp = current_states.timestamp;
    augmented_joint_commands.timestamp = joint_command.timestamp;
  }

  void InverseDynamics::convertRootLinkOdometry(nav_msgs::Odometry odom)
  {
    KDL::Frame frame;
    tf::poseMsgToKDL(odom.pose.pose, frame);
    if(m_adapt)
    {
      m_aspect.root_transform = kdlToEigen(frame);
    }
    else
    {
      m_rnea.root_transform = kdlToEigen(frame);
    }
  }

  void InverseDynamics::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    if(!m_adapt)
    {
      if(isnan(m_rnea.sample_time)) //skip first one to get the timestamps right
      {
        m_rnea.sample_time = msg.timestamp;
        return;
      }
    }

    matec_msgs::TaskCommand prioritized_command;
    nav_msgs::Odometry odom;
    matec_msgs::WrenchArray wrenches;
    if(!m_root_link_odom_sub.getCurrentMessage(odom))
    {
      ROS_WARN_THROTTLE(0.5, "InverseDynamics: No odometry received!");
      return;
    }
    if(!m_prioritized_command_sub.getCurrentMessage(prioritized_command))
    {
      ROS_WARN_THROTTLE(0.5, "InverseDynamics: No command received!");
      return;
    }
    if(!m_measured_external_forces_sub.getCurrentMessage(wrenches))
    {
//      ROS_WARN_THROTTLE(0.5, "InverseDynamics: No wrenches received!");
    }

    convertRootLinkOdometry(odom);

    matec_msgs::FullJointStates augmented_current_states, augmented_joint_commands;
    augmentJointStates(odom, msg, prioritized_command.joint_commands, augmented_current_states, augmented_joint_commands);

    if(m_adapt) //TODO interface this better...
    {
      m_aspect.computeTorques(augmented_current_states, augmented_joint_commands);

      matec_msgs::RobotInertiaProperties inertias = m_aspect.getInertias();
      m_inertias_pub.publish(inertias);

      matec_msgs::FullJointStates joint_commands;
      joint_commands = prioritized_command.joint_commands;
      joint_commands.torque = m_aspect.getTorques();
      joint_commands.timestamp = msg.timestamp;
      m_joint_command_pub.publish(joint_commands);
    }
    else
    {
      m_rnea.last_sample_time = m_rnea.sample_time;
      m_rnea.sample_time = msg.timestamp;

      if(m_rnea.last_sample_time == m_rnea.sample_time)
      {
        ROS_WARN("Received duplicate sample time at %g!", m_rnea.sample_time);
        return;
      }

      m_rnea.computeTorques(augmented_current_states, augmented_joint_commands, wrenches);

      matec_msgs::FullJointStates joint_commands;
      joint_commands = prioritized_command.joint_commands;
      joint_commands.torque = m_rnea.getTorques();
      joint_commands.timestamp = msg.timestamp;
      m_joint_command_pub.publish(joint_commands);
    }
  }

  void InverseDynamics::stop()
  {
    matec_msgs::FullJointStates joint_commands;
    joint_commands.velocity.resize(m_joint_names.size(), 0.0);
    joint_commands.acceleration.resize(m_joint_names.size(), 0.0);
    joint_commands.torque.resize(m_joint_names.size(), 0.0);
    joint_commands.timestamp = ros::Time::now().toSec();
    m_joint_command_pub.publish(joint_commands);
  }

  void InverseDynamics::spin()
  {
    ROS_INFO("InverseDynamics started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      if(m_adapt)
      {
        pollParameterServerAdaptive();
      }
      ros::spinOnce();
      loop_rate.sleep();
    }
    stop();
    stop();
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "inverse_dynamics");
  ros::NodeHandle nh("~");

  inverse_dynamics::InverseDynamics node(nh);
  node.spin();

  return 0;
}
