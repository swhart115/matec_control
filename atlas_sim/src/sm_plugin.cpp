#include <ros/ros.h>
#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <sdf/parser.hh>
#include <stdio.h>
#include <unistd.h>
#include <sensor_msgs/JointState.h>
#include <nav_msgs/Odometry.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/FullLinkState.h>
#include <matec_msgs/JointNames.h>
#include <matec_utils/biquad.h>
#include <matec_msgs/UnPin.h>
#include <matec_msgs/WrenchArray.h>
#include <geometry_msgs/WrenchStamped.h>

namespace gazebo
{
  class SMPlugin: public ModelPlugin
  {
  public:
    SMPlugin()
    {
      int argc = 0;
      ros::init(argc, NULL, "sm_plugin");
    }

    ~SMPlugin()
    {
    }

    template<typename T>
    T sdf_get_value(sdf::ElementPtr sdf, const std::string &key, const T &def)
    {
      if(sdf->HasElement(key))
      {
        std::string value = sdf->GetElement(key)->Get<std::string>();
        return boost::lexical_cast<T>(value);
      }
      else
        return def;
    }

    void Load(physics::ModelPtr _parent, sdf::ElementPtr sdfElement)
    {
      std::cerr << "Loading SM plugin" << std::endl;
      this->model = _parent;
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&SMPlugin::OnUpdate, this, _1));

      gazebo::physics::Joint_V joints = this->model->GetJoints();
      gazebo::physics::Link_V links = this->model->GetLinks();
      matec_msgs::JointNames joint_names;
      for(unsigned int i = 0; i < joints.size(); i++)
      {
        joint_names.data.push_back(joints[i]->GetName());
        m_original_low_stops.push_back(joints[i]->GetLowStop(0).Radian());
        m_original_high_stops.push_back(joints[i]->GetHighStop(0).Radian());
        std::cerr << "Original stops for " << joints[i]->GetName() << " were " << joints[i]->GetLowStop(0).Radian() << " and " << joints[i]->GetHighStop(0).Radian() << std::endl;
      }

      position_mode = sdf_get_value<bool>(sdfElement, "position_mode", false);
      velocity_mode = sdf_get_value<bool>(sdfElement, "velocity_mode", false);

      if(sdf_get_value<bool>(sdfElement, "pin", false))
      {
        std::string pin_link_name = sdf_get_value<std::string>(sdfElement, "pin_link_name", links[0]->GetName());
        std::cerr << "Pinning the robot at " << pin_link_name << std::endl;
        pinRobot(pin_link_name);
      }
      else
      {
        std::cerr << "Not pinning the robot." << std::endl;
      }

      joint_commands.position.resize(joints.size(), 0.0);
      joint_commands.velocity.resize(joints.size(), 0.0);
      joint_commands.torque.resize(joints.size(), 0.0);

      m_root_link_odom_pub.advertise("/root_link_odom");
      m_ankle_wrench_pub.advertise("/ankle_wrenches");
      m_joint_states_pub.advertise("/joint_states");
      m_joint_states_raw_pub.advertise("/joint_states_raw");
      m_joint_names_pub.advertise("/joint_names");
      m_joint_names_pub.publish(joint_names);

      m_joint_commands_sub.subscribe("/joint_commands", boost::bind(&SMPlugin::commandCallback, this, _1));

      this->m_nh = new ros::NodeHandle("~");
//      names_pub = this->m_nh->advertise<matec_msgs::JointNames>("/joint_names", 1, true);
//      names_pub.publish(joint_names);

      js_pub = this->m_nh->advertise<sensor_msgs::JointState>("/joint_states", 1, true);
      left_wrench_pub = this->m_nh->advertise<geometry_msgs::WrenchStamped>("/left_wrench", 1, true);
      right_wrench_pub = this->m_nh->advertise<geometry_msgs::WrenchStamped>("/right_wrench", 1, true);
      unpin_srv = m_nh->advertiseService("/unpin", &SMPlugin::unpinCallback, this);

      joint_states.name = joint_names.data;

      next_loop_time = boost::get_system_time();
      std::cerr << "Done loading SM plugin" << std::endl;
    }

    bool unpinCallback(matec_msgs::UnPin::Request& req, matec_msgs::UnPin::Response& res)
    {
      if(req.lowering_distance != 0.0)
      {
        dropRobot(-req.lowering_distance);
      }
      else
      {
        unpinRobot();
      }
      return true;
    }

    void commandCallback(matec_msgs::FullJointStates& msg)
    {
      joint_commands = msg;
    }

    void dropRobot(double lowering_distance)
    {
      pin_joint_handle->SetHighStop(0, -lowering_distance);
      pin_joint_handle->SetLowStop(0, -lowering_distance);
    }

    void pinRobot(std::string pin_link_name)
    {
      gazebo::physics::LinkPtr pin_link = this->model->GetLink(pin_link_name);
      if(!pin_link)
      {
        ROS_FATAL("Requested pin link %s does not exist!", pin_link_name.c_str());
        assert(0);
      }

      physics::LinkPtr world_link = physics::LinkPtr();
      pin_joint_handle = this->model->GetWorld()->GetPhysicsEngine()->CreateJoint("prismatic", this->model);
      pin_joint_handle->Attach(pin_link, world_link);
      pin_joint_handle->Load(pin_link, world_link, math::Pose(pin_link->GetWorldPose().pos, math::Quaternion()).GetInverse()); // load adds the joint to a vector of shared pointers kept in parent and child links, preventing joint from being destroyed.
      pin_joint_handle->SetAxis(0, math::Vector3(0, 0, 1));
      pin_joint_handle->SetHighStop(0, 0);
      pin_joint_handle->SetLowStop(0, 0);
      pin_joint_handle->SetName("pin");
      pin_joint_handle->Init();
    }

    void unpinRobot()
    {
      std::cerr << "Removing pin joint!";
      pin_joint_handle->Detach();
    }

    double clamp(double a, double b, double c)
    {
      return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
    }

    long last_iters;
    long iters;

    // Called by the world update start event
    void OnUpdate(const common::UpdateInfo & /*_info*/)
    {
      double seconds_per_step = this->model->GetWorld()->GetPhysicsEngine()->GetUpdatePeriod();
      double alternate_time = this->model->GetWorld()->GetSimTime().Double();

      last_iters = iters;
      iters = this->model->GetWorld()->GetIterations();

      gazebo::physics::Joint_V joints = this->model->GetJoints();
      ros::spinOnce();
      double last_timestamp = measurement.timestamp;
      measurement.timestamp = alternate_time; //ros::Time::now().toSec();

      if(last_timestamp == measurement.timestamp)
      {
        ROS_INFO("No update happened!? Time changed from %g to %g, iters %d to %d, %g, %g", last_timestamp, measurement.timestamp, (int) last_iters, (int) iters, seconds_per_step, alternate_time);
      }

      //joint states
      if(pos_lpfs.size() == 0)
      {
        double filter_frequency = 50.0;
        pos_lpfs.resize(joints.size(), Biquad(bq_type_lowpass, filter_frequency / 1000.0, 0.7071, 6));
        vel_lpfs.resize(joints.size(), Biquad(bq_type_lowpass, filter_frequency / 1000.0, 0.7071, 6));
        acc_lpfs.resize(joints.size(), Biquad(bq_type_lowpass, filter_frequency / 1000.0, 0.7071, 6));
        tor_lpfs.resize(joints.size(), Biquad(bq_type_lowpass, filter_frequency / 1000.0, 0.7071, 6));

        double wrench_filter_frequency = 5.0;
        l_wrench_lpfs.resize(3, Biquad(bq_type_lowpass, wrench_filter_frequency / 1000.0, 0.7071, 6));
        r_wrench_lpfs.resize(3, Biquad(bq_type_lowpass, wrench_filter_frequency / 1000.0, 0.7071, 6));
      }
      if(measurement_raw.position.size() == 0)
      {
        measurement_raw.position.resize(joints.size());
        measurement_raw.velocity.resize(joints.size());
        measurement_raw.acceleration.resize(joints.size());
        measurement_raw.torque.resize(joints.size());
        measurement.position.resize(joints.size());
        measurement.velocity.resize(joints.size());
        measurement.acceleration.resize(joints.size());
        measurement.torque.resize(joints.size());
      }

      for(unsigned int i = 0; i < joints.size(); i++)
      {
        double last_velocity = measurement_raw.velocity[i];
        gazebo::physics::JointWrench gazebo_wrench = joints[i]->GetForceTorque(0);
        math::Vector3 axis = joints[i]->GetLocalAxis(0);
        double raw_position = joints[i]->GetAngle(0).Radian();
        double raw_velocity = joints[i]->GetVelocity(0);
        double raw_acceleration = (raw_velocity - last_velocity) * 1000.0;
        double raw_torque = -(axis.x * gazebo_wrench.body2Torque.x + axis.y * gazebo_wrench.body2Torque.y + axis.z * gazebo_wrench.body2Torque.z);

        measurement_raw.position[i] = raw_position;
        measurement_raw.velocity[i] = raw_velocity;
        measurement_raw.acceleration[i] = raw_acceleration;
        measurement_raw.torque[i] = raw_torque;

        double position = pos_lpfs[i].process(raw_position);
        double velocity = vel_lpfs[i].process(raw_velocity);
        double acceleration = acc_lpfs[i].process(raw_acceleration);
        double torque = tor_lpfs[i].process(raw_torque);

        measurement.position[i] = position;
        measurement.velocity[i] = velocity;
        measurement.acceleration[i] = acceleration;
        measurement.torque[i] = torque;
      }
      measurement_raw.timestamp = measurement.timestamp;
      m_joint_states_pub.publish(measurement);
      m_joint_states_raw_pub.publish(measurement_raw);
      if(last_timestamp == measurement.timestamp || last_timestamp == measurement_raw.timestamp)
      {
        ROS_WARN("Duplicate timestamp detected at %g!", last_timestamp);
      }

      joint_states.position = measurement.position;
      joint_states.velocity = measurement.velocity;
      joint_states.effort = measurement.torque;
      joint_states.header.stamp = ros::Time::now();
      js_pub.publish(joint_states);

      //ankle wrenches (TODO: param which joints get sensors)
//      gazebo::physics::JointWrench left_wrench = this->model->GetJoint("l_leg_aky")->GetForceTorque(0);
//      gazebo::physics::JointWrench right_wrench = this->model->GetJoint("r_leg_aky")->GetForceTorque(0);
//      geometry_msgs::WrenchStamped left_wrench_msg;
//      left_wrench_msg.wrench.force.z = l_wrench_lpfs[0].process(left_wrench.body2Force.z);
//      left_wrench_msg.wrench.torque.x = l_wrench_lpfs[1].process(left_wrench.body2Torque.x);
//      left_wrench_msg.wrench.torque.y = l_wrench_lpfs[2].process(left_wrench.body2Torque.y);
//      left_wrench_msg.header.stamp = ros::Time::now();
//      left_wrench_msg.header.frame_id = this->model->GetJoint("l_leg_aky")->GetChild()->GetName();
//      left_wrench_pub.publish(left_wrench_msg);
//
//      geometry_msgs::WrenchStamped right_wrench_msg;
//      right_wrench_msg.wrench.force.z = r_wrench_lpfs[0].process(right_wrench.body2Force.z);
//      right_wrench_msg.wrench.torque.x = r_wrench_lpfs[1].process(right_wrench.body2Torque.x);
//      right_wrench_msg.wrench.torque.y = r_wrench_lpfs[2].process(right_wrench.body2Torque.y);
//      right_wrench_msg.header.stamp = ros::Time::now();
//      right_wrench_msg.header.frame_id = this->model->GetJoint("r_leg_aky")->GetChild()->GetName();
//      right_wrench_pub.publish(right_wrench_msg);
//
//      matec_msgs::WrenchArray wrenches;
//      wrenches.wrenches.push_back(left_wrench_msg);
//      wrenches.wrenches.push_back(right_wrench_msg);
//      m_ankle_wrench_pub.publish(wrenches);

      //root link odometry
      nav_msgs::Odometry odom;
      gazebo::physics::Link_V links = this->model->GetLinks();
      gazebo::math::Pose root_link_pose = links.at(0)->GetWorldPose();
      gazebo::math::Vector3 root_link_linear_vel = links.at(0)->GetWorldLinearVel();
      gazebo::math::Vector3 root_link_angular_vel = links.at(0)->GetWorldAngularVel();
      gazebo::math::Vector3 root_link_linear_accel = links.at(0)->GetWorldLinearAccel();
      gazebo::math::Vector3 root_link_angular_accel = links.at(0)->GetWorldAngularAccel();
      odom.pose.pose.position.x = root_link_pose.pos.x;
      odom.pose.pose.position.y = root_link_pose.pos.y;
      odom.pose.pose.position.z = root_link_pose.pos.z;
      odom.pose.pose.orientation.w = root_link_pose.rot.w;
      odom.pose.pose.orientation.x = root_link_pose.rot.x;
      odom.pose.pose.orientation.y = root_link_pose.rot.y;
      odom.pose.pose.orientation.z = root_link_pose.rot.z;
      odom.twist.twist.linear.x = root_link_linear_vel.x;
      odom.twist.twist.linear.y = root_link_linear_vel.y;
      odom.twist.twist.linear.z = root_link_linear_vel.z;
      odom.twist.twist.angular.x = root_link_angular_vel.x;
      odom.twist.twist.angular.y = root_link_angular_vel.y;
      odom.twist.twist.angular.z = root_link_angular_vel.z;
      odom.header.stamp = ros::Time(measurement.timestamp);
      m_root_link_odom_pub.publish(odom);

      //apply command
      for(unsigned int i = 0; i < joints.size(); i++)
      {
        if(position_mode)
        {
          double real_position_setpoint = clamp(joint_commands.position[i], m_original_low_stops[i], m_original_high_stops[i]);
          if(real_position_setpoint != 0)
          {
//            std::cerr << "Moving " << joints[i]->GetName() << " to " << real_position_setpoint << " between limits " << m_original_low_stops[i] << " and " << m_original_high_stops[i] << std::endl;
          }
          joints[i]->SetHighStop(0, real_position_setpoint);
          joints[i]->SetLowStop(0, real_position_setpoint);
        }
        else if(velocity_mode)
        {
          joints[i]->SetMaxForce(0, 300.0);
          joints[i]->SetVelocity(0, joint_commands.velocity[i]);
        }
        else
        {
          double damping_force = 0; //-1 * measurement_raw.velocity[i];
          double unclamped_torque = joint_commands.torque[i];
          double clamped_torque = clamp(unclamped_torque, -joints[i]->GetEffortLimit(0), joints[i]->GetEffortLimit(0));
          if(clamped_torque != unclamped_torque)
          {
            ROS_WARN_STREAM_THROTTLE(0.5, "Joint" << i << " hit torque limits!");
          }
          joints[i]->SetForce(0, clamped_torque + damping_force);
        }
        ROS_INFO_THROTTLE(10, "RTT: %g", measurement_raw.timestamp - joint_commands.timestamp);
      }
    }

  private:
    physics::ModelPtr model;
    event::ConnectionPtr updateConnection;

    ros::NodeHandle* m_nh;
    ros::Publisher names_pub;
    ros::Publisher js_pub;

    bool velocity_mode;
    bool position_mode;

    ros::Publisher left_wrench_pub;
    ros::Publisher right_wrench_pub;
    ros::ServiceServer unpin_srv;

    shared_memory_interface::Publisher<nav_msgs::Odometry> m_root_link_odom_pub;
    shared_memory_interface::Publisher<matec_msgs::WrenchArray> m_ankle_wrench_pub;
    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_states_pub;
    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_states_raw_pub;
    shared_memory_interface::Publisher<matec_msgs::JointNames> m_joint_names_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_commands_sub;

    physics::JointPtr pin_joint_handle;

    matec_msgs::FullJointStates measurement;
    matec_msgs::FullJointStates measurement_raw;
    std::vector<Biquad> pos_lpfs;
    std::vector<Biquad> vel_lpfs;
    std::vector<Biquad> acc_lpfs;
    std::vector<Biquad> tor_lpfs;
    std::vector<Biquad> l_wrench_lpfs;
    std::vector<Biquad> r_wrench_lpfs;

    std::vector<double> m_original_low_stops;
    std::vector<double> m_original_high_stops;

    sensor_msgs::JointState joint_states;

    matec_msgs::FullJointStates joint_commands;
    boost::posix_time::ptime next_loop_time;
  };

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN (SMPlugin)

} // namespace gazebo
