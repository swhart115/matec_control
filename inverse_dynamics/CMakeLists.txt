cmake_minimum_required(VERSION 2.4.6)
include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)

rosbuild_init()

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
#rosbuild_gensrv()

#common commands for building c++ executables and libraries
#rosbuild_add_library(${PROJECT_NAME} src/example.cpp)
#target_link_libraries(${PROJECT_NAME} another_library)
#rosbuild_add_boost_directories()
#rosbuild_link_boost(${PROJECT_NAME} thread)
#rosbuild_add_executable(example examples/example.cpp)
#target_link_libraries(example ${PROJECT_NAME})

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
find_package(Eigen3 3.0.0)
include_directories(${EIGEN3_INCLUDE_DIR})

rosbuild_add_executable(rbdl_inverse_dynamics src/rbdl_inverse_dynamics.cpp)
target_link_libraries(rbdl_inverse_dynamics shared_memory_interface rbdl_urdfreader)

rosbuild_add_executable(inverse_dynamics src/inverse_dynamics.cpp src/rnea.cpp src/aspect.cpp)
target_link_libraries(inverse_dynamics shared_memory_interface kdl_urdf_reader biquad)

rosbuild_add_executable(pe_signal_generator src/pe_signal_generator.cpp)
target_link_libraries(pe_signal_generator shared_memory_interface)

rosbuild_add_executable(zero src/zero.cpp)
target_link_libraries(zero shared_memory_interface)

rosbuild_add_executable(midpoints src/midpoints.cpp)
target_link_libraries(midpoints shared_memory_interface)
