#include "inverse_dynamics/aspect.h"

namespace inverse_dynamics
{
  ASPECT::ASPECT()
  {
    srand(time(NULL));
    base_position = Vector6::Zero();
    base_velocity = Vector6::Zero();
    base_gravity_acceleration = Vector6::Zero();
    gravity_constant = 9.81; //9.80665;

    initInertiaParameterMaps(R);
    //TODO: publish reference velocity instead of incoming desired velocity
    sample_time = std::numeric_limits<double>::quiet_NaN();
  }

  void ASPECT::computeTorques(matec_msgs::FullJointStates& current, matec_msgs::FullJointStates& desired)
  {
    if(isnan(sample_time)) //skip the first call so we can compute a dt.
    {
      sample_time = current.timestamp;
      return;
    }

    if(current.timestamp == sample_time)
    {
      ROS_WARN("ASPECT: Received duplicate sample time at %g!", sample_time);
      return;
    }

    dt = current.timestamp - sample_time;
    sample_time = current.timestamp;

    m_current = &current;
    m_desired = &desired;

    computeTorquesRecursive(6);
    updateRecursive(6);
  }

  void ASPECT::forwardPass(long i, AdaptiveDynamicsTreeNode* self, AdaptiveDynamicsTreeNode* parent)
  {
    if(i == 6) //model root
    {
      extractBaseStates(m_current, gravity_constant, base_position, base_velocity, base_gravity_acceleration);

      self->w = Vector6::Zero();
      self->v = Vector6::Zero();
      self->a = base_gravity_acceleration;
      self->u = base_gravity_acceleration;
      self->qr_dot = 0;
      self->qr_dot_dot = 0;
      self->iXip = Matrix6::Identity();
    }
    else
    {
      self->qd = m_desired->position[i - 1];
      self->qd_dot = m_desired->velocity[i - 1];
      self->qd_dot_dot = m_desired->acceleration[i - 1];
      self->q = m_current->position[i - 1];
      self->q_dot = m_current->velocity[i - 1];
      self->q_dot_dot = m_current->acceleration[i - 1];
      self->qr_dot = self->qd_dot - self->lambda * (self->q - self->qd);
      self->qr_dot_dot = self->qd_dot_dot - self->lambda * (self->q_dot - self->qd_dot);
      self->measured_torque = m_current->torque[i - 1];

      self->iXip = kdlToEigen(self->segment.pose(self->q).Inverse());

      self->v = (self->iXip * parent->v) + (self->S * self->q_dot);
      self->w = (self->iXip * parent->w) + (self->S * self->qr_dot);
      self->a = (self->iXip * parent->a) + (self->S * self->q_dot_dot) + cross(self->v) * (self->S * self->q_dot);
      self->u = (self->iXip * parent->u) + (self->S * self->qr_dot_dot) + cross(self->v) * (self->S * self->qr_dot);
    }
  }

  void ASPECT::backwardPass(long i, AdaptiveDynamicsTreeNode* self, AdaptiveDynamicsTreeNode* parent)
  {
    self->parameter_mutex->lock();
    //TODO: handle link 6 (base link) case
    if(i != 6)
    {
      for(unsigned int l = 0; l < 10; l++)
      {
        self->f.col(l) = R[l] * self->a - cross(self->v).transpose() * R[l] * self->v;
        self->F += self->f.col(l) * self->theta(l);
        // self->beta.col(l) = R[l] * self->u - cross(self->v).transpose() * R[l] * self->w;
        self->beta.col(l) = 0.5 * ((cross(self->v) * R[l] * self->w) + (cross(self->w) * R[l] * self->v) + (R[l] * cross(self->w) * self->v)) + (R[l] * self->u);
        self->B += self->beta.col(l) * self->theta(l);
      }

//      double scaled_gain = self->K * ((double) (self->S.transpose() * (self->estimated_composite_inertia * self->S)));
      double scaled_gain = self->K;
      self->torque = self->S.transpose() * self->B - scaled_gain * (self->q_dot - self->qr_dot) + self->viscous_friction_theta * self->qr_dot;
      self->torque_error = (self->S.transpose() * self->F + self->viscous_friction_theta * self->q_dot) - self->measured_torque;

      for(unsigned int l = 0; l < 10; l++)
      {
        self->phi_dot(l) = self->gamma * (self->theta_max(l) - self->theta_min(l)) * (self->v - self->w).transpose() * self->beta.col(l);
        assert(!isnan((double) self->theta_min(l)));
        assert(!isnan((double) self->theta_max(l)));
        assert(!isnan((double) self->phi_dot(l)));
      }
    }
    self->parameter_mutex->unlock();
  }

  void ASPECT::computeTorquesRecursive(long i)
  {
    AdaptiveDynamicsTreeNode* self = &nodes[i];
    AdaptiveDynamicsTreeNode* parent = &nodes[self->parent_index];

    //process this node forward
    forwardPass(i, self, parent);

    //process children forward
    self->F = Vector6::Zero();
    self->B = Vector6::Zero();
    self->estimated_composite_inertia = self->estimated_inertia;
    for(unsigned int j = 0; j < self->child_indices.size(); j++)
    {
      int child_idx = self->child_indices[j];
      AdaptiveDynamicsTreeNode* child = &nodes[child_idx];

      computeTorquesRecursive(child_idx); //recurse

      Matrix6 icXi = child->iXip;
      Matrix6 icXi_tranpose = icXi.transpose();
      self->F += icXi_tranpose * child->F;
      self->B += icXi_tranpose * child->B;
      self->estimated_composite_inertia += icXi_tranpose * child->estimated_composite_inertia * icXi;
    }

    //process this node back
    backwardPass(i, self, parent);
  }

  void ASPECT::updateRecursive(long i)
  {
    AdaptiveDynamicsTreeNode* self = &nodes[i];
    AdaptiveDynamicsTreeNode* parent = &nodes[self->parent_index];

    self->parameter_mutex->lock();
    if(i == 6)
    {
      self->e = Vector6::Zero();
    }
    else
    {
      double tau_p = 1 / (2 * M_PI * self->lambda_p);
      double alpha_p = dt / (tau_p + dt);
      double tau_u = 1 / (2 * M_PI * self->lambda_u);
      double alpha_u = dt / (tau_u + dt);

      //augment the update with the prediction error part
      self->e = (self->iXip * parent->e) + (self->S * self->torque_error);
      for(unsigned int l = 0; l < 10; l++)
      {
        self->filtered_prediction_error(l) = alpha_p * ((double) (self->e.transpose() * self->f.col(l))) + (1 - alpha_p) * self->filtered_prediction_error(l);
        self->phi_dot(l) += self->nu * self->gamma * (self->theta_max(l) - self->theta_min(l)) * self->filtered_prediction_error(l);
        double new_phi = alpha_u * (self->phi(l) + 0.5 * dt * (self->phi_dot(l) + self->old_phi_dot(l))) + (1 - alpha_u) * self->phi(l);
        double max_change_fraction = 0.25 * dt; //todo: param
        double max_next_phi = self->phi(l) + max_change_fraction * 4.0;
        double min_next_phi = self->phi(l) - max_change_fraction * 4.0;
        self->phi(l) = clamp(new_phi, min_next_phi, max_next_phi);
      }

      //do the phi-space update
      self->old_phi_dot = self->phi_dot;

      self->filtered_friction_prediction_error = alpha_p * (self->q_dot * self->torque_error) + (1 - alpha_p) * self->filtered_friction_prediction_error;
      self->viscous_friction_phi_dot = self->gamma * (self->viscous_friction_theta_max - self->viscous_friction_theta_min) * (self->q_dot * (self->q_dot - self->qr_dot) + self->nu * self->filtered_friction_prediction_error);
      self->viscous_friction_phi = alpha_u * (self->viscous_friction_phi + 0.5 * dt * (self->viscous_friction_phi_dot + self->old_viscous_friction_phi_dot)) + (1 - alpha_u) * self->viscous_friction_phi;
      self->old_viscous_friction_phi_dot = self->viscous_friction_phi_dot;

      //convert update to theta-space
      for(unsigned int l = 0; l < 10; l++)
      {
        self->theta(l) = 0.5 * (self->theta_max(l) - self->theta_min(l)) * (1 - tanh((double) self->phi(l))) + self->theta_min(l);
        assert(!isnan((double) self->theta(l)));
      }

      self->viscous_friction_theta = 0.5 * (self->viscous_friction_theta_max - self->viscous_friction_theta_min) * (1 - tanh((double) self->viscous_friction_phi)) + self->viscous_friction_theta_min;

      reevaluateParameterBounds(self);

      //todo: verify that estimated_inertia is PD
    }

    //recurse
    self->parameter_mutex->unlock();
    for(unsigned int j = 0; j < self->child_indices.size(); j++)
    {
      updateRecursive(self->child_indices[j]);
    }
  }

  void ASPECT::reevaluateParameterBounds(AdaptiveDynamicsTreeNode* self)
  {
    //note: bounds for mass (m) are constant
    self->theta_min(9) = self->epsilon_mass;
    self->theta_max(9) = self->omega_mass;
    self->theta(9) = asymptoteClamp((double) self->theta(9), (double) self->theta_min(9), (double) self->theta_max(9));
    ROS_DEBUG("Bounding %s:m at %g between %g and %g", self->link_name.c_str(), self->theta(9), self->theta_min(9), self->theta_max(9));

    //update bounds for (mCx)
    self->theta_min(6) = self->theta(9) * self->cx_min;
    self->theta_max(6) = self->theta(9) * self->cx_max;
    self->theta(6) = asymptoteClamp((double) self->theta(6), (double) self->theta_min(6), (double) self->theta_max(6));
    ROS_DEBUG("Bounding %s:mCx at %g between %g and %g", self->link_name.c_str(), self->theta(6), self->theta_min(6), self->theta_max(6));

    //update bounds for (mCy)
    self->theta_min(7) = self->theta(9) * self->cy_min;
    self->theta_max(7) = self->theta(9) * self->cy_max;
    self->theta(7) = asymptoteClamp((double) self->theta(7), (double) self->theta_min(7), (double) self->theta_max(7));
    ROS_DEBUG("Bounding %s:mCy at %g between %g and %g", self->link_name.c_str(), self->theta(7), self->theta_min(7), self->theta_max(7));

    //update bounds for (mCz)
    self->theta_min(8) = self->theta(9) * self->cz_min;
    self->theta_max(8) = self->theta(9) * self->cz_max;
    self->theta(8) = asymptoteClamp((double) self->theta(8), (double) self->theta_min(8), (double) self->theta_max(8));
    ROS_DEBUG("Bounding %s:mCz at %g between %g and %g", self->link_name.c_str(), self->theta(8), self->theta_min(8), self->theta_max(8));

    //update bounds for (Jxx)
    self->theta_min(0) = (self->theta(7) * self->theta(7) + self->theta(8) * self->theta(8)) / self->theta(9) + self->epsilon_inertia;
    self->theta_max(0) = self->theta_min(0) + self->omega_inertia;
    self->theta(0) = asymptoteClamp((double) self->theta(0), (double) self->theta_min(0), (double) self->theta_max(0));
    double ixx = (self->theta(0) - (self->theta(7) * self->theta(7) + self->theta(8) * self->theta(8)) / self->theta(9));
    ROS_DEBUG("Bounding %s:Jxx at %g between %g and %g", self->link_name.c_str(), self->theta(0), self->theta_min(0), self->theta_max(0));

    //update bounds for (Jyy)
    self->theta_min(1) = (self->theta(6) * self->theta(6) + self->theta(8) * self->theta(8)) / self->theta(9) + self->epsilon_inertia;
    self->theta_max(1) = self->theta_min(1) + self->omega_inertia;
    self->theta(1) = asymptoteClamp((double) self->theta(1), (double) self->theta_min(1), (double) self->theta_max(1));
    double iyy = (self->theta(1) - (self->theta(6) * self->theta(6) + self->theta(8) * self->theta(8)) / self->theta(9));
    ROS_DEBUG("Bounding %s:Jyy at %g between %g and %g", self->link_name.c_str(), self->theta(1), self->theta_min(1), self->theta_max(1));

    //update bounds for (Jzz)
    self->theta_min(2) = (self->theta(6) * self->theta(6) + self->theta(7) * self->theta(7)) / self->theta(9) + self->epsilon_inertia;
    self->theta_max(2) = self->theta_min(2) + self->omega_inertia;
    self->theta(2) = asymptoteClamp((double) self->theta(2), (double) self->theta_min(2), (double) self->theta_max(2));
    double izz = (self->theta(2) - (self->theta(6) * self->theta(6) + self->theta(7) * self->theta(7)) / self->theta(9));
    ROS_DEBUG("Bounding %s:Jzz at %g between %g and %g", self->link_name.c_str(), self->theta(2), self->theta_min(2), self->theta_max(2));

    //update bounds for (Jxy)
    double fxy1 = sqrt(ixx * iyy - self->epsilon_inertia * self->epsilon_inertia);
    double fxy2 = self->theta(6) * self->theta(7) / self->theta(9);
    self->theta_min(3) = -fxy1 - fxy2;
    self->theta_max(3) = fxy1 - fxy2;
    self->theta(3) = asymptoteClamp((double) self->theta(3), (double) self->theta_min(3), (double) self->theta_max(3));
    ROS_DEBUG("Bounding %s:Jxy at %g between %g and %g", self->link_name.c_str(), self->theta(3), self->theta_min(3), self->theta_max(3));

    //update bounds for (Jxz)
    double fxz1 = sqrt(ixx * izz - self->epsilon_inertia * self->epsilon_inertia);
    double fxz2 = self->theta(6) * self->theta(8) / self->theta(9);
    self->theta_min(4) = -fxz1 - fxz2;
    self->theta_max(4) = fxz1 - fxz2;
    self->theta(4) = asymptoteClamp((double) self->theta(4), (double) self->theta_min(4), (double) self->theta_max(4));
    ROS_DEBUG("Bounding %s:Jxz at %g between %g and %g", self->link_name.c_str(), self->theta(4), self->theta_min(4), self->theta_max(4));

    //update bounds for (Jyz)
    double fyz1 = sqrt(iyy * izz - self->epsilon_inertia * self->epsilon_inertia);
    double fyz2 = self->theta(7) * self->theta(8) / self->theta(9);
    self->theta_min(5) = -fyz1 - fyz2;
    self->theta_max(5) = fyz1 - fyz2;
    self->theta(5) = asymptoteClamp((double) self->theta(5), (double) self->theta_min(5), (double) self->theta_max(5));
    ROS_DEBUG("Bounding %s:Jyz at %g between %g and %g", self->link_name.c_str(), self->theta(5), self->theta_min(5), self->theta_max(5));

    //update phi with modified bounds and generate the inertia matrix
    self->estimated_inertia = Matrix6::Zero();
    for(unsigned int l = 0; l < 10; l++)
    {
      self->phi(l) = atanh((double) (1.0 - 2.0 * (self->theta(l) - self->theta_min(l)) / (self->theta_max(l) - self->theta_min(l))));
      self->estimated_inertia += R[l] * self->theta(l);
    }

    self->viscous_friction_theta = asymptoteClamp(self->viscous_friction_theta, self->viscous_friction_theta_min, self->viscous_friction_theta_max);
    self->viscous_friction_phi = atanh((double) (1.0 - 2.0 * (self->viscous_friction_theta - self->viscous_friction_theta_min) / (self->viscous_friction_theta_max - self->viscous_friction_theta_min)));
  }

  void ASPECT::setAdaptiveParameters(std::string joint_name, ASPECTLinkParameters params)
  {
    //todo add locks and checks for joint name existence
    boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(joint_name);
    long idx = std::find(link_names.begin(), link_names.end(), joint->child_link_name) - link_names.begin();

    nodes[idx].parameter_mutex->lock();
    nodes[idx].lambda_p = params.prediction_lpf;
    nodes[idx].lambda_u = params.update_lpf;
    nodes[idx].lambda = params.sliding_surface_angle;
    nodes[idx].K = params.feedback_gain;
    nodes[idx].gamma = params.parameter_update_gain;
    nodes[idx].nu = params.parameter_update_gain_prediction;
    nodes[idx].epsilon_inertia = params.epsilon_inertia;
    nodes[idx].omega_inertia = params.omega_inertia;
    nodes[idx].epsilon_mass = params.epsilon_mass;
    nodes[idx].omega_mass = params.omega_mass;

    reevaluateParameterBounds(&nodes[idx]);
    nodes[idx].parameter_mutex->unlock();
  }

  void ASPECT::initializeParametersFromInertia(AdaptiveDynamicsTreeNode* node, Matrix6& inertia)
  {
    //set most bounds based on urdf guess
    node->urdf_inertia = inertia;
    node->theta = parameterizeInertia(node->urdf_inertia);

    if(randomize_initial_parameters)
    {
      ROS_INFO("ASPECT: RANDOMIZING INITIAL PARAMETERS");
      for(unsigned int i = 0; i < 10; i++)
      {
        double fraction = 1.5 - ((double) rand() / (double) RAND_MAX); // between 0.5x and 1.5x the true value
        ROS_WARN("Starting %d:%d at %g (%g%% of the true value %g)", node->joint_idx - 7, i, node->theta(i) * fraction, fraction * 100.0, node->theta(i));
        node->theta(i) *= fraction;
      }
    }
    else
    {
      ROS_INFO("ASPECT: USING URDF AS INITIAL PARAMETERS");
    }
  }

  void ASPECT::setCoMBounds(AdaptiveDynamicsTreeNode* node)
  {
    boost::shared_ptr<const urdf::Link> link = m_urdf_model.getLink(node->link_name);
    if(link->collision)
    {
      std::pair<urdf::Vector3, urdf::Vector3> com_bounds = matec_utils::getBoundingBoxInJointFrame(link->collision);
      node->cx_min = com_bounds.first.x;
      node->cx_max = com_bounds.second.x;
      node->cy_min = com_bounds.first.y;
      node->cy_max = com_bounds.second.y;
      node->cz_min = com_bounds.first.z;
      node->cz_max = com_bounds.second.z;

      ROS_WARN("Node %s has volume bounds of x:(%g, %g), y:(%g, %g), z:(%g, %g)", node->link_name.c_str(), node->cx_min, node->cx_max, node->cy_min, node->cy_max, node->cz_min, node->cz_max);
    }
    else
    {
      ROS_WARN("Collision not defined for link %s! Using an excessively large range based on the urdf-defined CoM instead!", node->link_name.c_str());
      double px = link->inertial->origin.position.x;
      double py = link->inertial->origin.position.y;
      double pz = link->inertial->origin.position.z;
      double magnitude = sqrt(px * px + py * py + pz * pz);
      node->cx_min = px - magnitude;
      node->cx_max = px + magnitude;
      node->cy_min = py - magnitude;
      node->cy_max = py + magnitude;
      node->cz_min = pz - magnitude;
      node->cz_max = pz + magnitude;
    }
  }
//todo make sure set adaptive parameters has been called before we try to load
  void ASPECT::loadSegmentIndex(KDL::Segment segment, int idx)
  {
    nodes[idx].link_name = segment.getName();
    nodes[idx].joint_name = segment.getJoint().getName();
    nodes[idx].joint_idx = idx;

    //load kinematics
    nodes[idx].segment = segment;
    nodes[idx].S = Vector6::Zero();
    if(nodes[idx].segment.getJoint().getType() == KDL::Joint::RotAxis)
    {
      nodes[idx].type = AdaptiveDynamicsTreeNode::REVOLUTE;
      nodes[idx].S.topRows(3) = kdlToEigen(segment.getJoint().JointAxis());
    }
    else if(nodes[idx].segment.getJoint().getType() == KDL::Joint::TransAxis)
    {
      nodes[idx].type = AdaptiveDynamicsTreeNode::PRISMATIC;
      nodes[idx].S.bottomRows(3) = kdlToEigen(segment.getJoint().JointAxis());
    }
    else //fixed joint
    {
      nodes[idx].type = AdaptiveDynamicsTreeNode::FIXED;
    }

    //load dynamics
    boost::shared_ptr<const urdf::Link> link = m_urdf_model.getLink(nodes[idx].link_name);
    boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(nodes[idx].joint_name);

    if(!link || !joint)
    {
      ROS_WARN("Node %s was not in the urdf!", nodes[idx].link_name.c_str());
      return;
    }

    Matrix6 inertia = kdlToEigen(segment.getInertia());
    setCoMBounds(&nodes[idx]);
    initializeParametersFromInertia(&nodes[idx], inertia);

    nodes[idx].torque = 0;
    nodes[idx].torque_error = 0;
    nodes[idx].old_phi_dot = Vector10::Zero();
    nodes[idx].filtered_prediction_error = Vector10::Zero();

    nodes[idx].viscous_friction_theta = 1; //todo: load from urdf?
    nodes[idx].old_viscous_friction_phi_dot = 0;
    nodes[idx].viscous_friction_theta_min = 0; // todo: param?
    nodes[idx].viscous_friction_theta_max = 2;
    nodes[idx].filtered_friction_prediction_error = 0;
  }

  void ASPECT::loadRecursive(int idx, const KDL::SegmentMap::const_iterator segment)
  {
    std::string name = segment->second.segment.getName();
    std::string joint_name = segment->second.segment.getJoint().getName();
    ROS_INFO("Parsing link %s(%s)", name.c_str(), joint_name.c_str());

    loadSegmentIndex(segment->second.segment, idx);

    nodes[idx].supported_indices.push_back(idx);
    const std::vector<KDL::SegmentMap::const_iterator>& children = segment->second.children;
    for(unsigned int i = 0; i < children.size(); i++)
    {
      const KDL::Segment& child = children[i]->second.segment;
      std::string child_name = child.getName();
      std::string child_joint_name = child.getJoint().getName();
      long child_idx = std::find(link_names.begin(), link_names.end(), child_name) - link_names.begin();

      ROS_INFO("%s(%d)->%s(%d)", name.c_str(), (int) idx, child_name.c_str(), (int) child_idx);

      if(child_idx >= 0 && child_idx < (int) nodes.size())
      {
        nodes[idx].child_indices.push_back(child_idx);
        nodes[child_idx].parent_index = idx;

        loadRecursive(child_idx, children[i]);

        nodes[idx].supported_indices.insert(nodes[idx].supported_indices.end(), nodes[child_idx].supported_indices.begin(), nodes[child_idx].supported_indices.end());
      }
      else
      {
        ROS_WARN("Ignoring joints / links below fixed joint %s", child_joint_name.c_str());
      }
    }

    std::stringstream ss;
    ss << name.c_str() << "(" << idx << ") has children {";
    for(unsigned int i = 0; i < nodes[idx].child_indices.size(); i++)
    {
      if(i != 0)
      {
        ss << ", ";
      }
      ss << nodes[idx].child_indices[i];
    }
    ss << "}, supports {";
    for(unsigned int i = 0; i < nodes[idx].supported_indices.size(); i++)
    {
      if(i != 0)
      {
        ss << ", ";
      }
      ss << nodes[idx].supported_indices[i];
    }
    ss << "}";
    ROS_INFO("%s", ss.str().c_str());
  }

  void ASPECT::load(urdf::Model model, std::vector<std::string> joint_names, bool randomize_parameters)
  {
    randomize_initial_parameters = randomize_parameters;
    m_urdf_model = model;
    if(!matec_utils::treeFromUrdfModel((const urdf::Model) m_urdf_model, m_kdl_tree, true))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }

    std::string floating_link_names[] = {"FLOATING_0", "FLOATING_1", "FLOATING_2", "FLOATING_3", "FLOATING_4", "FLOATING_5"};
    link_names = std::vector<std::string>(&floating_link_names[0], &floating_link_names[6]);
    link_names.push_back(model.getRoot()->name);
    for(unsigned int i = 0; i < joint_names.size(); i++)
    {
      link_names.push_back(model.getJoint(joint_names[i])->child_link_name);
    }
    nodes.resize(link_names.size());

    for(unsigned int i = 0; i < link_names.size(); i++)
    {
      ROS_INFO("Link %d is %s", i, link_names[i].c_str());
    }

    //load the rest of the tree recursively
    loadRecursive(0, m_kdl_tree.getRootSegment());
  }

  matec_msgs::RobotInertiaProperties ASPECT::getInertias()
  {
    matec_msgs::RobotInertiaProperties inertias;

    for(unsigned int i = 7; i < nodes.size(); i++)
    {
      Vector10 psi = toDecoupledParameterization(nodes[i].theta);

      matec_msgs::InertiaProperties inertia;
      inertia.Ixx = psi(0);
      inertia.Iyy = psi(1);
      inertia.Izz = psi(2);
      inertia.Ixy = psi(3);
      inertia.Ixz = psi(4);
      inertia.Iyz = psi(5);
      inertia.com_x = psi(6);
      inertia.com_y = psi(7);
      inertia.com_z = psi(8);
      inertia.mass = psi(9);

      inertias.link_inertias.push_back(inertia);
    }

    return inertias;
  }

  std::vector<double> ASPECT::getTorques()
  {
    std::vector<double> torques;
    for(unsigned int i = 7; i < nodes.size(); i++)
    {
      torques.push_back(nodes[i].torque);
    }
    return torques;
  }
}
