#ifndef ORCHESTRATOR_H
#define ORCHESTRATOR_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <boost/thread.hpp>

#include <sensor_msgs/JointState.h>

#include <matec_utils/common_initialization_components.h>
#include <matec_msgs/RegisterTask.h>
#include <matec_msgs/ReprioritizeTasks.h>
#include <matec_msgs/Tare.h>

namespace orchestrator
{
  class TaskParameters
  {
  public:
    std::string task_name;
    int task_priority;
    std::string interpolator;
    std::vector<std::string> controlled_joints;
    std::vector<std::string> owned_joints; // joints for which this task has highest priority
    std::vector<std::string> new_joints; // new joints that may need to be tared
    shared_memory_interface::Subscriber<matec_msgs::TaskCommand> task_sub;
  };

  class Orchestrator
  {
  public:
    Orchestrator(const ros::NodeHandle& nh);
    ~Orchestrator();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    ros::ServiceServer m_registration_service_server;
    ros::ServiceServer m_reprioritization_service_server;
    ros::ServiceServer m_tare_service_server;

    std::vector<std::string> m_joint_names;

    shared_memory_interface::Publisher<matec_msgs::TaskCommand> m_prioritized_command_pub;
    shared_memory_interface::Publisher<sensor_msgs::JointState> m_prioritized_joint_states_pub;

    std::map<std::string, TaskParameters> m_task_list;
    matec_msgs::TaskCommand m_prioritized_command;

    void updateJointOwners(bool force_taring = false);
    void checkTask(TaskParameters new_task);
    void initializePrioritizedCommand();
    void updateCommand();
    TaskParameters loadTaskParameters(std::string task_name);

    bool registerTaskCallback(matec_msgs::RegisterTask::Request& req, matec_msgs::RegisterTask::Response& res);
    bool reprioritizeTasksCallback(matec_msgs::ReprioritizeTasks::Request& req, matec_msgs::ReprioritizeTasks::Response& res);
    bool tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res);
  };
}
#endif //ORCHESTRATOR_H
