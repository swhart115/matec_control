#ifndef COMMON_INITIALIZATION_COMPONENTS_H
#define COMMON_INITIALIZATION_COMPONENTS_H

#include <ros/ros.h>
#include "matec_utils/string_parameter_parsers.h"
#include "shared_memory_interface/shared_memory_publisher.hpp"
#include "shared_memory_interface/shared_memory_subscriber.hpp"
#include "matec_msgs/JointNames.h"
#include "matec_msgs/RegisterTask.h"

namespace matec_utils
{
  inline bool getControlledJoints(ros::NodeHandle nh, std::vector<std::string>& controlled_joints, std::string ns = "")
  {
    std::string controlled_joints_string;
    if(ns.length() == 0)
    {
      nh.param("controlled_joints", controlled_joints_string, std::string(""));
    }
    else
    {
      nh.param("/" + ns + "/controlled_joints", controlled_joints_string, std::string(""));
    }
    if(controlled_joints_string.length() == 0)
    {
      return false;
    }
    controlled_joints = matec_utils::parameterStringToStringVector(controlled_joints_string);
    return (controlled_joints.size() > 0);
  }

  inline bool getFPVector(ros::NodeHandle nh, std::string param_name, unsigned long expected_length, double default_value, std::vector<double>& values)
  {
    std::string string;
    nh.param(param_name, string, std::string(""));
    values = matec_utils::parameterStringToFPVector(string);

    if(values.size() != expected_length)
    {
      if(values.size() != 0)
      {
        ROS_WARN("Values provided in %s parameter were not the right length! Using default value %g instead!", param_name.c_str(), default_value);
      }
      values.clear();
      values.resize(expected_length, default_value);
      return false;
    }

    return true;
  }

  //wait for ros time to become valid
  inline void blockOnROSTime()
  {
    ros::NodeHandle nh("~");
    while(ros::ok() && (ros::Time::now().toSec() == 0.0))
    {
      ROS_WARN_THROTTLE(1.0, "%s waiting for ROS time to be populated...", nh.getNamespace().c_str());
    }
  }

  inline void blockOnSMJointNames(std::vector<std::string>& joint_names)
  {
    shared_memory_interface::Subscriber<matec_msgs::JointNames> names_sub;
    names_sub.subscribe("/joint_names");
    matec_msgs::JointNames names;
    ros::NodeHandle nh("~");
    while(ros::ok() && !names_sub.getCurrentMessage(names))
    {
      ROS_WARN_THROTTLE(1.0, "%s node is waiting for joint names...", nh.getNamespace().c_str());
    }
    joint_names = names.data;
  }

  inline bool generateControlledJointMapping(std::vector<std::string> all_joint_names, std::vector<std::string> controlled_joint_names, std::vector<unsigned char>& controlled_joint_map)
  {
    if(controlled_joint_names.size() > all_joint_names.size())
    {
      ROS_ERROR("The list of controlled joint names (%d) was longer than the list of available joints (%d)!", (int) controlled_joint_names.size(), (int) all_joint_names.size());
      return false;
    }

    controlled_joint_map.clear();
    for(unsigned int i = 0; i < controlled_joint_names.size(); i++)
    {
      unsigned char joint_idx = std::find(all_joint_names.begin(), all_joint_names.end(), controlled_joint_names[i]) - all_joint_names.begin();
      if(joint_idx == all_joint_names.size())
      {
        ROS_ERROR("Specified controllable joint %s was not found in the list of available joints!", controlled_joint_names[i].c_str());
        return false;
      }
      else
      {
        controlled_joint_map.push_back(joint_idx);
      }
    }
    return true;
  }

  inline void blockOnTaskRegistration(std::string task_name)
  {
    ROS_INFO("Registering task %s", task_name.c_str());
    while(!ros::service::exists("/task_registration", false))
    {
      ROS_WARN_THROTTLE(1.0, "JPC: Waiting for prioritizer to come up...");
    }

    matec_msgs::RegisterTask register_task;
    register_task.request.task_name = task_name;
    register_task.request.register_task = true;
    ros::service::call("/task_registration", register_task);
    ROS_INFO("Task %s registered", task_name.c_str());
  }
}

#endif //COMMON_INITIALIZATION_COMPONENTS_H
