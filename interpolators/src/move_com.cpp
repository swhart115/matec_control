#include <math.h>
#include <ros/ros.h>
#include <signal.h>
#include <matec_actions/FollowPoseTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/tf.h>

void halt(int sig);
using namespace std;

int main(int argc, char** argv)
{
  ros::init(argc, argv, "move_com");
  ros::NodeHandle nh;

  if(argc != 4)
  {
    cout << "Error: this program must be run with 3 parameters: target_x, target_y, target_z\n";
    return 0;
  }

  actionlib::SimpleActionClient<matec_actions::FollowPoseTrajectoryAction> client(nh, "/follow_pose_trajectory", true);

  while(ros::Time::now().toSec() == 0)
  {

  }

  // wait for action client to come up
  while(!client.waitForServer(ros::Duration(1.0)) && ros::ok())
  {
    ROS_INFO("Waiting for the server to start");
  }

  ROS_INFO("Connected to action server.");

  geometry_msgs::Pose pose;
  pose.position.x = atof(argv[1]);
  pose.position.y = atof(argv[2]);
  pose.position.z = atof(argv[3]);
  pose.orientation.w = 1.0;

  matec_actions::FollowPoseTrajectoryGoal goal;
  goal.poses.push_back(pose);
  goal.times_from_start.push_back(ros::Duration(5.0));
  goal.header.frame_id = "global";
  goal.header.stamp = ros::Time::now();
  client.sendGoal(goal);

  client.waitForResult(ros::Duration(10.0));

  actionlib::SimpleClientGoalState state = client.getState();
  ROS_INFO("Action returned: %s", state.toString().c_str());

  return 0;
}

