#include <matec_utils/model_helper.h>

namespace matec_utils
{
  ModelHelper::ModelHelper(urdf::Model model, std::vector<std::string> external_name_vector) :
      m_model(model)
  {
    m_global_frame = "global";
    m_external_name_vector = external_name_vector;

    parseModel();
    initInternalTree();
//    initRBDLModel();

    ROS_DEBUG("ModelHelper init complete!");
  }

  bool ModelHelper::updatePositions(std::vector<double>& joint_positions)
  {
    assert(joint_positions.size() == m_external_name_vector.size());
    //add all non-fixed transforms with the new angles
    tf::StampedTransform trans;
    for(unsigned int joint_idx = 0; joint_idx < joint_positions.size(); joint_idx++)
    {
      std::string joint_name = m_external_name_vector[joint_idx];
      std::map<std::string, SegmentPair>::const_iterator segment = m_segments.find(joint_name);
      if(segment != m_segments.end())
      {
        tf::transformKDLToTF(segment->second.segment.pose(joint_positions[joint_idx]), trans);
        trans.stamp_ = ros::Time(0);
        trans.frame_id_ = segment->second.root;
        trans.child_frame_id_ = segment->second.tip;
        m_transformer.setTransform(trans, "model_helper");
      }
    }
    return true;
  }

  bool ModelHelper::updateRootLinkOdometry(tf::StampedTransform& model_to_ground_transform)
  {
    if(!m_transformer.frameExists(model_to_ground_transform.frame_id_) && !m_transformer.frameExists(model_to_ground_transform.child_frame_id_)) //one frame has to be in the model somewhere
    {
      ROS_FATAL("Can't find ground parent %s", model_to_ground_transform.frame_id_.c_str());
      return false;
    }

    model_to_ground_transform.stamp_ = ros::Time(0);
    m_transformer.setTransform(model_to_ground_transform, "model_helper");
    return true;
  }

  bool ModelHelper::updateRootLinkOdometry(geometry_msgs::Pose& pose)
  {
    tf::Vector3 vec(pose.position.x, pose.position.y, pose.position.z);
    tf::Quaternion quat;
    tf::quaternionMsgToTF(pose.orientation, quat);
    tf::StampedTransform trans(tf::Transform(quat, vec), ros::Time(0), m_global_frame, m_tree.getRootSegment()->first);
    m_transformer.setTransform(trans, "model_helper");
    return true;
  }

  bool ModelHelper::getRootLinkTransform(tf::StampedTransform& trans)
  {
    if(!m_transformer.frameExists(m_global_frame) || !m_transformer.frameExists(m_tree.getRootSegment()->first))
    {
      return false;
    }
    m_transformer.lookupTransform(m_global_frame, m_tree.getRootSegment()->first, ros::Time(0), trans);
    trans.stamp_ = ros::Time::now();
    return true;
  }

  bool ModelHelper::getCoMTransform(tf::StampedTransform& trans)
  {
    if(!m_transformer.frameExists(m_global_frame) || !m_transformer.frameExists("com"))
    {
      return false;
    }
    m_transformer.lookupTransform(m_global_frame, "com", ros::Time(0), trans);
    trans.stamp_ = ros::Time::now();
    return true;
  }

  bool ModelHelper::updateModel(std::vector<double>& joint_positions, tf::StampedTransform& model_to_ground_transform)
  {
    if(!updatePositions(joint_positions))
    {
      return false;
    }

    return updateRootLinkOdometry(model_to_ground_transform);
  }

  bool ModelHelper::updateModel(std::vector<double>& joint_positions, geometry_msgs::Pose& pose)
  {
    if(!updatePositions(joint_positions))
    {
      return false;
    }

    return updateRootLinkOdometry(pose);
  }

  bool ModelHelper::findCOM(geometry_msgs::Point& com, double& total_mass)
  {
    if(!findCOMOfSubset(m_all_names, com, total_mass))
    {
      return false;
    }

    std::string parent_frame = m_global_frame;
    std::string child_frame = "com";

    tf::StampedTransform trans(tf::Transform(tf::createIdentityQuaternion(), tf::Vector3(com.x, com.y, com.z)), ros::Time(0), parent_frame, child_frame);
    m_transformer.setTransform(trans, "model_helper");

    return true;
  }

  bool ModelHelper::findCOMOfSubset(std::vector<std::string> link_names, geometry_msgs::Point& center_of_mass, double& total_subset_mass)
  {
    total_subset_mass = 0.0;
    for(unsigned int i = 0; i < link_names.size(); i++)
    {
      std::string frame_id = "/" + link_names[i];
      unsigned int massIdx = std::find(m_all_names.begin(), m_all_names.end(), link_names[i]) - m_all_names.begin();
      if(massIdx >= m_all_names.size())
      {
        ROS_ERROR("Couldn't find name %s! You should use link/frame names here, not joint names.", link_names[i].c_str());
        return false;
      }
      PointMass pointMass = m_masses[massIdx];
      assert(link_names[i].compare(pointMass.link_frame_id) == 0);

      if(!m_transformer.canTransform(m_global_frame, frame_id, ros::Time(0)))
      {
        ROS_FATAL("Bad chain from %s->%s", frame_id.c_str(), m_global_frame.c_str());
        return false;
      }
      tf::Stamped<tf::Vector3> ground_point;
      tf::Stamped<tf::Vector3> mass_point(tf::Vector3(pointMass.origin.x(), pointMass.origin.y(), pointMass.origin.z()), ros::Time(0), frame_id);
      m_transformer.transformPoint(m_global_frame, mass_point, ground_point);

      center_of_mass.x += ground_point.getX() * pointMass.mass;
      center_of_mass.y += ground_point.getY() * pointMass.mass;
      center_of_mass.z += ground_point.getZ() * pointMass.mass;
      total_subset_mass += pointMass.mass;
    }
    if(total_subset_mass > m_total_mass)
    {
      ROS_ERROR("Found a greater subset mass (%g) than the total mass of the robot (%g). Do you have duplicate names?", total_subset_mass, m_total_mass);
    }

    center_of_mass.x /= total_subset_mass;
    center_of_mass.y /= total_subset_mass;
    center_of_mass.z /= total_subset_mass;

    return true;
  }

  std::vector<std::string> ModelHelper::getAllLinkNames()
  {
    return m_all_names;
  }

  void ModelHelper::transformWrench(std::string target_frame, KDLWrenchStamped wrench_in, KDLWrenchStamped& wrench_out)
  {
    assert(target_frame.length() > 0);
    assert(wrench_in.frame_id.length() > 0);

    tf::StampedTransform trans;
    m_transformer.lookupTransform(target_frame, wrench_in.frame_id, ros::Time(0), trans); //TODO: add catch

    KDL::Frame frame;
    tf::transformTFToKDL(trans, frame);
    wrench_out.wrench = frame * wrench_in.wrench;
    wrench_out.frame_id = target_frame;

//    tf::Vector3 p = trans.getOrigin();
//    tf::Matrix3x3 R(trans.getRotation());
//    tf::Matrix3x3 R_transpose = R.transpose();
//    tf::Matrix3x3 p_cross_R = tf::Matrix3x3(0., -p.z(), p.y(), p.z(), 0., -p.x(), -p.y(), p.x(), 0.) * R;
//    RigidBodyDynamics::Math::SpatialMatrix adjoint_transpose;
//    adjoint_transpose.topLeftCorner<3, 3>() = R_transpose; //TODO: determine if we want R or R_transpose here by checking RBDL rotation convention
//    adjoint_transpose.bottomRightCorner<3, 3>() = R_transpose;
//    adjoint_transpose.topRightCorner<3, 3>() = p_cross_R.transpose();
//    adjoint_transpose.bottomLeftCorner<3, 3>() = RigidBodyDynamics::Math::Matrix3dZero;
//
//    return adjoint_transpose * local_wrench;
  }

  void ModelHelper::addContactFrame(std::string contact_parent, geometry_msgs::Pose contact_normal_pose)
  {
    tf::Vector3 vec(contact_normal_pose.position.x, contact_normal_pose.position.y, contact_normal_pose.position.z);
    tf::Quaternion quat;
    tf::quaternionMsgToTF(contact_normal_pose.orientation, quat);
    tf::StampedTransform trans(tf::Transform(quat, vec), ros::Time(0), contact_parent, contact_parent + "_contact");
    m_transformer.setTransform(trans, "model_helper");
  }

  geometry_msgs::Point ModelHelper::getFramePosition(std::string target_frame, std::string output_frame)
  {
    tf::StampedTransform trans;
    m_transformer.lookupTransform(target_frame, output_frame, ros::Time(0), trans);

    geometry_msgs::Point point;
    point.x = trans.getOrigin().x();
    point.y = trans.getOrigin().y();
    point.z = trans.getOrigin().z();

    return point;
  }

  geometry_msgs::PoseStamped ModelHelper::getFramePose(std::string target_frame, std::string output_frame)
  {
    tf::StampedTransform trans;
    m_transformer.lookupTransform(output_frame, target_frame, ros::Time(0), trans);

    geometry_msgs::PoseStamped pose;
    pose.pose.position.x = trans.getOrigin().x();
    pose.pose.position.y = trans.getOrigin().y();
    pose.pose.position.z = trans.getOrigin().z();
    tf::quaternionTFToMsg(trans.getRotation(), pose.pose.orientation);
    pose.header.frame_id = output_frame;

    return pose;
  }

  geometry_msgs::PoseStamped ModelHelper::transformPose(geometry_msgs::PoseStamped pose, std::string output_frame)
  {
    tf::Stamped<tf::Transform> trans_in, trans_out;
    tf::poseStampedMsgToTF(pose, trans_in);
    trans_in.stamp_ = ros::Time(0);
    m_transformer.transformPose(output_frame, trans_in, trans_out);

    geometry_msgs::PoseStamped transformed_pose;
    tf::poseStampedTFToMsg(trans_out, transformed_pose);
    return transformed_pose;
  }

  KDL::Tree* ModelHelper::getTree()
  {
    return &m_tree;
  }

  geometry_msgs::Point ModelHelper::getGlobalFramePosition(std::string target_frame)
  {
    return getFramePosition(target_frame, m_global_frame);
  }

  void ModelHelper::addCoMFrames()
  {
    for(unsigned int i = 0; i < m_all_names.size(); i++)
    {
      std::string parent_frame = m_all_names[i];
      std::string child_frame = parent_frame + "_com";
      PointMass mass = m_masses[i];

      tf::StampedTransform trans(tf::Transform(tf::createIdentityQuaternion(), mass.origin), ros::Time(0), parent_frame, child_frame);
      m_transformer.setTransform(trans, "model_helper");
    }
  }

  void ModelHelper::addChildrenSegments(const KDL::SegmentMap::const_iterator segment)
  {
    const std::string& root = segment->second.segment.getName();

    const std::vector<KDL::SegmentMap::const_iterator>& children = segment->second.children;
    for(unsigned int i = 0; i < children.size(); i++)
    {
      const KDL::Segment& child = children[i]->second.segment;
      SegmentPair s(children[i]->second.segment, root, child.getName());
      if(child.getJoint().getType() == KDL::Joint::None)
      {
        m_fixed_segments.insert(make_pair(child.getJoint().getName(), s));
        ROS_DEBUG("Adding fixed segment from %s to %s", root.c_str(), child.getName().c_str());
      }
      else
      {
        m_segments.insert(make_pair(child.getJoint().getName(), s));
        ROS_DEBUG("Adding moving segment from %s to %s", root.c_str(), child.getName().c_str());
      }
      addChildrenSegments(children[i]);
    }
  }

  void ModelHelper::initInternalTree()
  {
    if(!kdl_parser::treeFromUrdfModel((const urdf::Model) m_model, m_tree))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }
    addChildrenSegments(m_tree.getRootSegment());

    //add static transforms only once
    tf::StampedTransform trans;
    trans.stamp_ = ros::Time(0);

    // loop over all fixed segments
    for(std::map<std::string, SegmentPair>::const_iterator segment = m_fixed_segments.begin(); segment != m_fixed_segments.end(); segment++)
    {
      ROS_DEBUG("Using fixed transform for %s", segment->first.c_str());
      tf::transformKDLToTF(segment->second.segment.pose(0), trans);
      trans.frame_id_ = segment->second.root;
      trans.child_frame_id_ = segment->second.tip;
      m_transformer.setTransform(trans, "model_helper");
    }

    //initialize all non-fixed segments to 0
    for(std::map<std::string, SegmentPair>::const_iterator segment = m_segments.begin(); segment != m_segments.end(); segment++)
    {
      if(segment != m_segments.end())
      {
        tf::transformKDLToTF(segment->second.segment.pose(0), trans);
        trans.stamp_ = ros::Time(0);
        trans.frame_id_ = segment->second.root;
        trans.child_frame_id_ = segment->second.tip;
        m_transformer.setTransform(trans, "model_helper");
      }
    }
  }

  bool ModelHelper::parseMass(boost::shared_ptr<const urdf::Link> link, PointMass& pmass)
  {
    if(!link->name.compare(""))
    {
      return false; //empty link name == fail
    }

    boost::shared_ptr<const urdf::Inertial> inertial = link->inertial;
    if(inertial)
    {
      pmass.mass = inertial->mass;
      pmass.origin = tf::Vector3(inertial->origin.position.x, inertial->origin.position.y, inertial->origin.position.z);
      pmass.link_frame_id = link->name;
      ROS_DEBUG("%s has mass %g and origin (%g,%g,%g)", pmass.link_frame_id.c_str(), pmass.mass, pmass.origin.x(), pmass.origin.y(), pmass.origin.z());
      return true;
    }
    else
    {
      ROS_ERROR("Link %s has no inertial component", link->name.c_str());
      return false;
    }
  }

  void ModelHelper::parseModel()
  {
    std::vector<boost::shared_ptr<urdf::Link> > links;
    m_model.getLinks(links);

    m_total_mass = 0.0;
    for(unsigned int i = 0; i < links.size(); i++)
    {
      if(links[i]->name.length() == 0)
      {
        continue;
      }
      PointMass pmass;
      if(parseMass(links[i], pmass))
      {
        m_all_names.push_back(links[i]->name);
        m_masses.push_back(pmass);
        m_total_mass += pmass.mass;
      }
    }
    addCoMFrames();
    ROS_DEBUG("Found a total mass of %f", m_total_mass);
  }

  //Point will be expressed in the same frame as the wrench.
  //wrench should be the wrench exterted by the environment, not the robot (e.g. sign(z) should be positive for gravity-induced wrenches)
  bool ModelHelper::calculateWrenchCoP(matec_utils::KDLWrenchStamped wrench, geometry_msgs::PointStamped& point)
  {
    if(fabs(wrench.wrench.force.z()) < 1.0e-3 || wrench.wrench.force.z() < 0) //no contact
    {
      return false;
    }

    point.point.x = -wrench.wrench.torque.y() / wrench.wrench.force.z(); //- m_s_p_y / f_r_z
    point.point.y = wrench.wrench.torque.x() / wrench.wrench.force.z(); //m_s_p_x / f_r_z
    point.point.z = 0;
    point.header.frame_id = wrench.frame_id;

    return true;
  }

  //RBDL Stuff
  void ModelHelper::initRBDLModel()
  {
    RigidBodyDynamics::Addons::read_urdf_model(m_model, m_rbdl_model, false);
    for(unsigned int i = 0; i < m_rbdl_model.mBodies.size(); i++)
    {
      if(i > 6)
      {
        int rbdl_idx = i - 1;
        std::string joint_name = m_model.getLink(m_rbdl_model.GetBodyName(i))->parent_joint->name;
        int external_idx = std::find(m_external_name_vector.begin(), m_external_name_vector.end(), joint_name) - m_external_name_vector.begin();
        m_rbdl_to_external[rbdl_idx] = external_idx;
        m_external_to_rbdl[external_idx] = rbdl_idx;
      }
    }
  }

//  void ModelHelper::updateRBDL(std::vector<double>& joint_positions, geometry_msgs::Pose& pose)
//  {
//    RigidBodyDynamics::Math::VectorNd q(m_rbdl_model.dof_count), q_dot(m_rbdl_model.dof_count), qd_dot(m_rbdl_model.dof_count);
//
//    double roll, pitch, yaw;
//    tf::Quaternion quat;
//    tf::quaternionMsgToTF(m_odom.pose.pose.orientation, quat);
//    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);
//    q(0) = m_odom.pose.pose.position.x;
//    q(1) = m_odom.pose.pose.position.y;
//    q(2) = m_odom.pose.pose.position.z;
//    q(3) = roll;
//    q(4) = pitch;
//    q(5) = yaw;
//
//    //eigen doesn't initialize things to zero
//    for(unsigned int i = 0; i < 6; i++)
//    {
//      q_dot(i) = 0;
//      qd_dot(i) = 0;
//    }
//
//    for(unsigned int i = 6; i < m_model.dof_count; i++)
//    {
//      unsigned int external_idx = m_rbdl_to_external[i];
//      q(i) = msg.position[external_idx];
//      q_dot(i) = msg.position[external_idx];
//      qd_dot(i) = 0;
//    }
//
//    RigidBodyDynamics::UpdateKinematics(m_rbdl_model, q, q_dot, qd_dot);
//
//    RigidBodyDynamics::CompositeRigidBodyAlgorithm(m_rbdl_model, q, m_inertia_matrix, false);
//  }

//  RigidBodyDynamics::Math::SpatialMatrix ModelHelper::getInertiaMatrix() //M(q) (n+6)x(n+6)
//  {
//    return m_inertia_matrix;
//  }
//
//  RigidBodyDynamics::Math::SpatialMatrix ModelHelper::getBiasMatrix() //N(q,qd) (n+6)
//  {
//    return m_rbdl_model.pA;
//  }
//
//  RigidBodyDynamics::Math::SpatialMatrix ModelHelper::getTorqueSelectionMatrix() //S, (n+6)xn, probably 0,I
//  {
//    return m_rbdl_model.IA;
//  }
}

