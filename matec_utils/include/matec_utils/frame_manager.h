#ifndef FRAME_MANAGER_H
#define FRAME_MANAGER_H
#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_utils/model_helper.h"
 
namespace matec_utils
{
  class FrameManager
  {
  public:
    FrameManager(const ros::NodeHandle& nh);
    ~FrameManager();
    void spin();
 
  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    tf::TransformBroadcaster m_tf_broadcaster;

    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;
    
    matec_utils::ModelHelper* m_model;
    std::vector<std::string> m_joint_names;
    geometry_msgs::Point m_com;

    void initModel();
    void updateFrames();

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //FRAME_MANAGER_H
