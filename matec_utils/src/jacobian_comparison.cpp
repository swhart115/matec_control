#include "ros/ros.h"
#include "ros/package.h"

#include "kdl/jacobian.hpp"
#include "kdl/treejnttojacsolver.hpp"

#include <rbdl/rbdl.h>
#include <rbdl/Model.h>
#include <rbdl/Dynamics.h>
#include <rbdl/rbdl_utils.h>

#include <matec_utils/model_helper.h>
#include <matec_utils/rbdl_urdfreader.h>

using namespace matec_utils;
int main()
{
  std::string filename = ros::package::getPath("atlas_sim") + "/models/atlas_full_utorso_root/urdf/atlas_full.urdf";
  std::cerr << "Reading from urdf file " << filename << std::endl;

  std::vector<std::string> canon_joint_order;
  canon_joint_order.push_back("back_bkz");
  canon_joint_order.push_back("back_bky");
  canon_joint_order.push_back("back_bkx");
  canon_joint_order.push_back("l_arm_shy");
  canon_joint_order.push_back("l_arm_shx");
  canon_joint_order.push_back("l_arm_ely");
  canon_joint_order.push_back("l_arm_elx");
  canon_joint_order.push_back("l_arm_wry");
  canon_joint_order.push_back("l_arm_wrx");
  canon_joint_order.push_back("neck_ry");
  canon_joint_order.push_back("r_arm_shy");
  canon_joint_order.push_back("r_arm_shx");
  canon_joint_order.push_back("r_arm_ely");
  canon_joint_order.push_back("r_arm_elx");
  canon_joint_order.push_back("r_arm_wry");
  canon_joint_order.push_back("r_arm_wrx");
  canon_joint_order.push_back("l_leg_hpz");
  canon_joint_order.push_back("l_leg_hpx");
  canon_joint_order.push_back("l_leg_hpy");
  canon_joint_order.push_back("l_leg_kny");
  canon_joint_order.push_back("l_leg_aky");
  canon_joint_order.push_back("l_leg_akx");
  canon_joint_order.push_back("r_leg_hpz");
  canon_joint_order.push_back("r_leg_hpx");
  canon_joint_order.push_back("r_leg_hpy");
  canon_joint_order.push_back("r_leg_kny");
  canon_joint_order.push_back("r_leg_aky");
  canon_joint_order.push_back("r_leg_akx");

  std::vector<std::string> canon_field_names;
  canon_field_names.push_back("p_x");
  canon_field_names.push_back("p_y");
  canon_field_names.push_back("p_z");
  canon_field_names.push_back("r_x");
  canon_field_names.push_back("r_y");
  canon_field_names.push_back("r_z");

  urdf::Model urdf_model;
  urdf_model.initFile(filename);
  KDL::Tree tree;
  if(!kdl_parser::treeFromUrdfModel((const urdf::Model) urdf_model, tree))
  {
    std::cerr << "Failed to parse urdf model!";
  }

  KDL::JntArray kdl_q(28);
  Eigen::VectorXd rbdl_q(28+6);
  rbdl_q(0) = 0;
  rbdl_q(1) = 0;
  rbdl_q(2) = 0;
  rbdl_q(3) = 0;
  rbdl_q(4) = 0;
  rbdl_q(5) = 0;
  for(unsigned int i = 0; i < 28; i++)
  {
    kdl_q(i) = 0.0;
    rbdl_q(i+6) = 0.0;
  }

  kdl_q(19) = 0.5;
  rbdl_q(19+6) = 0.5;

  //calculate with KDL
  KDL::TreeJntToJacSolver kdl_solver(tree);
  KDL::Jacobian kdl_jac(28);
  kdl_solver.JntToJac(kdl_q, kdl_jac, "l_foot");
  std::cerr << "Computed KDL jacobian size " << kdl_jac.rows() << "x" << kdl_jac.columns() << std::endl;

  std::vector<int> canon_to_kdl(28);
  for(KDL::SegmentMap::const_iterator iter = tree.getSegments().begin(); iter != tree.getSegments().end(); iter++)
  {
    std::string joint_name = iter->second.segment.getJoint().getName();
    int kdl_idx = iter->second.q_nr;
    int canon_idx = std::find(canon_joint_order.begin(), canon_joint_order.end(), joint_name) - canon_joint_order.begin();
    canon_to_kdl[canon_idx] = kdl_idx;
  }

  /*
  std::map<std::basic_string<char>, KDL::TreeElement>::const_iterator {aka std::_Rb_tree_const_iterator<std::pair<const std::basic_string<char>, KDL::TreeElement> >}’
  std::map<std::basic_string<char>, KDL::TreeElement>::iterator {aka std::_Rb_tree_iterator<std::pair<const std::basic_string<char>, KDL::TreeElement> >}’
*/

  //calculate with RBDL
  RigidBodyDynamics::Model rbdl_model;
  RigidBodyDynamics::Addons::read_urdf_model(urdf_model, rbdl_model, false);
  int body_id = rbdl_model.GetBodyId("l_foot");
  std::cerr << "RBDL body id for l_foot is " << body_id << std::endl;

  std::vector<int> canon_to_rbdl(28);
  for(unsigned int i = 0; i < rbdl_model.mBodies.size(); i++)
  {
    if(i > 6)
    {
      int rbdl_idx = i - 1;
      std::string joint_name = urdf_model.getLink(rbdl_model.GetBodyName(i))->parent_joint->name;
      int canon_idx = std::find(canon_joint_order.begin(), canon_joint_order.end(), joint_name) - canon_joint_order.begin();
      canon_to_rbdl[canon_idx] = rbdl_idx;
//      std::cerr << joint_name << " is rbdl " << rbdl_idx << ", " << canon_idx << " in canon." << std::endl;
    }
  }

  //RBDL version wins!
  RigidBodyDynamics::Math::MatrixNd Jpv, Jpw, Jc;
  Jpv.resize(3, 28+6);
  Jpw.resize(3, 28+6);
  Jc.resize(6, 28+6);
  RigidBodyDynamics::Math::Vector3d contact_point(0, 0, 0);
  RigidBodyDynamics::CalcPointJacobian(rbdl_model, rbdl_q, body_id, contact_point, Jpv, true);
  RigidBodyDynamics::CalcPointJacobianW(rbdl_model, rbdl_q, body_id, contact_point, Jpw, true);
  Jc.topRows(3) = Jpv;
  Jc.bottomRows(3) = Jpw;

  std::cerr << Jpv;

//  for(unsigned int i = 0; i < 3; i++)
//  {
//    std::cerr << canon_field_names[i] << "\t";
//    for(unsigned int j = 0; j < 28+6; j++)
//    {
//      if(j != 0)
//      {
//        std::cerr << "\t\t";
//      }
//
//      if(j < 6)
//      {
//        if(i == j)
//        {
//          std::cerr << "1";
//        }
//        else
//        {
//          std::cerr << "0";
//        }
//      }
//      else
//      {
//        std::cerr << kdl_jac(i, canon_to_kdl[j-6]);
//      }
//    }
//    std::cerr << std::endl;
//  }


  //compare
  std::cerr << std::endl;
  std::cerr << "KDL:\t";
  for(unsigned int j = 0; j < 6; j++)
  {
    std::cerr << "floating" << "\t";
  }
  for(unsigned int j = 0; j < 28; j++)
  {
    std::cerr << canon_joint_order[j] << "\t";
  }
  std::cerr << std::endl;
  for(unsigned int i = 0; i < 6; i++)
  {
    std::cerr << canon_field_names[i] << "\t";
    for(unsigned int j = 0; j < 28+6; j++)
    {
      if(j != 0)
      {
        std::cerr << "\t\t";
      }

      if(j < 6)
      {
        if(i == j)
        {
          std::cerr << "1";
        }
        else
        {
          std::cerr << "0";
        }
      }
      else
      {
        std::cerr << kdl_jac(i, canon_to_kdl[j-6]);
      }
    }
    std::cerr << std::endl;
  }

  std::cerr << std::endl;
  std::cerr << std::endl;

  std::cerr << "RBDL:\t";

  for(unsigned int j = 0; j < 6; j++)
  {
    std::cerr << "floating" << "\t";
  }
  for(unsigned int j = 0; j < 28; j++)
  {
    std::cerr << canon_joint_order[j] << "\t";
  }
  std::cerr << std::endl;
  for(unsigned int i = 0; i < 6; i++)
  {
    std::cerr << canon_field_names[i] << "\t";
    for(unsigned int j = 0; j < 28+6; j++)
    {
      if(j != 0)
      {
        std::cerr << "\t\t";
      }

      std::cerr << Jc(i, j);
    }
    std::cerr << std::endl;
  }
}
