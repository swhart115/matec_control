#ifndef ADAPTIVE_WINDOW_ESTIMATOR_H
#define ADAPTIVE_WINDOW_ESTIMATOR_H

#include "rolling_buffer.h"

namespace estimators
{
  class AdaptiveWindowEstimator
  {
  public:
    AdaptiveWindowEstimator(unsigned int maximum_size, double noise_margin, double sampling_rate) :
        m_buffer(maximum_size)
    {
      m_noise_margin = noise_margin;
      m_T = 1.0 / sampling_rate;
      m_k = 0;
    }

    ~AdaptiveWindowEstimator()
    {
    }

    void process(ScalarSample sample, double& position, double& velocity, double& acceleration)
    {
      std::cerr << "----------------------------\nGot value " << sample.value << std::endl;

      m_buffer.push(sample);
      m_k++; //TODO: make this algorithm LTI so we don't run out of k's eventually

      if(m_buffer.size() < 3) //not enough data
      {
        position = sample.value;
        velocity = 0;
        acceleration = 0;
        return;
      }

      double s1 = m_buffer(0).value + m_buffer(1).value + m_buffer(2).value;
      double s2 = m_buffer(1).value + 2 * m_buffer(2).value;
      double s3 = m_buffer(1).value + 4 * m_buffer(2).value;
      for(unsigned int n = 3; n <= m_buffer.size(); n++)
      {
        double dn = (double) n;
        double dn2 = dn * dn;
        s1 += m_buffer(n-1).value;
        s2 += dn * m_buffer(n-1).value;
        s3 += dn2 * m_buffer(n-1).value;
        double A = 3.0 * dn * (2.0 * dn2 - dn - 1.0) - 10.0 * m_k * dn * (dn - 1.0);
        double B = -4.0 * dn * (8.0 * dn + 1.0) + 6.0 * (10.0 * m_k * dn + 1.0);
        double C = 30.0 * (dn - 2.0 * m_k);
        double D = 10.0 * m_k * m_k * dn * (dn - 1.0) - 6.0 * m_k * dn * (2.0 * dn2 + dn - 1.0) + dn * (3.0 * dn2 * dn - dn - 2.0);
        double E = 8.0 * m_k * dn * (8.0 * dn + 1.0) - 6.0 * dn * (2.0 * dn2 - dn - 1.0) - 12.0 * m_k * (5.0 * m_k * dn + 1.0);
        double F = 10.0 * dn * (dn - 1.0) + 60.0 * m_k * (m_k - dn);
        double denom = dn * (dn - 1.0) * (dn + 1.0) * (dn + 2.0) * (dn + 3.0); //todo: calculate beforehand
        double c1 = 30.0 * (dn * s1 * (dn - 1) - 6.0 * dn * s2 + 6.0 * s3) / (m_T * m_T * denom);
        double c2 = 6.0 * (A * s1 + B * s2 + C * s3) / (m_T * denom);
        double c3 = 3.0 * (D * s1 + E * s2 + F * s3) / (denom);

        std::cerr << "s1:" << s1 << " s2:" << s2 << " s3:" << s3 << std::endl;
        std::cerr << "A:" << A << " B:" << B << " C:" << C << std::endl;
        std::cerr << "D:" << D << " E:" << E << " F:" << F << std::endl;
        std::cerr << "c1:" << c1 << " c2:" << c2 << " c3:" << c3 << std::endl;
        std::cerr << "n=" << dn << ", k=" << m_k << " T=" << m_T << " denom=" << denom << std::endl;

        for(unsigned int i = 1; i < n; i++)
        {
          double iT = (m_k - i) * m_T;
          double fit_pos = c1 * iT * iT + c2 * iT + c3;
          double measured_pos = m_buffer(i-1).value;
          double error = fabs(measured_pos - fit_pos);

          std::cerr << "i=" << i << ": " << measured_pos << " vs " << fit_pos << std::endl;

          if(error > m_noise_margin)
          {
            std::cerr << "error was " << error << "!" << std::endl;
            return;
          }
        }
        double kT = m_k * m_T;
        position = c1 * kT * kT + c2 * kT + c3;
        velocity = 2 * kT * c1 + c2;
        acceleration = 2 * c1;
      }
    }

  private:
    RollingBuffer<ScalarSample> m_buffer;
    double m_noise_margin;
    double m_T;
    uint64_t m_k;
  };
}

#endif //ADAPTIVE_WINDOW_ESTIMATOR_H
