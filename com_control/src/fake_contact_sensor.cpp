#include "ros/ros.h"
#include "shared_memory_interface/shared_memory_publisher.hpp"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "matec_msgs/ContactSpecifications.h"

using namespace shared_memory_interface;
int main(int argc, char **argv)
{
  ros::init(argc, argv, "fake_contact_sensor");
  ros::NodeHandle n;

  //shared memory interface connections
  shared_memory_interface::Publisher<matec_msgs::ContactSpecifications> contact_pub;
  contact_pub.advertise("/contacts");

  matec_msgs::ContactSpecification left_contact, right_contact;
  left_contact.contact_point.position.x = 0.049;
  left_contact.contact_point.position.y = 0.0;
  left_contact.contact_point.position.z = -0.081;
  left_contact.contact_point.orientation.w = 1.0;
  left_contact.contact_surface_height = 0.27;
  left_contact.contact_surface_width = 0.1301;
  left_contact.contact_type = matec_msgs::ContactSpecification::FLAT_CONTACT;
  left_contact.linear_friction = 10.0;
  left_contact.rotational_friction = 10.0;
  right_contact = left_contact;

  left_contact.link_name = "l_foot";
  right_contact.link_name = "r_foot";

  matec_msgs::ContactSpecifications contacts;
  contacts.contacts.push_back(left_contact);
  contacts.contacts.push_back(right_contact);

  ROS_INFO("FakeContactSensor: starting to publish fake data!");
  ros::Rate loop_rate(100);
  while(ros::ok())
  {
    contact_pub.publish(contacts);
    loop_rate.sleep();
  }

  return 0;
}
