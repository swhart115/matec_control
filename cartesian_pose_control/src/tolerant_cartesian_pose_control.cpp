#include "cartesian_pose_control/tolerant_cartesian_pose_control.h"

namespace cartesian_pose_control
{
  TolerantCartesianPoseControl::TolerantCartesianPoseControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    srand(time(NULL));
    matec_utils::blockOnSMJointNames(m_joint_names);
    matec_utils::blockOnTaskRegistration(m_nh.getNamespace());
    matec_utils::blockOnROSTime();

    m_nh.param("loop_rate", m_loop_rate, 50.0);
    m_nh.param("interpolator", m_interpolator, std::string(m_nh.getNamespace()));

    m_nh.param("controlled_frame", m_controlled_frame, std::string(""));

    if(m_controlled_frame.length() == 0)
    {
      ROS_FATAL("CPC: controlled_frame must be specified for TolerantCartesianPoseControl to work!");
      ros::shutdown();
      return;
    }

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints))
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("JointPositionControl couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }

    initModel();
    findNearestActuatedLink(m_controlled_frame);
    findLongestControlChain();
    m_chain_base_frame = getChainBase(m_chain);

    matec_utils::getFPVector(m_nh, "controlled_joint_weights", m_controlled_joints.size(), 1.0, m_controlled_joint_weights);
    m_cartesian_weights.resize(6, 0.0);
    unpackPositionTolerance(Config::CENTIMETER);
    unpackOrientationTolerance(Config::SPHERE);
    m_fwd_vel_solver = new KDL::ChainFkSolverVel_recursive(m_chain);
    m_fwd_pos_solver = new KDL::ChainFkSolverPos_recursive(m_chain);
    m_inv_vel_solver = new KDL::ChainIkSolverVel_wdls(m_chain);
    configureWeights();

    m_field_names.push_back("p_x");
    m_field_names.push_back("p_y");
    m_field_names.push_back("p_z");
    m_field_names.push_back("r_x");
    m_field_names.push_back("r_y");
    m_field_names.push_back("r_z");
    m_kps.resize(m_field_names.size(), 0.0);
    m_kvs.resize(m_field_names.size(), 0.0);
    m_kis.resize(m_field_names.size(), 0.0);
    m_i_clamps.resize(m_field_names.size(), 0.0);
    m_i_livebands.resize(m_field_names.size(), 0.0);
    m_i_accumulators.resize(m_field_names.size(), 0.0);
    getGains();

    m_error_pub = m_nh.advertise<geometry_msgs::Twist>("cartesian_pose_error", 1, true);
    m_current_pub = m_nh.advertise<geometry_msgs::PoseStamped>("measured_cartesian_pose", 1, true);
    m_target_pub = m_nh.advertise<geometry_msgs::PoseStamped>("target_cartesian_pose", 1, true);

    m_first_callback = true;
    m_last_deltas.resize(m_controlled_joints.size(), 0.0);

    m_command_pub.advertise("task_command");
    m_pose_carrot_sub.subscribe("/" + m_interpolator + "/carrot");
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&TolerantCartesianPoseControl::sensorCallback, this, _1));

    m_config_server = m_nh.advertiseService("configure", &TolerantCartesianPoseControl::configureCallback, this);
  }

  TolerantCartesianPoseControl::~TolerantCartesianPoseControl()
  {

  }

  void TolerantCartesianPoseControl::findNearestActuatedLink(std::string frame)
  {
    boost::shared_ptr<const urdf::Link> link = m_urdf_model.getLink(frame);
    if(link)
    {
      m_last_controlled_link = frame;
    }
    else
    {
      std::string parent;
      while(ros::ok() && !m_tf_listener.getParent(frame, ros::Time(0), parent))
      {
        ROS_WARN("%s is waiting for tf to find the parent of %s", m_nh.getNamespace().c_str(), frame.c_str());
      }

      findNearestActuatedLink(parent);
    }
  }

  bool TolerantCartesianPoseControl::configureCallback(matec_msgs::ConfigureCartesianControl::Request& req, matec_msgs::ConfigureCartesianControl::Response& res)
  {
    if(req.joint_weights.size() == m_controlled_joints.size())
    {
      m_controlled_joint_weights = req.joint_weights;
    }

    unpackPositionTolerance(req.allowable_position_variance);
    unpackOrientationTolerance(req.allowable_angle_variance);

    configureWeights();
    return true;
  }

  void TolerantCartesianPoseControl::unpackPositionTolerance(int preset)
  {
    switch(preset)
    {
    case Config::USE_CURRENT_POSITION_VARIANCE:
    {
      return;
    }
    case Config::MILLIMETER:
    {
      m_tolerance.x = 0.001;
      m_tolerance.y = 0.001;
      m_tolerance.z = 0.001;
      m_cartesian_weights[0] = 1.0;
      m_cartesian_weights[1] = 1.0;
      m_cartesian_weights[2] = 1.0;
      return;
    }
    case Config::CENTIMETER:
    {
      m_tolerance.x = 0.01;
      m_tolerance.y = 0.01;
      m_tolerance.z = 0.01;
      m_cartesian_weights[0] = 1.0;
      m_cartesian_weights[1] = 1.0;
      m_cartesian_weights[2] = 1.0;
      return;
    }
    case Config::DECIMETER:
    {
      m_tolerance.x = 0.1;
      m_tolerance.y = 0.1;
      m_tolerance.z = 0.1;
      m_cartesian_weights[0] = 0.1;
      m_cartesian_weights[1] = 0.1;
      m_cartesian_weights[2] = 0.1;
      return;
    }
    default:
    {
      ROS_ERROR("Unknown position preset requested!");
      return;
    }
    }
  }

  void TolerantCartesianPoseControl::unpackOrientationTolerance(int preset)
  {
    switch(preset)
    {
    case Config::USE_CURRENT_ANGLE_VARIANCE:
    {
      return;
    }
    case Config::FULL_CYLINDER:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::HALF_CYLINDER:
    {
      m_tolerance.roll = M_PI / 2;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.01;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::QUARTER_CYLINDER:
    {
      m_tolerance.roll = M_PI / 4;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.05;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::EIGHTH_CYLINDER:
    {
      m_tolerance.roll = M_PI / 8;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::SIXTEENTH_CYLINDER:
    {
      m_tolerance.roll = M_PI / 16;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::SIXTEENTH_CONE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 16;
      m_tolerance.yaw = M_PI / 16;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::EIGHTH_CONE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 8;
      m_tolerance.yaw = M_PI / 8;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::QUARTER_CONE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 4;
      m_tolerance.yaw = M_PI / 4;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.05;
      m_cartesian_weights[5] = 0.05;
      return;
    }
    case Config::HEMISPHERE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI / 2;
      m_tolerance.yaw = M_PI / 2;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.01;
      m_cartesian_weights[5] = 0.01;
      return;
    }
    case Config::SPHERE:
    {
      m_tolerance.roll = M_PI;
      m_tolerance.pitch = M_PI;
      m_tolerance.yaw = M_PI;
      m_cartesian_weights[3] = 0.0;
      m_cartesian_weights[4] = 0.0;
      m_cartesian_weights[5] = 0.0;
      return;
    }
    case Config::EXACT_ANGLE:
    {
      m_tolerance.roll = EPSILON_ANGLE;
      m_tolerance.pitch = EPSILON_ANGLE;
      m_tolerance.yaw = EPSILON_ANGLE;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::SIXTEENTH:
    {
      m_tolerance.roll = M_PI / 16;
      m_tolerance.pitch = M_PI / 16;
      m_tolerance.yaw = M_PI / 16;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::EIGHTH:
    {
      m_tolerance.roll = M_PI / 8;
      m_tolerance.pitch = M_PI / 8;
      m_tolerance.yaw = M_PI / 8;
      m_cartesian_weights[3] = 0.1;
      m_cartesian_weights[4] = 0.1;
      m_cartesian_weights[5] = 0.1;
      return;
    }
    case Config::QUARTER:
    {
      m_tolerance.roll = M_PI / 4;
      m_tolerance.pitch = M_PI / 4;
      m_tolerance.yaw = M_PI / 4;
      m_cartesian_weights[3] = 0.05;
      m_cartesian_weights[4] = 0.05;
      m_cartesian_weights[5] = 0.05;
      return;
    }
    case Config::HALF:
    {
      m_tolerance.roll = M_PI / 2;
      m_tolerance.pitch = M_PI / 2;
      m_tolerance.yaw = M_PI / 2;
      m_cartesian_weights[3] = 0.01;
      m_cartesian_weights[4] = 0.01;
      m_cartesian_weights[5] = 0.01;
      return;
    }
    default:
    {
      ROS_ERROR("Unknown orientation preset requested!");
      return;
    }
    }
  }

  void TolerantCartesianPoseControl::configureWeights()
  {
    //figure out what parts of the chain we can actually use and weight the solver accordingly
    Eigen::MatrixXd cartesian_weight_matrix = Eigen::MatrixXd::Identity(6, 6);

    if(m_cartesian_weights.size() == 6)
    {
      for(unsigned int i = 0; i < 6; i++)
      {
        cartesian_weight_matrix(i, i) = m_cartesian_weights[i];
      }
    }

    Eigen::MatrixXd joint_weight_matrix = Eigen::MatrixXd::Identity(m_chain.getNrOfJoints(), m_chain.getNrOfJoints());
    for(unsigned int i = 0; i < m_chain.getNrOfSegments(); i++) //possibly need to decrease this range by 1
    {
      std::string joint_name = m_chain.getSegment(i).getJoint().getName();
      ROS_DEBUG("Processing segment %d with joint %s", i, joint_name.c_str());

      unsigned int controlled_joint_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), joint_name) - m_controlled_joints.begin();
      if(controlled_joint_idx == m_controlled_joints.size())
      {
        joint_weight_matrix(i, i) = 0; //weight 0 ==> don't move the joint
        ROS_DEBUG("Joint %s is not controllable.", joint_name.c_str());
      }
      else
      {
        assert(controlled_joint_idx < m_controlled_joints.size());
        assert(m_controlled_joints.size() == m_controlled_joint_weights.size());
        joint_weight_matrix(i, i) = m_controlled_joint_weights[controlled_joint_idx];
        ROS_DEBUG("Joint %s is controllable.", joint_name.c_str());
      }
    }
    m_inv_vel_solver->setWeightJS(joint_weight_matrix);
    m_inv_vel_solver->setWeightTS(cartesian_weight_matrix);
  }

  void TolerantCartesianPoseControl::findLongestControlChain()
  {
    //NOTE: currently assuming that all controlled joints are in the same chain!
    unsigned int longest_chain_length = 0;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      if(std::find(m_joint_names.begin(), m_joint_names.end(), m_controlled_joints[i]) == m_joint_names.end())
      {
        ROS_FATAL("Tried to control a joint that the SMI didn't know about! Shutting down!");
        ros::shutdown();
      }

      std::string parent_frame = m_urdf_model.getJoint(m_controlled_joints[i])->parent_link_name;

      KDL::Chain chain;
      m_tree.getChain(parent_frame, m_last_controlled_link, chain);

      if(chain.getNrOfJoints() > longest_chain_length)
      {
        longest_chain_length = chain.getNrOfJoints();
        m_chain = chain;
      }
    }

    ROS_INFO("Using chain of %d joints from %s to %s", m_chain.getNrOfJoints(), m_chain.getSegment(0).getJoint().getName().c_str(), m_chain.getSegment(m_chain.getNrOfSegments() - 1).getJoint().getName().c_str());
  }

  bool TolerantCartesianPoseControl::addControlledFrameOffsetToChain(KDL::Chain& chain)
  {
    tf::StampedTransform controlled_frame_offset;
    if(m_tf_listener.canTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0)) && m_tf_listener.canTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0)))
    {
      m_tf_listener.lookupTransform(m_controlled_frame, m_last_controlled_link, ros::Time(0), controlled_frame_offset);
    }
    else
    {
      ROS_ERROR("Couldn't find transform from the last controllable link (%s) and the controlled frame (%s)", m_last_controlled_link.c_str(), m_controlled_frame.c_str());
      return false;
    }

    KDL::Frame fixed_offset;
    tf::transformTFToKDL(controlled_frame_offset.inverse(), fixed_offset);
    KDL::Segment fixedSegment = KDL::Segment(m_controlled_frame, KDL::Joint(m_controlled_frame + "_fixed", KDL::Joint::None), fixed_offset);
    chain.addSegment(fixedSegment);

    return true;
  }

  void TolerantCartesianPoseControl::getGains()
  {
    m_nh.param("gains/center_spring", m_center_spring, 0.0);
    for(unsigned int i = 0; i < m_field_names.size(); i++)
    {
      boost::mutex::scoped_lock lock(m_mutex);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/p", m_kps[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/v", m_kvs[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i", m_kis[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i_clamp", m_i_clamps[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i_liveband", m_i_livebands[i]);
    }

    m_nh.param("max_joint_velocity", m_max_velocity, 3.0);
    m_nh.param("num_whole_range_attempts", m_num_whole_range_attempts, 25);
    m_nh.param("num_best_attempts", m_num_best_attempts, 25);
  }

  double clamp(double a, double b, double c)
  {
    return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
  }

  double sign(double v)
  {
    if(v > 0.0)
    {
      return 1.0;
    }
    else if(v < 0.0)
    {
      return -1.0;
    }
    else
    {
      return 0.0;
    }
  }

  std::vector<double> extractPoseVector(geometry_msgs::PoseStamped pose)
  {
    double roll, pitch, yaw;
    tf::Quaternion quat;
    tf::quaternionMsgToTF(pose.pose.orientation, quat);
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

    std::vector<double> vector; //should match m_field_names
    vector.push_back(pose.pose.position.x);
    vector.push_back(pose.pose.position.y);
    vector.push_back(pose.pose.position.z);
    vector.push_back(roll);
    vector.push_back(pitch);
    vector.push_back(yaw);

    return vector;
  }

  std::vector<double> extractPoseVector(KDL::FrameVel frame_vel)
  {
    std::vector<double> vector;
    vector.push_back(frame_vel.p.v.x());
    vector.push_back(frame_vel.p.v.y());
    vector.push_back(frame_vel.p.v.z());
    vector.push_back(frame_vel.M.w.x());
    vector.push_back(frame_vel.M.w.y());
    vector.push_back(frame_vel.M.w.z());
    return vector;
  }

  KDL::Twist poseVectorToTwist(std::vector<double> pose_vector)
  {
    assert(pose_vector.size() == 6);
    return KDL::Twist(KDL::Vector(pose_vector[0], pose_vector[1], pose_vector[2]), KDL::Vector(pose_vector[3], pose_vector[4], pose_vector[5]));
  }

  KDL::JntArray stdToKDL(std::vector<double> array, std::vector<unsigned char> indices = std::vector<unsigned char>())
  {
    if(indices.size() > 0) //use subset of array
    {
      KDL::JntArray kdl_array(indices.size());
      for(unsigned int i = 0; i < indices.size(); i++)
      {
        kdl_array(i) = array[indices[i]];
      }
      return kdl_array;
    }
    else //use whole array
    {
      KDL::JntArray kdl_array(array.size());
      for(unsigned int i = 0; i < array.size(); i++)
      {
        kdl_array(i) = array[i];
      }
      return kdl_array;
    }
  }

  std::vector<double> KDLToStd(KDL::JntArray kdl_array, std::vector<unsigned char> indices = std::vector<unsigned char>())
  {
    std::vector<double> array;
    if(indices.size() > 0) //use subset of array
    {
      for(unsigned int i = 0; i < indices.size(); i++)
      {
        assert(!isnan(kdl_array(indices[i])));
        array.push_back(kdl_array(indices[i]));
      }
    }
    else //use whole array
    {
      for(unsigned int i = 0; i < kdl_array.rows() * kdl_array.columns(); i++)
      {
        assert(!isnan(kdl_array(i)));
        array.push_back(kdl_array(i));
      }
    }
    return array;
  }

  double smashSmall(double val)
  {
    if(val < 1)
    {
      return 0;
    }
    return val;
  }

  double scoreFrame(KDL::Frame iter, KDL::Frame target, Tolerance tol)
  {
    KDL::Frame frame_shift = target.Inverse() * iter;
    double delta_roll, delta_pitch, delta_yaw;
    frame_shift.M.GetRPY(delta_roll, delta_pitch, delta_yaw);

//    ROS_INFO("(%g,%g,%g):(%g,%g,%g)", tol.x, tol.y, tol.z, tol.roll, tol.pitch, tol.yaw);

    double x_score = smashSmall(std::abs(frame_shift.p.x()) / tol.x);
    double y_score = smashSmall(std::abs(frame_shift.p.y()) / tol.y);
    double z_score = smashSmall(std::abs(frame_shift.p.z()) / tol.z);
    double roll_score = smashSmall(std::abs(delta_roll) / tol.roll);
    double pitch_score = smashSmall(std::abs(delta_pitch) / tol.pitch);
    double yaw_score = smashSmall(std::abs(delta_yaw) / tol.yaw);

    return (x_score + y_score + z_score + roll_score + pitch_score + yaw_score) / 6;
  }

  double scoreMovement(KDL::JntArray start, KDL::JntArray iter)
  {
    double sum = 0;

    assert(start.rows()*start.columns() == iter.rows()*iter.columns());

    for(unsigned int i = 0; i < start.rows() * start.columns(); i++)
    {
      double err = iter(i) - start(i);
      sum += err * err;
    }

    return sum;
  }

  bool TolerantCartesianPoseControl::getKDLSolution(matec_msgs::FullJointStates& current, geometry_msgs::PoseStamped pose_carrot, geometry_msgs::PoseStamped current_pose, KDL::JntArray& solution)
  {
    KDL::JntArray q, qd;
    q = stdToKDL(current.position, m_joint_map);
    qd = stdToKDL(current.velocity, m_joint_map);
    KDL::JntArrayVel q_qd(q, qd);
    KDL::FrameVel frame_vel;
    m_fwd_vel_solver->JntToCart(q_qd, frame_vel);

    std::vector<double> current_pose_velocity_vector = extractPoseVector(frame_vel);
    std::vector<double> current_pose_vector = extractPoseVector(current_pose);
    std::vector<double> carrot_pose_vector = extractPoseVector(pose_carrot);

    std::vector<double> control_pose_vector(carrot_pose_vector.size());
    std::vector<double> error_pose_vector(carrot_pose_vector.size());

    for(unsigned int i = 0; i < m_field_names.size(); i++)
    {
      boost::mutex::scoped_lock lock(m_mutex);
      double pos_error = carrot_pose_vector[i] - current_pose_vector[i];
      double vel_error = 0 - current_pose_velocity_vector[i]; //TODO: feed forward velocity and acceleration?

      if(fabs(pos_error) <= m_i_livebands[i])
      {
        m_i_accumulators[i] = clamp(m_i_accumulators[i] + pos_error, -m_i_clamps[i], m_i_clamps[i]);
      }
      else
      {
        m_i_accumulators[i] = 0;
      }

      control_pose_vector[i] = m_kps[i] * pos_error + m_kvs[i] * vel_error + m_kis[i] * m_i_accumulators[i];
      error_pose_vector[i] = pos_error;
    }

    KDL::JntArray qd_soln;
    if(m_inv_vel_solver->CartToJnt(q, poseVectorToTwist(control_pose_vector), qd_soln) != 0)
    {
      return false;
    }

    double max_output_vel = 0.001; //nonzero to prevent division by zero
    for(unsigned int i = 0; i < qd_soln.rows(); i++)
    {
      if(fabs(qd_soln(i)) > max_output_vel)
      {
        max_output_vel = fabs(qd_soln(i));
      }
    }

    double scale_factor = std::min(1.0, m_max_velocity / max_output_vel);

    solution = q;
    for(unsigned int i = 0; i < solution.rows(); i++)
    {
      solution(i) += scale_factor * qd_soln(i) * m_dt; //todo: use trapezoidal integration
    }

    return true;
  }

  void TolerantCartesianPoseControl::getSearchBounds(KDL::JntArray& current, double dt, double max_velocity, std::vector<double>& mins, std::vector<double>& maxes)
  {
    assert(m_controlled_joint_mins.size() == m_joint_map.size());
    assert(m_controlled_joint_maxes.size() == m_joint_map.size());
    mins = m_controlled_joint_mins;
    maxes = m_controlled_joint_maxes;
    for(unsigned int i = 0; i < m_chain.getNrOfJoints(); i++)
    {
      current(i) = clamp(current(i), mins[i], maxes[i]); //fix things that shouldn't have to be fixed...

      if(m_controlled_joint_weights[i] == 0.0)
      {
        mins[i] = current(i);
        maxes[i] = current(i);
      }
      else
      {
        mins[i] = std::max(mins[i], current(i) - max_velocity * dt);
        maxes[i] = std::min(maxes[i], current(i) + max_velocity * dt);
      }
      ROS_DEBUG("Bounding %s between %g and %g", m_controlled_joints[i].c_str(), mins[i], maxes[i]);
    }
  }

  std::string TolerantCartesianPoseControl::getChainBase(KDL::Chain& chain)
  {
    return m_urdf_model.getJoint(m_chain.getSegment(0).getJoint().getName())->parent_link_name;
  }

  bool TolerantCartesianPoseControl::moveCarrotToChainBaseFrame(geometry_msgs::PoseStamped& pose_carrot)
  {
    if(!m_tf_listener.canTransform(m_chain_base_frame, pose_carrot.header.frame_id, pose_carrot.header.stamp))
    {
      pose_carrot.header.stamp = ros::Time(0);
    }
    if(!m_tf_listener.canTransform(m_chain_base_frame, pose_carrot.header.frame_id, pose_carrot.header.stamp))
    {
      ROS_ERROR("Can't transform from %s to %s at any time!", m_chain_base_frame.c_str(), pose_carrot.header.frame_id.c_str());
      return false;
    }
    m_tf_listener.transformPose(m_chain_base_frame, pose_carrot, pose_carrot);
    return true;
  }

  void TolerantCartesianPoseControl::replaceIfBetter(KDL::JntArray& iter_joints, KDL::JntArray& current_joints, KDL::Frame& target_frame, KDL::JntArray& best_joints, KDL::Frame& best_frame, double& best_accuracy_score, double& best_movement_score)
  {
    KDL::Frame iter_frame;
    m_fwd_pos_solver->JntToCart(iter_joints, iter_frame);
    double accuracy_score = scoreFrame(iter_frame, target_frame, m_tolerance);
    if(accuracy_score <= best_accuracy_score)
    {
      double movement_score = scoreMovement(current_joints, iter_joints);
      if((accuracy_score < best_accuracy_score) || (movement_score < best_movement_score))
      {
        best_accuracy_score = accuracy_score;
        best_movement_score = movement_score;
        best_joints = iter_joints;
        best_frame = iter_frame;
      }
    }
  }

  void TolerantCartesianPoseControl::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    matec_msgs::TaskCommand task_command;
    geometry_msgs::PoseStamped pose_carrot, current_pose;
    if(!m_pose_carrot_sub.getCurrentMessage(pose_carrot))
    {
      ROS_WARN_THROTTLE(0.5, "CPC: No carrot received!");
      return;
    }

    //TODO: do this better
    KDL::Chain augmented_chain = m_chain;
    if(!addControlledFrameOffsetToChain(augmented_chain))
    {
      return;
    }
    delete m_fwd_vel_solver;
    delete m_fwd_pos_solver;
    delete m_inv_vel_solver;
    m_fwd_vel_solver = new KDL::ChainFkSolverVel_recursive(augmented_chain);
    m_fwd_pos_solver = new KDL::ChainFkSolverPos_recursive(augmented_chain);
    m_inv_vel_solver = new KDL::ChainIkSolverVel_wdls(augmented_chain);
    configureWeights();

    KDL::Frame current_frame, target_frame;
    if(!moveCarrotToChainBaseFrame(pose_carrot))
    {
      return;
    }
    tf::poseMsgToKDL(pose_carrot.pose, target_frame);

    KDL::JntArray current_joints = stdToKDL(msg.position, m_joint_map);
    m_fwd_pos_solver->JntToCart(current_joints, current_frame);
    tf::poseKDLToMsg(current_frame, current_pose.pose);
    current_pose.header.frame_id = m_chain_base_frame;
    current_pose.header.stamp = ros::Time::now();

    m_current_pub.publish(current_pose);
    m_target_pub.publish(pose_carrot);

    if(m_first_callback)
    {
      m_dt = 1.0 / 1000.0;
    }
    else
    {
      m_dt = msg.timestamp - m_last_timestamp; //TODO: calculate average dt
      if(m_dt == 0.0)
      {
        ROS_DEBUG("TCPC GOT DUPLICATE_TIMESTAMP!!");
        return;
      }
    }

    std::vector<double> min_bounds, max_bounds;
    getSearchBounds(current_joints, m_dt, m_max_velocity, min_bounds, max_bounds);

    //begin the search
    KDL::JntArray best_joints = current_joints;
    KDL::JntArray iter_joints = current_joints;
    KDL::Frame best_frame;
    double best_accuracy_score = std::numeric_limits<double>::max();
    double best_movement_score = std::numeric_limits<double>::max();

    //see if we're already good enough
    replaceIfBetter(current_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);
    if(best_accuracy_score == 0) //everything is already within tolerance! No need to move!
    {
      return;
    }

    if(!m_first_callback)
    {
      //see if the last command is already better than the current joints
      for(unsigned int i = 0; i < augmented_chain.getNrOfJoints(); i++)
      {
        m_last_positions(i) = clamp(m_last_positions(i), min_bounds[i], max_bounds[i]);
      }
      replaceIfBetter(m_last_positions, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      //see what happens if we just move the same amount we did last time from the current positions
      for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
      {
        iter_joints(i) = clamp(current_joints(i) + m_last_deltas[i], min_bounds[i], max_bounds[i]);
      }
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      //see what happens if we just move fast in the same direction we did last time from the current positions
//      for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
//      {
//        iter_joints(i) = clamp(current_joints(i) + sign(m_last_deltas[i]) * max_velocity * dt, min_bounds[i], max_bounds[i]);
//      }
//      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      //see what happens if we just move the same amount we did last time from the last command
      for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
      {
        iter_joints(i) = clamp(m_last_positions(i) + m_last_deltas[i], min_bounds[i], max_bounds[i]);
      }
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      //see what happens if we just move fast in the same direction we did last time from the last command
//      for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
//      {
//        //TODO: keep delta ratios the same!
//        iter_joints(i) = clamp(m_last_command(i) + sign(m_last_deltas[i]) * max_velocity * dt, min_bounds[i], max_bounds[i]);
//      }
//      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);
    }

    //see what happens if we wiggle each joint up and down independently
    iter_joints = current_joints;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      iter_joints(i) = clamp(current_joints(i) + EPSILON_ANGLE, min_bounds[i], max_bounds[i]);
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      iter_joints(i) = clamp(current_joints(i) - EPSILON_ANGLE, min_bounds[i], max_bounds[i]);
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      iter_joints(i) = clamp(current_joints(i) + m_max_velocity * m_dt / 2.0, min_bounds[i], max_bounds[i]);
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      iter_joints(i) = clamp(current_joints(i) - m_max_velocity * m_dt / 2.0, min_bounds[i], max_bounds[i]);
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      iter_joints(i) = clamp(current_joints(i) + m_max_velocity * m_dt, min_bounds[i], max_bounds[i]);
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      iter_joints(i) = clamp(current_joints(i) - m_max_velocity * m_dt, min_bounds[i], max_bounds[i]);
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);

      iter_joints(i) = current_joints(i);
    }

    //see what happens if we use kdl's solution
    if(getKDLSolution(msg, pose_carrot, current_pose, iter_joints))
    {
      //make sure we're within bounds
      for(unsigned int j = 0; j < augmented_chain.getNrOfJoints(); j++)
      {
        iter_joints(j) = clamp(iter_joints(j), min_bounds[j], max_bounds[j]);
      }
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);
    }

    //randomly guess over the whole range
    for(int i = 0; i < m_num_whole_range_attempts; i++)
    {
      for(unsigned int j = 0; j < augmented_chain.getNrOfJoints(); j++)
      {
        double min = min_bounds[j];
        double max = max_bounds[j];
        iter_joints(j) = (max - min) * ((double) rand()) / ((double) RAND_MAX) + min;
      }
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);
    }

    //randomly guess, centered on the best joint configuration
    for(int i = 0; i < m_num_best_attempts; i++)
    {
      iter_joints = best_joints;
      for(unsigned int j = 0; j < augmented_chain.getNrOfJoints(); j++)
      {
        double min = min_bounds[j];
        double max = max_bounds[j];
        iter_joints(j) += (max - min) * (2.0 * ((double) rand() / (double) RAND_MAX) - 1.0);
        iter_joints(j) = clamp(iter_joints(j), min, max);
      }
      replaceIfBetter(iter_joints, current_joints, target_frame, best_joints, best_frame, best_accuracy_score, best_movement_score);
    }

    //remember how much we moved this time and calculate velocities
    double alpha = 0.5; //TODO: set with dt?
    KDL::JntArray best_velocities(augmented_chain.getNrOfJoints());
    for(unsigned int i = 0; i < augmented_chain.getNrOfJoints(); i++)
    {
      m_last_deltas[i] = (best_joints(i) - current_joints(i));
      best_velocities(i) = clamp(m_last_deltas[i] / m_dt, -m_max_velocity, m_max_velocity);
      m_last_positions(i) = alpha * best_joints(i) + (1 - alpha) * m_last_positions(i);
      m_last_velocities(i) = alpha * best_velocities(i) + (1 - alpha) * m_last_velocities(i); //TODO: decide whether or not this should be clamped
    }

    //package it up
    task_command.joint_commands.joint_indices = m_joint_map;
    task_command.joint_commands.position = KDLToStd(m_last_positions);
    task_command.joint_commands.velocity = KDLToStd(m_last_velocities);
    assert(task_command.joint_commands.joint_indices.size() == task_command.joint_commands.position.size());
    for(int i = m_controlled_joint_weights.size() - 1; i >= 0; --i)
    {
      if(m_controlled_joint_weights[i] == 0)
      {
        task_command.joint_commands.joint_indices.erase(task_command.joint_commands.joint_indices.begin() + i);
        task_command.joint_commands.position.erase(task_command.joint_commands.position.begin() + i);
        task_command.joint_commands.velocity.erase(task_command.joint_commands.velocity.begin() + i);
      }
    }
    m_command_pub.publish(task_command);

    for(unsigned int i = 0; i < augmented_chain.getNrOfJoints(); i++)
    {
      assert(best_joints(i) >= min_bounds[i]);
      assert(best_joints(i) <= max_bounds[i]);
    }

    m_last_timestamp = msg.timestamp;
    m_first_callback = false;
  }

  void TolerantCartesianPoseControl::initModel()
  {
    m_urdf_model.initParam("/dynamics/robot_description");
    if(!kdl_parser::treeFromUrdfModel((const urdf::Model) m_urdf_model, m_tree))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }

    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(m_controlled_joints[i]);
      m_controlled_joint_mins.push_back(joint->limits->lower);
      m_controlled_joint_maxes.push_back(joint->limits->upper);
    }
  }

  void TolerantCartesianPoseControl::spin()
  {
    ROS_INFO("TolerantCartesianPoseControl started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "tolerant_cartesian_pose_control");
  ros::NodeHandle nh("~");

  cartesian_pose_control::TolerantCartesianPoseControl node(nh);
  node.spin();

  return 0;
}
