#ifndef LINEAR_ALGEBRA_MATH_H
#define LINEAR_ALGEBRA_MATH_H

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace inverse_dynamics
{
  typedef Eigen::Matrix<double, 3, 1> Vector3;
  typedef Eigen::Matrix<double, 6, 1> Vector6;
  typedef Eigen::Matrix<double, 10, 1> Vector10;
  typedef Eigen::Matrix<double, 11, 1> Vector11;
  typedef Eigen::Matrix<double, 6, 6> Matrix6;
  typedef Eigen::Matrix<double, 3, 3> Matrix3;
  typedef Eigen::Matrix<double, 6, 10> Matrix610;
  typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> Matrix;
  typedef Eigen::Matrix<double, Eigen::Dynamic, 1> Vector;

  template<typename T>
  void amnn(T matrix) //eigen matrices only
  {
    for(unsigned int i = 0; i < matrix.rows(); i++)
    {
      for(unsigned int j = 0; j < matrix.cols(); j++)
      {
        assert(!isnan(matrix(i, j)));
        assert(!isinf(matrix(i, j)));
      }
    }
  }
  template<typename T>
  void avnn(T vector) //std vectors only
  {
    for(unsigned int i = 0; i < vector.size(); i++)
    {
      assert(!isnan(vector[i]));
      assert(!isinf(vector[i]));
    }
  }

  template<typename T>
  void roundToNearest(T& value, double step) //eigen matrices only
  {
    value = sign(value) * (round(fabs(value) / step) * step);
  }

  template<typename T>
  void roundAllToNearest(T& matrix, double step) //eigen matrices only
  {
    for(unsigned int i = 0; i < matrix.rows(); i++)
    {
      for(unsigned int j = 0; j < matrix.cols(); j++)
      {
        roundToNearest(matrix(i, j), step);
      }
    }
  }

  inline Matrix3 cross(Vector3 v)
  {
    Matrix3 m = Matrix3::Zero();

    m << 0, -v(2), v(1), v(2), 0, -v(0), -v(1), v(0), 0;

    return m;
  }

  inline Matrix6 cross(Vector6 sv)
  {
    Matrix6 m;

    Vector3 w = sv.topRows(3);
    Vector3 v = sv.bottomRows(3);
    m.topLeftCorner(3, 3) = cross(w);
    m.bottomLeftCorner(3, 3) = cross(v);
    m.topRightCorner(3, 3) = Matrix3::Zero();
    m.bottomRightCorner(3, 3) = m.topLeftCorner(3, 3);

    return m;
  }

  inline Vector3 kdlToEigen(KDL::Vector v)
  {
    return Vector3(v.x(), v.y(), v.z());
  }

  inline Matrix3 kdlToEigen(KDL::Rotation rot)
  {
    Matrix3 m;
    for(unsigned int i = 0; i < 3; i++)
    {
      for(unsigned int j = 0; j < 3; j++)
      {
        m(i, j) = rot(i, j);
      }
    }
    return m;
  }

  inline Matrix3 kdlToEigen(KDL::RotationalInertia rot)
  {
    Matrix3 m;
    for(unsigned int i = 0; i < 9; i++)
    {
      m(i) = rot.data[i];
    }
    return m;
  }

  inline Matrix6 constructInertia(double m, Vector3 r, Matrix3 Ic)
  {
    Matrix6 inertia;

    //    inertia.topLeftCorner(3, 3) = I + m * (((double) (r.transpose() * r)) * Matrix3::Identity(3, 3) + r * r.transpose());
    inertia.topLeftCorner(3, 3) = Ic + m * cross(r) * cross(r).transpose();
    inertia.topRightCorner(3, 3) = m * cross(r);
    inertia.bottomLeftCorner(3, 3) = inertia.topRightCorner(3, 3).transpose();
    inertia.bottomRightCorner(3, 3) = m * Matrix3::Identity(3, 3);

    return inertia;
  }

  inline Matrix6 kdlToEigen(KDL::RigidBodyInertia kdl_inertia)
  {
    Matrix6 inertia;

    Matrix3 J = kdlToEigen(kdl_inertia.getRotationalInertia());
    Vector3 r = kdlToEigen(kdl_inertia.getCOG());
    double m = kdl_inertia.getMass();
    Matrix3 Ic = J - m * cross(r) * cross(r).transpose();

    return constructInertia(m, r, Ic);
  }

  inline Matrix6 kdlToEigen(KDL::Frame frame)
  {
    Matrix6 trans;

    trans.topLeftCorner(3, 3) = kdlToEigen(frame.M);
    trans.bottomLeftCorner(3, 3) = cross(kdlToEigen(frame.p)) * trans.topLeftCorner(3, 3);
    trans.topRightCorner(3, 3) = Matrix3::Zero();
    trans.bottomRightCorner(3, 3) = trans.topLeftCorner(3, 3);

    return trans;
  }

  inline void extractBaseStates(matec_msgs::FullJointStates* states, double gravity_magnitude, Vector6& base_position, Vector6& base_velocity, Vector6& gravity)
  {
    for(unsigned int i = 0; i < 6; i++)
    {
      base_position(i) = states->position[i];
      base_velocity(i) = states->velocity[i];
    }

    Eigen::AngleAxisd rollAngle(base_position(0), Eigen::Vector3d::UnitX());
    Eigen::AngleAxisd pitchAngle(base_position(1), Eigen::Vector3d::UnitY());
    Eigen::AngleAxisd yawAngle(base_position(2), Eigen::Vector3d::UnitZ());
    Eigen::Quaternion<double> q = rollAngle * pitchAngle * yawAngle;

    gravity.bottomRows(3) = q.matrix() * Eigen::Vector3d(0, 0, gravity_magnitude);
  }

  inline double sign(double val)
  {
    return (val == 0.0)? 0.0 : ((val < 0)? -1.0 : 1.0);
  }

  inline double clamp(double a, double b, double c)
  {
    return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
  }

  //default to taking in the boundaries by 0.1% of the total range
  inline double asymptoteClamp(double val, double min, double max, double asymptote_epsilon_fraction = 0.001)
  {
    assert(max >= min);
    double asymptote_epsilon = fabs((max - min) * asymptote_epsilon_fraction);
    double new_min = min + asymptote_epsilon;
    double new_max = max - asymptote_epsilon;
    return clamp(val, new_min, new_max);
  }

  inline Vector10 overflowToMassChange(Vector10 overflow, Vector10 theta, Vector10 theta_dot)
  {
    Vector10 mass_change;
    mass_change(9) = 0;
    mass_change(8) = theta(9) / theta(8) * (theta_dot(8) - overflow(8) * theta(9)) - theta_dot(9);
    mass_change(7) = theta(9) / theta(7) * (theta_dot(7) - overflow(7) * theta(9)) - theta_dot(9);
    mass_change(6) = theta(9) / theta(6) * (theta_dot(6) - overflow(6) * theta(9)) - theta_dot(9);
    mass_change(5) = 0; //theta(9) / (theta(7) * theta(8)) * (theta(7) * theta_dot(8) + theta(8) * theta_dot(7) + theta(9) * theta_dot(5) - overflow(5) * theta(9)) - theta_dot(9);
    mass_change(4) = 0; //theta(9) / (theta(6) * theta(8)) * (theta(6) * theta_dot(8) + theta(8) * theta_dot(6) + theta(9) * theta_dot(4) - overflow(4) * theta(9)) - theta_dot(9);
    mass_change(3) = 0; //theta(9) / (theta(6) * theta(7)) * (theta(6) * theta_dot(7) + theta(7) * theta_dot(6) + theta(9) * theta_dot(3) - overflow(3) * theta(9)) - theta_dot(9);
    mass_change(2) = 0; //theta(9) / (theta(6) * theta(6) + theta(7) * theta(7)) * (2 * theta(6) * theta_dot(6) + 2 * theta(7) * theta_dot(7) - theta(9) * theta_dot(2) + overflow(2) * theta(9)) - theta_dot(9);
    mass_change(1) = 0; //theta(9) / (theta(6) * theta(6) + theta(8) * theta(8)) * (2 * theta(6) * theta_dot(6) + 2 * theta(8) * theta_dot(8) - theta(9) * theta_dot(1) + overflow(1) * theta(9)) - theta_dot(9);
    mass_change(0) = 0; //theta(9) / (theta(7) * theta(7) + theta(8) * theta(8)) * (2 * theta(7) * theta_dot(7) + 2 * theta(8) * theta_dot(8) - theta(9) * theta_dot(0) + overflow(0) * theta(9)) - theta_dot(9);

    for(unsigned int i = 0; i < mass_change.rows(); i++)
    {
      if(isnan((double) mass_change(i)) || isinf((double) mass_change(i)))
      {
        mass_change(i) = 0;
      }

      if(mass_change(i) != 0)
      {
        mass_change(i) = 1.0 / mass_change(i);
      }
    }

    return mass_change;
  }

  inline Vector10 toDecoupledParameterization(Vector10 theta, Vector10 theta_dot)
  {
    Vector10 phi_dot;

    double t9_squared = (theta(9) * theta(9));
    double t9_squared_inverse = 1.0 / t9_squared;

    phi_dot(9) = theta_dot(9);
    phi_dot(8) = t9_squared_inverse * (theta_dot(8) * theta(9) - theta(8) * theta_dot(9));
    phi_dot(7) = t9_squared_inverse * (theta_dot(7) * theta(9) - theta(7) * theta_dot(9));
    phi_dot(6) = t9_squared_inverse * (theta_dot(6) * theta(9) - theta(6) * theta_dot(9));
    phi_dot(5) = t9_squared_inverse * (theta(7) * theta(9) * theta_dot(8) - theta(7) * theta(8) * theta_dot(9) + theta(8) * theta(9) * theta_dot(7) + t9_squared * theta_dot(5));
    phi_dot(4) = t9_squared_inverse * (theta(6) * theta(9) * theta_dot(8) - theta(6) * theta(8) * theta_dot(9) + theta(8) * theta(9) * theta_dot(6) + t9_squared * theta_dot(4));
    phi_dot(3) = t9_squared_inverse * (theta(6) * theta(9) * theta_dot(7) - theta(6) * theta(7) * theta_dot(9) + theta(7) * theta(9) * theta_dot(6) + t9_squared * theta_dot(3));
    phi_dot(2) = t9_squared_inverse * (theta(6) * theta(6) * theta_dot(9) - 2 * theta(6) * theta(9) * theta_dot(6) + theta(7) * theta(7) * theta_dot(9) - 2 * theta(7) * theta(9) * theta_dot(7) + t9_squared * theta_dot(2));
    phi_dot(1) = t9_squared_inverse * (theta(6) * theta(6) * theta_dot(9) - 2 * theta(6) * theta(9) * theta_dot(6) + theta(8) * theta(8) * theta_dot(9) - 2 * theta(8) * theta(9) * theta_dot(8) + t9_squared * theta_dot(1));
    phi_dot(0) = t9_squared_inverse * (theta(7) * theta(7) * theta_dot(9) - 2 * theta(7) * theta(9) * theta_dot(7) + theta(8) * theta(8) * theta_dot(9) - 2 * theta(8) * theta(9) * theta_dot(8) + t9_squared * theta_dot(0));

    return phi_dot;
  }

  inline Vector10 toDecoupledParameterization(Vector10 theta)
  {
    amnn(theta);

    Vector10 phi;
    phi(9) = theta(9);

    Vector3 com = Vector3(theta(6), theta(7), theta(8)) / theta(9);
    phi(8) = com(2);
    phi(7) = com(1);
    phi(6) = com(0);

    Matrix3 J;
    J << theta(0), theta(3), theta(4), //
    theta(3), theta(1), theta(5), //
    theta(4), theta(5), theta(2); //
    Matrix3 I = J - theta(9) * cross(com) * cross(com).transpose();

    phi(5) = I(1, 2);
    phi(4) = I(0, 2);
    phi(3) = I(0, 1);
    phi(2) = I(2, 2);
    phi(1) = I(1, 1);
    phi(0) = I(0, 0);

    amnn(phi);
    return phi;
  }

  inline Vector10 toCoupledParameterization(Vector10 phi)
  {
    amnn(phi);
    Vector10 theta;
    theta(9) = phi(9);

    Vector3 com = Vector3(phi(6), phi(7), phi(8));
    theta(8) = phi(8) * phi(9);
    theta(7) = phi(7) * phi(9);
    theta(6) = phi(6) * phi(9);

    Matrix3 I;
    I << phi(0), phi(3), phi(4), //
    phi(3), phi(1), phi(5), //
    phi(4), phi(5), phi(2); //
    Matrix3 J = I + phi(9) * cross(com) * cross(com).transpose();

    theta(5) = J(1, 2);
    theta(4) = J(0, 2);
    theta(3) = J(0, 1);
    theta(2) = J(2, 2);
    theta(1) = J(1, 1);
    theta(0) = J(0, 0);

    amnn(theta);
    return theta;
  }

  inline Vector10 parameterizeInertia(Matrix6 inertia)
  {
    Vector10 parameterized_inertia;
    parameterized_inertia(0) = inertia(0, 0);
    parameterized_inertia(1) = inertia(1, 1);
    parameterized_inertia(2) = inertia(2, 2);
    parameterized_inertia(3) = inertia(0, 1);
    parameterized_inertia(4) = inertia(0, 2);
    parameterized_inertia(5) = inertia(1, 2);
    parameterized_inertia(6) = inertia(2, 4);
    parameterized_inertia(7) = inertia(0, 5);
    parameterized_inertia(8) = inertia(1, 3);
    parameterized_inertia(9) = inertia(5, 5);
    return parameterized_inertia;
  }

  inline Matrix6 deparameterizeInertia(Vector10 parameterized_inertia, boost::array<Matrix6, 10>& R)
  {
    Matrix6 inertia;
    for(unsigned int l = 0; l < 10; l++)
    {
      inertia += R[l] * parameterized_inertia(l);
    }
    return inertia;
  }

  inline void initInertiaParameterMaps(boost::array<Matrix6, 10>& R)
  {
    R[0] << 1, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0; //

    R[1] << 0, 0, 0, 0, 0, 0, //
    0, 1, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0; //

    R[2] << 0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 1, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0; //

    R[3] << 0, 1, 0, 0, 0, 0, //
    1, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0; //

    R[4] << 0, 0, 1, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    1, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0; //

    R[5] << 0, 0, 0, 0, 0, 0, //
    0, 0, 1, 0, 0, 0, //
    0, 1, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0; //

    R[6] << 0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, -1, //
    0, 0, 0, 0, 1, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 1, 0, 0, 0, //
    0, -1, 0, 0, 0, 0; //

    R[7] << 0, 0, 0, 0, 0, 1, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, -1, 0, 0, //
    0, 0, -1, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    1, 0, 0, 0, 0, 0; //

    R[8] << 0, 0, 0, 0, -1, 0, //
    0, 0, 0, 1, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 1, 0, 0, 0, 0, //
    -1, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0; //

    R[9] << 0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 0, 0, 0, //
    0, 0, 0, 1, 0, 0, //
    0, 0, 0, 0, 1, 0, //
    0, 0, 0, 0, 0, 1; //
  }
}

#endif //LINEAR_ALGEBRA_MATH_H
