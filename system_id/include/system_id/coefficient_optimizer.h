#ifndef COEFFICIENT_OPTIMIZER_H
#define COEFFICIENT_OPTIMIZER_H

#include "system_id/debug_levels.h"
#include "system_id/training_data.h"
#include "system_id/genetic.h"
#include "system_id/plotter.h"
#include <boost/thread/thread_time.hpp>

namespace system_id
{
  class CoefficientOptimizer
  {
  public:
    CoefficientOptimizer(system_id::TrainingData* data);

    system_id::ArrayGenome optimize(std::string expression_string, std::vector<std::string> coefficient_symbols, std::vector<std::string> input_symbols, system_id::GAParams ga_params, ArrayGenome::GeneType* min_bound = NULL, ArrayGenome::GeneType* max_bound = NULL, system_id::ArrayGenome::GeneType* initial_guess = NULL);

    void fitness_function(system_id::ArrayGenome& genome);

  private:
    boost::posix_time::ptime m_next_plot_time;

    std::vector<long double> m_coefficients;
    std::vector<long double> m_inputs;
    long double m_output_min;
    long double m_output_max;
    system_id::TrainingData* m_training_data;
    system_id::Plotter m_plotter;
    std::string m_expression_string;
    long double m_best_fitness;
    std::vector<long double> m_t_data;
    std::vector<long double> m_target_output_data;
  };
}

#endif //COEFFICIENT_OPTIMIZER_H
