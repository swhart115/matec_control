#include "interpolators/joint_space_interpolator.h"

namespace interpolators
{
  JointSpaceInterpolator::JointSpaceInterpolator(const ros::NodeHandle& nh) :
      m_nh(nh), m_server(m_nh, "/follow_joint_trajectory", boost::bind(&JointSpaceInterpolator::goalCallback, this, _1), boost::bind(&JointSpaceInterpolator::cancelCallback, this, _1), false)
  { //TODO: use namespaced server name
    m_nh.param("publish_rate", m_publish_rate, 250.0);
    m_nh.param("always_tare", m_always_tare, true);

    matec_utils::blockOnSMJointNames(m_joint_names);

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints))
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("JointSpaceInterpolator couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }

    m_interpolators.resize(m_controlled_joints.size());
    m_last_command.position.resize(m_controlled_joints.size());
    m_last_command.velocity.resize(m_controlled_joints.size());
    m_last_command.acceleration.resize(m_controlled_joints.size());

    m_joint_states_sub.subscribe("/joint_states");
    tare();

    m_tare_service_server = m_nh.advertiseService("tare", &JointSpaceInterpolator::tareCallback, this);

    m_joint_carrot_pub.advertise("carrot");
    m_server.start();
  }

  JointSpaceInterpolator::~JointSpaceInterpolator()
  {

  }

  bool JointSpaceInterpolator::tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res)
  {
    std::vector<int> subset;
    for(unsigned int i = 0; i < req.joint_list.size(); i++)
    {
      int idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), req.joint_list[i]) - m_controlled_joints.begin();
      if(idx < m_controlled_joints.size())
      {
        subset.push_back(idx);
      }
      else
      {
        ROS_WARN("Tare requested for uncontrolled joint %s", req.joint_list[i].c_str());
      }
    }
    tare(subset);
    return true;
  }

  void JointSpaceInterpolator::tare(std::vector<int> subset)
  {
    boost::mutex::scoped_lock lock(m_mutex);

    ROS_INFO("Taring joints!");

    matec_msgs::FullJointStates measurement;
    while(!m_joint_states_sub.getCurrentMessage(measurement))
    {
      ROS_WARN_THROTTLE(1.0, "Waiting for joint states to tare!");
    }

    if(subset.size() == 0)
    {
      for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
      {
        m_last_command.position[i] = measurement.position[m_joint_map[i]];
        m_last_command.velocity[i] = 0.0;
        m_last_command.acceleration[i] = 0.0;
      }
    }
    else
    {
      for(unsigned int i = 0; i < subset.size(); i++)
      {
        int joint_idx = subset[i];
        m_last_command.position[joint_idx] = measurement.position[m_joint_map[joint_idx]];
        m_last_command.velocity[joint_idx] = 0.0;
        m_last_command.acceleration[joint_idx] = 0.0;
      }
    }
  }

  bool JointSpaceInterpolator::checkGoalJoints(std::vector<std::string> goal_joints)
  {
    for(unsigned int i = 0; i < goal_joints.size(); i++)
    {
      if(std::find(m_controlled_joints.begin(), m_controlled_joints.end(), goal_joints[i]) == m_controlled_joints.end())
      {
        ROS_ERROR("Goal contained joint %s that we aren't supposed to control!", goal_joints[i].c_str());
        return false;
      }
    }
    return true;
  }

  void JointSpaceInterpolator::goalCallback(Server::GoalHandle goal_handle)
  {
    if(checkGoalJoints(goal_handle.getGoal()->trajectory.joint_names))
    {
      ROS_INFO("Starting Trajectory!");
      cancelCurrentGoal();
      m_goal_handle = goal_handle;
      m_goal_handle.setAccepted();

      control_msgs::FollowJointTrajectoryFeedback feedback;
      m_goal_handle.publishFeedback(feedback);

      setupGoal();
    }
    else
    {
      goal_handle.setRejected();
    }
  }

  void JointSpaceInterpolator::cancelCallback(Server::GoalHandle goal_handle)
  {
    cancelCurrentGoal();
    shutdown();
  }

  void JointSpaceInterpolator::cancelCurrentGoal()
  {
    if(m_goal_handle.getGoal().get()) //goal doesn't actually exist, so no need to cancel
    {
      actionlib_msgs::GoalStatus status = m_goal_handle.getGoalStatus(); //cancel if current goal is pending, recalling, active, or preempting
      if(status.status == actionlib_msgs::GoalStatus::PENDING || status.status == actionlib_msgs::GoalStatus::RECALLING || status.status == actionlib_msgs::GoalStatus::ACTIVE || status.status == actionlib_msgs::GoalStatus::PREEMPTING)
      {
        m_goal_handle.setCanceled();
        m_goal_handle = Server::GoalHandle();
      }

      for(unsigned int i = 0; i < m_interpolators.size(); i++)
      {
        m_interpolators[i].cancelTrajectory();
      }
    }
  }

  void JointSpaceInterpolator::shutdown()
  {
    //stop everything that we were doing
  }

  bool JointSpaceInterpolator::goalActive()
  {
    return m_goal_handle.getGoal();
  }

  void JointSpaceInterpolator::setupGoal()
  {
    if(m_always_tare)
    {
      std::vector<int> subset;
      for(unsigned int i = 0; i < m_goal_handle.getGoal()->trajectory.joint_names.size(); i++)
      {
        int joint_idx = std::find(m_joint_names.begin(), m_joint_names.end(), m_goal_handle.getGoal()->trajectory.joint_names[i]) - m_joint_names.begin();
        assert(joint_idx < (int) m_joint_names.size());
        subset.push_back(joint_idx);
      }
      tare(subset);
    }

    for(unsigned int i = 0; i < m_interpolators.size(); i++)
    {
      m_interpolators[i].cancelTrajectory();
    }
    ros::Time goal_time = m_goal_handle.getGoalID().stamp;
    for(unsigned int goal_idx = 0; goal_idx < m_goal_handle.getGoal()->trajectory.joint_names.size(); goal_idx++)
    {
      int msg_idx = m_joint_map[goal_idx];
      unsigned int interpolator_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), m_goal_handle.getGoal()->trajectory.joint_names[goal_idx]) - m_controlled_joints.begin();

      Trajectory traj;

      unsigned int waypoint_idx_start = 0;
      if(m_goal_handle.getGoal()->trajectory.points[0].time_from_start.toSec() != 0.0) //initial states are not already included in the goal
      {
        ROS_INFO("Adding initial state");
        boost::mutex::scoped_lock lock(m_mutex);
        ViaPoint start_point(goal_time, m_last_command.position[interpolator_idx], m_last_command.velocity[interpolator_idx]);
        traj.push_back(start_point);
      }
      else //initial states are in the goal already
      {
        if(m_always_tare) //initial states are already in goal, but we want to overwrite
        {
          ROS_INFO("Replacing initial state");
          boost::mutex::scoped_lock lock(m_mutex);
          ViaPoint start_point(goal_time, m_last_command.position[interpolator_idx], m_last_command.velocity[interpolator_idx]);
          traj.push_back(start_point);
          waypoint_idx_start = 1;
        }
        else
        {
          ROS_INFO("Using pre-existing initial state");
        }
      }

      for(unsigned int waypoint_idx = waypoint_idx_start; waypoint_idx < m_goal_handle.getGoal()->trajectory.points.size(); waypoint_idx++)
      {
        trajectory_msgs::JointTrajectoryPoint goal_point = m_goal_handle.getGoal()->trajectory.points[waypoint_idx];
        ViaPoint point;
        point.finish_time = goal_time + goal_point.time_from_start;
        point.target_state.position = goal_point.positions[goal_idx];
        traj.push_back(point);
      }

      m_interpolators[interpolator_idx].loadTrajectory(traj);
    }
  }

  void JointSpaceInterpolator::finishGoal(bool success)
  {
    if(success)
    {
      ROS_INFO("Finished trajectory!");
      control_msgs::FollowJointTrajectoryFeedback feedback;
      //feedback.progress = 1.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setSucceeded();
      m_goal_handle = Server::GoalHandle();
    }
    else
    {
      ROS_ERROR("Trajectory following failed!");
      control_msgs::FollowJointTrajectoryFeedback feedback;
      //feedback.progress = 0.0;

      m_goal_handle.publishFeedback(feedback);
      m_goal_handle.setAborted();
      m_goal_handle = Server::GoalHandle();
    }
  }

  void JointSpaceInterpolator::calculateNextCommand(ros::Time time, matec_msgs::FullJointStates& command)
  {
    boost::mutex::scoped_lock lock(m_mutex);

    bool all_done = true;
    for(unsigned int joint = 0; joint < m_interpolators.size(); joint++)
    {
      if(!m_interpolators[joint].hasTrajectory())
      {
        command.velocity[joint] = 0;
        command.acceleration[joint] = 0;
        continue;
      }
      all_done = false;

      State state;
      if(m_interpolators[joint].getInterpolatedState(time, state))
      {
        command.position[joint] = state.position;
        command.velocity[joint] = state.velocity;
        command.acceleration[joint] = state.acceleration;
      }
    }
    command.joint_indices = m_joint_map;

    if(goalActive() && all_done)
    {
      finishGoal(true);
    }
  }

  void JointSpaceInterpolator::spin()
  {
    ROS_INFO("JointSpaceInterpolator started.");
    ros::Rate loop_rate(m_publish_rate);
    while(ros::ok())
    {
      calculateNextCommand(ros::Time::now(), m_last_command);
      m_joint_carrot_pub.publish(m_last_command);

      loop_rate.sleep();
      ros::spinOnce();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "joint_space_interpolator");
  ros::NodeHandle nh("~");

  interpolators::JointSpaceInterpolator node(nh);
  node.spin();

  return 0;
}
