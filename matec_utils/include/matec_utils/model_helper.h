#ifndef MODEL_HELPER_H
#define MODEL_HELPER_H

#include <math.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <urdf/model.h>
#include <tf/tf.h>
#include <geometric_shapes/bodies.h>
#include <geometric_shapes/body_operations.h>
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/shape_operations.h>
#include <kdl/tree.hpp>
#include <kdl/frames.hpp>
#include <kdl/segment.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <tf_conversions/tf_kdl.h>
#include <tf/transform_datatypes.h>
#include <tf2/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/WrenchStamped.h>

#include <rbdl/rbdl.h>
#include <rbdl/Model.h>
#include <rbdl/Dynamics.h>
#include <rbdl/rbdl_utils.h>
#include <matec_utils/rbdl_urdfreader.h>

namespace matec_utils
{
  class SegmentPair
  {
  public:
    SegmentPair(const KDL::Segment& p_segment, const std::string& p_root, const std::string& p_tip) :
        segment(p_segment), root(p_root), tip(p_tip)
    {
    }

    KDL::Segment segment;
    std::string root;
    std::string tip;
  };

  class KDLWrenchStamped
  {
  public:
    KDLWrenchStamped()
    {
      wrench = KDL::Wrench::Zero();
      frame_id = "";
    }

    KDLWrenchStamped(KDL::Wrench wrench_, std::string frame_id_)
    {
      wrench = wrench_;
      frame_id = frame_id_;
    }

    KDLWrenchStamped(geometry_msgs::WrenchStamped wrench_msg)
    {
      tf::wrenchMsgToKDL(wrench_msg.wrench, wrench);
      frame_id = wrench_msg.header.frame_id;
    }

    geometry_msgs::WrenchStamped toMsg()
    {
      geometry_msgs::WrenchStamped wrench_msg;

      tf::wrenchKDLToMsg(wrench, wrench_msg.wrench);
      wrench_msg.header.frame_id = frame_id;
      wrench_msg.header.stamp = ros::Time::now();

      return wrench_msg;
    }

    KDL::Wrench wrench;
    std::string frame_id;
  };

  class ModelHelper
  {
  public:
    ModelHelper(urdf::Model m_model, std::vector<std::string> external_name_vector);

    bool updatePositions(std::vector<double>& joint_positions);
    bool updateRootLinkOdometry(tf::StampedTransform& model_to_ground_transform);
    bool updateRootLinkOdometry(geometry_msgs::Pose& pose);
    bool updateModel(std::vector<double>& joint_positions, tf::StampedTransform& model_to_ground_transform);
    bool updateModel(std::vector<double>& joint_positions, geometry_msgs::Pose& pose);

    bool findCOM(geometry_msgs::Point& com, double& total_mass);
    bool findCOMOfSubset(std::vector<std::string> link_names, geometry_msgs::Point& center_of_mass, double& total_subset_mass);

    bool getRootLinkTransform(tf::StampedTransform& trans);
    bool getCoMTransform(tf::StampedTransform& trans);

    std::vector<std::string> getAllLinkNames();

    static bool calculateWrenchCoP(matec_utils::KDLWrenchStamped wrench, geometry_msgs::PointStamped& point);
    void transformWrench(std::string target_frame, KDLWrenchStamped wrench_in, KDLWrenchStamped& wrench_out);
    void addContactFrame(std::string contact_parent, geometry_msgs::Pose contact_normal_pose);
    void addFrame(std::string parent, geometry_msgs::PoseStamped frame_pose);

    geometry_msgs::Point getGlobalFramePosition(std::string target_frame);
    geometry_msgs::Point getFramePosition(std::string target_frame, std::string output_frame);
    geometry_msgs::PoseStamped getFramePose(std::string target_frame, std::string output_frame);
    geometry_msgs::PoseStamped transformPose(geometry_msgs::PoseStamped pose, std::string output_frame);

    KDL::Tree* getTree();

    struct PointMass
    {
      std::string link_frame_id;
      double mass;
      tf::Vector3 origin;
    };

  private:
    urdf::Model& m_model;
    KDL::Tree m_tree;
    tf::Transformer m_transformer;
    std::vector<std::string> frame_names;
    double m_total_mass;

    std::vector<PointMass> m_masses;
    std::vector<std::string> m_all_names;
    std::vector<std::string> m_external_name_vector;
    std::string m_global_frame;
    std::map<std::string, SegmentPair> m_segments;
    std::map<std::string, SegmentPair> m_fixed_segments;

    bool parseMass(boost::shared_ptr<const urdf::Link> link, PointMass& box);
    void parseModel();
    void initInternalTree();
    void addChildrenSegments(const KDL::SegmentMap::const_iterator segment);
    void addCoMFrames();

    //RBDL
    RigidBodyDynamics::Model m_rbdl_model;
    std::vector<int> m_rbdl_to_external;
    std::vector<int> m_external_to_rbdl;
    RigidBodyDynamics::Math::MatrixNd m_inertia_matrix;
    void initRBDLModel();
    //    getInertiaMatrix();
    //    getGravityTerms();
    //    getJointTorqueMapMatrix()
  };
}
#endif //MODEL_HELPER_H
