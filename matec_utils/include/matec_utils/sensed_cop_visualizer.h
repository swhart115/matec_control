#ifndef SENSED_COP_VISUALIZER_H
#define SENSED_COP_VISUALIZER_H

#include <math.h>
#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <std_msgs/String.h>
#include <urdf/model.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/PointCloud2.h>
#include <matec_msgs/WrenchArray.h>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/ContactSpecification.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/filters/passthrough.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include "matec_utils/common_initialization_components.h"

#include "matec_utils/urdf_manipulations.h"
#include "model_helper.h"

#include "boost/thread/mutex.hpp"

namespace matec_utils
{
  class SensedCoPVisualizer
  {
  public:
    SensedCoPVisualizer(const ros::NodeHandle& nh);
    ~SensedCoPVisualizer();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;

    std::vector<std::string> m_joint_names;

    tf::TransformListener m_tf_listener;

    ros::Subscriber m_wrenches_sub;

    ros::Publisher m_cop_pub;
    sensor_msgs::PointCloud2 m_cops;

    ros::Publisher m_total_cop_pub;
    geometry_msgs::PointStamped m_total_cop;

    ros::Publisher m_total_com_pub;
    geometry_msgs::PointStamped m_total_com;

    urdf::Model m_model;
    matec_utils::ModelHelper* m_helper;

    boost::mutex m_mutex;

    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;
    shared_memory_interface::Subscriber<matec_msgs::WrenchArray> m_wrench_array_sub;

    void sensorCallback(matec_msgs::FullJointStates& msg);
//    void wrenchesCallback(const matec_msgs::WrenchArrayConstPtr& msg);
  };
}
#endif //SENSED_COP_VISUALIZER_H
