from sympy.solvers import solve
from sympy.mpmath import diff
from sympy import *

t = Symbol('t')
t0 = Symbol('t0')
t1 = Symbol('t1')
t2 = Symbol('t2')
t3 = Symbol('t3')
t4 = Symbol('t4')
t5 = Symbol('t5')
t6 = Symbol('t6')
t7 = Symbol('t7')
t8 = Symbol('t8')
t9 = Symbol('t9')

p9 = t9(t)
p8 = t8(t)/t9(t)
p7 = t7(t)/t9(t)
p6 = t6(t)/t9(t)
p5 = t5(t) + t7(t)*t8(t)/t9(t)
p4 = t4(t) + t6(t)*t8(t)/t9(t)
p3 = t3(t) + t6(t)*t7(t)/t9(t)
p2 = t2(t) - t6(t)**2/t9(t) - t7(t)**2/t9(t)
p1 = t1(t) - t6(t)**2/t9(t) - t8(t)**2/t9(t)
p0 = t0(t) - t7(t)**2/t9(t) - t8(t)**2/t9(t)

dp9 = (p9.diff(t).simplify())
dp8 = (p8.diff(t).simplify())
dp7 = (p7.diff(t).simplify())
dp6 = (p6.diff(t).simplify())
dp5 = (p5.diff(t).simplify())
dp4 = (p4.diff(t).simplify())
dp3 = (p3.diff(t).simplify())
dp2 = (p2.diff(t).simplify())
dp1 = (p1.diff(t).simplify())
dp0 = (p0.diff(t).simplify())

print "\n9:"
pprint(dp9)
print "\n8:"
pprint(dp8)
print "\n7:"
pprint(dp7)
print "\n6:"
pprint(dp6)
print "\n5:"
pprint(dp5)
print "\n4:"
pprint(dp4)
print "\n3:"
pprint(dp3)
print "\n2:"
pprint(dp2)
print "\n1:"
pprint(dp1)
print "\n0:"
pprint(dp0)


dp0s = Symbol('dp0')
dp1s = Symbol('dp1')
dp2s = Symbol('dp2')
dp3s = Symbol('dp3')
dp4s = Symbol('dp4')
dp5s = Symbol('dp5')
dp6s = Symbol('dp6')
dp7s = Symbol('dp7')
dp8s = Symbol('dp8')
dp9s = Symbol('dp9')

dt0s = t0(t).diff(t)
dt1s = t1(t).diff(t)
dt2s = t2(t).diff(t)
dt3s = t3(t).diff(t)
dt4s = t4(t).diff(t)
dt5s = t5(t).diff(t)
dt6s = t6(t).diff(t)
dt7s = t7(t).diff(t)
dt8s = t8(t).diff(t)
dt9s = t9(t).diff(t)
eq0 = dp0 - dp0s
eq1 = dp1 - dp1s
eq2 = dp2 - dp2s
eq3 = dp3 - dp3s
eq4 = dp4 - dp4s
eq5 = dp5 - dp5s
eq6 = dp6 - dp6s
eq7 = dp7 - dp7s
eq8 = dp8 - dp8s
eq9 = dp9 - dp9s

dp0_zero = dp0.subs(t9(t).diff(t),0).simplify()
dp1_zero = dp1.subs(t9(t).diff(t),0).simplify()
dp2_zero = dp2.subs(t9(t).diff(t),0).simplify()
dp3_zero = dp3.subs(t9(t).diff(t),0).simplify()
dp4_zero = dp4.subs(t9(t).diff(t),0).simplify()
dp5_zero = dp5.subs(t9(t).diff(t),0).simplify()
dp6_zero = dp6.subs(t9(t).diff(t),0).simplify()
dp7_zero = dp7.subs(t9(t).diff(t),0).simplify()
dp8_zero = dp8.subs(t9(t).diff(t),0).simplify()
dp9_zero = dp9.subs(t9(t).diff(t),0).simplify()

dt90 = solve(eq0, dt9s)[0].simplify()
dt91 = solve(eq1, dt9s)[0].simplify()
dt92 = solve(eq2, dt9s)[0].simplify()
dt93 = solve(eq3, dt9s)[0].simplify()
dt94 = solve(eq4, dt9s)[0].simplify()
dt95 = solve(eq5, dt9s)[0].simplify()
dt96 = solve(eq6, dt9s)[0].simplify()
dt97 = solve(eq7, dt9s)[0].simplify()
dt98 = solve(eq8, dt9s)[0].simplify()
dt99 = solve(eq9, dt9s)[0].simplify()

print "\n9 for 9:"
pprint(dt99)
print "\n8 for 9:"
pprint(dt98)
print "\n7 for 9:"
pprint(dt97)
print "\n6 for 9:"
pprint(dt96)
print "\n5 for 9:"
pprint(dt95)
print "\n4 for 9:"
pprint(dt94)
print "\n3 for 9:"
pprint(dt93)
print "\n2 for 9:"
pprint(dt92)
print "\n1 for 9:"
pprint(dt91)
print "\n0 for 9:"
pprint(dt90)


print "\nall for 9:"

pprint((dt90 + dt91 + dt92 + dt93 + dt94 + dt95 + dt96 + dt97 + dt98 + dt99).simplify())
