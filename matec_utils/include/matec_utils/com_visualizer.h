#ifndef COM_VISUALIZER_H
#define COM_VISUALIZER_H

#include <math.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <urdf/model.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/MarkerArray.h>

#include "matec_utils/urdf_manipulations.h"

namespace matec_utils
{
  class COMFinder
  {
  public:
    COMFinder(const ros::NodeHandle& nh);
    ~COMFinder();
    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
//    std::string m_root_frame;

    tf::TransformListener m_tf_listener;
//    tf::TransformBroadcaster m_tf_broadcaster;

    visualization_msgs::MarkerArray m_com_array;
    visualization_msgs::MarkerArray m_cov_array;

//    ros::Publisher m_com_pub;
//    ros::Publisher m_acom_pub;
//    ros::Publisher m_acov_pub;
    ros::Publisher m_acom_marker_pub;
    ros::Publisher m_acov_marker_pub;

    urdf::Model m_model;

    void configureMarkers();
//    void updateCOM();
  };
}
#endif //COM_VISUALIZER_H
