#ifndef TOLERANT_CARTESIAN_POSE_CONTROL_H
#define TOLERANT_CARTESIAN_POSE_CONTROL_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <geometry_msgs/Point.h>
#include <boost/thread.hpp>
#include <geometry_msgs/WrenchStamped.h>
#include <numeric>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <math.h>
#include <urdf/model.h>
#include <tf/tf.h>
#include <geometric_shapes/bodies.h>
#include <geometric_shapes/body_operations.h>
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/shape_operations.h>
#include <kdl/tree.hpp>
#include <kdl/frames.hpp>
#include <kdl/segment.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <tf_conversions/tf_kdl.h>
#include <tf/transform_datatypes.h>
#include <tf2/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/WrenchStamped.h>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <sensor_msgs/JointState.h>
#include <cmath>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/ConfigureCartesianControl.h>

#include <matec_utils/common_initialization_components.h>

namespace cartesian_pose_control
{
  typedef matec_msgs::ConfigureCartesianControl::Request Config;

#define EPSILON 0.0001
#define EPSILON_ANGLE M_PI/360.0
  class Tolerance
  {
  public:
    double x;
    double y;
    double z;
    double roll;
    double pitch;
    double yaw;

    Tolerance()
    {
      x = EPSILON;
      y = EPSILON;
      z = EPSILON;
      roll = EPSILON;
      pitch = EPSILON;
      yaw = EPSILON;
    }
  };

  class TolerantCartesianPoseControl
  {
  public:
    TolerantCartesianPoseControl(const ros::NodeHandle& nh);
    ~TolerantCartesianPoseControl();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    std::string m_interpolator;
    std::string m_controlled_frame;
    std::string m_last_controlled_link;
    std::vector<std::string> m_controlled_joints;
    std::vector<unsigned char> m_joint_map;
    std::vector<double> m_controlled_joint_mins;
    std::vector<double> m_controlled_joint_maxes;

    double m_max_velocity;
    int m_num_whole_range_attempts;
    int m_num_best_attempts;

    double m_dt;
    double m_last_timestamp;

    bool m_first_callback;

    ros::ServiceServer m_config_server;

    tf::TransformListener m_tf_listener;

    std::string m_chain_base_frame;

    sensor_msgs::JointState m_js;
    std::vector<std::string> m_joint_names;
    unsigned int m_num_joints;

    std::vector<std::string> m_field_names;
    std::vector<double> m_kps;
    std::vector<double> m_kvs;
    std::vector<double> m_kis;
    std::vector<double> m_i_clamps;
    std::vector<double> m_i_livebands;
    std::vector<double> m_i_accumulators;
    double m_center_spring;

    std::vector<double> m_last_deltas;
    KDL::JntArray m_last_positions;
    KDL::JntArray m_last_velocities;

    std::vector<double> m_controlled_joint_weights;
    std::vector<double> m_cartesian_weights;

    ros::Publisher m_error_pub;
    ros::Publisher m_current_pub;
    ros::Publisher m_target_pub;
    ros::Publisher m_cmd_pub;
    ros::Publisher m_cmd_vis_pub;
    ros::Subscriber m_js_sub;
    ros::Subscriber m_twist_sub;

    boost::mutex m_mutex;

    shared_memory_interface::Publisher<matec_msgs::TaskCommand> m_command_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<geometry_msgs::PoseStamped> m_pose_carrot_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;

    urdf::Model m_urdf_model;
    KDL::Tree m_tree;
    KDL::Chain m_chain;
    KDL::ChainIkSolverVel_wdls* m_inv_vel_solver;
    KDL::ChainFkSolverVel_recursive* m_fwd_vel_solver;
    KDL::ChainFkSolverPos_recursive* m_fwd_pos_solver;

    Tolerance m_tolerance;

    void getGains();

    void initModel();
    void findNearestActuatedLink(std::string frame);
    void findLongestControlChain();
    bool addControlledFrameOffsetToChain(KDL::Chain& chain);

    void unpackPositionTolerance(int preset);
    void unpackOrientationTolerance(int preset);
    void configureWeights();

    std::string getChainBase(KDL::Chain& chain);
    bool moveCarrotToChainBaseFrame(geometry_msgs::PoseStamped& pose_carrot);

    void getSearchBounds(KDL::JntArray& current, double dt, double max_velocity, std::vector<double>& mins, std::vector<double>& maxes);
    void replaceIfBetter(KDL::JntArray& iter_joints, KDL::JntArray& current_joints, KDL::Frame& target_frame, KDL::JntArray& best_joints, KDL::Frame& best_frame, double& best_accuracy_score, double& best_movement_score);

    bool getKDLSolution(matec_msgs::FullJointStates& current, geometry_msgs::PoseStamped pose_carrot, geometry_msgs::PoseStamped current_pose, KDL::JntArray& solution);

    void jsCallback(const sensor_msgs::JointStateConstPtr& msg);
    void twistCallback(const geometry_msgs::TwistStampedConstPtr& msg);
    void sensorCallback(matec_msgs::FullJointStates& msg);

    bool configureCallback(matec_msgs::ConfigureCartesianControl::Request& req, matec_msgs::ConfigureCartesianControl::Response& res);
  };
}
#endif //TOLERANT_CARTESIAN_POSE_CONTROL_H
