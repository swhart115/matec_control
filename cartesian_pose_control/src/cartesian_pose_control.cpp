#include "cartesian_pose_control/cartesian_pose_control.h"

namespace cartesian_pose_control
{
  CartesianPoseControl::CartesianPoseControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    matec_utils::blockOnSMJointNames(m_joint_names);
    matec_utils::blockOnTaskRegistration(m_nh.getNamespace());

    m_nh.param("loop_rate", m_loop_rate, 50.0);
    m_nh.param("controlled_frame", m_controlled_frame, std::string(""));

    if(m_controlled_frame.length() == 0)
    {
      ROS_FATAL("CPC: controlled_frame must be specified for CartesianPoseControl to work!");
      ros::shutdown();
      return;
    }

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints))
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("JointPositionControl couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }

    matec_utils::getFPVector(m_nh, "controlled_joint_weights", m_controlled_joints.size(), 1.0, m_controlled_joint_weights);
    matec_utils::getFPVector(m_nh, "cartesian_weights", 6, 1.0, m_cartesian_weights);

    initModel();
    findLongestControlChain();

    //figure out what parts of the chain we can actually use and weight the solver accordingly
    Eigen::MatrixXd cartesian_weight_matrix = Eigen::MatrixXd::Identity(6, 6); //TODO: param
    Eigen::MatrixXd joint_weight_matrix = Eigen::MatrixXd::Identity(m_chain.getNrOfJoints(), m_chain.getNrOfJoints());
    for(unsigned int i = 0; i < m_chain.getNrOfSegments(); i++) //possibly need to decrease this range by 1
    {
      std::string joint_name = m_chain.getSegment(i).getJoint().getName();
      ROS_INFO("Processing segment %d with joint %s", i, joint_name.c_str());

      unsigned int controlled_joint_idx = std::find(m_controlled_joints.begin(), m_controlled_joints.end(), joint_name) - m_controlled_joints.begin();
      if(controlled_joint_idx == m_controlled_joints.size())
      {
        joint_weight_matrix(i, i) = 0; //weight 0 ==> don't move the joint
        ROS_INFO("Joint %s is not controllable.", joint_name.c_str());
      }
      else
      {
        assert(controlled_joint_idx < m_controlled_joints.size());
        assert(m_controlled_joints.size() == m_controlled_joint_weights.size());
        joint_weight_matrix(i, i) = m_controlled_joint_weights[controlled_joint_idx];
        ROS_INFO("Joint %s is controllable.", joint_name.c_str());
      }
    }
    m_fwd_solver = new KDL::ChainFkSolverVel_recursive(m_chain);
    m_inv_solver = new KDL::ChainIkSolverVel_wdls(m_chain);
    m_inv_solver->setWeightJS(joint_weight_matrix);
    m_inv_solver->setWeightTS(cartesian_weight_matrix);

    m_field_names.push_back("p_x");
    m_field_names.push_back("p_y");
    m_field_names.push_back("p_z");
    m_field_names.push_back("r_x");
    m_field_names.push_back("r_y");
    m_field_names.push_back("r_z");
    m_kps.resize(m_field_names.size(), 0.0);
    m_kvs.resize(m_field_names.size(), 0.0);
    m_kis.resize(m_field_names.size(), 0.0);
    m_i_clamps.resize(m_field_names.size(), 0.0);
    m_i_livebands.resize(m_field_names.size(), 0.0);
    m_i_accumulators.resize(m_field_names.size(), 0.0);
    getGains();

    m_error_pub = m_nh.advertise<geometry_msgs::Twist>("cartesian_pose_error", 1, true);
    m_current_pub = m_nh.advertise<geometry_msgs::PoseStamped>("measured_cartesian_pose", 1, true);

    m_command_pub.advertise("task_command");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&CartesianPoseControl::sensorCallback, this, _1));
    m_pose_carrot_sub.subscribe("pose_carrot");
    m_root_link_odom_sub.subscribe("/root_link_odom");
  }

  CartesianPoseControl::~CartesianPoseControl()
  {

  }

  void CartesianPoseControl::findLongestControlChain()
  {
    //NOTE: currently assuming that all controlled joints are in the same chain!
    unsigned int longest_chain_length = 0;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      if(std::find(m_joint_names.begin(), m_joint_names.end(), m_controlled_joints[i]) == m_joint_names.end())
      {
        ROS_FATAL("Tried to control a joint that the SMI didn't know about! Shutting down!");
        ros::shutdown();
      }

      std::string parent_frame = m_urdf_model.getJoint(m_controlled_joints[i])->parent_link_name;

      KDL::Chain chain;
      m_model->getTree()->getChain(parent_frame, m_controlled_frame, chain);

      if(chain.getNrOfJoints() > longest_chain_length)
      {
        longest_chain_length = chain.getNrOfJoints();
        m_chain = chain;
      }
    }

    ROS_INFO("Using chain of %d joints from %s to %s", m_chain.getNrOfJoints(), m_chain.getSegment(0).getJoint().getName().c_str(), m_chain.getSegment(m_chain.getNrOfSegments() - 1).getJoint().getName().c_str());
  }

  void CartesianPoseControl::getGains()
  {
    m_nh.param("gains/center_spring", m_center_spring, 0.0);
    for(unsigned int i = 0; i < m_field_names.size(); i++)
    {
      boost::mutex::scoped_lock lock(m_mutex);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/p", m_kps[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/v", m_kvs[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i", m_kis[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i_clamp", m_i_clamps[i]);
      m_nh.getParamCached("gains/" + m_field_names[i] + "/i_liveband", m_i_livebands[i]);
    }
  }

  double clamp(double a, double b, double c)
  {
    return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
  }

  std::vector<double> extractPoseVector(geometry_msgs::PoseStamped pose)
  {
    double roll, pitch, yaw;
    tf::Quaternion quat;
    tf::quaternionMsgToTF(pose.pose.orientation, quat);
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

    std::vector<double> vector; //should match m_field_names
    vector.push_back(pose.pose.position.x);
    vector.push_back(pose.pose.position.y);
    vector.push_back(pose.pose.position.z);
    vector.push_back(roll);
    vector.push_back(pitch);
    vector.push_back(yaw);

    return vector;
  }

  std::vector<double> extractPoseVector(KDL::FrameVel frame_vel)
  {
    std::vector<double> vector;
    vector.push_back(frame_vel.p.v.x());
    vector.push_back(frame_vel.p.v.y());
    vector.push_back(frame_vel.p.v.z());
    vector.push_back(frame_vel.M.w.x());
    vector.push_back(frame_vel.M.w.y());
    vector.push_back(frame_vel.M.w.z());
    return vector;
  }

  KDL::Twist poseVectorToTwist(std::vector<double> pose_vector)
  {
    assert(pose_vector.size() == 6);
    return KDL::Twist(KDL::Vector(pose_vector[0], pose_vector[1], pose_vector[2]), KDL::Vector(pose_vector[3], pose_vector[4], pose_vector[5]));
  }

  KDL::JntArray stdToKDL(std::vector<double> array, std::vector<unsigned char> indices = std::vector<unsigned char>())
  {
    if(indices.size() > 0) //use subset of array
    {
      KDL::JntArray kdl_array(indices.size());
      for(unsigned int i = 0; i < indices.size(); i++)
      {
        kdl_array(i) = array[indices[i]];
      }
      return kdl_array;
    }
    else //use whole array
    {
      KDL::JntArray kdl_array(array.size());
      for(unsigned int i = 0; i < array.size(); i++)
      {
        kdl_array(i) = array[i];
      }
      return kdl_array;
    }
  }

  std::vector<double> KDLToStd(KDL::JntArray kdl_array, std::vector<unsigned char> indices = std::vector<unsigned char>())
  {
    std::vector<double> array;
    if(indices.size() > 0) //use subset of array
    {
      for(unsigned int i = 0; i < indices.size(); i++)
      {
        assert(!isnan(kdl_array(indices[i])));
        array.push_back(kdl_array(indices[i]));
      }
    }
    else //use whole array
    {
      for(unsigned int i = 0; i < kdl_array.rows() * kdl_array.columns(); i++)
      {
        assert(!isnan(kdl_array(i)));
        array.push_back(kdl_array(i));
      }
    }
    return array;
  }

  double score(KDL::Frame iter, KDL::Frame target, KDL::Frame current)
  {
    KDL::Frame current_frame_shift = target.Inverse() * current;
    double current_delta_roll, current_delta_pitch, current_delta_yaw;
    current_frame_shift.M.GetRPY(current_delta_roll, current_delta_pitch, current_delta_yaw);

    KDL::Frame frame_shift = target.Inverse() * iter;
    double delta_roll, delta_pitch, delta_yaw;
    frame_shift.M.GetRPY(delta_roll, delta_pitch, delta_yaw);

    double x_score = (fabs(frame_shift.p.x()) / fabs(current_frame_shift.p.x()));
    double y_score = (fabs(frame_shift.p.y()) / fabs(current_frame_shift.p.y()));
    double z_score = (fabs(frame_shift.p.z()) / fabs(current_frame_shift.p.z()));
    double roll_score = (fabs(delta_roll) / fabs(current_delta_roll));
    double pitch_score = (fabs(delta_pitch) / fabs(current_delta_pitch));
    double yaw_score = (fabs(delta_yaw) / fabs(current_delta_yaw));

    return (x_score + y_score + z_score + roll_score + pitch_score + yaw_score) / 6;
  }

  void CartesianPoseControl::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    matec_msgs::TaskCommand task_command;
    geometry_msgs::Twist error;
    geometry_msgs::PoseStamped pose_carrot, current_pose;
    if(!m_pose_carrot_sub.getCurrentMessage(pose_carrot))
    {
      ROS_WARN_THROTTLE(0.5, "CPC: No carrot received!");
      return;
    }

    nav_msgs::Odometry root_link_odom;
    if(!m_root_link_odom_sub.getCurrentMessage(root_link_odom))
    {
      ROS_WARN_THROTTLE(0.5, "CPC: No root link odometry received!");
      return;
    }
    m_model->updateModel(msg.position, root_link_odom.pose.pose);

    std::string chain_root = m_chain.getSegment(0).getName();
    current_pose = m_model->getFramePose(m_controlled_frame, chain_root);
    pose_carrot = m_model->transformPose(pose_carrot, chain_root); //TODO: return bool and check for success

    KDL::JntArray q, qd, qd_dot;
    q = stdToKDL(msg.position, m_joint_map);
    qd = stdToKDL(msg.velocity, m_joint_map);
    KDL::JntArrayVel q_qd(q, qd);
    KDL::FrameVel frame_vel;
    m_fwd_solver->JntToCart(q_qd, frame_vel);

    std::vector<double> current_pose_velocity_vector = extractPoseVector(frame_vel);
    std::vector<double> current_pose_vector = extractPoseVector(current_pose);
    std::vector<double> carrot_pose_vector = extractPoseVector(pose_carrot);

    std::vector<double> control_pose_vector(carrot_pose_vector.size());
    std::vector<double> error_pose_vector(carrot_pose_vector.size());

    for(unsigned int i = 0; i < m_field_names.size(); i++)
    {
      boost::mutex::scoped_lock lock(m_mutex);
      double pos_error = carrot_pose_vector[i] - current_pose_vector[i];
      double vel_error = 0 - current_pose_velocity_vector[i]; //TODO: feed forward velocity and acceleration?

      if(fabs(pos_error) <= m_i_livebands[i])
      {
        m_i_accumulators[i] = clamp(m_i_accumulators[i] + pos_error, -m_i_clamps[i], m_i_clamps[i]);
      }
      else
      {
        m_i_accumulators[i] = 0;
      }

      control_pose_vector[i] = m_kps[i] * pos_error + m_kvs[i] * vel_error + m_kis[i] * m_i_accumulators[i];
      error_pose_vector[i] = pos_error;
    }

    KDL::Twist twist = poseVectorToTwist(control_pose_vector);

    if(m_inv_solver->CartToJnt(q, twist, qd_dot) != 0)
    {
      ROS_WARN("Cartesian velocity solver failed");
      return;
    }

    double max_output_vel = 0.001; //nonzero to prevent division by zero
    for(unsigned int i = 0; i < qd_dot.rows(); i++)
    {
      if(fabs(qd(i)) > max_output_vel)
      {
        max_output_vel = fabs(qd(i));
      }
    }

    double max_allowable_vel = 0.5; //TODO: param
    double scale_factor = std::min(1.0, max_allowable_vel / max_output_vel);

    task_command.joint_commands.velocity.clear();
    for(unsigned int i = 0; i < qd_dot.rows(); i++)
    {
//      boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(m_chain.getSegment(i).getJoint().getName());
//      double min = joint->limits->lower;
//      double max = joint->limits->upper;
//      double center = (min + max) / 2;
//      double current_pos = (q(i) < min)? min : ((q(i) > max)? max : q(i));
//      double max_bound_distance = fabs(max - current_pos);
//      double min_bound_distance = fabs(current_pos - min);

      double vel = scale_factor*qd_dot(i);
      double pos = q(i) + vel / 1000.0; //todo: use trapezoids
      task_command.joint_commands.velocity.push_back(vel);
      task_command.joint_commands.position.push_back(pos);
    }

    task_command.joint_commands.joint_indices = m_joint_map;

    for(int i = m_controlled_joint_weights.size() - 1; i >= 0; --i)
    {
      if(m_controlled_joint_weights[i] == 0)
      {
        task_command.joint_commands.joint_indices.erase(task_command.joint_commands.joint_indices.begin() + i);
        task_command.joint_commands.velocity.erase(task_command.joint_commands.velocity.begin() + i);
        task_command.joint_commands.position.erase(task_command.joint_commands.position.begin() + i);
      }
    }
    m_command_pub.publish(task_command);

    error.linear.x = error_pose_vector[0];
    error.linear.y = error_pose_vector[1];
    error.linear.z = error_pose_vector[2];
    error.angular.x = error_pose_vector[3];
    error.angular.y = error_pose_vector[4];
    error.angular.z = error_pose_vector[5];
    m_error_pub.publish(error);
    m_current_pub.publish(current_pose);
  }

  void CartesianPoseControl::initModel()
  {
    m_urdf_model.initParam("/dynamics/robot_description");
    m_model = new matec_utils::ModelHelper(m_urdf_model, m_joint_names);
  }

  void CartesianPoseControl::spin()
  {
    ROS_INFO("CartesianPoseControl started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();

      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "cartesian_pose_control");
  ros::NodeHandle nh("~");

  cartesian_pose_control::CartesianPoseControl node(nh);
  node.spin();

  return 0;
}
