#ifndef URDF_MANIPULATIONS_H
#define URDF_MANIPULATIONS_H

#include <ros/ros.h>
#include <urdf/model.h>
#include <geometric_shapes/bodies.h>
#include <geometric_shapes/body_operations.h>
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/shape_operations.h>
#include <geometric_shapes/mesh_operations.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf/tf.h>

namespace matec_utils
{
  typedef std::vector<tf::Stamped<tf::Point> > PointPoly;

  struct BoundingBox
  {
    std::string link_frame_id;
    PointPoly box; //can have any number of points (though it should probably form a 3D polygon) and be specified relative to link_frame_id
  };

  //x: width, y: height, z: depth
  inline void boxInertia(double mass, double box_width, double box_height, double box_depth, double& ixx, double& iyy, double& izz)
  {
    ixx = mass * (box_height * box_height + box_depth * box_depth) / 12.0;
    iyy = mass * (box_width * box_width + box_depth * box_depth) / 12.0;
    izz = mass * (box_width * box_width + box_height * box_height) / 12.0;
  }

  //with z as the axis of the cylinder
  inline void cylinderInertia(double mass, double radius, double length, double& ixx, double& iyy, double& izz)
  {
    ixx = mass * (3.0 * radius * radius + length * length) / 12.0;
    iyy = mass * (3.0 * radius * radius + length * length) / 12.0;
    izz = mass * radius * radius / 2.0;
  }

  inline void getBoundingCylinderOfMesh(std::string mesh_file, bodies::BoundingCylinder &cyl)
  {
    if(!mesh_file.empty())
    {
      shapes::Shape* mesh;
      mesh = shapes::createMeshFromResource(mesh_file);
      const bodies::Body *body = new bodies::ConvexMesh(mesh);
      body->computeBoundingCylinder(cyl);
      //ROS_INFO("Bounding cylinder has radius: %0.3f  length: %0.3f", cyl.radius, cyl.length);
    }
    else
    {
      ROS_WARN("Empty mesh filename");
    }
  }

  inline urdf::Vector3 getBoundingBox(boost::shared_ptr<const urdf::Geometry> geom)
  {
    switch(geom->type)
    {
    case urdf::Geometry::SPHERE:
    {
      double diameter = 2 * dynamic_cast<const urdf::Sphere*>(geom.get())->radius;
      return urdf::Vector3(diameter, diameter, diameter);
    }
    case urdf::Geometry::BOX:
    {
      return dynamic_cast<const urdf::Box*>(geom.get())->dim;
    }
    case urdf::Geometry::CYLINDER:
    {
      double diameter = 2 * dynamic_cast<const urdf::Cylinder*>(geom.get())->radius;
      double length = dynamic_cast<const urdf::Cylinder*>(geom.get())->length;
      return urdf::Vector3(diameter, diameter, length);
    }
    case urdf::Geometry::MESH:
    {
      ROS_ERROR("Collision meshes are not currently supported. Use the collision_simplifier to make your meshes into cylinders.");
      return urdf::Vector3(0, 0, 0);
    }
    default:
    {
      ROS_ERROR("Unknown geometry type: %d", (int)geom->type);
      return urdf::Vector3(0, 0, 0);
    }
    }
  }

  inline std::pair<urdf::Vector3, urdf::Vector3> getBoundingBoxInJointFrame(boost::shared_ptr<const urdf::Collision> collision)
  {
    boost::shared_ptr<const urdf::Geometry> geom = collision->geometry;
    urdf::Vector3 dim = getBoundingBox(geom);

    double volume_percentage = 1.0; //TODO: param
    dim.x *= volume_percentage;
    dim.y *= volume_percentage;
    dim.z *= volume_percentage;

    //construct box of the right dimensions
    PointPoly box_points;
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(dim.x/2.0, dim.y/2.0, dim.z/2.0), ros::Time(0), "box_origin"));
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(dim.x/2.0, dim.y/2.0, -dim.z/2.0), ros::Time(0), "box_origin"));
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(dim.x/2.0, -dim.y/2.0, dim.z/2.0), ros::Time(0), "box_origin"));
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(dim.x/2.0, -dim.y/2.0, -dim.z/2.0), ros::Time(0), "box_origin"));
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(-dim.x/2.0, dim.y/2.0, dim.z/2.0), ros::Time(0), "box_origin"));
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(-dim.x/2.0, dim.y/2.0, -dim.z/2.0), ros::Time(0), "box_origin"));
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(-dim.x/2.0, -dim.y/2.0, dim.z/2.0), ros::Time(0), "box_origin"));
    box_points.push_back(tf::Stamped<tf::Point>(tf::Point(-dim.x/2.0, -dim.y/2.0, -dim.z/2.0), ros::Time(0), "box_origin"));

    //transform and min-max
    tf::Vector3 point(collision->origin.position.x, collision->origin.position.y, collision->origin.position.z);
    tf::Quaternion quat(collision->origin.rotation.x,collision->origin.rotation.y,collision->origin.rotation.z,collision->origin.rotation.w);
    tf::Transform origin(quat, point);
    tf::StampedTransform stamped_transform(origin, ros::Time(0), "joint_frame", "box_origin");
    tf::Transformer transformer;
    transformer.setTransform(stamped_transform);

    urdf::Vector3 min = urdf::Vector3(std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
    urdf::Vector3 max = urdf::Vector3(-std::numeric_limits<double>::max(), -std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
    for(unsigned int i = 0; i < box_points.size(); i++)
    {
      tf::Stamped<tf::Point> p;
      transformer.transformPoint("joint_frame", box_points[i], p);
      if(p.x() < min.x)
      {
        min.x = p.x();
      }
      if(p.y() < min.y)
      {
        min.y = p.y();
      }
      if(p.z() < min.z)
      {
        min.z = p.z();
      }
      if(p.x() > max.x)
      {
        max.x = p.x();
      }
      if(p.y() > max.y)
      {
        max.y = p.y();
      }
      if(p.z() > max.z)
      {
        max.z = p.z();
      }
    }

    return std::pair<urdf::Vector3, urdf::Vector3>(min, max);
  }

  inline bool getCOV(boost::shared_ptr<urdf::Link> link_ptr, geometry_msgs::Point& point)
  {
    switch(link_ptr->collision->geometry->type)
    {
    case urdf::Geometry::BOX:
    {
      point.x = link_ptr->collision->origin.position.x;
      point.y = link_ptr->collision->origin.position.y;
      point.z = link_ptr->collision->origin.position.z;
      break;
    }
    case urdf::Geometry::MESH:
    {
      urdf::Mesh* mesh = (urdf::Mesh*) link_ptr->collision->geometry.get();
      std::string mesh_file = mesh->filename.c_str();
      bodies::BoundingCylinder cyl;
      getBoundingCylinderOfMesh(mesh_file, cyl);

      geometry_msgs::Pose pose;
      tf::poseEigenToMsg(cyl.pose, pose);

      point.x = pose.position.x;
      point.y = pose.position.y;
      point.z = pose.position.z;
      break;
    }
    default:
      ROS_ERROR("Couldn't find the COV for link %s!", link_ptr->name.c_str());
      return false;
    }
    return true;
  }
}

#endif //URDF_MANIPULATIONS_H
