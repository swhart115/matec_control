#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H

#include <ros/ros.h>
#include <deque>

namespace interpolators
{
  class State
  {
  public:
    double position;
    double velocity;
    double acceleration;

    State()
    {
      position = 0;
      velocity = 0;
      acceleration = 0;
    }

    State(double position_, double velocity_, double acceleration_)
    {
      position = position_;
      velocity = velocity_;
      acceleration = acceleration_;
    }
  };

  class ViaPoint
  {
  public:
    ros::Time finish_time;
    State target_state;

    ViaPoint()
    {
    }

    ViaPoint(ros::Time finish_time_, double position_, double velocity_)
    {
      finish_time = finish_time_;
      target_state.position = position_;
      target_state.velocity = velocity_;
    }

    void print()
    {
      std::cerr << "(" << target_state.position << "," << target_state.velocity << ") @ " << finish_time;
    }
  };
  typedef std::deque<ViaPoint> Trajectory;

  class Interpolator
  {
  public:
    Interpolator()
    {
      m_have_valid_trajectory = false;
    }

    bool hasTrajectory()
    {
      return m_have_valid_trajectory;
    }

    void cancelTrajectory()
    {
      m_traj.clear();
      m_have_valid_trajectory = false;
    }

    //NOTE: the first ViaPoint in the trajectory should be the start state
    //      with finish_time equal to the time the trajectory should start
    virtual void loadTrajectory(Trajectory traj, bool populate_non_position_fields = true) = 0;

    virtual bool getInterpolatedState(ros::Time time, State& state)=0;

  protected:
    unsigned long m_last_slice_index;
    bool m_have_valid_trajectory;
    Trajectory m_traj;
  };

//  class LinearInterpolator: public Interpolator
//  {
//  public:
//    LinearInterpolator() :
//        Interpolator()
//    {
//
//    }
//
//    void loadTrajectory(Trajectory traj, bool populate_non_position_fields = true)
//    {
//      if(!populate_non_position_fields)
//      {
//        m_traj = traj;
//        return;
//      }
//      else //assume only positions and times have been set in the trajectory
//      {
//        m_traj.push_back(traj[0]);
////        traj[0].print();
//        assert(!std::isnan(traj[0].target_state.position) && !std::isinf(traj[0].target_state.position));
//        assert(!std::isnan(traj[0].target_state.velocity) && !std::isinf(traj[0].target_state.velocity));
//        for(unsigned int i = 1; i < traj.size(); i++)
//        {
//          double slice_time = (traj[i].finish_time - m_traj[i - 1].finish_time).toSec();
//          assert(slice_time >= 0.0);
//
//          if(slice_time <= 0.000001)
//          {
//            m_traj.push_back(m_traj[i - 1]);
//            ROS_WARN("Very small duration detected! Skipping waypoint!");
//            continue;
//          }
//
//          ViaPoint point;
//          point.finish_time = traj[i].finish_time;
//          point.target_state.position = traj[i].target_state.position;
//          point.target_state.velocity = (traj[i].target_state.position - m_traj[i - 1].target_state.position) / slice_time;
//          point.target_state.acceleration = 0;
//
//          assert(!std::isnan(traj[i].target_state.position) && !std::isinf(traj[i].target_state.position));
//          assert(!std::isnan(traj[i].target_state.velocity) && !std::isinf(traj[i].target_state.velocity));
//
////          point.print();
////          if(i == (traj.size() - 1))
////          {
////            std::cerr << std::endl;
////          }
////          else
////          {
////            std::cerr << ", ";
////          }
//          m_traj.push_back(point);
//        }
//      }
//
//      m_have_valid_trajectory = true;
//      m_last_slice_index = 1; //m_traj[0] is the start state
//    }
//
//    bool getInterpolatedState(ros::Time time, State& state)
//    {
//      if(!m_have_valid_trajectory)
//      {
//        return false;
//      }
//
//      //get current trajectory slice (assumes that finish_times are ordered)
//      ViaPoint from_via;
//      ViaPoint to_via;
//      for(unsigned int i = m_last_slice_index; i < m_traj.size(); i++)
//      {
//        if(time < m_traj[i].finish_time) //found the slice!
//        {
//          from_via = m_traj[i - 1];
//          to_via = m_traj[i];
//        }
//        else if(i == (m_traj.size() - 1)) //ran out of slices!
//        {
//          ROS_INFO("Trajectory complete!");
//          m_have_valid_trajectory = false;
//          return false;
//        }
//      }
//
//      //figure out where we should be
//      double dt = (time - from_via.finish_time).toSec();
//      double slice_time = (to_via.finish_time - from_via.finish_time).toSec();
//      double time_fraction = dt / slice_time;
//      state.position = (1 - time_fraction) * from_via.target_state.position + (time_fraction) * to_via.target_state.position;
//      state.velocity = from_via.target_state.velocity;
//      state.acceleration = (to_via.target_state.velocity - from_via.target_state.velocity) / slice_time;
//
//      ROS_INFO("dt:%g, st:%g, tf:%g, p:%g, v:%g, a:%g", dt, slice_time, time_fraction, state.position, state.velocity, state.acceleration);
//
//      return true;
//    }
//  };
//
//  class BangOffBangInterpolator: public Interpolator
//  {
//  public:
//    BangOffBangInterpolator(double max_accel = 0.5) :
//        Interpolator()
//    {
//      m_max_accel = max_accel;
//    }
//
//    void loadTrajectory(Trajectory traj, bool populate_non_position_fields = true)
//    {
//      m_traj.push_back(traj[0]);
////        traj[0].print();
//      assert(!std::isnan(traj[0].target_state.position) && !std::isinf(traj[0].target_state.position));
//      assert(!std::isnan(traj[0].target_state.velocity) && !std::isinf(traj[0].target_state.velocity));
//      unsigned int previous_idx = 0;
//      for(unsigned int i = 1; i < traj.size(); i++)
//      {
//        double T = (traj[i].finish_time - m_traj[previous_idx].finish_time).toSec();
//        ROS_INFO("Slice %d expected to take %gs", (int)i, T);
//        assert(T >= 0.0);
//
//        if(T <= 0.000001)
//        {
//          m_traj.push_back(m_traj[i - 1]);
//          ROS_WARN("Very small duration detected! Skipping waypoint!");
//          continue;
//        }
//
//        double p0 = m_traj[previous_idx].target_state.position;
//        double v0 = m_traj[previous_idx].target_state.velocity;
//        double pf = traj[i].target_state.position;
//        double vf = traj[i].target_state.velocity;
////        double dp = -(p0 - pf);
////        double A = (dp > 0.0)? -m_max_accel : m_max_accel;
//        double A = m_max_accel;
//
//        m_traj[previous_idx].print();
//        std::cerr << "->";
//        traj[i].print();
//        std::cerr << "=>" << std::endl;
//
//        //TODO: verify that the minimum time < requested time, and use minimum time instead if it's not possible
//        double minimum_time = 0; //TODO: fix calcs //dv + 2.0 * sqrt(dp + dv * dv / 2.0);
//
//        if(minimum_time >= T)
//        {
//          ROS_ERROR("Can't make the trajectory in %gs! Minimum time is (%g)!", T, minimum_time);
//          return;
//        }
//
//        //turn each point in the original trajectory into three accelerations
//        double dt1 = (A * A * T - A * v0 + A * vf - sqrt(A * A * (A * A * T * T + 2 * A * T * v0 + 2 * A * T * vf + 4 * A * p0 - 4 * A * pf - v0 * v0 + 2 * v0 * vf - vf * vf))) / (2 * A * A);
//        double dt2 = (A * T - A * dt1 - v0 + vf) / A;
//        double p1 = p0 + v0 * dt1 + A * dt1 * dt1 / 2.0;
//        double v1 = v0 + A * dt1;
//        double p2 = p1 + v1 * (dt2 - dt1);
//        double v2 = v1;
//
//        ROS_INFO("A:%g, dt1: %g, dt2: %g, p1: %g, v1: %g, p2: %g, v2: %g", A, dt1, dt2, p1, v1, p2, v2);
//
//        ViaPoint bang1;
//        bang1.finish_time = m_traj[previous_idx].finish_time + ros::Duration(dt1);
//        bang1.target_state.position = p1;
//        bang1.target_state.velocity = v1;
//        bang1.target_state.acceleration = A; //accel is accel during this segement
//        std::cerr << "bang1: ";
//        bang1.print();
//        std::cerr << std::endl;
//
//        ViaPoint off;
//        off.finish_time = m_traj[previous_idx].finish_time + ros::Duration(dt2);
//        off.target_state.position = p2;
//        off.target_state.velocity = v2;
//        off.target_state.acceleration = 0; //accel is accel during this segement
//        std::cerr << "off: ";
//        off.print();
//        std::cerr << std::endl;
//
//        ViaPoint bang2;
//        bang2.finish_time = m_traj[previous_idx].finish_time + ros::Duration(T);
//        bang2.target_state.position = pf; //accel is accel during this segement
//        bang2.target_state.velocity = vf; //TODO: set final velocity more intelligently
//        bang2.target_state.acceleration = -A;
//        std::cerr << "bang2: ";
//        bang2.print();
//        std::cerr << std::endl;
//
//        m_traj.push_back(bang1);
//        m_traj.push_back(off);
//        m_traj.push_back(bang2);
//      }
//
//      m_have_valid_trajectory = true;
//      m_last_slice_index = 1; //m_traj[0] is the start state
//    }
//
//    bool getInterpolatedState(ros::Time time, State& state)
//    {
//      if(!m_have_valid_trajectory)
//      {
//        return false;
//      }
//
//      //get current trajectory slice (assumes that finish_times are ordered)
//      ViaPoint from_via;
//      ViaPoint to_via;
//      for(unsigned int i = m_last_slice_index; i < m_traj.size(); i++)
//      {
//        if(time < m_traj[i].finish_time) //found the slice!
//        {
//          from_via = m_traj[i - 1];
//          to_via = m_traj[i];
//        }
//        else if(i == (m_traj.size() - 1)) //ran out of slices!
//        {
//          ROS_INFO("Trajectory complete!");
//          m_have_valid_trajectory = false;
//          return false;
//        }
//      }
//
//      //figure out where we should be
//      double dt = (time - from_via.finish_time).toSec();
//      double slice_time = (to_via.finish_time - from_via.finish_time).toSec();
//      double time_fraction = dt / slice_time;
//      state.position = from_via.target_state.position + from_via.target_state.acceleration * dt * dt;
//      state.velocity = from_via.target_state.velocity + from_via.target_state.acceleration * dt;
//      state.acceleration = from_via.target_state.acceleration;
//
////      ROS_INFO("dt:%g, st:%g, tf:%g, p:%g, v:%g, a:%g", dt, slice_time, time_fraction, state.position, state.velocity, state.acceleration);
//
//      return true;
//    }
//
//  private:
//    double m_max_accel;
////  };
//
//  class MinimumJerkInterpolator: public Interpolator
//  {
//  public:
//    MinimumJerkInterpolator() :
//        Interpolator()
//    {
//
//    }
//
//    void loadTrajectory(Trajectory traj, bool populate_non_position_fields = true)
//    {
////      m_last_state = traj[0].target_state;
//      m_traj.clear();
//      m_traj.push_back(traj[0]);
//      m_traj.push_back(traj[traj.size() - 1]);
//
//      if(traj.size() != 2)
//      {
//        ROS_WARN("IGNORING INTERMEDIATE VIAPOINTS AND SKIPPING TO THE END!");
//      }
//
//      m_have_valid_trajectory = true;
//      m_last_slice_index = 1; //m_traj[0] is the start state
//    }
//
////    State m_last_state;
////    double m_last_jerk;
////    ros::Time m_last_state_time;
//
//    bool getInterpolatedState(ros::Time time, State& state)
//    {
//      if(!m_have_valid_trajectory)
//      {
//        return false;
//      }
//
//      //get current trajectory slice (assumes that finish_times are ordered)
//      ViaPoint from_via;
//      ViaPoint to_via;
//      for(unsigned int i = m_last_slice_index; i < m_traj.size(); i++)
//      {
//        if(time < m_traj[i].finish_time) //found the slice!
//        {
//          from_via = m_traj[i - 1];
//          to_via = m_traj[i];
//        }
//        else if(i == (m_traj.size() - 1)) //ran out of slices!
//        {
//          ROS_INFO("Trajectory complete!");
//          m_have_valid_trajectory = false;
//          state = m_traj[m_traj.size() - 1].target_state;
//          return false;
//        }
//      }
//
//      //figure out where we should be
//      double xi = from_via.target_state.position;
//      double xf = to_via.target_state.position;
//
//      double t = (time - from_via.finish_time).toSec();
//      double t2 = t * t;
//      double t3 = t2 * t;
//      double t4 = t3 * t;
//      double t5 = t4 * t;
////      double dt = (time - m_last_state_time).toSec();
//      double T = (to_via.finish_time - from_via.finish_time).toSec();
//      double T3 = T * T * T;
//      double T4 = T3 * T;
//      double T5 = T4 * T;
//      double jerk = (xf - xi) * (360.0 / T5 * t2 - 360 / T4 * t + 60 / T3);
//      state.acceleration = (xf - xi) * (120 * t3 / T5 - 180 * t2 / T4 + 60 * t / T3); //m_last_state.acceleration + 0.5*dt*(m_last_jerk + jerk);
//      state.velocity = (xf - xi) * 60 * (t4 / 2.0 / T5 - t3 / T4 + t2 / 2.0 / T3); //m_last_state.velocity + 0.5*dt*(m_last_state.acceleration + state.acceleration);
//      state.position = (xf - xi) * (6 * t5 / T5 - 15 * t4 / T4 + 10 * t3 / T3) + xi; //m_last_state.position + 0.5*dt*(m_last_state.velocity + state.velocity);
//
////      ROS_INFO("Moving from %g to %g in %gs", xi, xf, );
//
////      m_last_jerk = jerk;
////      m_last_state = state;
////      m_last_state_time = time;
//
//      return true;
//    }
//  };
//
//  class JerkInterpolator: public Interpolator
//  {
//  public:
//    JerkInterpolator(double max_vel = 1.0, double max_accel = 1.0, double max_jerk = 1.0) :
//        Interpolator()
//    {
//      m_max_vel = max_vel;
//      m_max_accel = max_accel;
//      m_max_jerk = max_jerk;
//    }
//
//    void loadTrajectory(Trajectory traj, bool populate_non_position_fields = true)
//    {
//      assert(traj.size() >= 2);
//      unsigned int previous_idx = 0;
//      m_slices.clear();
//      for(unsigned int i = 1; i < traj.size(); i++)
//      {
//        ros::Time start_time = traj[previous_idx].finish_time;
////        double T_desired = (traj[i].finish_time - start_time).toSec();
//        int sign = (traj[i].target_state.position - traj[previous_idx].target_state.position) > 0.0? 1 : -1;
//
//        double q0 = sign * traj[previous_idx].target_state.position;
//        double v0 = sign * traj[previous_idx].target_state.velocity;
//        double q1 = sign * traj[i].target_state.position;
//        double v1 = sign * traj[i].target_state.velocity;
//        double v_max = sign * m_max_vel;
//        double a_max = sign * m_max_accel;
//        double j_max = sign * m_max_jerk;
//
//        //TODO: see http://www.amazon.com/Trajectory-Planning-Automatic-Machines-Robots/dp/3642099238
//        //section 3.4.5 pg 101
//
////        //see if we can do it with a double jerk impulse
//        double Tj = std::min(sqrt(fabs(v1 - v0) / j_max), a_max / j_max);
////        if(Tj < (a_max / j_max))
////        {
////          if((q1 - q0) > (Tj * (v0 + v1))) //feasible
////          {
////          }
////          else
////          {
////            ROS_ERROR("BAD1?");
////            return;
////          }
////        }
////        else if(Tj == (a_max / j_max))
////        {
////          if((q1 - q0) > (0.5 * (v0 + v1) * (Tj + fabs(v1 - v0) / (a_max)))) //feasible
////          {
////          }
////          else
////          {
////            ROS_ERROR("BAD2?");
////            return;
////          }
////        }
////        else
////        {
////          ROS_ERROR("BAD3?");
////          return;
////        }
//
//        double Tj1, Tj2, Ta, Tv, Td, T, v_lim;
//
//        //case 1: velocity hits max
//        if(((v_max - v0) * j_max) < (a_max * a_max)) //a_max not reached
//        {
//          Tj1 = sqrt((v_max - v0) / j_max);
//          Ta = 2 * Tj1;
//        }
//        else
//        {
//          Tj1 = a_max / j_max;
//          Ta = Tj1 + (v_max - v0) / a_max;
//        }
//        if(((v_max - v1) * j_max) < (a_max * a_max)) //a_min not reached
//        {
//          Tj2 = sqrt((v_max - v1) / j_max);
//          Td = 2 * Tj2;
//        }
//        else
//        {
//          Tj2 = a_max / j_max;
//          Td = Tj2 + (v_max - v1) / a_max;
//        }
//        Tv = (q1 - q0) / v_max - 0.5 * Ta * (1 + v0 / v_max) - 0.5 * Td * (1 + v1 / v_max);
//        v_lim = v_max;
//
//        if(Tv <= 0) //case 2: velocity didn't hit max
//        {
//          Tv = 0;
//          Tj1 = a_max / j_max;
//          Tj2 = a_max / j_max;
//          double sqrt_del = sqrt((a_max * a_max * a_max * a_max) / (j_max * j_max) + 2 * (v0 * v0 + v1 * v1) + a_max * (4 * (q1 - q0) - 2 * a_max / j_max * (v0 + v1)));
//          Ta = ((a_max * a_max) / j_max - 2 * v0 + sqrt_del) / (2 * a_max);
//          Td = ((a_max * a_max) / j_max - 2 * v1 + sqrt_del) / (2 * a_max);
//
//          if(Ta >= 0 && Td <= 0 && (Ta <= (2 * Tj) || Td <= (2 * Tj)))
//          {
//            std::cerr << "Maximum acceleration too low to achieve goal!";
//            return;
//          }
//          if(Td < 0) //3.29b
//          {
//            Td = 0;
//            Ta = 2.0 * (q1 - q0) / (v1 + v0);
//            Tj1 = (j_max * (q1 - q0) - sqrt(j_max * (j_max * (q1 - q0) * (q1 - q0) - (v1 + v0) * (v1 + v0) * (v1 - v0)))) / (j_max * (v1 + v0));
//          }
//          if(Ta < 0) //3.28b
//          {
//            Ta = 0;
//            Td = 2.0 * (q1 - q0) / (v1 + v0);
//            Tj2 = (j_max * (q1 - q0) - sqrt(j_max * (j_max * (q1 - q0) * (q1 - q0) + (v1 + v0) * (v1 + v0) * (v1 - v0)))) / (j_max * (v1 + v0));
//          }
//
//          double a_lim_a = j_max * Tj1;
////          double a_lim_d = -j_max * Tj2;
//          v_lim = v0 + (Ta - Tj1) * a_lim_a;
//        }
//
//        T = Ta + Tv + Td;
//
//        ROS_INFO("Segment time is %g: Ta=%g, Tv=%g, Td=%g", T, Ta, Tv, Td);
//
//        JerkViaPoint slice;
//        slice.q0 = q0;
//        slice.q1 = q1;
//        slice.v0 = v0;
//        slice.v1 = v1;
//        slice.v_lim = v_lim;
//        slice.Tj1 = Tj1;
//        slice.Ta = Ta;
//        slice.Tj2 = Tj2;
//        slice.Td = Td;
//        slice.Tv = Tv;
//        slice.T = T;
//        slice.start_time = start_time;
//        slice.sign = sign;
//        m_slices.push_back(slice);
//      }
//
//      m_have_valid_trajectory = true;
//      m_last_slice_index = 0;
//    }
//
//    bool getInterpolatedState(ros::Time time, State& state)
//    {
//      if(!m_have_valid_trajectory)
//      {
//        return false;
//      }
//
//      //get current trajectory slice (assumes that finish_times are ordered)
//      JerkViaPoint slice;
//      for(unsigned int i = m_last_slice_index; i < m_slices.size(); i++)
//      {
//        if(time <= (m_slices[i].start_time + ros::Duration(m_slices[i].T))) //found the current slice!
//        {
//          slice = m_slices[i];
//        }
//        else if(i == (m_slices.size() - 1)) //ran out of slices!
//        {
//          ROS_INFO("Trajectory complete!");
//          m_have_valid_trajectory = false;
//          return false;
//        }
//      }
//
//      //unpack
//      double q0 = slice.q0;
//      double q1 = slice.q1;
//      double v0 = slice.v0;
//      double v1 = slice.v1;
//      double v_lim = slice.v_lim;
//      double Tj1 = slice.Tj1;
//      double Ta = slice.Ta;
//      double Tj2 = slice.Tj2;
//      double Td = slice.Td;
//      double Tv = slice.Tv;
//      double T = slice.T;
//      ros::Time start_time = slice.start_time;
//      int sign = slice.sign;
////      double a_max = m_max_accel * sign;
//      double j_max = m_max_jerk * sign;
////      double a_min = -m_max_accel * sign;
//      double j_min = -m_max_jerk * sign;
//
//      //figure out where we should be
//      double t = (time - start_time).toSec();
//      if(t <= Tj1) //acceleration A (T: 0->Tj1, jerk=j_max)
//      {
//        std::cerr << "acc A: ";
//        state.position = q0 + v0 * t + j_max * t * t * t / 6.0;
//        state.velocity = v0 + j_max * t * t / 2.0;
//        state.acceleration = j_max * t;
//      }
//      else if(t <= Ta - Tj1)
//      {
//        std::cerr << "acc B: ";
//        //acceleration B (T: Tj1->Ta-Tj1, jerk=0)
//        double a_lim_a = j_max * Tj1;
//        state.position = q0 + v0 * t + a_lim_a * (3 * t * t - 3 * Tj1 * t + Tj1 * Tj1) / 6.0;
//        state.velocity = v0 + a_lim_a * (t - Tj1 / 2.0);
//        state.acceleration = a_lim_a;
//      }
//      else if(t <= Ta)
//      {
//        std::cerr << "acc C: ";
//        //acceleration C (T: Ta-Tj1->Ta, jerk=-j_max)
//        state.position = q0 + (v_lim + v0) * Ta / 2.0 - v_lim * (Ta - t) - j_min * (Ta - t) * (Ta - t) * (Ta - t) / 6.0;
//        state.velocity = v_lim + j_min * (Ta - t) * (Ta - t) / 2.0;
//        state.acceleration = -j_min * (Ta - t);
//      }
//      else if(t <= Ta + Tv)
//      {
//        std::cerr << "const: ";
//        //constant velocity (T: Ta->Ta+Tv, jerk=0)
//        state.position = q0 + (v_lim + v0) * Ta / 2.0 + v_lim * (t - Ta);
//        state.velocity = v_lim;
//        state.acceleration = 0.0;
//      }
//      else if(t <= T - Td + Tj2)
//      {
//        std::cerr << "dec A: ";
//        //deceleration A (T: T-Td->T-Td+Tj2, jerk=-j_max)
//        state.position = q1 - (v_lim + v1) * Td / 2.0 + v_lim * (t - T + Td) - j_max * (t - T + Td) * (t - T + Td) * (t - T + Td) / 6.0;
//        state.velocity = v_lim - j_max * (t - T + Td) * (t - T + Td) / 2.0;
//        state.acceleration = -j_max * (t - T + Td);
//      }
//      else if(t <= T - Tj2)
//      {
//        std::cerr << "dec B: ";
//        //deceleration B (T: T-Td+Tj2->T-Tj2, jerk=0)
//        double a_lim_d = -j_max * Tj2;
//        state.position = q1 - (v_lim + v1) * Td / 2.0 + v_lim * (t - T + Td) + a_lim_d * (3 * (t - T + Td) * (t - T + Td) - 3 * Tj2 * (t - T + Td) + Tj2 * Tj2) / 6.0;
//        state.velocity = v_lim + a_lim_d * (t - T + Td - Tj2 / 2.0);
//        state.acceleration = a_lim_d;
//      }
//      else if(t <= T)
//      {
//        std::cerr << "dec C: ";
//        //deceleration C (T: T-Tj2->T, jerk=j_max)
//        state.position = q1 - v1 * (T - t) - j_max * (T - t) * (T - t) * (T - t) / 6.0;
//        state.velocity = v1 + j_max * (T - t) * (T - t) / 2.0;
//        state.acceleration = -j_max * (T - t);
//      }
//      else
//      {
//        ROS_INFO("Trajectory complete!");
//        m_have_valid_trajectory = false;
//        ROS_WARN("Trajectory expired, but we didn't catch it earlier for some reason...");
//        return false;
//      }
//
//      state.position *= sign;
//      state.velocity *= sign;
//      state.acceleration *= sign;
//      return true;
//    }
//
//  private:
//    struct JerkViaPoint
//    {
//      double q0;
//      double q1;
//      double v0;
//      double v1;
//      double v_lim;
//      double Tj1;
//      double Ta;
//      double Tj2;
//      double Td;
//      double Tv;
//      double T;
//      ros::Time start_time;
//      int sign;
//    };
//
//    double m_max_vel;
//    double m_max_accel;
//    double m_max_jerk;
//    std::vector<JerkViaPoint> m_slices;
//  };
}

#endif //INTERPOLATOR_H
