#include "inverse_dynamics/rnea.h"

namespace inverse_dynamics
{
  RNEA::RNEA()
  {
    srand(time(NULL));
    base_position = Vector6::Zero();
    base_velocity = Vector6::Zero();
    base_gravity_acceleration = Vector6::Zero();

    sample_time = std::numeric_limits<double>::quiet_NaN();
    last_sample_time = std::numeric_limits<double>::quiet_NaN();
  }

  void RNEA::computeTorques(matec_msgs::FullJointStates& current, matec_msgs::FullJointStates& desired, matec_msgs::WrenchArray& external_wrenches)
  {
    m_current = &current;
    m_desired = &desired;
    m_external_wrenches = &external_wrenches;
    computeTorquesRecursive(6);
  }

  Vector6 findExternalWrench(std::string link_name, matec_msgs::WrenchArray* external_wrenches)
  {
    Vector6 external_wrench = Vector6::Zero();
    for(unsigned int i = 0; i < external_wrenches->wrenches.size(); i++)
    {
      if(external_wrenches->wrenches[i].header.frame_id == link_name)
      {
        external_wrench(0) = external_wrenches->wrenches[i].wrench.torque.x;
        external_wrench(1) = external_wrenches->wrenches[i].wrench.torque.y;
        external_wrench(2) = external_wrenches->wrenches[i].wrench.torque.z;
        external_wrench(3) = external_wrenches->wrenches[i].wrench.force.x;
        external_wrench(4) = external_wrenches->wrenches[i].wrench.force.y;
        external_wrench(5) = external_wrenches->wrenches[i].wrench.force.z;
      }
    }
    return external_wrench;
  }

  void RNEA::computeTorquesRecursive(long i)
  {
    DynamicsTreeNode* self = &nodes[i];
    DynamicsTreeNode* parent = &nodes[self->parent_index];

    //process this node forward
    if(i == 6) //model root
    {
      extractBaseStates(m_current, gravity_constant, base_position, base_velocity, base_gravity_acceleration);

      self->v = Vector6::Zero();
      self->a = base_gravity_acceleration;

      self->iXip = Matrix6::Identity();
    }
    else
    {
      self->qd = m_desired->position[i - 1];
      self->qd_dot = m_desired->velocity[i - 1];
      self->qd_dot_dot = m_desired->acceleration[i - 1];
      self->q = m_current->position[i - 1];
      self->q_dot = m_current->velocity[i - 1];
      self->f_ext = findExternalWrench(self->link_name, m_external_wrenches);

      self->iXip = kdlToEigen(self->segment.pose(self->q).Inverse());

      Vector6 joint_spatial_velocity = (self->S * self->q_dot);
      self->v = (self->iXip * parent->v) + joint_spatial_velocity;
      self->a = (self->iXip * parent->a) + (self->S * self->qd_dot_dot) + cross(self->v) * joint_spatial_velocity;
    }

    //process children forward
    self->f = self->inertia * self->a - cross(self->v).transpose() * self->inertia * self->v; // + self->f_ext;
    self->composite_inertia = self->inertia;
    for(unsigned int j = 0; j < self->child_indices.size(); j++)
    {
      int child_idx = self->child_indices[j];
      DynamicsTreeNode* child = &nodes[child_idx];

      computeTorquesRecursive(child_idx); //recurse

      Matrix6 icXi = child->iXip;
      Matrix6 icXi_tranpose = icXi.transpose();
      self->f += icXi_tranpose * child->f;
      self->composite_inertia += icXi_tranpose * child->composite_inertia * icXi;
    }

    double gain = 1.0; //((double) (self->S.transpose() * self->estimated_composite_inertia * self->S));
    self->torque = self->S.transpose() * self->f;
  }

  void RNEA::loadSegmentIndex(KDL::Segment segment, int idx)
  {
    nodes[idx].link_name = segment.getName();
    nodes[idx].joint_name = segment.getJoint().getName();
    nodes[idx].joint_idx = idx;

    //load kinematics
    nodes[idx].segment = segment;
    nodes[idx].S = Vector6::Zero();
    if(nodes[idx].segment.getJoint().getType() == KDL::Joint::RotAxis)
    {
      nodes[idx].type = DynamicsTreeNode::REVOLUTE;
      nodes[idx].S.topRows(3) = kdlToEigen(segment.getJoint().JointAxis());
    }
    else if(nodes[idx].segment.getJoint().getType() == KDL::Joint::TransAxis)
    {
      nodes[idx].type = DynamicsTreeNode::PRISMATIC;
      nodes[idx].S.bottomRows(3) = kdlToEigen(segment.getJoint().JointAxis());
    }
    else //fixed joint
    {
      nodes[idx].type = DynamicsTreeNode::FIXED;
    }

    //load dynamics
    boost::shared_ptr<const urdf::Link> link = m_urdf_model.getLink(nodes[idx].link_name);
    boost::shared_ptr<const urdf::Joint> joint = m_urdf_model.getJoint(nodes[idx].joint_name);

    if(!link || !joint)
    {
      ROS_WARN("Node %s was not in the urdf!", nodes[idx].link_name.c_str());
      return;
    }

    nodes[idx].inertia = kdlToEigen(segment.getInertia());
  }

  void RNEA::loadRecursive(int idx, const KDL::SegmentMap::const_iterator segment)
  {
    std::string name = segment->second.segment.getName();
    std::string joint_name = segment->second.segment.getJoint().getName();
    ROS_INFO("Parsing link %s(%s)", name.c_str(), joint_name.c_str());

    loadSegmentIndex(segment->second.segment, idx);

    const std::vector<KDL::SegmentMap::const_iterator>& children = segment->second.children;
    for(unsigned int i = 0; i < children.size(); i++)
    {
      const KDL::Segment& child = children[i]->second.segment;
      std::string child_name = child.getName();
      std::string child_joint_name = child.getJoint().getName();
      long child_idx = std::find(link_names.begin(), link_names.end(), child_name) - link_names.begin();

      ROS_INFO("%s(%d)->%s(%d)", name.c_str(), (int) idx, child_name.c_str(), (int) child_idx);

      if(child_idx >= 0 && child_idx < (int) nodes.size())
      {
        nodes[idx].child_indices.push_back(child_idx);
        nodes[child_idx].parent_index = idx;

        loadRecursive(child_idx, children[i]);
      }
      else
      {
        ROS_INFO("Combining %s and %s attached via fixed link %s", name.c_str(), child_name.c_str(), child_joint_name.c_str());

        Matrix6 child_inertia = kdlToEigen(child.getInertia());

        KDL::Frame child_frame = child.pose(0).Inverse();
        Matrix6 icXi = kdlToEigen(child_frame);
        nodes[idx].inertia += icXi.transpose() * child_inertia * icXi;
      }
    }

    std::stringstream ss;
    ss << name.c_str() << "(" << idx << ") has children {";
    for(unsigned int i = 0; i < nodes[idx].child_indices.size(); i++)
    {
      if(i != 0)
      {
        ss << ", ";
      }
      ss << nodes[idx].child_indices[i];
    }
    ss << "}";
    ROS_INFO("%s", ss.str().c_str());
  }

  void RNEA::load(urdf::Model model, std::vector<std::string> joint_names)
  {
    m_urdf_model = model;
    if(!matec_utils::treeFromUrdfModel((const urdf::Model) m_urdf_model, m_kdl_tree, true))
    {
      ROS_ERROR("Failed to parse urdf model!");
      return;
    }

    std::string floating_link_names[] = {"FLOATING_0", "FLOATING_1", "FLOATING_2", "FLOATING_3", "FLOATING_4", "FLOATING_5"};
    link_names = std::vector<std::string>(&floating_link_names[0], &floating_link_names[6]);
    link_names.push_back(model.getRoot()->name);
    for(unsigned int i = 0; i < joint_names.size(); i++)
    {
      link_names.push_back(model.getJoint(joint_names[i])->child_link_name);
    }
    nodes.resize(link_names.size());

    for(unsigned int i = 0; i < link_names.size(); i++)
    {
      ROS_INFO("Link %d is %s", i, link_names[i].c_str());
    }

    //load the rest of the tree recursively
    loadRecursive(0, m_kdl_tree.getRootSegment());
  }

  std::vector<double> RNEA::getTorques()
  {
    std::vector<double> torques;
    for(unsigned int i = 7; i < nodes.size(); i++)
    {
      torques.push_back(nodes[i].torque);
    }
    return torques;
  }
}
