#include "inverse_dynamics/rbdl_inverse_dynamics.h"

namespace inverse_dynamics
{
  RBDLInverseDynamics::RBDLInverseDynamics(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);
    m_nh.param("print_debug", m_print_debug, false);
    m_nh.param("gravity_constant", m_gravity_constant, 9.81);

    m_cmd_received = false;
    m_odom_received = false;

    matec_utils::blockOnSMJointNames(m_joint_names);
    assert(m_joint_names.size() > 0);

    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      m_name_to_idx_map[m_joint_names[i]] = i;
    }

    m_num_joints = m_joint_names.size();

    initModel();
    m_reload_model_service_server = m_nh.advertiseService("/reload_model", &RBDLInverseDynamics::reloadModelCallback, this);

    m_js_pub = m_nh.advertise<sensor_msgs::JointState>("/joint_commands", 1, true);

    m_joint_command_pub.advertise("/joint_commands");
    m_com_position_pub.advertise("/com_position");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&RBDLInverseDynamics::sensorCallback, this, _1));
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_prioritized_command_sub.subscribe("/prioritized_command");
  }

  RBDLInverseDynamics::~RBDLInverseDynamics()
  {
  }

  bool RBDLInverseDynamics::reloadModelCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
  {
    initModel();
    return true;
  }

  void RBDLInverseDynamics::initModel()
  {
    while(!m_nh.hasParam("/dynamics/robot_description") && ros::ok())
    {
      ROS_WARN_THROTTLE(0.5, "RBDLInverseDynamics waiting for /dynamics/robot_description parameter!");
    }

    m_urdf_model.initParam("/dynamics/robot_description");
    RigidBodyDynamics::Addons::read_urdf_model(m_urdf_model, m_model, m_print_debug); //todo: force indices to match sm?
    ROS_INFO("RBDLInverseDynamics: Loaded model with %d-actuated-DOFs!", m_model.dof_count - 6);
    assert(m_model.mBodies.size() > 6);
    //6 DOFs are for the floating base

    m_rbdl_to_sm.resize(std::max(m_num_joints, m_model.dof_count), -1);
    m_sm_to_rbdl.resize(std::max(m_num_joints, m_model.dof_count), -1);
    m_rbdl_body_names.clear();
    for(unsigned int i = 0; i < m_model.mBodies.size(); i++)
    {
      std::string rbdl_body_name = m_model.GetBodyName(i);
      m_rbdl_body_names.push_back(rbdl_body_name);
      if(i == 6)
      {
        if(m_print_debug)
        {
          ROS_INFO("RBDL body #%d is %s, attached to the world as a floating base.", i, m_model.GetBodyName(i).c_str());
        }
      }
      if(i > 6)
      {
        int rbdl_idx = i - 1;
        std::string joint_name = m_urdf_model.getLink(rbdl_body_name)->parent_joint->name; //TODO: don't just assume that it's in there
        int sm_idx = m_name_to_idx_map[joint_name];
        assert(sm_idx < (int) m_joint_names.size());
        m_rbdl_to_sm[rbdl_idx] = sm_idx;
        m_sm_to_rbdl[sm_idx] = rbdl_idx;
        if(m_print_debug)
        {
          ROS_INFO("RBDL body #%d is %s, attached to joint %s (rbdl:%d, sm:%d)", i, m_model.GetBodyName(i).c_str(), joint_name.c_str(), rbdl_idx, sm_idx);
        }
      }
    }
  }

  void RBDLInverseDynamics::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    //sanity check
    if(msg.position.size() != m_num_joints)
    {
      ROS_ERROR("Got position measurement of size %d, expected %d", (int) msg.position.size(), m_num_joints);
      return;
    }
    if(msg.velocity.size() != m_num_joints)
    {
      ROS_ERROR("Got velocity measurement of size %d, expected %d", (int) msg.velocity.size(), m_num_joints);
      return;
    }
    assert(msg.position.size() == m_num_joints && msg.velocity.size() == m_num_joints);

    if(!m_root_link_odom_sub.getCurrentMessage(m_odom))
    {
      ROS_WARN_THROTTLE(0.5, "RBDLInverseDynamics: No command received!");
      return;
    }

    RigidBodyDynamics::Math::VectorNd q(m_model.dof_count), q_dot(m_model.dof_count), qd_dot(m_model.dof_count), tau(m_model.dof_count);

    //root link measurements
    double roll, pitch, yaw;
    tf::Quaternion quat;
    tf::quaternionMsgToTF(m_odom.pose.pose.orientation, quat);
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);
    q(0) = m_odom.pose.pose.position.x;
    q(1) = m_odom.pose.pose.position.y;
    q(2) = m_odom.pose.pose.position.z;
    q(3) = roll;
    q(4) = pitch;
    q(5) = yaw;
    assert(!isnan(roll));
    assert(!isnan(pitch));
    assert(!isnan(yaw));

    //eigen doesn't initialize things to zero
    for(unsigned int i = 0; i < 6; i++)
    {
      q_dot(i) = 0;
      qd_dot(i) = 0;
    }

    //joint measurements
    for(unsigned int i = 6; i < m_model.dof_count; i++)
    {
      unsigned int sm_idx = m_rbdl_to_sm[i];
      assert(sm_idx < msg.position.size());
      q(i) = msg.position[sm_idx];
      q_dot(i) = msg.position[sm_idx];
      assert(!isnan((double)q(i)));
      assert(!isnan((double)q_dot(i)));
    }

    double mass;
    RigidBodyDynamics::Math::Vector3d com_position, com_velocity;
    RigidBodyDynamics::Utils::CalcCenterOfMass(m_model, q, q_dot, mass, com_position, &com_velocity);
    geometry_msgs::Point com_point;
    com_point.x = com_position(0);
    com_point.y = com_position(1);
    com_point.z = com_position(2);
    m_com_position_pub.publish(com_point);

    //calculate torques
    if(!m_prioritized_command_sub.getCurrentMessage(m_cmd))
    {
      ROS_WARN_THROTTLE(0.5, "RBDLInverseDynamics: No command received!");
      return;
    }

    //load accelerations
    for(unsigned int i = 6; i < m_model.dof_count; i++)
    {
      unsigned int sm_idx = m_rbdl_to_sm[i];
      if(sm_idx < m_cmd.joint_commands.acceleration.size())
      {
        qd_dot(i) = m_cmd.joint_commands.acceleration[sm_idx];
      }
      else
      {
        qd_dot(i) = 0;
      }
      assert(!isnan((double)qd_dot(i)));
    }

    //load wrenches
    std::vector<RigidBodyDynamics::Math::SpatialVector> f_ext;
    for(unsigned int i = 0; i < m_rbdl_body_names.size(); i++)
    {
      RigidBodyDynamics::Math::SpatialVector wrench(0, 0, 0, 0, 0, 0);
//      unsigned int msg_idx = std::find(m_cmd.wrenched_link_names.begin(), m_cmd.wrenched_link_names.end(), m_rbdl_body_names[i]) - m_cmd.wrenched_link_names.begin();
//      if(msg_idx != m_cmd.wrenched_link_names.size())
//      {
//        wrench(0) = m_cmd.reaction_forces[msg_idx].force.x;
//        wrench(1) = m_cmd.reaction_forces[msg_idx].force.y;
//        wrench(2) = m_cmd.reaction_forces[msg_idx].force.z;
//        wrench(3) = m_cmd.reaction_forces[msg_idx].torque.x;
//        wrench(4) = m_cmd.reaction_forces[msg_idx].torque.y;
//        wrench(5) = m_cmd.reaction_forces[msg_idx].torque.z;
//      }
      f_ext.push_back(wrench);
    }

    //calculate dynamics
    RigidBodyDynamics::InverseDynamics(m_model, q, q_dot, qd_dot, tau, &f_ext);

    bool nan_found = false;

    //repack torque
    matec_msgs::FullJointStates joint_commands;
    joint_commands.position.resize(m_num_joints, 0.0);
    joint_commands.velocity.resize(m_num_joints, 0.0);
    joint_commands.acceleration.resize(m_num_joints, 0.0);
    joint_commands.torque.resize(m_num_joints, 0.0);
    for(unsigned int i = 0; i < m_model.dof_count; i++)
    {
      double torque = tau(i);
      if(isnan(torque))
      {
        nan_found = true;
        ROS_ERROR("RBDL produced NAN for joint %d", m_rbdl_to_sm[i]);
        continue;
      }

      if(fabs(torque) < 1e-6)
      {
        torque = 0;
      }

      if(i >= 6)
      {
        double dt = 0.001; //TODO: param or measure loop rate
        unsigned int sm_idx = m_rbdl_to_sm[i];
        joint_commands.torque[sm_idx] = torque;
        joint_commands.acceleration[sm_idx] = qd_dot(i);

        if(m_cmd.joint_commands.velocity.size() > sm_idx)
        {
          joint_commands.velocity[sm_idx] = m_cmd.joint_commands.velocity[sm_idx];
        }
        else
        {
          joint_commands.velocity[sm_idx] = msg.velocity[sm_idx] + qd_dot(i) * dt; //predict what the velocity will be in one time step
        }

        if(m_cmd.joint_commands.position.size() > sm_idx)
        {
          joint_commands.position[sm_idx] = m_cmd.joint_commands.position[sm_idx];
        }
        else
        {
          joint_commands.position[sm_idx] = msg.position[sm_idx] + msg.velocity[sm_idx] * dt + 0.5 * qd_dot(i) * dt * dt; //predict what the position will be in one time step
        }
      }
    }

    sensor_msgs::JointState js_msg;
    js_msg.name = m_joint_names;
    js_msg.position = joint_commands.position;
    js_msg.velocity = joint_commands.velocity;
    js_msg.effort = joint_commands.torque;

    if(nan_found)
    {
      ROS_INFO_STREAM("Bad nan caused by?" << msg << "\n" << m_cmd << "\n" << m_odom << "\nq\n" << q<< "\nq_dot\n" << q_dot<< "\nqd_dot\n" << qd_dot);
    }

    m_joint_command_pub.publish(joint_commands);
    m_js_pub.publish(js_msg);
  }

  void RBDLInverseDynamics::spin()
  {
    ROS_INFO("RBDLInverseDynamics started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "inverse_dynamics");
  ros::NodeHandle nh("~");

  inverse_dynamics::RBDLInverseDynamics node(nh);
  node.spin();

  return 0;
}
