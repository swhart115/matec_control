#ifndef ADAPTIVE_WINDOW_ESTIMATOR_H
#define ADAPTIVE_WINDOW_ESTIMATOR_H

#include "rolling_buffer.h"
#include "median_filter.h"
#include "matec_utils/biquad.h"

namespace estimators
{
  class AdaptiveWindowEstimator
  {
  public:
    AdaptiveWindowEstimator(unsigned int maximum_size, double noise_margin, double sampling_rate) :
        m_buffer(maximum_size), m_velocity_filter(5)
    {
      m_noise_margin = noise_margin;

      double filter_frequency = 2.0;
//      m_velocity_filter.setBiquad(bq_type_lowpass, filter_frequency / 1000.0, 0.7071, 6);
    }

    ~AdaptiveWindowEstimator()
    {
    }

    void process(ScalarSample sample, double& position, double& velocity)
    {
      ScalarSample filtered_sample = sample;
      filtered_sample.time = sample.time;

      m_buffer.push(filtered_sample);

      position = filtered_sample.value;
      velocity = 0;

      if(m_buffer.size() == 1) //not enough data
      {
        velocity = 0;
        return;
      }
      else if(m_buffer.size() == 2)
      {
        velocity = (m_buffer(0).value - m_buffer(1).value) / (m_buffer(0).time - m_buffer(1).time);
            return;
      }

      double xy_bar = 0;
      double x_bar = 0;
      double y_bar = 0;
      double x_squared_bar = 0;
      for(unsigned int n = 1; n < m_buffer.size(); n++)
      {
        xy_bar = 0;
        x_bar = 0;
        y_bar = 0;
        x_squared_bar = 0;
        for(unsigned int i = 0; i < n; i++)
        {
          x_bar += m_buffer(i).time;
          y_bar += m_buffer(i).value;
          xy_bar += m_buffer(i).time * m_buffer(i).value;
          x_squared_bar += m_buffer(i).time * m_buffer(i).time;
        }
        x_bar /= (double) n;
        y_bar /= (double) n;
        xy_bar /= (double) n;
        x_squared_bar /= (double) n;
        double vel = (xy_bar - x_bar * y_bar) / (x_squared_bar - x_bar * x_bar);
        double error = 0;
        for(unsigned int i = 0; i < n; i++)
        {
          double predicted_position = vel * (m_buffer(i).time - x_bar) + y_bar;
          error = fabs(m_buffer(i).value - predicted_position);
          if(error > m_noise_margin)
          {
            break;
          }
        }
        if(error > m_noise_margin)
        {
          break;
        }

        velocity = vel;
      }

      if(isnan(velocity))
      {
        ROS_ERROR("NAN VELOCITY!");
        return;
      }

      //do median filter
      position = velocity * (m_buffer(0).time - x_bar) + y_bar;
      m_buffer.set(0, ScalarSample(position, m_buffer(0).time));
      velocity = m_velocity_filter.process(velocity);
    }

  private:
    RollingBuffer<ScalarSample> m_buffer;
    MedianFilter<double, std::less<double> > m_velocity_filter;
//    Biquad m_velocity_filter;
    double m_noise_margin;
  };
}

#endif //ADAPTIVE_WINDOW_ESTIMATOR_H
