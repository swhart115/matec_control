#include "joint_position_control/joint_position_control.h"

namespace joint_position_control
{
  JointPositionControl::JointPositionControl(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 10.0);
    m_nh.param("interpolator", m_interpolator, std::string(m_nh.getNamespace()));

    matec_utils::blockOnSMJointNames(m_joint_names);

    if(!matec_utils::getControlledJoints(m_nh, m_controlled_joints))
    {
      ROS_ERROR("Must specify at least one controlled joint!");
      ros::shutdown();
      return;
    }
    if(!matec_utils::generateControlledJointMapping(m_joint_names, m_controlled_joints, m_joint_map))
    {
      ROS_ERROR("JointPositionControl couldn't generate joint mapping! Shutting down!");
      ros::shutdown();
      return;
    }

    matec_utils::blockOnTaskRegistration(m_nh.getNamespace());

    m_kps.resize(m_controlled_joints.size(), 0.0);
    m_kvs.resize(m_controlled_joints.size(), 0.0);
    m_kis.resize(m_controlled_joints.size(), 0.0);
    m_i_clamps.resize(m_controlled_joints.size(), 0.0);
    m_i_livebands.resize(m_controlled_joints.size(), 0.0);
    m_i_accumulators.resize(m_controlled_joints.size(), 0.0);
    getGains();

    m_error_pub = m_nh.advertise<matec_msgs::FullJointStates>("errors", 1, true);
    m_total_error_pub = m_nh.advertise<std_msgs::Float64>("total_joint_error_degrees", 1, true);
    m_max_error_pub = m_nh.advertise<std_msgs::Float64>("max_joint_error_degrees", 1, true);

    m_task_pub.advertise("task_command");
    m_joint_states_sub.subscribe("/joint_states", boost::bind(&JointPositionControl::sensorCallback, this, _1));
    m_joint_carrot_sub.subscribe("/" + m_interpolator + "/carrot");
  }

  JointPositionControl::~JointPositionControl()
  {

  }

  void JointPositionControl::getGains()
  {
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      boost::mutex::scoped_lock lock(m_mutex);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/p", m_kps[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/v", m_kvs[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/i", m_kis[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/i_clamp", m_i_clamps[i]);
      m_nh.getParamCached("gains/" + m_controlled_joints[i] + "/i_liveband", m_i_livebands[i]);
    }
  }

  double clamp(double a, double b, double c)
  {
    return ((b) < (a)? (c) < (b)? (b) : (a) < (c)? (a) : (c) : (c) < (a)? (a) : (b) < (c)? (b) : (c));
  }

  void JointPositionControl::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    matec_msgs::FullJointStates position_control_carrot;
    if(!m_joint_carrot_sub.getCurrentMessage(position_control_carrot))
    {
      ROS_WARN_THROTTLE(0.5, "JPC: No carrot received (%s)!", m_nh.resolveName("position_control_carrot").c_str());
      return;
    }

    if(position_control_carrot.position.size() != m_controlled_joints.size())
    {
      ROS_ERROR("Received position control carrot that was the wrong size (got %d, expected %d)", (int) position_control_carrot.position.size(), (int) m_controlled_joints.size());
      return;
    }

    matec_msgs::TaskCommand task_command;
    matec_msgs::FullJointStates error;
    std_msgs::Float64 total_error;
    std_msgs::Float64 max_error;
    std_msgs::Int32 worst_joint;
    total_error.data = 0;
    max_error.data = 0;
    worst_joint.data = -1;
    for(unsigned int i = 0; i < m_controlled_joints.size(); i++)
    {
      int msg_idx = m_joint_map[i];
      unsigned int carrot_idx;

      if(position_control_carrot.joint_indices.size() == 0)
      {
        carrot_idx = msg_idx;
      }
      else
      {
        carrot_idx = std::find(position_control_carrot.joint_indices.begin(), position_control_carrot.joint_indices.end(), msg_idx) - position_control_carrot.joint_indices.begin();
        if(carrot_idx == position_control_carrot.joint_indices.size())
        {
          ROS_ERROR("Carrot did not provide a setpoint for joint %s (%d)!", m_joint_names[msg_idx].c_str(), msg_idx);
          return;
        }
      }

//      ROS_INFO("%s=>sm %d, carrot %d, jpc %d", m_joint_names[msg_idx].c_str(), msg_idx, carrot_idx, i);

      double pos_error = position_control_carrot.position[carrot_idx] - msg.position[msg_idx];
      double vel_error = position_control_carrot.velocity[carrot_idx] - msg.velocity[msg_idx];

      boost::mutex::scoped_lock lock(m_mutex);
      if(fabs(pos_error) <= m_i_livebands[i])
      {
        m_i_accumulators[i] = clamp(m_i_accumulators[i] + pos_error, -m_i_clamps[i], m_i_clamps[i]);
      }
      else
      {
        m_i_accumulators[i] = 0;
      }

      double feedback = m_kps[i] * pos_error + m_kvs[i] * vel_error + m_kis[i] * m_i_accumulators[i];
      double desired_acceleration = position_control_carrot.acceleration[carrot_idx] + feedback;
      task_command.joint_commands.acceleration.push_back(desired_acceleration);
      task_command.joint_commands.velocity.push_back(position_control_carrot.velocity[carrot_idx]);
      task_command.joint_commands.position.push_back(position_control_carrot.position[carrot_idx]);
      error.position.push_back(pos_error);
      error.velocity.push_back(vel_error);
      error.acceleration.push_back(desired_acceleration);

      total_error.data += fabs(pos_error) * 180.0 / M_PI;
      if(fabs(pos_error * 180.0 / M_PI) > max_error.data)
      {
        max_error.data = fabs(pos_error * 180.0 / M_PI);
        worst_joint.data = msg_idx;
      }
    }

    task_command.joint_commands.joint_indices = m_joint_map;
    m_task_pub.publish(task_command);
    m_error_pub.publish(error);
    m_total_error_pub.publish(total_error);
    m_max_error_pub.publish(max_error);
  }

  void JointPositionControl::spin()
  {
    ROS_INFO("JointPositionControl started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      getGains();
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "joint_position_control");
  ros::NodeHandle nh("~");

  joint_position_control::JointPositionControl node(nh);
  node.spin();

  return 0;
}
