#ifndef CARTESIAN_SPACE_INTERPOLATOR_H
#define CARTESIAN_SPACE_INTERPOLATOR_H

#include <ros/ros.h>
#include <actionlib/server/action_server.h>
#include <matec_actions/FollowPoseTrajectoryAction.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <boost/thread/mutex.hpp>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>

#include <geometry_msgs/Pose.h>

#include "interpolators/minimum_jerk_interpolator.h"
#include "interpolators/quaternion_interpolator.h"

#include "matec_utils/common_initialization_components.h"
#include "matec_msgs/Tare.h"

namespace interpolators
{
  typedef actionlib::ActionServer<matec_actions::FollowPoseTrajectoryAction> Server;

  class CartesianSpaceInterpolator
  {
  public:
    CartesianSpaceInterpolator(const ros::NodeHandle& nh);
    ~CartesianSpaceInterpolator();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_publish_rate;
    bool m_always_tare;
    std::string m_controlled_frame;
    std::string m_output_frame;
    std::string m_output_field_name;

    ros::ServiceServer m_tare_service_server;

    Server m_server;
    Server::GoalHandle m_goal_handle;

    shared_memory_interface::Publisher<geometry_msgs::PoseStamped> m_pose_carrot_pub;

    tf::TransformListener m_tf_listener;

    MinimumJerkInterpolator m_x_interpolator;
    MinimumJerkInterpolator m_y_interpolator;
    MinimumJerkInterpolator m_z_interpolator;
    QuaternionInterpolator m_quaternion_interpolator;

    geometry_msgs::PoseStamped m_last_pose;

    boost::mutex m_mutex;

    void goalCallback(Server::GoalHandle goal_handle);
    void cancelCallback(Server::GoalHandle goal_handle);
    void cancelCurrentGoal();
    void processGoal();
    void shutdown();
    bool goalActive();

    void finishGoal(bool success);
    void calculateNextCommand(ros::Time time, geometry_msgs::PoseStamped& pose_command);
    void tare();

    bool tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res);
  };
}
#endif //CARTESIAN_SPACE_INTERPOLATOR_H
