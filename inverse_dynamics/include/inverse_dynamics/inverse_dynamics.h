#ifndef INVERSE_DYNAMICS_H
#define INVERSE_DYNAMICS_H

#include <ros/ros.h>
#include <boost/thread.hpp>
#include <time.h>

#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <matec_msgs/WrenchArray.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>

#include <urdf/model.h>
#include <urdf_parser/urdf_parser.h>

#include <sensor_msgs/JointState.h>

#include <tf/tf.h>

#include "matec_utils/common_initialization_components.h"
#include "matec_utils/kdl_urdf_reader.h"

#include "matec_msgs/RobotInertiaProperties.h"

#include "inverse_dynamics/aspect.h"
#include "inverse_dynamics/rnea.h"

namespace inverse_dynamics
{
  class InverseDynamics
  {
  public:
    InverseDynamics(const ros::NodeHandle& nh);
    ~InverseDynamics();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    bool m_adapt;

    ros::Publisher m_inertias_pub;

    urdf::Model m_urdf_model;
    KDL::Tree m_tree;

    std::vector<ASPECTLinkParameters> aspect_params;
    ASPECT m_aspect;
    RNEA m_rnea;

    boost::mutex m_mutex;

    std::vector<std::string> m_joint_names;

    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_command_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;
    shared_memory_interface::Subscriber<matec_msgs::TaskCommand> m_prioritized_command_sub;
    shared_memory_interface::Subscriber<matec_msgs::WrenchArray> m_measured_external_forces_sub;

    void initModel();
    bool parameterChanged(std::string joint_name, std::string param_name, double& value);
    void pollParameterServerAdaptive();
    void stop();

    void convertRootLinkOdometry(nav_msgs::Odometry odom);

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //INVERSE_DYNAMICS_H
