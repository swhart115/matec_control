#ifndef PLOTTER_H
#define PLOTTER_H

#include <vector>
#include <boost/python.hpp>
#include <boost/python/call.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/thread.hpp>
#include "system_id/debug_levels.h"

namespace system_id
{
  enum PlotLayout
  {
    ONE_ROW,
    ONE_COLUMN,
    TWO_ROWS,
    SQUARE
  };

  class Dataset
  {
  public:
    Dataset()
    {

    }

    Dataset(std::vector<long double> x, std::vector<long double> y, std::string col)
    {
      x_data = x;
      y_data = y;
      color = col;
      size = x_data.size();
      assert(x_data.size() == y_data.size());
    }

    std::vector<long double> x_data;
    std::vector<long double> y_data;
    std::string color;
    unsigned int size;
  };

  class Plotter
  {
  public:
    Plotter(unsigned int num_plots, PlotLayout layout=TWO_ROWS);
    void setData(unsigned int figure, std::string name, std::vector<long double> x_data, std::vector<long double> y_data, std::string color = "k", bool use_for_scaling = true);
    void removeData(unsigned int figure, std::string name);
    bool isReady();
    void refreshPlot();

  private:
    boost::mutex m_mutex;
    boost::condition_variable m_new_data;
    std::vector<std::map<std::string, Dataset> > m_data;
    std::map<std::string, bool> m_use_for_scaling;
    boost::python::object m_figure;
    std::vector<boost::python::object> m_axes;
    boost::python::object m_canvas;
    bool m_ready;
    unsigned int m_num_plots;
    PlotLayout m_layout;

    bool update();
    void processingThread();
  };
}

#endif //PLOTTER_H
