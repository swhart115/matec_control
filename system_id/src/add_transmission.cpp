#include <cmath>
#include <set>

#include "system_id/training_data.h"

long double lookupTransmission(long double angle, system_id::TrainingData& transmissions)
{
  std::vector<std::string> input_labels = transmissions.getInputLabels();
  int angle_idx = std::find(input_labels.begin(), input_labels.end(), "Q") - input_labels.begin();
  for(unsigned int i = 0; i < transmissions.size(); i++)
  {
    std::vector<long double> input_set = transmissions.getInputSet(i);
    std::vector<long double> output_set = transmissions.getOutputSet(i);
    if(input_set[angle_idx] == angle)
    {
      return output_set[0];
    }
    else if(input_set[angle_idx] > angle)
    {
      if(i == 0)
      {
        std::cerr << "Reached end of transmissions without finding the angle" << std::endl;
        return output_set[0];
      }
      else
      {
        long double a1 = transmissions.getInputSet(i - 1)[angle_idx];
        long double a2 = transmissions.getInputSet(i)[angle_idx];
        long double tr1 = transmissions.getOutputSet(i - 1)[0];
        long double tr2 = transmissions.getOutputSet(i)[0];
        long double d1 = angle - a1;
        long double d2 = a2 - angle;
        long double norm = d1 + d2;
        assert(d1 > 0.0);
        assert(d2 > 0.0);
        d1 /= norm;
        d2 /= norm;
        assert((d1 + d2 - 1.0) < 0.001);

        return tr1 * (1-d1) + tr2 * (1-d2);
      }
    }
    else if(i == transmissions.size() - 1) //no more to check
    {
      std::cerr << "Reached end of transmissions without finding the angle" << std::endl;
      return output_set[0];
    }
  }
  return 1.0;
}

int main(int argc, char* argv[])
{
  if(argc != 4)
  {
    std::cerr << "Args: [log filename], [transmission filename], [output filename]" << std::endl;
    return 0;
  }

  std::string filename_in(argv[1]);
  std::string filename_trans(argv[2]);
  std::string filename_out(argv[3]);
  system_id::TrainingData data(filename_in);
  system_id::TrainingData transmissions(filename_trans);

  std::vector<std::string> input_labels = data.getInputLabels();
  int position_idx = std::find(input_labels.begin(), input_labels.end(), "p") - input_labels.begin();

  if(position_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'p'!";
    return 0;
  }

  std::vector<std::vector<long double> > input_sets, output_sets;
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    std::vector<long double> output_set = data.getOutputSet(i);

    long double position = input_set[position_idx];
    long double tr = lookupTransmission(position, transmissions);
    input_set.push_back(tr);

    input_sets.push_back(input_set);
    output_sets.push_back(output_set);
  }

  std::vector<std::string> new_input_labels = data.getInputLabels();
  new_input_labels.push_back("tr");

  system_id::TrainingData::writeDataFile(filename_out, input_sets, new_input_labels, output_sets, data.getOutputLabels());
}
