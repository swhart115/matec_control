#ifndef COP_BALANCER_H
#define COP_BALANCER_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/ContactSpecifications.h>
#include <geometry_msgs/Point.h>
#include <matec_msgs/TaskCommand.h>
#include <boost/thread.hpp>
#include <geometry_msgs/WrenchStamped.h>
#include <numeric>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include <geometry_msgs/PoseStamped.h>

#include <cmath>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

#include "matec_utils/model_helper.h"
#include "contact.h"
#include "matec_utils/common_initialization_components.h"
#include <matec_msgs/WrenchArray.h>

namespace com_control
{
  class CoPBalancer
  {
  public:
    CoPBalancer(const ros::NodeHandle& nh);
    ~CoPBalancer();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    double m_stance_width;
    double m_stance_length;
    double m_stance_height;
    double m_foot_center_offset;
    double m_turn_out;
    bool m_one_foot;

    matec_utils::ModelHelper* m_model;

    std::vector<std::string> m_joint_names;

    geometry_msgs::Point m_com;
    geometry_msgs::PoseStamped m_left_target_pose;
    geometry_msgs::PoseStamped m_right_target_pose;
    bool m_have_poses;

    boost::mutex m_mutex;

    shared_memory_interface::Publisher<geometry_msgs::PoseStamped> m_left_carrot_pub;
    shared_memory_interface::Publisher<geometry_msgs::PoseStamped> m_right_carrot_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;
    shared_memory_interface::Subscriber<matec_msgs::WrenchArray> m_wrench_array_sub;

    void initModel();

    bool calculateCoP(matec_msgs::WrenchArray wrenches, geometry_msgs::Point cop);
    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //COP_BALANCER_H
