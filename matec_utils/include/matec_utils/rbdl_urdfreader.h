#ifndef _RBDL_URDFREADER_H
#define _RBDL_URDFREADER_H

#include <rbdl/rbdl_config.h>
#include <urdf/model.h>

namespace RigidBodyDynamics
{
  class Model;

  namespace Addons
  {
    bool read_urdf_model(urdf::Model urdf_model, Model& rbdl_model, bool verbose = false);
  }
}

/* _RBDL_URDFREADER_H */
#endif
