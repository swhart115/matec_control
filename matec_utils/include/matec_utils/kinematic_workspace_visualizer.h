#ifndef KINEMATIC_WORKSPACE_VISUALIZER_H
#define KINEMATIC_WORKSPACE_VISUALIZER_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <kdl/tree.hpp>
#include <kdl/frames.hpp>
#include <kdl/segment.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <urdf/model.h>

namespace matec_utils
{
  class KinematicWorkspaceVisualizer
  {
  public:
    KinematicWorkspaceVisualizer(const ros::NodeHandle& nh);
    ~KinematicWorkspaceVisualizer();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    int m_steps_per_joint;
    std::string m_root_frame;
    std::string m_end_frame;
    std::string m_model_path;

    urdf::Model m_urdf_model;
    KDL::Tree m_tree;
    KDL::Chain m_chain;
    std::vector<double> m_min_bounds;
    std::vector<double> m_max_bounds;

    ros::Time m_start_time;
    int m_num_points_tested;
    int m_total_points_to_test;

    pcl::PointCloud<pcl::PointXYZRGB> m_pcl_cloud;
    sensor_msgs::PointCloud2 m_cloud;
    ros::Publisher m_cloud_pub;

    void initModel();
    void calculateKinematicsRecursive(int my_idx, KDL::JntArray& j);
    void calculateCloud();
  };
}
#endif //KINEMATIC_WORKSPACE_VISUALIZER_H
