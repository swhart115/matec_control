#include <matec_utils/sensed_cop_visualizer.h>

namespace matec_utils
{
  SensedCoPVisualizer::SensedCoPVisualizer(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 100.0);

    m_total_cop_pub = m_nh.advertise<geometry_msgs::PointStamped>("/total_center_of_pressure", 1, true);
    m_total_com_pub = m_nh.advertise<geometry_msgs::PointStamped>("/total_center_of_mass", 1, true);
    m_cop_pub = m_nh.advertise<sensor_msgs::PointCloud2>("/centers_of_pressure", 1, true);

    matec_utils::blockOnSMJointNames(m_joint_names);

    //get robot model
    while(!m_nh.hasParam("/robot_description") && ros::ok())
    {
      ROS_WARN_THROTTLE(2.0, "sensed_cop_visualizer is waiting for robot description.");
    }
    m_model.initParam("/robot_description");
    m_helper = new ModelHelper(m_model, m_joint_names);

    geometry_msgs::Pose contact_pose;
    contact_pose.position.x = 0.049;
    contact_pose.position.y = 0.0;
    contact_pose.position.z = -0.081;
    contact_pose.orientation.w = 1.0;
    m_helper->addContactFrame("l_foot", contact_pose);
    m_helper->addContactFrame("r_foot", contact_pose);

    m_joint_states_sub.subscribe("/joint_states", boost::bind(&SensedCoPVisualizer::sensorCallback, this, _1));
    m_root_link_odom_sub.subscribe("/root_link_odom");
    m_wrench_array_sub.subscribe("/ankle_wrenches");
  }

  SensedCoPVisualizer::~SensedCoPVisualizer()
  {
  }

  pcl::PointXYZ pointToPoint(geometry_msgs::Point point)
  {
    return pcl::PointXYZ(point.x, point.y, point.z);
  }

  void SensedCoPVisualizer::sensorCallback(matec_msgs::FullJointStates& msg)
  {
    boost::mutex::scoped_lock lock(m_mutex);
    nav_msgs::Odometry root_link_odom;
    if(!m_root_link_odom_sub.getCurrentMessage(root_link_odom))
    {
      ROS_WARN_THROTTLE(0.5, "SCV: No root link odometry received!");
      return;
    }
    m_helper->updateModel(msg.position, root_link_odom.pose.pose);

    double total_mass;
    m_helper->findCOM(m_total_com.point, total_mass);
    m_total_com.point.z = 0;

    matec_msgs::WrenchArray wrenches;
    if(!m_wrench_array_sub.getCurrentMessage(wrenches))
    {
      ROS_WARN_THROTTLE(0.5, "SCV: No ankle wrenches received!");
      return;
    }

    pcl::PointCloud<pcl::PointXYZ> points;
    m_total_cop.point.x = 0;
    m_total_cop.point.y = 0;
    m_total_cop.point.z = 0;

    assert(wrenches.wrenches.size() == 2);
    //TODO: assuming left, right for now
    int cop_count = 0;
    for(unsigned int i = 0; i < wrenches.wrenches.size(); i++)
    {
      KDLWrenchStamped wrench(wrenches.wrenches[i]);
      KDLWrenchStamped transformed_wrench;
      if(i == 0)
      {
        m_helper->transformWrench("l_foot_contact", wrench, transformed_wrench);
      }
      if(i == 1)
      {
        m_helper->transformWrench("r_foot_contact", wrench, transformed_wrench);
      }

      geometry_msgs::PointStamped point;
      if(!matec_utils::ModelHelper::calculateWrenchCoP(transformed_wrench, point))
      {
        continue;
      }

      geometry_msgs::PoseStamped pose;
      pose.pose.position = point.point;
      pose.pose.orientation.w = 1;
      pose.header = point.header;

      pose = m_helper->transformPose(pose, "global");

      m_total_cop.point.x += pose.pose.position.x;
      m_total_cop.point.y += pose.pose.position.y;
      m_total_cop.point.z += pose.pose.position.z;

      points.points.push_back(pointToPoint(pose.pose.position));
      cop_count++;
    }
    points.height = 1;
    points.width = points.points.size();

    m_total_cop.point.x /= cop_count;
    m_total_cop.point.y /= cop_count;
    m_total_cop.point.z /= cop_count;

    if(points.points.size() != 0)
    {
      pcl::toROSMsg(points, m_cops);
    }
  }

  void SensedCoPVisualizer::spin()
  {
    ros::Rate loop_rate(m_loop_rate);

    while(ros::ok())
    {
      ros::spinOnce();
      loop_rate.sleep();

      boost::mutex::scoped_lock lock(m_mutex);
      m_cops.header.stamp = ros::Time::now();
      m_cops.header.frame_id = "global";
      m_total_cop.header = m_cops.header;
      m_total_com.header = m_cops.header;

      m_cop_pub.publish(m_cops);
      m_total_cop_pub.publish(m_total_cop);
      m_total_com_pub.publish(m_total_com);
    }
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "sensed_cop_visualizer");
  ros::NodeHandle nh("~");

  matec_utils::SensedCoPVisualizer node(nh);
  node.spin();

  return 0;
}
