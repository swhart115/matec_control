#ifndef ROLLING_BUFFER_H
#define ROLLING_BUFFER_H

#include <vector>
#include <cassert>

namespace estimators
{
  class ScalarSample
  {
  public:
    double value;
    double time;

    ScalarSample()
    {
      value = 0;
      time = 0;
    }

    ScalarSample(double v, double t)
    {
      value = v;
      time = t;
    }

    ScalarSample& operator+=(const ScalarSample &rhs)
    {
      this->value += rhs.value;
      this->time += rhs.time;
      return *this;
    }

    const ScalarSample operator+(const ScalarSample &other) const
    {
      ScalarSample result = *this; // Make a copy of myself.  Same as MyClass result(*this);
      result += other; // Use += to add other to the copy.
      return result; // All done!
    }
  };

  struct ScalarSampleComparator
  {
    bool operator()(ScalarSample const& ss1, ScalarSample const& ss2)
    {
      return (ss1.value < ss2.value);
    }
  };

  template<typename SampleType>
  class RollingBuffer
  {
  public:
    RollingBuffer()
    {
      m_size = 0;
      m_reserved = 0;
      m_start_idx = 0;
    }

    RollingBuffer(unsigned int size)
    {
      reserve(size);
    }

    ~RollingBuffer()
    {

    }

    //erases the oldest sample and adds a new one to the beginning
    void push(SampleType sample)
    {
      m_start_idx = (m_start_idx == 0)? m_reserved - 1 : m_start_idx - 1;
      m_buffer[m_start_idx] = sample;
      if(m_size < m_reserved)
      {
        m_size++;
      }
    }

    //overwrites a sample without rolling the buffer
    void set(unsigned int i, SampleType sample)
    {
      assert(i < m_size);
      m_buffer[(m_start_idx + i) % m_reserved] = sample;
    }

    //gets the sample i entries ago
    SampleType& operator()(unsigned int i)
    {
      assert(i < m_size);
      return m_buffer[(m_start_idx + i) % m_reserved];
    }

    void reserve(unsigned int size)
    {
      m_reserved = size;
      m_size = 0;
      m_buffer.resize(size, SampleType());
      m_start_idx = 0;
    }

    unsigned int size()
    {
      return m_size;
    }

    unsigned int reserved()
    {
      return m_reserved;
    }

    std::vector<SampleType>* data()
    {
      return &m_buffer;
    }

  private:
    unsigned int m_start_idx;
    unsigned int m_size;
    unsigned int m_reserved;
    std::vector<SampleType> m_buffer;
  };
}

#endif //ROLLING_BUFFER_H
