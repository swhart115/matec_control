#ifndef TRAINING_DATA_H
#define TRAINING_DATA_H

#include "system_id/debug_levels.h"
#include <vector>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>

namespace system_id
{
  class TrainingData
  {
  public:
    TrainingData()
    {
    }

    TrainingData(std::string filename, unsigned int downsample_skips = 0, unsigned int lines_to_read=std::numeric_limits<unsigned int>::max())
    {
      readDataFile(filename, m_input_sets, m_input_labels, m_output_sets, m_output_labels, downsample_skips, lines_to_read);
    }

    TrainingData(std::vector<std::vector<long double> > input_sets, std::vector<std::vector<long double> > output_sets)
    {
      m_input_sets = input_sets;
      m_output_sets = output_sets;
    }

    void getInputBounds(long double& input_min, long double& input_max)
    {
      input_min = std::numeric_limits<long double>::max();
      input_max = -std::numeric_limits<long double>::max();
      for(unsigned int i = 0; i < m_input_sets.size(); i++)
      {
        for(unsigned int j = 0; j < m_input_sets[i].size(); j++)
        {
          long double input = m_input_sets[i][j];

          if(input < input_min)
          {
            input_min = input;
          }
          if(input > input_max)
          {
            input_max = input;
          }
        }
      }
    }

    void getOutputBounds(long double& output_min, long double& output_max, long double& output_total)
    {
      output_min = std::numeric_limits<long double>::max();
      output_max = -std::numeric_limits<long double>::max();
      output_total = 0;
      for(unsigned int i = 0; i < m_output_sets.size(); i++)
      {
        for(unsigned int j = 0; j < m_output_sets[i].size(); j++)
        {
          long double output = m_output_sets[i][j];
          output_total += fabs(output);

          if(output < output_min)
          {
            output_min = output;
          }
          if(output > output_max)
          {
            output_max = output;
          }
        }
      }
    }

    static void readDataFile(std::string filename, std::vector<std::vector<long double> >& inputs, std::vector<std::string>& input_labels, std::vector<std::vector<long double> >& outputs, std::vector<std::string>& output_labels, unsigned int downsample_skips, unsigned int lines_to_read)
    {
      std::ifstream file(filename.c_str());
      boost::char_separator<char> sep(" ,");
      unsigned long lines_read = 0;
      unsigned long lines_skipped = 0;
      if(file.is_open())
      {
        PRINTLN_INFO_STREAM("Parsing data file " << filename);
        std::string line;
        getline(file, line); //first line contains labels
        boost::tokenizer<boost::char_separator<char> > tokens(line, sep);
        std::vector<std::string> labels = std::vector<std::string>(tokens.begin(), tokens.end());
        input_labels.clear();
        output_labels.clear();
        PRINT_INFO("Found %d fields: ", (int) labels.size());
        for(unsigned int i = 0; i < labels.size(); i++)
        {
          if(labels[i].substr(0, 2) == "IN")
          {
            input_labels.push_back(labels[i].substr(3));
          }
          else
          {
            output_labels.push_back(labels[i].substr(4));
          }
        }
        PRINTLN_INFO("%d inputs and %d outputs.", (int) input_labels.size(), (int) output_labels.size());

        while(getline(file, line))
        {
          boost::tokenizer<boost::char_separator<char> > tokens(line, sep);
          std::vector<long double> line_values = stringVectorToDoubleVector(std::vector<std::string>(tokens.begin(), tokens.end()));

          if(line_values.size() != (input_labels.size() + output_labels.size()))
          {
            PRINTLN_ERROR("Number of elements in each line of the file does not match the number of inputs and outputs specified!");
            return;
          }

          std::vector<long double> input_set, output_set;
          int value_idx = 0;
          for(unsigned int i = 0; i < input_labels.size(); i++)
          {
            input_set.push_back(line_values[value_idx]);
            value_idx++;
          }
          for(unsigned int i = 0; i < output_labels.size(); i++)
          {
            output_set.push_back(line_values[value_idx]);
            value_idx++;
          }

          inputs.push_back(input_set);
          outputs.push_back(output_set);

          lines_read++;

          if(lines_read > lines_to_read)
          {
            break;
          }

          for(unsigned int i = 0; i < downsample_skips; i++)
          {
            getline(file, line);
          }
        }

        file.close();
      }
      else
      {
        PRINTLN_ERROR_STREAM("Couldn't open file!");
      }

      PRINTLN_INFO_STREAM("Parsing complete! Read " << lines_read << " lines!");
    }

    static void writeDataFile(std::string filename, std::vector<std::vector<long double> >& input_sets, std::vector<std::string> input_labels, std::vector<std::vector<long double> >& output_sets, std::vector<std::string> output_labels)
    {
      //TODO: add sanity checks for array sizes

      std::ofstream file(filename.c_str());
      if(file.is_open())
      {
        //first line contains labels
        for(unsigned int i = 0; i < input_labels.size(); i++)
        {
          if(i != 0)
          {
            file << ", ";
          }
          file << "IN_" << input_labels[i];
        }
        for(unsigned int i = 0; i < output_labels.size(); i++)
        {
          file << ", " << "OUT_" << output_labels[i];
        }
        file << std::endl;

        unsigned long size = input_sets.size();
        assert(size == output_sets.size());
        for(unsigned int i = 0; i < size; i++)
        {
          for(unsigned int j = 0; j < input_sets[i].size(); j++)
          {
            if(j != 0)
            {
              file << ", ";
            }
            file << input_sets[i][j];
          }
          for(unsigned int j = 0; j < output_sets[i].size(); j++)
          {
            file << ", " << output_sets[i][j];
          }
          file << std::endl;
        }
        file.close();
        PRINTLN_INFO_STREAM("Writing complete! Wrote " << size << " lines!");
      }
      else
      {
        PRINTLN_ERROR_STREAM("Couldn't open file!");
      }
    }

    std::vector<long double> getInputSet(unsigned long idx, std::vector<long> shifts = std::vector<long>())
    {
      if(shifts.size() != m_input_sets.begin()->size())
      {
        return m_input_sets[idx];
      }
      else
      {
        std::vector<long double> shifted_input_set;
        for(unsigned int i = 0; i < m_input_sets.begin()->size(); i++)
        {
          long shifted_idx = idx + shifts[i];
          if(shifted_idx < 0 || shifted_idx > m_input_sets.size())
          {
            PRINTLN_ERROR("Input set shifting failed!");
            assert(shifted_idx > 0 && shifted_idx < m_input_sets.size());
          }
          shifted_input_set.push_back(m_input_sets[shifted_idx][i]);
        }
        return shifted_input_set;
      }
    }

    std::vector<long double> getOutputSet(unsigned long idx, std::vector<long> shifts = std::vector<long>())
    {
      if(shifts.size() != m_output_sets.begin()->size())
      {
        return m_output_sets[idx];
      }
      else
      {
        std::vector<long double> shifted_input_set;
        for(unsigned int i = 0; i < m_output_sets.begin()->size(); i++)
        {
          long shifted_idx = idx + shifts[i];
          if(shifted_idx < 0 || shifted_idx > m_output_sets.size())
          {
            PRINTLN_ERROR("Output set shifting failed!");
            assert(shifted_idx > 0 && shifted_idx < m_output_sets.size());
          }
          shifted_input_set.push_back(m_output_sets[shifted_idx][i]);
        }
        return shifted_input_set;
      }
    }

    bool addInputSet(std::vector<long double> input_set)
    {
      if(m_input_sets.size() != 0 && input_set.size() != m_input_sets.begin()->size())
      {
        std::cerr << "Tried to add input set of size " << input_set.size() << ", but the other sets are size " << m_input_sets.begin()->size() << "!" << std::endl;
        return false;
      }
      m_input_sets.push_back(input_set);
      return true;
    }

    bool addOutputSet(std::vector<long double> output_set)
    {
      if(m_output_sets.size() != 0 && output_set.size() != m_output_sets.begin()->size())
      {
        std::cerr << "Tried to add output set of size " << output_set.size() << ", but the other sets are size " << m_output_sets.begin()->size() << "!" << std::endl;
        return false;
      }
      m_output_sets.push_back(output_set);
      return true;
    }

    bool addSingleInput(long double input)
    {
      std::vector<long double> input_set;
      input_set.push_back(input);
      return addInputSet(input_set);
    }

    bool addSingleOutput(long double output)
    {
      std::vector<long double> output_set;
      output_set.push_back(output);
      return addOutputSet(output_set);
    }

    unsigned long size()
    {
      if(m_input_sets.size() != m_output_sets.size())
      {
        std::cerr << "WARNING: the number of input sets (" << m_input_sets.size() << ") is different from the number of output sets (" << m_output_sets.size() << ")" << std::endl;
      }
      return m_input_sets.size();
    }

    std::vector<std::string> getInputLabels()
    {
      return m_input_labels;
    }

    std::vector<std::string> getOutputLabels()
    {
      return m_output_labels;
    }

  private:
    std::vector<std::vector<long double> > m_input_sets;
    std::vector<std::vector<long double> > m_output_sets;
    std::vector<std::string> m_input_labels;
    std::vector<std::string> m_output_labels;

    static std::vector<long double> stringVectorToDoubleVector(std::vector<std::string> input)
    {
      std::vector<long double> output;

      for(unsigned int i = 0; i < input.size(); i++)
      {
        output.push_back(std::atof(input[i].c_str()));
      }

      return output;
    }

  };
}
#endif //TRAINING_DATA_H
