#ifndef INVERSE_DYNAMICS_H
#define INVERSE_DYNAMICS_H

#include <ros/ros.h>
#include <boost/thread.hpp>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include <matec_msgs/FullJointStates.h>
#include <matec_msgs/JointNames.h>
#include <matec_msgs/TaskCommand.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>

#include <std_srvs/Empty.h>

#include <urdf/model.h>
#include <urdf_parser/urdf_parser.h>

//#include <rbdl/rbdl.h>
#include <rbdl/Model.h>
#include <rbdl/Dynamics.h>
#include <rbdl/rbdl_utils.h>
#include <matec_utils/rbdl_urdfreader.h>

#include <sensor_msgs/JointState.h>

#include <tf/tf.h>

#include "matec_utils/common_initialization_components.h"

namespace inverse_dynamics
{
  class RBDLInverseDynamics
  {
  public:
    RBDLInverseDynamics(const ros::NodeHandle& nh);
    ~RBDLInverseDynamics();

    void spin();

  private:
    ros::NodeHandle m_nh;
    double m_loop_rate;
    bool m_print_debug;
    double m_gravity_constant;

    ros::Publisher m_js_pub;
    ros::ServiceServer m_reload_model_service_server;

    urdf::Model m_urdf_model;
    RigidBodyDynamics::Model m_model;

    std::vector<int> m_rbdl_to_sm;
    std::vector<int> m_sm_to_rbdl;
    std::vector<std::string> m_joint_names;
    std::vector<std::string> m_rbdl_body_names;
    std::map<std::string, int> m_name_to_idx_map;

    unsigned int m_num_joints;
    boost::mutex m_mutex;

    matec_msgs::TaskCommand m_cmd;
    bool m_cmd_received;

    nav_msgs::Odometry m_odom;
    bool m_odom_received;

    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_joint_command_pub;
    shared_memory_interface::Publisher<geometry_msgs::Point> m_com_position_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;
    shared_memory_interface::Subscriber<nav_msgs::Odometry> m_root_link_odom_sub;
    shared_memory_interface::Subscriber<matec_msgs::TaskCommand> m_prioritized_command_sub;

    void initModel();

    bool reloadModelCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);
    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}
#endif //INVERSE_DYNAMICS_H
