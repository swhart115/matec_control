#include <cmath>
#include <set>

#include "system_id/training_data.h"

bool lookupTransmission(long double angle_x, long double angle_y, std::vector<long double> & xs, std::vector<long double> & ys, std::vector<std::vector<long double> >& trs, long double& transmission)
{
  assert(trs.size() == ys.size());
  assert(trs[0].size() == xs.size());

  if(angle_x < xs[0] || angle_x > xs[xs.size() - 1] || angle_y < ys[0] || angle_y > ys[ys.size() - 1])
  {
    return false;
  }

  int x_before, x_after, y_before, y_after;
  for(unsigned int i = 0; i < xs.size(); i++)
  {
    if(angle_x < xs[i])
    {
      if(i == 0)
      {
        std::cerr << "x angle " << angle_x << " is less than the first entry in the table " << xs[0] << std::endl;
        return false;
      }
      else
      {
        x_before = i - 1;
        x_after = i;
      }
    }
  }
  for(unsigned int i = 0; i < ys.size(); i++)
  {
    if(angle_y < ys[i])
    {
      if(i == 0)
      {
        std::cerr << "y angle " << angle_y << " is less than the first entry in the table " << ys[0] << std::endl;
        return false;
      }
      else
      {
        y_before = i - 1;
        y_after = i;
      }
    }
  }

  double x = angle_x;
  double y = angle_y;
  double x1 = xs[x_before];
  double x2 = xs[x_after];
  double y1 = ys[y_before];
  double y2 = ys[y_after];
  double Q11 = trs[y_before][x_before];
  double Q12 = trs[y_after][x_before];
  double Q21 = trs[y_before][x_after];
  double Q22 = trs[y_after][x_after];

  transmission = (Q11 * (x2 - x) * (y2 - y) + Q21 * (x - x1) * (y2 - y) + Q12 * (x2 - x) * (y - y1) + Q22 * (x - x1) * (y - y1)) / ((x2 - x1) * (y2 - y1));
  return true;
}

std::vector<long double> stringVectorToDoubleVector(std::vector<std::string> input)
{
  std::vector<long double> output;

  for(unsigned int i = 0; i < input.size(); i++)
  {
    output.push_back(std::atof(input[i].c_str()));
  }

  return output;
}

void readDataFile(std::string filename, std::vector<long double> & x, std::vector<long double> & y, std::vector<std::vector<long double> >& z)
{
  std::ifstream file(filename.c_str());
  boost::char_separator<char> sep(" ,");
  unsigned long lines_read = 0;
  unsigned long lines_skipped = 0;
  if(file.is_open())
  {
    PRINTLN_INFO_STREAM("Parsing data file " << filename);
    std::string line;
    getline(file, line); //first line contains x values
    boost::tokenizer<boost::char_separator<char> > tokens(line, sep);
    std::vector<std::string> x_strings = std::vector<std::string>(tokens.begin(), tokens.end());
    x = stringVectorToDoubleVector(std::vector<std::string>(x_strings.begin() + 1, x_strings.end()));
    y.clear();
    while(getline(file, line))
    {
      boost::tokenizer<boost::char_separator<char> > tokens(line, sep);
      std::vector<long double> line_values = stringVectorToDoubleVector(std::vector<std::string>(tokens.begin(), tokens.end()));

      if(line_values.size() != (x.size() + 1))
      {
        PRINTLN_ERROR("Number of elements in each line of the file does not match the expected length (%d vs %d)!", (int) line_values.size(), (int)(x.size() + 1));
        return;
      }

      y.push_back(line_values[0]);
      std::vector<long double> z_row = std::vector<long double>(line_values.begin() + 1, line_values.end());
      z.push_back(z_row);

      lines_read++;
    }

    file.close();
  }
  else
  {
    PRINTLN_ERROR_STREAM("Couldn't open file!");
  }

  PRINTLN_INFO_STREAM("Parsing complete! Read " << lines_read << " lines!");
  PRINTLN_INFO("%d y values and %d x values!", (int) x.size(), (int) y.size());
}

int main(int argc, char* argv[])
{
  if(argc != 5)
  {
    std::cerr << "Args: [log filename], [x transmission filename], [y transmission filename], [output filename]" << std::endl;
    return 0;
  }

  std::string filename_in(argv[1]);
  std::string filename_trans_x(argv[2]);
  std::string filename_trans_y(argv[3]);
  std::string filename_out(argv[4]);

  std::vector<long double> x_xs, x_ys, y_xs, y_ys;
  std::vector<std::vector<long double> > x_trs, y_trs;
  readDataFile(filename_trans_x, x_xs, x_ys, x_trs);
  readDataFile(filename_trans_y, y_xs, y_ys, y_trs);

  system_id::TrainingData data(filename_in);
  std::vector<std::string> input_labels = data.getInputLabels();
  int position_x_idx = std::find(input_labels.begin(), input_labels.end(), "px") - input_labels.begin();
  int position_y_idx = std::find(input_labels.begin(), input_labels.end(), "py") - input_labels.begin();

  if(position_x_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'px'!";
    return 0;
  }
  if(position_y_idx == input_labels.size())
  {
    std::cerr << "Couldn't find a position field called 'py'!";
    return 0;
  }

  std::vector<std::vector<long double> > input_sets, output_sets;
  for(unsigned int i = 0; i < data.size(); i++)
  {
    std::vector<long double> input_set = data.getInputSet(i);
    std::vector<long double> output_set = data.getOutputSet(i);

    long double position_x = input_set[position_x_idx];
    long double position_y = input_set[position_y_idx];
    long double tr_x, tr_y;
    if(lookupTransmission(position_x, position_y, x_xs, x_ys, x_trs, tr_x) && lookupTransmission(position_x, position_y, y_xs, y_ys, y_trs, tr_y))
    {
      input_set.push_back(tr_x);
      input_set.push_back(tr_y);

      input_sets.push_back(input_set);
      output_sets.push_back(output_set);
    }
  }

  std::vector<std::string> new_input_labels = data.getInputLabels();
  new_input_labels.push_back("tr_x");
  new_input_labels.push_back("tr_y");

  system_id::TrainingData::writeDataFile(filename_out, input_sets, new_input_labels, output_sets, data.getOutputLabels());
}
