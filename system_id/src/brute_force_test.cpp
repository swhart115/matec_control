#include "system_id/coefficient_optimizer.h"
#include <cmath>
#include <set>
#include "system_id/common_math.h"

#include "system_id/training_data.h"
#include "system_id/expression_genome.h"

using namespace system_id;

int main(int argc, char* argv[])
{
  if(argc != 2)
  {
    std::cerr << "Args: filename" << std::endl;
    return 0;
  }

  std::string filename(argv[1]);
  system_id::TrainingData data(filename, 5);

  long double output_min, output_max, output_total;
  data.getOutputBounds(output_min, output_max, output_total);
  long double max_coeff = 10.0 * std::max(fabs(output_min), fabs(output_max));
  long max_shift = data.size() * 0.005;

  std::vector<std::string> input_symbols = data.getInputLabels();

  PRINTLN_INFO("input symbols: %s, %s", input_symbols[0].c_str(), input_symbols[1].c_str());

  EGParams eg_params(input_symbols, max_shift, max_coeff);
  eg_params.complicate_probability = 0.15;
  eg_params.function_selection_probability = 0.25;
  eg_params.stop_probability = 0.75;

  system_id::CoefficientOptimizer opt(&data);
  std::vector<std::string> coefficient_symbols;
  ExpressionNode expression;
  ExpressionGenome::generateRandomExpression(eg_params, expression, coefficient_symbols);
  std::vector<std::string> equations_tried;

  std::string overall_best_equation;
  system_id::ArrayGenome overall_best_coefficients(ArrayGenome::defaultMin(1, 1), ArrayGenome::defaultMax(1, 1));
  std::vector<std::string> overall_best_coefficient_symbols;

  long max_equations = 100;
  long unique_equation_count = 0;
  while(unique_equation_count < max_equations)
  {
    coefficient_symbols.clear();
    ExpressionGenome::generateRandomExpression(eg_params, expression, coefficient_symbols);
    std::string expression_string = expression.getExpressionString();
    if(std::find(equations_tried.begin(), equations_tried.end(), expression_string) == equations_tried.end())
    {
      std::cerr << "\n\nTrying eq " << unique_equation_count << ": " << expression_string << std::endl;
      equations_tried.push_back(expression_string);
      unique_equation_count++;

      GAParams params(std::max((int) coefficient_symbols.size() / 2, (int) 1), 4, 0.001, 1000, 10, 10*output_total, 1500);

      ArrayGenome::GeneType min, max;
      for(unsigned int i = 0; i < input_symbols.size(); i++)
      {
        min.first.push_back(-max_shift);
        max.first.push_back(max_shift);
      }
      for(unsigned int i = 0; i < coefficient_symbols.size(); i++)
      {
        min.second.push_back(-max_coeff);
        max.second.push_back(max_coeff);
      }

      system_id::ArrayGenome best = opt.optimize(expression_string, coefficient_symbols, input_symbols, params, &min, &max);
      std::cerr << best << std::endl;

      if(best < overall_best_coefficients)
      {
        overall_best_coefficients = best;
        overall_best_equation = expression_string;
        overall_best_coefficient_symbols = coefficient_symbols;
        std::cerr << "Best so far!" << std::endl;
      }
    }
  }

  std::cerr << "\n\nOverall best was: " << overall_best_equation << ", " << overall_best_coefficients << ". Refining!" << std::endl;

  std::cerr << "coeff labels: ";
  for(unsigned int i = 0; i < overall_best_coefficient_symbols.size(); i++)
  {
    std::cerr << overall_best_coefficient_symbols[i] << ",";
  }
  std::cerr << std::endl;

  ArrayGenome::GeneType min, max;
  GAParams params;

  //TODO: add equation simplification / mutation step?

//  //lock shift, optimize coeff========================================================================================================================
//  params = GAParams(overall_best_coefficient_symbols.size(), 4, 0.001, 10000, 10);
//  min.first.clear();
//  min.second.clear();
//  max.first.clear();
//  max.second.clear();
//  for(unsigned int i = 0; i < input_symbols.size(); i++)
//  {
//    long double current = overall_best_coefficients.gene.first[i];
//    min.first.push_back(current);
//    max.first.push_back(current);
//  }
//  for(unsigned int i = 0; i < overall_best_coefficients.gene.second.size(); i++)
//  {
//    long double current = overall_best_coefficients.gene.second[i];
//    min.second.push_back(current-(max_coeff*0.25)); //assume that by now the coefficients are already pretty close
//    max.second.push_back(current+(max_coeff*0.25));
//  }
//
//  overall_best_coefficients = opt.optimize(overall_best_equation, overall_best_coefficient_symbols, input_symbols, params, &min, &max, &overall_best_coefficients.gene);
//
//  //lock coeff, optimize shift========================================================================================================================
//  params = GAParams(overall_best_coefficient_symbols.size(), 4, 0.001, 100, 10);
//  min.first.clear();
//  min.second.clear();
//  max.first.clear();
//  max.second.clear();
//  for(unsigned int i = 0; i < input_symbols.size(); i++)
//  {
//    min.first.push_back(-max_shift);
//    max.first.push_back(max_shift);
//  }
//  for(unsigned int i = 0; i < overall_best_coefficients.gene.second.size(); i++)
//  {
//    long double current = overall_best_coefficients.gene.second[i];
//    min.second.push_back(current);
//    max.second.push_back(current);
//  }
//
//  overall_best_coefficients = opt.optimize(overall_best_equation, overall_best_coefficient_symbols, input_symbols, params, &min, &max, &overall_best_coefficients.gene);

//do final optimization========================================================================================================================
  params = GAParams(overall_best_coefficient_symbols.size(), 4, 0.001, 50000, 10);
  min.first.clear();
  min.second.clear();
  max.first.clear();
  max.second.clear();
  for(unsigned int i = 0; i < input_symbols.size(); i++)
  {
    long double current = overall_best_coefficients.gene.first[i];
    min.first.push_back(current - 1); //assume that by now the shifts are very close
    max.first.push_back(current + 1);
  }
  for(unsigned int i = 0; i < overall_best_coefficients.gene.second.size(); i++)
  {
    long double current = overall_best_coefficients.gene.second[i];
    min.second.push_back(current * 0.9); //assume that by now the coefficients are very close
    max.second.push_back(current * 1.1);
  }

  overall_best_coefficients = opt.optimize(overall_best_equation, overall_best_coefficient_symbols, input_symbols, params, &min, &max, &overall_best_coefficients.gene);

  std::cerr << "\n\n Final equation: " << overall_best_equation << ", " << overall_best_coefficients << std::endl;

  while(1)
  {

  }
}
