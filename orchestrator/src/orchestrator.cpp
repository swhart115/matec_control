#include "orchestrator/orchestrator.h"

namespace orchestrator
{
  Orchestrator::Orchestrator(const ros::NodeHandle& nh) :
      m_nh(nh)
  {
    m_nh.param("loop_rate", m_loop_rate, 2000.0);

    matec_utils::blockOnSMJointNames(m_joint_names);
    initializePrioritizedCommand();

    m_prioritized_command_pub.advertise("/prioritized_command");
    m_prioritized_joint_states_pub.advertise("/prioritized_joint_states");
    m_registration_service_server = m_nh.advertiseService("/task_registration", &Orchestrator::registerTaskCallback, this);
    m_reprioritization_service_server = m_nh.advertiseService("/reprioritize", &Orchestrator::reprioritizeTasksCallback, this);
    m_tare_service_server = m_nh.advertiseService("tare", &Orchestrator::tareCallback, this);
  }

  Orchestrator::~Orchestrator()
  {

  }

  bool Orchestrator::tareCallback(matec_msgs::Tare::Request& req, matec_msgs::Tare::Response& res)
  {
    updateJointOwners(true);
    return true;
  }

  TaskParameters Orchestrator::loadTaskParameters(std::string task_name)
  {
    TaskParameters new_task;

    new_task.task_name = task_name;
    m_nh.param<int>("/" + task_name + "/priority", new_task.task_priority, std::numeric_limits<int>::max() - 1);
    m_nh.param<std::string>("/" + task_name + "/interpolator", new_task.interpolator, task_name);
    matec_utils::getControlledJoints(m_nh, new_task.controlled_joints, task_name);

    return new_task;
  }

  std::string stripLeadingSlash(std::string s)
  {
    if(s.at(0) == '/')
    {
      s.erase(s.begin());
    }
    return s;
  }

  bool Orchestrator::registerTaskCallback(matec_msgs::RegisterTask::Request& req, matec_msgs::RegisterTask::Response& res)
  {
    std::string task_name = stripLeadingSlash(req.task_name);
    if(req.register_task)
    {
      ROS_INFO("Orchestrator: Registering task %s", task_name.c_str());
      if(m_task_list.find(task_name) != m_task_list.end())
      {
        ROS_WARN("Orchestrator: Task %s was registered multiple times! Overwriting old entry!", task_name.c_str());
      }

      m_task_list[task_name] = loadTaskParameters(task_name);
      m_task_list[task_name].task_sub.subscribe("/" + task_name + "/task_command");
    }
    else
    {
      ROS_INFO("Orchestrator: Un-registering task %s", task_name.c_str());
      if(m_task_list.find(task_name) == m_task_list.end())
      {
        ROS_WARN("Orchestrator: Tried to unregister unknown task %s!", task_name.c_str());
      }
      else
      {
        m_task_list.erase(task_name); //TODO: monitor uncontrolled joints and set their fields back to zero!
      }
    }
    updateJointOwners();

    return true;
  }

  void Orchestrator::updateJointOwners(bool force_taring)
  {
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      std::string joint_name = m_joint_names[i];
      std::string controlling_task = "";
      int highest_priority = std::numeric_limits<int>::max();
      for(std::map<std::string, TaskParameters>::iterator iter = m_task_list.begin(); iter != m_task_list.end(); iter++)
      {
        std::string task_name = iter->first;
        if((std::find(iter->second.controlled_joints.begin(), iter->second.controlled_joints.end(), joint_name) != iter->second.controlled_joints.end()) && (iter->second.task_priority < highest_priority))
        {
          highest_priority = iter->second.task_priority;
          controlling_task = task_name;
        }
      }

      if(controlling_task.length() > 0)
      {
        //make sure it's removed from everyone else's owned joints
        for(std::map<std::string, TaskParameters>::iterator iter = m_task_list.begin(); iter != m_task_list.end(); iter++)
        {
          if(iter->first != controlling_task)
          {
            int idx = std::find(iter->second.owned_joints.begin(), iter->second.owned_joints.end(), joint_name) - iter->second.owned_joints.begin();
            if(idx < iter->second.owned_joints.size())
            {
              iter->second.owned_joints.erase(iter->second.owned_joints.begin() + idx);
              ROS_INFO("Joint %s is no longer controlled by task %s (priority %d)", joint_name.c_str(), iter->first.c_str(), iter->second.task_priority);
            }
          }
        }

        if(std::find(m_task_list[controlling_task].owned_joints.begin(), m_task_list[controlling_task].owned_joints.end(), joint_name) == m_task_list[controlling_task].owned_joints.end())
        {
          m_task_list[controlling_task].owned_joints.push_back(joint_name);
          m_task_list[controlling_task].new_joints.push_back(joint_name);
          ROS_INFO("Joint %s is now controlled by task %s (priority %d)", joint_name.c_str(), controlling_task.c_str(), highest_priority);
        }
        else if(force_taring)
        {
          m_task_list[controlling_task].new_joints.push_back(joint_name);
        }
      }
      else
      {
        ROS_WARN("Joint %s has no controller", joint_name.c_str());
      }
    }

    //tare as necessary
    bool tared = false;
    for(std::map<std::string, TaskParameters>::iterator iter = m_task_list.begin(); iter != m_task_list.end(); iter++)
    {
      if(iter->second.new_joints.size() != 0)
      {
        std::string interpolator_service_name = "/" + iter->second.interpolator + "/tare";
        if(!ros::service::exists(interpolator_service_name, false))
        {
          ROS_WARN("Orchestrator: Attempted to tare %s task but couldn't find the service at %s!", iter->first.c_str(), interpolator_service_name.c_str());
          continue;
        }

        matec_msgs::Tare tare;
        tare.request.joint_list = iter->second.new_joints;
        if(!ros::service::call(interpolator_service_name, tare))
        {
          ROS_WARN("Orchestrator: Attempted to tare %s task but the service call failed!", iter->first.c_str());
        }

        iter->second.new_joints.clear();
        tared = true;
      }
    }

    if(tared)
    {
      ros::Duration(0.1).sleep(); // wait for tare to go through
    }
  }

  bool Orchestrator::reprioritizeTasksCallback(matec_msgs::ReprioritizeTasks::Request& req, matec_msgs::ReprioritizeTasks::Response& res)
  {
    if(req.task_names.size() != req.new_priorities.size())
    {
      ROS_ERROR("Orchestrator: Reprioritize called with task names and priorities of different lengths!");
      return false;
    }

    ROS_INFO("Orchestrator: Reprioritizing tasks");
    for(unsigned int i = 0; i < req.task_names.size(); i++)
    {
      std::string task_name = stripLeadingSlash(req.task_names[i]);
      if(m_task_list.find(task_name) == m_task_list.end())
      {
        ROS_WARN("Orchestrator: Task %s has not been registered! Skipping reprioritization!", task_name.c_str());
        continue;
      }

      m_task_list[task_name].task_priority = req.new_priorities[i];
    }
    //todo: call tare on everyone involved

    ROS_INFO("Joint owners after reprioritization:");
    updateJointOwners();

    return true;
  }

  void Orchestrator::initializePrioritizedCommand()
  {
    for(unsigned int i = 0; i < m_joint_names.size(); i++)
    {
      m_prioritized_command.joint_commands.joint_indices.push_back(i);
      m_prioritized_command.joint_commands.position.push_back(0);
      m_prioritized_command.joint_commands.velocity.push_back(0);
      m_prioritized_command.joint_commands.acceleration.push_back(0);
      m_prioritized_command.joint_commands.torque.push_back(0);
    }
  }

  void Orchestrator::updateCommand()
  {
    //todo: mutex
    std::vector<int> priorities(m_joint_names.size(), std::numeric_limits<int>::max());

    for(std::map<std::string, TaskParameters>::iterator iter = m_task_list.begin(); iter != m_task_list.end(); iter++)
    {
      matec_msgs::TaskCommand task_command;
      if(iter->second.task_sub.getCurrentMessage(task_command))
      {
        for(unsigned int i = 0; i < iter->second.owned_joints.size(); i++)
        {
          //TODO: find these indices once and store so we don't have to search here
          unsigned int joint_idx = std::find(m_joint_names.begin(), m_joint_names.end(), iter->second.owned_joints[i]) - m_joint_names.begin();
          unsigned int task_idx = std::find(task_command.joint_commands.joint_indices.begin(), task_command.joint_commands.joint_indices.end(), joint_idx) - task_command.joint_commands.joint_indices.begin();

          if(task_idx < task_command.joint_commands.position.size())
          {
            m_prioritized_command.joint_commands.position[joint_idx] = task_command.joint_commands.position[task_idx];
          }
          if(task_idx < task_command.joint_commands.velocity.size())
          {
            m_prioritized_command.joint_commands.velocity[joint_idx] = task_command.joint_commands.velocity[task_idx];
          }
          if(task_idx < task_command.joint_commands.acceleration.size())
          {
            m_prioritized_command.joint_commands.acceleration[joint_idx] = task_command.joint_commands.acceleration[task_idx];
          }
          if(task_idx < task_command.joint_commands.torque.size())
          {
            m_prioritized_command.joint_commands.torque[joint_idx] = task_command.joint_commands.torque[task_idx];
          }
        }
      }
    }

    m_prioritized_command_pub.publish(m_prioritized_command);

    sensor_msgs::JointState prioritized_joint_states;
    prioritized_joint_states.header.stamp = ros::Time::now();
    prioritized_joint_states.name = m_joint_names;
    prioritized_joint_states.position = m_prioritized_command.joint_commands.position;
    prioritized_joint_states.velocity = m_prioritized_command.joint_commands.velocity;
    prioritized_joint_states.effort = m_prioritized_command.joint_commands.torque;
    m_prioritized_joint_states_pub.publish(prioritized_joint_states);
  }

  void Orchestrator::spin()
  {
    ROS_INFO("Orchestrator started.");
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
      updateCommand();
      ros::spinOnce();
      loop_rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "orchestrator");
  ros::NodeHandle nh("~");

  orchestrator::Orchestrator node(nh);
  node.spin();

  return 0;
}
