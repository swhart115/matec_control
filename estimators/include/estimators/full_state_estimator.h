#ifndef FULL_STATE_ESTIMATOR_H
#define FULL_STATE_ESTIMATOR_H

#include <ros/ros.h>
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>

#include <matec_utils/common_initialization_components.h>
#include <matec_msgs/FullJointStates.h>


namespace estimators
{
  class FullStateEstimator
  {
  public:
    FullStateEstimator();
    ~FullStateEstimator();
    void spin();

  private:
    ros::NodeHandle m_nh;

    shared_memory_interface::Publisher<matec_msgs::FullJointStates> m_full_states_pub;
    shared_memory_interface::Subscriber<matec_msgs::FullJointStates> m_joint_states_sub;

    void sensorCallback(matec_msgs::FullJointStates& msg);
  };
}

#endif //FULL_STATE_ESTIMATOR_H
