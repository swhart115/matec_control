#include "ros/ros.h"
#include <shared_memory_interface/shared_memory_publisher.hpp>
#include <shared_memory_interface/shared_memory_subscriber.hpp>
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "matec_msgs/FullJointStates.h"
#include "matec_msgs/JointNames.h"
#include "matec_msgs/TaskCommand.h"

#include <boost/thread/thread_time.hpp>

#include "matec_utils/common_initialization_components.h"

double pos(double dt, double magnitude, double frequency, double phase_offset = 0.0)
{
  assert(phase_offset == 0);
  return -(magnitude * sin(2.0 * M_PI * frequency * dt + phase_offset)) / (4 * M_PI * frequency * M_PI * frequency);
}

double vel(double dt, double magnitude, double frequency, double phase_offset = 0.0)
{
  assert(phase_offset == 0);
  return -(magnitude * cos(2.0 * M_PI * frequency * dt + phase_offset)) / (2 * M_PI * frequency);
}

double acc(double dt, double magnitude, double frequency, double phase_offset = 0.0)
{
  assert(phase_offset == 0);
  return magnitude * sin(2.0 * M_PI * frequency * dt + phase_offset);
}

using namespace std;
using namespace shared_memory_interface;
int main(int argc, char **argv)
{
  ros::init(argc, argv, "pe_signal_generator");
  ros::NodeHandle n;

  std::vector<std::string> joint_names;
  matec_utils::blockOnSMJointNames(joint_names);
  shared_memory_interface::Publisher<matec_msgs::FullJointStates> carrot_pub;
  carrot_pub.advertise("/pe_carrot");

  shared_memory_interface::Subscriber<matec_msgs::FullJointStates> joint_states_sub;
  joint_states_sub.subscribe("/joint_states");

  matec_msgs::FullJointStates initial_joint_states;
  while(ros::ok() && !joint_states_sub.getCurrentMessage(initial_joint_states))
  {
    ROS_WARN_THROTTLE(0.5, "Waiting for joint states");
  }

  matec_msgs::FullJointStates full_cmd;
  full_cmd.position = initial_joint_states.position;
  full_cmd.velocity.resize(full_cmd.position.size(), 0.0);
  full_cmd.acceleration.resize(full_cmd.position.size(), 0.0);
  full_cmd.torque.resize(full_cmd.position.size(), 0.0);
  initial_joint_states = full_cmd; //clear everything but position field

  double magnitude, frequency;
  bool add_harmonics, flip;
  if(argc == 5)
  {
    magnitude = atof(argv[1]);
    frequency = atof(argv[2]);
    add_harmonics = atoi(argv[3]);
    flip = atoi(argv[4]);
  }
  else
  {
    std::cerr << "args: [magnitude] [frequency] [add harmonics 0/1] [flip signs every other joint 0/1]" << std::endl;
    return 0;
  }

  int num_frequencies = add_harmonics? 2 * initial_joint_states.position.size() * 10 : 1; //assuming 10 parameters adapted per joint, rule of thumb is 10*n frequencies in the signals
  std::vector<double> frequencies(num_frequencies, frequency);
  std::vector<double> magnitudes(num_frequencies, magnitude);
  for(unsigned int i = 1; i < frequencies.size(); i++)
  {
    frequencies[i] *= i * i;
    magnitudes[i] = -magnitudes[i - 1];
  }

  while(ros::Time::now().toSec() == 0)
  {

  }

  ros::Rate loop_rate(1000);
  ros::Time start = ros::Time::now();
  while(ros::ok())
  {
    double dt = (ros::Time::now() - start).toSec();

    std::vector<double> pos_carrots(frequencies.size());
    std::vector<double> vel_carrots(frequencies.size());
    std::vector<double> acc_carrots(frequencies.size());
    full_cmd = initial_joint_states;
    for(unsigned int i = 0; i < frequencies.size(); i++)
    {
      pos_carrots[i] = pos(dt, magnitudes[i], frequencies[i]);
      vel_carrots[i] = vel(dt, magnitudes[i], frequencies[i]);
      acc_carrots[i] = acc(dt, magnitudes[i], frequencies[i]);

//      std::cerr << i << "@" << dt << ": " << pos_carrots[i] << std::endl;

      for(unsigned int joint_idx = 0; joint_idx < initial_joint_states.position.size(); joint_idx++)
      {
        if((joint_idx % 2) == 0)
        {
          full_cmd.position[joint_idx] += pos_carrots[i];
          full_cmd.velocity[joint_idx] += vel_carrots[i];
          full_cmd.acceleration[joint_idx] += acc_carrots[i];
        }
        else
        {
          if(flip)
          {
            full_cmd.position[joint_idx] -= pos_carrots[i];
            full_cmd.velocity[joint_idx] -= vel_carrots[i];
            full_cmd.acceleration[joint_idx] -= acc_carrots[i];
          }
          else
          {
            full_cmd.position[joint_idx] += pos_carrots[i];
            full_cmd.velocity[joint_idx] += vel_carrots[i];
            full_cmd.acceleration[joint_idx] += acc_carrots[i];
          }
        }
      }
    }

    carrot_pub.publish(full_cmd);

    loop_rate.sleep();
  }

  //stop
  for(unsigned int joint_idx = 0; joint_idx < initial_joint_states.position.size(); joint_idx++)
  {
    full_cmd.velocity[joint_idx] = 0;
    full_cmd.acceleration[joint_idx] = 0;
  }
  carrot_pub.publish(full_cmd);

  return 0;
}
