#include "system_id/plotter.h"
#include <pygtk/pygtk.h>
#include <gtkmm.h>

namespace system_id
{
  Plotter::Plotter(unsigned int num_plots, PlotLayout layout)
  {
    m_ready = false;
    m_num_plots = num_plots;
    m_data.resize(num_plots);
    m_layout = layout;
    boost::thread processing_thread(boost::bind(&Plotter::processingThread, this));
  }

  bool Plotter::isReady()
  {
    return m_ready;
  }

  void Plotter::setData(unsigned int figure, std::string name, std::vector<long double> x_data, std::vector<long double> y_data, std::string color, bool use_for_scaling)
  {
    if(figure >= m_data.size())
    {
      PRINTLN_INFO("Plotter got bad figure index %d! Ignoring data!", (int) figure);
      return;
    }

    boost::mutex::scoped_lock lock(m_mutex);
    m_data[figure][name] = Dataset(x_data, y_data, color);

    m_use_for_scaling[name] = use_for_scaling;
    m_new_data.notify_all();
  }

  void Plotter::refreshPlot()
  {
    m_new_data.notify_all();
  }

  void Plotter::removeData(unsigned int figure, std::string name)
  {
    m_data[figure].erase(name);
  }

  bool Plotter::update()
  {
//    std::cerr << "draw\n";
    boost::mutex::scoped_lock lock(m_mutex);
    m_new_data.wait(lock);

    for(unsigned int figure = 0; figure < m_data.size(); figure++)
    {
      m_axes[figure].attr("clear")();
      m_axes[figure].attr("hold")(true);

      boost::python::object Rectangle = boost::python::object(boost::python::handle<>(PyImport_ImportModule("matplotlib.patches"))).attr("Rectangle");
      boost::python::object why_list = boost::python::list();
      why_list.attr("append")(0);
      why_list.attr("append")(0);

      long double x_min = std::numeric_limits<long double>::max();
      long double x_max = -std::numeric_limits<long double>::max();
      long double y_min = std::numeric_limits<long double>::max();
      long double y_max = -std::numeric_limits<long double>::max();
      boost::python::object x_data = boost::python::list();
      boost::python::object y_data = boost::python::list();
      boost::python::object plot_handles = boost::python::list();
      boost::python::object names = boost::python::list();
      boost::python::object pl = boost::python::list();
      for(std::map<std::string, Dataset>::iterator iter = m_data[figure].begin(); iter != m_data[figure].end(); iter++)
      {
        std::string name = iter->first;
        Dataset data = iter->second;
        bool use_for_scaling = m_use_for_scaling[name];
        x_data = boost::python::list();
        y_data = boost::python::list();
        for(unsigned int i = 0; i < data.size; i++)
        {
          x_data.attr("append")(data.x_data[i]);
          y_data.attr("append")(data.y_data[i]);

          //get limits
          if(use_for_scaling)
          {
            if(data.x_data[i] < x_min)
            {
              x_min = data.x_data[i];
            }
            if(data.x_data[i] > x_max)
            {
              x_max = data.x_data[i];
            }
            if(data.y_data[i] < y_min)
            {
              y_min = data.y_data[i];
            }
            if(data.y_data[i] > y_max)
            {
              y_max = data.y_data[i];
            }
          }
        }
        plot_handles.attr("append")(m_axes[figure].attr("plot")(x_data, y_data, data.color + "o"));
        names.attr("append")(name);

        boost::python::object p = Rectangle(why_list, 1, 1);
        p.attr("set_fc")(data.color);
        pl.attr("append")(p);
      }

      m_axes[figure].attr("legend")(pl, names);

      m_axes[figure].attr("set_xlim")(x_min, x_max);
      m_axes[figure].attr("set_ylim")(y_min, y_max);
      m_axes[figure].attr("hold")(false);
    }

    m_canvas.attr("draw")();
    return true;
  }

  void Plotter::processingThread()
  {
    try
    {
      // Python startup code
      Py_Initialize();
      PyRun_SimpleString("import signal");
      PyRun_SimpleString("signal.signal(signal.SIGINT, signal.SIG_DFL)");

      // Normal Gtk startup code
      Gtk::Main kit(0, 0);

      // Get the python Figure and FigureCanvas types.
      boost::python::object Figure = boost::python::object(boost::python::handle<>(PyImport_ImportModule("matplotlib.figure"))).attr("Figure");
      boost::python::object FigureCanvas = boost::python::object(boost::python::handle<>(PyImport_ImportModule("matplotlib.backends.backend_gtkagg"))).attr("FigureCanvasGTKAgg");

      // Instantiate a canvas
      m_figure = Figure();
      m_canvas = FigureCanvas(m_figure);
      m_axes.resize(m_num_plots);

      unsigned int plot_rows, plot_cols;
      switch(m_layout)
      {
      case ONE_ROW:
        plot_rows = 1;
        plot_cols = m_num_plots;
        break;
      case ONE_COLUMN:
        plot_rows = m_num_plots;
        plot_cols = 1;
        break;
      case TWO_ROWS:
      {
        if(m_num_plots <= 2)
        {
          plot_rows = m_num_plots;
          plot_cols = 1;
        }
        else
        {
          plot_rows = std::ceil(m_num_plots / 2.0);
          plot_cols = 2;
        }
        break;
      }
      case SQUARE:
      {
        double side = ceil(sqrt(m_num_plots));
        plot_rows = side;
        plot_cols = side;
        break;
      }
      default:
        assert(0);
        break;
      }

      for(unsigned int i = 0; i < m_num_plots; i++)
      {
        m_axes[i] = m_figure.attr("add_subplot")(plot_rows, plot_cols, i);
        m_axes[i].attr("hold")(false);
      }

      // Create our window.
      Gtk::Window window;
      window.set_title("Plot");
      window.set_default_size(1000, 600);

      // Grab the Gtk::DrawingArea from the canvas.
      Gtk::DrawingArea *plot = Glib::wrap(GTK_DRAWING_AREA(pygobject_get(m_canvas.ptr())));

      // Add the plot to the window.
      window.add(*plot);
      window.show_all();

      // On the idle loop, we'll call update(axes, canvas).
      Glib::signal_idle().connect(sigc::bind(&Plotter::update, this));

      // And start the Gtk event loop.
      m_ready = true;
      Gtk::Main::run(window);
    }
    catch(boost::python::error_already_set)
    {
      PyErr_Print();
    }
  }
}
