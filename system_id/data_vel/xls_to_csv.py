#!/usr/bin/env python
import xlrd
with xlrd.open_workbook("data/l_arm.xls") as book:
    for sheet in book.sheets():
        with open("data/csv/"+sheet.name.split()[0]+".csv",'w') as f:
            # f.write("IN_p,IN_v,IN_g,OUT_cmd\n")
            f.write("IN_p,IN_v,OUT_cmd\n")
            for i in range(1,sheet.nrows):
                #f.write(str(sheet.cell(i,4).value)+","+str(sheet.cell(i,2).value)+","+str(sheet.cell(i,5).value)+","+str(sheet.cell(i,0).value)+"\n") #legs
                #f.write(str(sheet.cell(i,5).value)+","+str(sheet.cell(i,3).value)+","+str(sheet.cell(i,0).value)+"\n") #r arm
                f.write(str(sheet.cell(i,3).value)+","+str(sheet.cell(i,2).value)+","+str(sheet.cell(i,0).value)+"\n") #l arm


